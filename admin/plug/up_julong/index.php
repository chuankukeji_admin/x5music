<?php
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
error_reporting(0);
include '../../../include/x5music.conn.php';
include "../../function_common.php";
admincheck(6);
$version="v1.2";
include "../../../plug/up/julong/conf.inc.php";
$action=SafeRequest("action", "get");
$ac=SafeRequest("ac", "get");
if($action=="config") {
	$julong_off=SafeRequest("julong_off", "post");
	$julong_Server=SafeRequest("julong_Server", "post");
	$julong_ak=SafeRequest("julong_ak", "post");
	$strs="<?php" . "\n";
	$strs=$strs . "define(\"julong_off\",\"" . $julong_off . "\");\n";
	$strs=$strs . "define(\"julong_Server\",\"" . $julong_Server . "\");\n";
	$strs=$strs . "define(\"julong_ak\",\"" . $julong_ak . "\");\n";
	$strs=$strs . "?>";
	if(!$fp=fopen('../../../plug/up/julong/conf.inc.php', 'w')) {
		showmessage("出错了，文件 /plug/julong/conf.inc.php 没有写入权限！", $_SERVER['HTTP_REFERER'], 0);
	}
	$ifile=new iFile('../../../plug/up/julong/conf.inc.php', 'w');
	$ifile->WriteFile($strs, 3);
	showmessage("恭喜您，保存设置成功！", $_SERVER['HTTP_REFERER'], 0);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head> 
  <meta http-equiv="Content-Type" content="text/html; charset=gb2312" /> 
  <meta name="renderer" content="webkit" /> 
  <title>x5Music 管理后台</title> 
  <link rel="stylesheet" href="../../css/add.css" type="text/css" media="screen" /> 
<style type="text/css">
.table2{width: 100%;max-width: 100%;border-collapse: collapse;border-spacing: 0;}
.table2 td{font-size: 12px;border-top: 1px solid #ddd;padding: 8px;vertical-align: top;}
.table2 tr:hover{text-decoration: none;background-color: #e6f2fb;}
.table2 th{border-bottom: 2px solid #ddd;vertical-align: bottom;padding: 2px;text-align: left;}
.table3{width: 100%;max-width: 100%;border-collapse: collapse;border-spacing: 0;}
.table3 tr:hover{text-decoration: none;background-color: #e6f2fb;}
.table3 td{font-size: 12px;line-height:25px;border-top: 1px solid #ddd;padding: 5px;vertical-align: top;border-right: solid 1px #ddd;}
.table3 th{border-bottom: 2px solid #ddd;vertical-align: bottom;padding: 2px;text-align: left;}
</style>
 </head> 
 <body> 
  <div class="contents"> 
   <div class="panel"> 
    <div class="panel-head">
     <strong>聚龙网盘上传</strong>
    </div> 
    <form action="?action=config" method="post" name="form2" >
    <table class="table3" id="dellist"> 
     <tbody>
	 <tr>
       <td width="110" align="left">版本信息</td>
       <td align="left"><?php echo $version;?>　<a href="http://www.x5mp3.com/free/7.html">查看更新</a></td>
      </tr>
	 <tr>
       <td align="left">是否启用</td>
       <td align="left">
<label class="mr20"><input name="julong_off" type="radio" value="yes" <?php if(julong_off=="yes"){echo "checked";} ?>>&nbsp;启用&nbsp;</label>
<label class="mr20"><input type="radio" name="julong_off" value="no" <?php if(julong_off=="no"){echo "checked";} ?>>&nbsp;关闭&nbsp;</label>
	   </td>
      </tr>
      <tr>
       <td align="left">站长密钥</td>
       <td align="left"><input id="julong_ak" name="julong_ak" value="<?php echo julong_ak;?>" type="text" class="input input_hd length_6"></td>
      </tr>
      <tr>
       <td align="left">关联服务器</td>
       <td align="left">
	   <select name="julong_Server" id="julong_Server">
        <option value="0" style="color:#FF0000;">不设置</option>
<?php
global $db;
$sqlclass="select * from " . tname('server') . "";
$results=$db->query($sqlclass);
if($results) {
	while($row3=$db->fetch_array($results)) {
		if(julong_Server==$row3['CD_ID']) {
			echo "        <option value='" . $row3['CD_ID'] . "' selected='selected'>" . $row3['CD_Name'] . "</option>
";
		} else {
			echo "        <option value='" . $row3['CD_ID'] . "' >" . $row3['CD_Name'] . "</option>
";
		}
	}
}
?>
        </select></td>
      </tr>
      <tr>
       <td align="left">友情提醒</td>
       <td align="left">您需要到聚龙网盘中注册帐号申请自己的站长密钥&nbsp;&nbsp;<a href="http://x5mp3.com/url.php/http://pan.julongdj.com/account.php?action=register&uid=1028" target="_blank" title="x5mp3.com">注册聚龙网盘</a></td>
      </tr>
      <tr>
       <td height="35" colspan="2" align="left" bgcolor="#FAFBF7" class="td_border"><button type="submit" id="adddance" class="btn btn_submit J_ajax_submit_btn" style="margin-left: 80px;">保存设置</button></td>
      </tr>
     </tbody>
    </table> 
	</form>
   </div> 
  </div>  
 </body>
</html>