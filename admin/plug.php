<?php
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
error_reporting(0);
include '../include/x5music.conn.php';
include "function_common.php";
$action=SafeRequest('action', 'get');

switch($action) {
	case 'save':
		Save();
		break;
	default:
		Main();
		break;
}

Function Save() {
	if(!$fp=fopen('../data/plug.txt', 'w')) {
		showmessage('文件 ../data/plug.txt 读写权限设置错误，请设置为可写后再执行！', 'plug.php', 1);
	} else {
		$x5music_Com_Str=SafeRequest('x5music_Com_Plug', 'post');
		fwrite(fopen('../data/plug.txt', 'wb'), $x5music_Com_Str);
	}
	echo '<script type="text/javascript" language="javascript">
window.parent.frames[1].location.reload();
</script>
';
	showmessage('恭喜您，编辑常用操作成功！', 'plug.php', 0);
}
Function Main() {
	echo '
<!doctype html>
<html>
<head>
<meta charset="gbk">
<meta name="renderer" content="webkit" /> 
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>x5Music 后台管理中心 -x5mp3.com</title> 
<link href="css/add.css" rel="stylesheet" />
</head> 
<body> 
<div class="contents"> 
<table style="width:500px;padding: 10px;"  border="0" cellpadding="0" cellspacing="0"  class="tb_style">
<form method="post" action="?action=save">
<tr>
<td colspan="2" class="td_border">
<textarea class="input" style="width:400px;padding: 10px;height:400px;line-height:25px;font-size:14px;" name="x5music_Com_Plug" rows="25"">' . file_get_contents('../data/plug.txt') . '</textarea>
</tr>
<tr>
<td height="30">&nbsp;一行一个，用“|”符号来格开</td>
</tr>
<tr>
<td>&nbsp;<input style="margin-top:10px" type="submit" class="btn btn_submit J_ajax_submit_btn" value="确认提交" /></td>
</tr>
</form>
</table>
</div>
</body>
</html>
';
}
?>