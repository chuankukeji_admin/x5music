<?php
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
include "../../include/x5music.conn.php";
include('qq_sdk.php');
$code=$_GET['code'];
if(empty($code)) {
	echo "<meta charset=\"gb2312\" /><script>alert('抱歉，登录失败，请重新登录。');window.location.href='https://graph.qq.com/oauth2.0/authorize?response_type=code&client_id=" . qqapp_id . "&redirect_uri=" . qqapp_pcredirect . "';</script>";
	exit();
}
$qq_sdk=new Qq_sdk();
$token=$qq_sdk->get_access_token($_GET['code']);
$open_id=$qq_sdk->get_open_id($token['access_token']);
$user_info=$qq_sdk->get_user_info($token['access_token'], $open_id['openid']);
//print_r($user_info);
$openid=$open_id['openid'];
$nicheng=iconv("UTF-8", "GBK", $user_info['nickname']);
$sex=iconv("UTF-8", "GBK", $user_info['gender']);
$photo=$user_info['figureurl_2'];
$cd_logintime=date('Y-m-d H:i:s');
$cd_loginip=$_SERVER['SERVER_ADDR'];
$cookietime='' . cd_userlogintime . '';
$cd_logintime=date('Y-m-d H:i:s');
//判断登录状态
if($openid=="") {
	echo "<meta charset=\"gb2312\" /><script>alert('抱歉，登录失败，请重新登录。');window.location.href='https://graph.qq.com/oauth2.0/authorize?response_type=code&client_id=" . qqapp_id . "&redirect_uri=" . qqapp_pcredirect . "';</script>";
	exit();
}
global $db;
$rowqq=$db->Getrow("select * from " . tname('user') . " where cd_qqopenid='" . $openid . "'");
$cd_id=$rowqq['cd_id'];
$cd_name=$rowqq['cd_name'];
$cd_password=$rowqq['cd_password'];
$cd_qqopenid=$rowqq['cd_qqopenid'];
?>
<?php
if($cd_qqopenid) {
	$db->query("update " . tname('user') . " set cd_loginnum=cd_loginnum+1,cd_loginip='" . $cd_loginip . "',cd_logintime='" . $cd_logintime . "' where cd_id='" . $cd_id . "'");
	$cd_ids=$db->Getone("select cd_id from " . tname('session') . " where cd_uid=" . $cd_id . "");
	if($cd_ids) {
		updatetable('session', array(
			'cd_logintime'=>time()
		), array(
			'cd_id'=>$cd_id
		));
	} else {
		$setarr=array(
			'cd_uid'=>$cd_id,
			'cd_uname'=>$cd_name,
			'cd_uip'=>$cd_loginip,
			'cd_logintime'=>time()
		);
		inserttable('session', $setarr, 1);
	}
	setcookie("cd_id", $cd_id, time() + $cookietime, cd_cookiepath);
	setcookie("cd_name", $cd_name, time() + $cookietime, cd_cookiepath);
	setcookie("cd_password", $cd_password, time() + $cookietime, cd_cookiepath);
	if(cd_mobile=="yes") {
		if(isMobile()) {
			include('conf.inc.php');
			Header("Location:" . cd_webpath . "mobile"); //转跳至手机
		} else {
			Header("Location:" . cd_webpath . "user/space.php?do=home"); //转跳至个人中心	
		}
	} else {
		Header("Location:" . cd_webpath . "user/space.php?do=home"); //转跳至个人中心	
	}
} else {
	$password=rand(10,100).rand(10,100).rand(10,100).date('YmdHis'); //随机密码
	$cd_password=substr(md5($password), 8, 16); //MD5密码加密
	$cd_name="1" . date('mdHis') . "".qqapp_mail.""; //随机帐号
	$setarr=array(
		'cd_name'=>$cd_name,
		'cd_nicheng'=>$nicheng,
		'cd_password'=>$cd_password,
		'cd_question'=>'',
		'cd_answer'=>'',
		'cd_email'=>'',
		'cd_sex'=>$sex,
		'cd_birthprovince'=>'',
		'cd_birthcity'=>'',
		'cd_birthday'=>'',
		'cd_regdate'=>date('Y-m-d H:i:s'),
		'cd_loginnum'=>1,
		'cd_points'=>cd_points,
		'cd_photo'=>$photo,
		'cd_qqopenid'=>$openid,
		'cd_grade'=>0,
		'cd_lock'=>0,
		'cd_hidden'=>0,
		'cd_hits'=>0,
		'cd_isbest'=>0,
		'cd_money'=>0,
		'cd_djmub'=>0,
		'cd_ufav'=>0,
		'cd_ufans'=>0,
		'cd_weblock'=>0,
		'cd_friendnum'=>0,
		'cd_rank'=>0,
		'cd_credit'=>0,
		'cd_weblock'=>0
	);
	inserttable('user', $setarr, 1);
	$db->query('update ' . tname('system') . " set cd_usermub=cd_usermub+1"); //全局数据统计
	//发消息通知
	$rowlast=$db->getrow("select * from " . tname('user') . " where cd_name='$cd_name' order by cd_id desc");
	$cd_lastid=$rowlast['cd_id'];
	$setarrs=array(
		'cd_uid'=>0,
		'cd_uname'=>'系统消息',
		'cd_uids'=>$cd_lastid,
		'cd_unames'=>$cd_name,
		'cd_title'=>'欢迎注册',
		'cd_content'=>'欢迎您注册本站，您是QQ登录用户，系统为您生成了一组登录信息：<br>帐号：' . $cd_name . '<br>密码：' . $password . '<br>请您尽快修改密码！',
		'cd_class'=>1,
		'cd_dela'=>1,
		'cd_delb'=>0,
		'cd_addtime'=>date('Y-m-d H:i:s')
	);
	inserttable('message', $setarrs, 1);
	//开始登录
	$cd_id=$rowlast['cd_id'];
	$cd_name=$rowlast['cd_name'];
	$cd_password=$rowlast['cd_password'];
	$cd_qqopenid=$rowlast['cd_qqopenid'];
	$db->query("update " . tname('user') . " set cd_loginnum=cd_loginnum+1,cd_loginip='" . $cd_loginip . "',cd_logintime='" . $cd_logintime . "' where cd_id='" . $cd_id . "'");
	$cd_ids=$db->Getone("select cd_id from " . tname('session') . " where cd_uid=" . $cd_id . "");
	if($cd_ids) {
		updatetable('session', array(
			'cd_logintime'=>time()
		), array(
			'cd_id'=>$cd_id
		));
	} else {
		$setarr=array(
			'cd_uid'=>$cd_id,
			'cd_uname'=>$cd_name,
			'cd_uip'=>$cd_loginip,
			'cd_logintime'=>time()
		);
		inserttable('session', $setarr, 1);
	}
	setcookie("cd_id", $cd_id, time() + $cookietime, cd_cookiepath);
	setcookie("cd_name", $cd_name, time() + $cookietime, cd_cookiepath);
	setcookie("cd_password", $cd_password, time() + $cookietime, cd_cookiepath);
	if(cd_mobile=="yes") {
		if(isMobile()) {
			include('conf.inc.php');
			Header("Location:" . cd_webpath . "mobile"); //转跳至手机
		} else {
			Header("Location:" . cd_webpath . "user/space.php?do=home"); //转跳至个人中心	
		}
	} else {
		Header("Location:" . cd_webpath . "user/space.php?do=home"); //转跳至个人中心	
	}
}
?>