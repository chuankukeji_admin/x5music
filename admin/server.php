<?php
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
error_reporting(0);
include "../include/x5music.conn.php";
include "function_common.php";
admincheck(3);
$action=SafeRequest("action", "get");
$CD_ClassID=SafeRequest("CD_ClassID", "get");
?>
<!DOCTYPE html>
<html>
 <head> 
  <meta charset="gbk" /> 
  <meta name="renderer" content="webkit" /> 
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" /> 
  <title>x5Music 后台管理中心 -x5mp3.com</title> 
  <link href="css/add.css" rel="stylesheet" /> 
  <style type="text/css">
.table2{width: 100%;max-width: 100%;border-collapse: collapse;border-spacing: 0;}
.table2 tr{border-bottom: 1px solid #ddd;}
.table2 tr:hover{text-decoration: none;background-color: #e6f2fb;}
.table2 th{vertical-align: bottom;padding: 10px;text-align: left;}
.table3{width: 100%;max-width: 100%;border-collapse: collapse;border-spacing: 0;}
.table3 td{font-size: 12px;border-top: 1px solid #ddd;padding: 8px;vertical-align: top;}
.table3 tr:hover{text-decoration: none;background-color: #e6f2fb;}
.table3 th{border-bottom: 2px solid #ddd;vertical-align: bottom;padding: 2px;text-align: left;}
</style>
 </head>   
 <body> 
<?php
switch($action){
case 'add':
Add();
break;
case 'saveadd':
SaveAdd();
break;
case 'edit':
Edit();
break;
case 'saveedit':
SaveEdit();
break;
case 'del':
Del();
break;
case 'alldel':
AllDel();
break;
default:
main("select * from ".tname('server')." order by CD_ID desc",30);
break;
}
?>
</body>
</html>
<?php
Function EditBoard($Arr, $url) {
	$CD_Name=$Arr[0];
	$CD_Url=$Arr[1];
	$CD_DownUrl=$Arr[2];
?>
  <div class="contents"> 
   <div class="panel"> 
    <div class="panel-head"> 
     <strong><?php if($_GET['action']=="add"){echo"添加";}else{echo"编辑";}?>服务器</strong> 
    </div> 
    <div style="padding: 8px;"> 
     <form action="<?php echo $url?>" method="post" name="form2"> 
      <table class="table2"> 
       <tbody>
        <tr> 
         <th width="70">服务器名称</th> 
         <td> <input type="text" class="input" name="CD_Name" id="CD_Name" value="<?php echo $CD_Name?>" /> </td> 
         <td></td> 
        </tr> 
        <tr> 
         <th>试听地址</th> 
         <td> <input type="text" class="input k500" name="CD_Url" id="CD_Url" value="<?php echo $CD_Url?>" /> </td> 
         <td></td> 
        </tr> 
        <tr> 
         <th>下载地址</th> 
         <td> <input type="text" class="input k500" name="CD_DownUrl" id="CD_DownUrl" value="<?php echo $CD_DownUrl?>" /> </td> 
         <td></td> 
        </tr> 
       </tbody>
      </table> 
      <input type="submit" name="Submit" value="确定提交" class="btn btn_submit mr10 J_ajax_submit_btn" style="margin-left: 80px;margin-top: 20px;" onclick="return CheckForm();" /> 
      <a href="server.php" class="btn btn-default" style="margin-left: 80px;margin-top: 20px;">返回服务器列表</a> 
     </form> 
    </div> 
   </div> 
  </div> 
<?php
}
Function main($sql, $size) {
	global $db;
	$Arr=getpagerow($sql, $size); //sql,每页显示条数
	$result=$db->query($Arr[2]);
	$videonum=$db->num_rows($result);
?>
  <div class="contents"> 
   <div class="panel"> 
    <div class="panel-head"> 
     <strong>音乐服务器</strong> 
    </div> 
    <div style="padding: 8px;"> 
     <a href="server.php" class="btn btn-default">服务器列表</a> 
     <a href="server.php?action=add" class="btn btn-default">添加服务器</a> 
    </div> 
    <table class="table3"> 
     <tbody> 
      <tr> 
       <td width="30" align="left"> 序号</td> 
       <td width="120" align="left">服务器名称</td> 
       <td align="left">试听服务器</td> 
       <td align="left">下载服务器</td> 
       <td width="100" align="left">操作</td> 
      </tr> 
<?php
if($videonum==0)
	echo "<tr><td height='30' colspan='9' align='center' bgcolor='#FFFFFF' class='td_border'><br><br>没有数据<br><br><br></td></tr>";
if($result) {
	while($row=$db->fetch_array($result)) {
?> 
      <tr> 
       <td align="left"> <?php echo $row['CD_ID']?></td>
       <td align="left"> <a href="?action=edit&CD_ID=<?php echo $row['CD_ID']?>"><?php echo $row['CD_Name']?></a></td>
       <td align="left"> <?php if(!IsNul($row['CD_Url'])){?><font color='#FF0000'>未设置</font><?php }else{ echo $row['CD_Url']; }?></td>
       <td align="left"> <?php if(!IsNul($row['CD_DownUrl'])){?><font color='#FF0000'>未设置</font><?php }else{ echo $row['CD_DownUrl']; }?></td>
       <td align="left"> <a class="btn" href="?action=edit&CD_ID=<?php echo $row['CD_ID']?>">编辑</a> <a class="btn" href="?action=del&CD_ID=<?php echo $row['CD_ID']?>" onClick="return confirm('确定删除吗？不可恢复！');">删除</a></td> 
      </tr> 
<?php
}}
?> 
     </tbody> 
    </table> 
   </div> 
  </div> 
  <div class="p10" style="margin-bottom: 0px;"> 
   <div class="pages"><?php echo $Arr[0];?></div> 
  </div> 
<?php			
}
//删除
Function Del() {
	global $db;
	$CD_ID=SafeRequest("CD_ID", "get");
	$array=array('1','2','3','4','5','6','7','8','9','10','11','12','13');
	if(in_array($CD_ID, $array)) {
		showmessage("删除失败,系统自带数据您只可以编辑不可删除。", "server.php", 0);
	}

	$sql="delete from " . tname('server') . " where CD_ID='" . $CD_ID . "'";
	if($db->query($sql)) {
		die("<script>window.location='" . $_SERVER['HTTP_REFERER'] . "'</script>");
	}
}

//编辑
function Edit() {
	global $db;
	$CD_ID=SafeRequest("CD_ID", "get");
	$sql="Select * from " . tname('server') . " where CD_ID=" . $CD_ID . "";
	if($row=$db->Getrow($sql)) {
		$Arr=array(
			$row['CD_Name'],
			$row['CD_Url'],
			$row['CD_DownUrl']
		);
	}
	EditBoard($Arr, "?action=saveedit&CD_ID=" . $CD_ID . "");
}

//添加数据
function Add() {
	$Arr=array("","","","","","","","","","","","","","","","","","","","","");
	EditBoard($Arr, "?action=saveadd");
}

//执行保存
function SaveAdd() {
	global $db;
	$CD_Name=SafeRequest("CD_Name", "post");
	$CD_Url=SafeRequest("CD_Url", "post");
	$CD_DownUrl=SafeRequest("CD_DownUrl", "post");
	$sql="Insert " . tname('server') . " (CD_Name,CD_Url,CD_DownUrl,CD_Yes) values ('" . $CD_Name . "','" . $CD_Url . "','" . $CD_DownUrl . "',1)";
	if($db->query($sql)) {
		showmessage("恭喜您，添加服务器成功！", "server.php", 0);
	} else {
		showmessage("出错了，添加服务器失败！", "server.php", 0);
	}
}

//保存编辑
function SaveEdit() {
	global $db;
	$CD_ID=SafeRequest("CD_ID", "get");
	$CD_Name=SafeRequest("CD_Name", "post");
	$CD_Url=SafeRequest("CD_Url", "post");
	$CD_DownUrl=SafeRequest("CD_DownUrl", "post");
	$sql="update " . tname('server') . " set CD_Name='" . $CD_Name . "',CD_Url='" . $CD_Url . "',CD_DownUrl='" . $CD_DownUrl . "' where CD_ID=" . $CD_ID . "";
	if($db->query($sql)) {
		showmessage("恭喜您，编辑服务器成功！", "server.php", 0);
	} else {
		showmessage("出错了，编辑服务器失败！", "server.php", 0);
	}
}
?>
