<?php if(!$userlogined==true){showmessage("出错了，请登陆后再操作。","login.php",0);}?>
<div class="user">
<div class="user_center">
<?php include_once('source/space_menu.php');?>
<div class="user_main">
<div class="uMain_content">
<div class="main_nav">
<ul>
<li class="pass"><a <?php if($view=="" and $orderby==""){ echo ' class="on"'; } ?> href="<?php echo rewrite_url('space.php?do=music');?>">我上传的</a></li>
<li class="like"><a <?php if($view=="fav" and $orderby==""){ echo ' class="on"'; } ?> href="<?php echo rewrite_url('space.php?do=music&view=fav');?>">我收藏的</a></li>
<li class="download"><a <?php if($view=="down" and $orderby==""){ echo ' class="on"'; } ?> href="<?php echo rewrite_url('space.php?do=music&view=down');?>">我下载的</a></li>
<li class="me_query"><a href="<?php echo rewrite_url('space.php?do=special');?>">我的专辑</a></li>
<li class="message"><a href="<?php echo rewrite_url('space.php?do=special&view=add');?>">制作专辑</a></li>
</ul>
</div>
<?php if($view=="add"){ ?>

<?php }elseif($view=="down"){ ?>
<!--我下载的音乐-->
<div id="favoritesList" class="minHeight500">
<div class="private_dance_list">
<ul id="list">
<li class="title">
<div class="song" style="width: 615px;">
标题名称
</div>
<div class="time">
下载时间
</div>
<div class="down">
下载
</div>
<div class="deleting">
删除
</div></li>
<?
global $db;
$a=0;
if($orderby=="all"){
$sql="select * from ".tname('fav')." where cd_type='down' order by cd_addtime desc";
}else{
$sql="select * from ".tname('fav')." where cd_type='down' and cd_uid='$x5music_com_userid' order by cd_addtime desc";
}
$Arr=getuserpage($sql,12);//sql,每页显示条数
$result=$db->query($Arr[2]);
$num=$db->num_rows($result);
if($num==0) echo "<div class=\"private_dance_list\"><div class=\"nothing\">暂无下载记录, 赶快下载吧! ^_^</div></div>";
if($result){
while ($row = $db ->fetch_array($result)){
$a=$a+1;
echo "    <li>\n";
echo "     <div class=\"song\" style=\"width: 615px;\">\n";
echo "      <div class=\"aleft\">\n";
echo "       <a class=\"mname\" href=\"".LinkUrl("dj",GetAlias("x5music_dj","CD_ClassID","CD_ID",$row['cd_musicid']),1,$row['cd_musicid'])."\" target=\"p\">".$a.".&nbsp;&nbsp;".$row["cd_musicname"]."</a>\n";
echo "      </div>\n";
echo "     </div>\n";
echo "     <div class=\"time\">\n";
echo "      ".datetime(date('Y-m-d H:i:s',$row['cd_addtime']))."\n";
echo "     </div>\n";
echo "     <div class=\"down\">\n";
echo "      <a class=\"download\" target=\"_blank\" href=\"".DownLinkUrl($row['cd_musicid'])."\" title=\"下载\"></a>\n";
echo "     </div>\n";
echo "     <div class=\"action\">\n";
echo "      <a class=\"del\" title=\"删除\" href=\"do.php?ac=delfav&id=".$row["cd_id"]."\"></a>\n";
echo "     </div></li>\n";
}
}
?>

</ul>
</div>

<div class="page"> 
<div class="pages">
<?php echo $Arr[0];?>
</div>
</div> 

</div>
<?php }elseif($view=="fav"){ ?>
<!--我收藏的音乐-->
<div id="favoritesList" class="minHeight500">
<div class="private_dance_list">
<ul id="list">
<li class="title">
<div class="song" style="width: 615px;">
标题名称
</div>
<div class="time">
收藏时间
</div>
<div class="down">
下载
</div>
<div class="deleting">
删除
</div></li>
<?php
global $db;
$a=0;
if($orderby=="all"){
$sql="select * from ".tname('fav')." where cd_type='fav' order by cd_addtime desc";
}else{
$sql="select * from ".tname('fav')." where cd_type='fav' and cd_uid='$x5music_com_userid' order by cd_addtime desc";
}
$Arr=getuserpage($sql,12);//sql,每页显示条数
$result=$db->query($Arr[2]);
$num=$db->num_rows($result);
if($num==0) echo "<div class=\"private_dance_list\"><div class=\"nothing\">暂无内容, 赶快收藏吧! ^_^</div></div>";
if($result){
while ($row = $db ->fetch_array($result)){
$a=$a+1;
echo "    <li>\n";
echo "     <div class=\"song\" style=\"width: 615px;\">\n";
echo "      <div class=\"aleft\">\n";
echo "       <a class=\"mname\" href=\"".LinkUrl("dj",GetAlias("x5music_dj","CD_ClassID","CD_ID",$row['cd_musicid']),1,$row['cd_musicid'])."\" target=\"p\">".$a.".&nbsp;&nbsp;".$row["cd_musicname"]."</a>\n";
echo "      </div>\n";
echo "     </div>\n";
echo "     <div class=\"time\">\n";
echo "      ".datetime(date('Y-m-d H:i:s',$row['cd_addtime']))."\n";
echo "     </div>\n";
echo "     <div class=\"down\">\n";
echo "      <a class=\"download\" target=\"_blank\" href=\"".DownLinkUrl($row['cd_musicid'])."\" title=\"下载\"></a>\n";
echo "     </div>\n";
echo "     <div class=\"action\">\n";
echo "      <a class=\"del\" title=\"删除\" href=\"do.php?ac=delfav&id=".$row["cd_id"]."\"></a>\n";
echo "     </div></li>\n";
}
}
?>

</ul>
</div>

<div class="page"> 
<div class="pages">
<?php echo $Arr[0];?>
</div>
</div> 

</div>

<?php }else{ ?>
<!--我分享的音乐-->
<div class="main_nav2">
<ul>
<li<?php if($_GET['day']==""){ echo ' class="current"'; } ?>><a href="<?php echo rewrite_url('space.php?do=music'); ?>">全部类型</a></li>
<?php
$sqlclass="select * from ".tname('class')." where CD_FatherID=0 and CD_IsHide=0";
$results=$db->query($sqlclass);
if($results){
while ($row3=$db->fetch_array($results)){
if($_GET['day']==$row3['CD_ID']){
echo "<li class=\"current\"><a href='".rewrite_url('space.php?do=music&view=&orderby='.$orderby.'&day='.$row3['CD_ID'])."'>".$row3['CD_Name']."</a></li>";
}else{
echo "<li><a href='".rewrite_url('space.php?do=music&view=&orderby='.$orderby.'&day='.$row3['CD_ID'])."'>".$row3['CD_Name']."</a></li>";
}
}
}?>
</ul>
</div>
<div id="favoritesList" class="minHeight500">
<div class="private_dance_list">
<ul id="list">
<li class="title">
<div class="song">
标题名称
</div>
<div class="time">
上传时间
</div>
<div class="file">
文件大小
</div>
<div class="add">
审核
</div>
<div class="down">
下载
</div>
<div class="deleting">
删除
</div></li>
<?
global $db;
$a=0;
if($day) {
$daysql="CD_ClassID='$day' and";
}
if($orderby=="all"){
$sql="select CD_ID,CD_Name,CD_ClassID,CD_Siz,CD_Singer,CD_Passed,CD_AddTime,CD_User from ".tname('dj')." where $daysql CD_Deleted=0 and CD_Passed=0 order by CD_AddTime desc";
}else{
$sql="select CD_ID,CD_Name,CD_ClassID,CD_Siz,CD_Singer,CD_Passed,CD_AddTime from ".tname('dj')." where $daysql CD_Deleted=0 and CD_User='$x5music_com_username' order by CD_AddTime desc";
}
$Arr=getuserpage($sql,12);//sql,每页显示条数
$result=$db->query($Arr[2]);
$num=$db->num_rows($result);
if($num==0) echo "<div class=\"private_dance_list\"><div class=\"nothing\">暂无内容, 赶快上传吧! ^_^</div></div>";
if($result){
while ($row = $db ->fetch_array($result)){
$a=$a+1;
echo "    <li>\n";
echo "     <div class=\"song\">\n";
echo "      <div class=\"aleft\">\n";
echo "       <a class=\"mname\" href=\"".LinkUrl("dj",$row['CD_ClassID'],1,$row['CD_ID'])."\" target=\"p\">".$a.".&nbsp;&nbsp;".$row["CD_Name"]."</a> - \n";
echo "       <a class=\"nickname user_card\" title=\"".$row["CD_Singer"]."\" target=\"_blank\" href=\"".cd_webpath."include/search.php?key=".$row["CD_Singer"]."\">".$row["CD_Singer"]."</a>\n";
echo "      </div>\n";
echo "     </div>\n";
echo "     <div class=\"time\">\n";
echo "      ".datetime($row['CD_AddTime'])."\n";
echo "     </div>\n";
echo "     <div class=\"file\">\n";
echo "      ".$row["CD_Siz"]."\n";
echo "     </div>\n";
echo "     <div class=\"add\">\n";

if($orderby=="all"){

}else{
if($row['CD_Passed']==1){ echo "<font color='red'>未审</font>"; }else{ echo "<font color='gray'>已审</font>"; }
}

echo "     </div>\n";
echo "     <div class=\"down\">\n";
echo "      <a class=\"download\" target=\"_blank\" href=\"".DownLinkUrl($row['CD_ID'])."\" title=\"下载\"></a>\n";
echo "     </div>\n";
echo "     <div class=\"action\">\n";
echo "      <a class=\"del\" title=\"删除\" href=\"do.php?ac=delmusic&id=".$row['CD_ID']."\"></a>\n";
echo "     </div></li>\n";

}
}
?>

</ul>
</div>

<div class="page"> 
<div class="pages">
<?php echo $Arr[0];?>
</div>
</div> 

</div>
<?php } ?>
</div>
</div>
</div>
</div>