<?php
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
error_reporting(0);
include "../../include/x5music.conn.php";
include "../function_common.php";
admincheck(9);
$action=SafeRequest("action", "get");
$CD_ClassID=SafeRequest("CD_ClassID", "get");
?>
<!DOCTYPE html>
<html>
 <head> 
  <meta charset="gbk" /> 
  <meta name="renderer" content="webkit" /> 
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" /> 
  <title>x5Music 后台管理中心 -x5mp3.com</title> 
  <link href="../css/add.css" rel="stylesheet" /> 
  <style type="text/css">
.table2{width: 100%;max-width: 100%;border-collapse: collapse;border-spacing: 0;}
.table2 td{font-size: 12px;border-top: 1px solid #ddd;padding: 8px;vertical-align: top;}
.table2 tr:hover{text-decoration: none;background-color: #e6f2fb;}
.table2 th{border-bottom: 2px solid #ddd;vertical-align: bottom;padding: 2px;text-align: left;}
.table3{width: 100%;max-width: 100%;border-collapse: collapse;border-spacing: 0;}
.table3 tr:hover{text-decoration: none;background-color: #e6f2fb;}
.table3 td{font-size: 12px;line-height:25px;border-top: 1px solid #ddd;padding: 5px;vertical-align: top;border-right: solid 1px #ddd;}
.table3 th{border-bottom: 2px solid #ddd;vertical-align: bottom;padding: 2px;text-align: left;}
</style>
 </head>   
 <body> 
<?php
switch($action) {
	case 'add':
		Add();
		break;
	case 'saveadd':
		SaveAdd();
		break;
	case 'edit':
		Edit();
		break;
	case 'saveedit':
		SaveEdit();
		break;
	case 'del':
		Del();
		break;
	case 'editisindex':
		editisindex();
		break;
	case 'alleditsave':
		alleditsave();
		break;
	default:
		main("select * from " . tname('link') . " order by cd_theorder asc", 30);
		break;
}
?>
</body>
</html>
<?php
Function EditBoard($Arr, $url)
{
    $cd_name=$Arr[0];
    $cd_url=$Arr[1];
    $cd_pic=$Arr[2];
    $cd_classid=$Arr[3];
    $cd_isindex=$Arr[4];
    $cd_input=$Arr[5];
    if(!IsNul($cd_classid)) {
        $cd_classid=1;
    }
    if(!IsNul($cd_isindex)) {
        $cd_isindex=0;
    }
?>
  <div class="contents"> 
   <div class="panel"> 
    <div class="panel-head"> 
     <strong><?php if($_GET['action']=="add"){echo"添加";}else{echo"编辑";}?>友链</strong> 
    </div> 
    <form action="<?php echo $url?>" method="post" name="form2"> 
     <table class="table3"> 
      <tbody> 
       <tr> 
        <td width="90" align="left" class="p10">网站名称</td> 
        <td align="left"> <input type="text" class="input input_hd length_3" value="<?php echo $cd_name?>" name="cd_name" id="cd_name" />　 </td> 
       </tr> 
       <tr> 
        <td align="left" class="p10">网站地址</td> 
        <td align="left"> <input type="text" class="input input_hd length_4" value="<?php echo $cd_url?>" name="cd_url" id="cd_url" /></td> 
       </tr> 
       <tr> 
        <td align="left" class="p10">网站Logo地址</td> 
        <td align="left"> <input type="text" class="input input_hd length_6" value="<?php echo $cd_pic?>" name="cd_pic" id="cd_pic" /></td> 
       </tr> 
       <tr> 
        <td align="left" class="p10">友情链接类型</td> 
        <td align="left">
<label class="mr20"><input type="radio" name="cd_classid" value="1"<?php if($cd_classid==1){echo " checked";} ?>>&nbsp;文字&nbsp;</label>
<label class="mr20"><input type="radio" name="cd_classid" value="2"<?php if($cd_classid==2){echo " checked";} ?>>&nbsp;图片&nbsp;</label>
		</td> 
       </tr> 
       <tr> 
        <td align="left" class="p10">是否首页显示</td> 
        <td align="left">
<label class="mr20"><input type="radio" name="cd_isindex" value="0"<?php if($cd_isindex==0){echo " checked";} ?>>&nbsp;是&nbsp;</label>
<label class="mr20"><input type="radio" name="cd_isindex" value="1"<?php if($cd_isindex==1){echo " checked";} ?>>&nbsp;否&nbsp;</label>
		</td> 
       </tr> 
       <tr> 
        <td align="left" class="p10">网站说明</td> 
        <td align="left"> <input type="text" class="input input_hd length_6" value="<?php echo $cd_input?>" name="cd_input" id="cd_input" /></td> 
       </tr> 
       <tr> 
        <td height="35" colspan="2" align="left" bgcolor="#FAFBF7" class="td_border">
<button type="submit" name="form2" class="btn btn_submit J_ajax_submit_btn" style="margin-left: 100px;" onclick="return CheckForm();">确定提交</button>
　<a href="link.php" class="btn btn-default">返回友链列表</a>
</td>
       </tr> 
      </tbody> 
     </table> 
    </form>
   </div> 
  </div>
<?php
}
Function main($sql, $size)
{
 global $db;
 $Arr=getpagerow($sql, $size);
 $result=$db->query($Arr[2]);
 $videonum=$db->num_rows($result);
?>
  <div class="contents"> 
   <div class="panel"> 
    <div class="panel-head"> 
     <strong>友情链接管理</strong> 
    </div> 
    <div style="padding: 8px;"> 
     <a href="link.php" class="btn btn-default">友链列表</a> 
     <a href="link.php?action=add" class="btn btn-default">添加友链</a> 
    </div> 
    <table class="table2"> 
     <tbody> 
      <tr> 
       <td width="30" align="left"> 序号</td> 
       <td width="120" align="left">站点名称</td> 
       <td align="left">站点地址</td> 
       <td align="left">链接内型</td> 
	   <td align="left">显示</td>
       <td width="120" align="left">操作</td> 
      </tr> 
<?php
if($videonum==0)
	echo "<tr><td height='30' colspan='9' align='center' bgcolor='#FFFFFF' class='td_border'><br><br>没有数据<br><br><br></td></tr>";
if($result) {
	while($row=$db->fetch_array($result)) {
?> 
      <tr> 
       <td align="left"> <?php echo $row['cd_id']?></td> 
       <td align="left"><?php echo $row['cd_name']?></td> 
       <td align="left"><?php echo $row['cd_url']?></td> 
       <td align="left"><?php if($row['cd_classid']==1){ echo "文字类型"; }else{ echo "图片类型"; }?></td> 
	   <td align="left"><?php if($row['cd_isindex']==1){?><a href="?action=editisindex&cd_isindex=0&cd_id=<?php echo $row['cd_id']?>"><img src='../images/no.gif' border='0'></a><?php }else{ ?><a href="?action=editisindex&cd_isindex=1&cd_id=<?php echo $row['cd_id']?>"><img src='../images/yes.gif' border='0'></a><?php } ?></td>
       <td align="left"><a class="btn" href="?action=edit&cd_id=<?php echo $row['cd_id']?>">编辑</a> <a class="btn" href="?action=del&cd_id=<?php echo $row['cd_id']?>" onClick="return confirm('确定将链接删除吗?');">删除</a></td> 
      </tr>  
<?php
}}
?> 
     </tbody> 
    </table> 
   </div> 
  </div> 
<?php			
}
function Del() {
	global $db;
	$cd_id=SafeRequest("cd_id", "get");
	$sql="delete from " . tname('link') . " where cd_id=$cd_id";
	if($db->query($sql)) {
		die("<script>window.location='" . $_SERVER['HTTP_REFERER'] . "'</script>");
	}
}
function Edit() {
	global $db;
	$cd_id=SafeRequest("cd_id", "get");
	$sql="Select * from " . tname('link') . " where cd_id=$cd_id";
	if($row=$db->Getrow($sql)) {
		$Arr=array(
			$row['cd_name'],
			$row['cd_url'],
			$row['cd_pic'],
			$row['cd_classid'],
			$row['cd_isindex'],
			$row['cd_input']
		);
	}
	EditBoard($Arr, "?action=saveedit&cd_id=" . $cd_id . "", "编辑");
}
function Add() {
	$Arr=array("","","","","","");
	EditBoard($Arr, "?action=saveadd", "添加");
}
function SaveAdd() {
	global $db;
	$cd_name=SafeRequest("cd_name", "post");
	$cd_url=SafeRequest("cd_url", "post");
	$cd_pic=SafeRequest("cd_pic", "post");
	$cd_classid=SafeRequest("cd_classid", "post");
	$cd_input=SafeRequest("cd_input", "post");
	$cd_isindex=SafeRequest("cd_isindex", "post");
	$setarr=array(
		'cd_name'=>$cd_name,
		'cd_url'=>$cd_url,
		'cd_pic'=>$cd_pic,
		'cd_classid'=>$cd_classid,
		'cd_input'=>$cd_input,
		'cd_isverify'=>0,
		'cd_isindex'=>$cd_isindex,
		'cd_theorder'=>0
	);
	inserttable('link', $setarr, 1);
	showmessage("恭喜您，添加友情链接成功！", "link.php", 0);
}
function SaveEdit() {
	global $db;
	$cd_id=SafeRequest("cd_id", "get");
	$cd_name=SafeRequest("cd_name", "post");
	$cd_url=SafeRequest("cd_url", "post");
	$cd_pic=SafeRequest("cd_pic", "post");
	$cd_classid=SafeRequest("cd_classid", "post");
	$cd_input=SafeRequest("cd_input", "post");
	$cd_isindex=SafeRequest("cd_isindex", "post");
	$setarr=array(
		'cd_name'=>$cd_name,
		'cd_url'=>$cd_url,
		'cd_pic'=>$cd_pic,
		'cd_classid'=>$cd_classid,
		'cd_input'=>$cd_input,
		'cd_isindex'=>$cd_isindex
	);
	updatetable('link', $setarr, array(
		'cd_id'=>$cd_id
	));
	showmessage("恭喜您，编辑友情链接成功！", "link.php", 0);
}
function editisindex() {
	global $db;
	$cd_id=SafeRequest("cd_id", "get");
	$cd_isindex=SafeRequest("cd_isindex", "get");
	$sql="update " . tname('link') . " set cd_isindex=" . $cd_isindex . " where cd_id=" . $cd_id . "";
	if($db->query($sql)) {
		echo "<script>window.location='" . $_SERVER['HTTP_REFERER'] . "'</script>";
	}
}
function alleditsave() {
	global $db;
	$cd_id=RequestBox("cd_id");
	if($cd_id=="0") {
		showmessage("出错了，请选择要编辑的友情链接！", "link.php", 1);
	} else {
		$ID=explode(",", $cd_id);
		for($i=0; $i < count($ID); $i++) {
			$cd_name=SafeRequest("cd_name" . $ID[$i] . "", "post");
			$cd_url=SafeRequest("cd_url" . $ID[$i] . "", "post");
			$cd_theorder=SafeRequest("cd_theorder" . $ID[$i] . "", "post");
			if(!IsNul($cd_name)) {
				showmessage("出错了，网站名称不能为空！", "link.php", 1);
			}
			if(!IsNul($cd_url)) {
				showmessage("出错了，网站地址不能为空！", "link.php", 1);
			}
			if(!IsNum($cd_theorder)) {
				showmessage("出错了，排序不能为空！", "link.php", 1);
			}
			$setarr=array(
				'cd_name'=>$cd_name,
				'cd_url'=>$cd_url,
				'cd_theorder'=>$cd_theorder
			);
			updatetable('link', $setarr, array(
				'cd_id'=>$ID[$i]
			));
		}
		showmessage("恭喜您，编辑友情链接成功！", "link.php", 0);
	}
}
?>
