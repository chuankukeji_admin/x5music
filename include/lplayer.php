<?php
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
error_reporting(0);
include 'x5music.conn.php';
$lpid=SafeRequest('id', 'get');
$Type=str_replace(",", '', $lpid);
$Type=str_replace("{song}", '', $Type);
setcookie("open_player", "Y", time()+60, "/");
if(!IsNumID($Type)) {
    $error="参数错误，请检查Url地址是否正确！";
    exit(include _x5music_root_ . "/include/error/content_error.php");
}
function replaceRepeated($words)
{
    $arrSrc=explode(",", $words);
    $arrDst=array();
    foreach($arrSrc as $key=>$val) {
        if(!in_array($val, $arrDst))
            $arrDst[]=$val;
    }
    return join(",", $arrDst);
}
$lpid=explode(",", $lpid);
for($i=0; $i < sizeof($lpid); $i++) {
    $lpids.="{song}" . $lpid[$i] . ",";
}
$lpids=replaceRepeated($lpids);
$lpids=str_replace('{song},', '', $lpids);
$tplpath=_x5music_root_ . cd_templateurl . 'lplayer.html';
if(file_exists($tplpath)) {
    $Mark_Text=@file_get_contents($tplpath);
    $Mark_Text=ReplaceStr($Mark_Text, '[x5music:lpid]', $lpids);
    $Mark_Text=Common_Mark($Mark_Text, 0);
    echo $Mark_Text;
} else {
    $error="lplayer.html模板文件不存在！";
    exit(include _x5music_root_ . "/include/error/content_error.php");
}
?>