<?php
error_reporting(0);
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
include "../include/x5music.conn.php";
include "../include/x5music.inc.php";
include "source/function_common.php";
?>
<?php include_once('source/space_header.php');?>
<?php if(cd_userlogin==1){showmessage("系统关闭了会员登录！",$_SERVER['HTTP_REFERER'],0);}?>
<?php if($userlogined){showmessage("出错了，您已经登录！",$_SERVER['HTTP_REFERER'],0);}?>
  <div class="album_banner">
<h2>欢迎回来！</h2>
<?php 
include "../plug/qqhl/conf.inc.php";
if(qqapp_open=="yes") {
?>
<div style="color: #808080;margin: 10px 10px; font-size: 18px;">
<div style="float:right">快捷登录↓<br /><br />
<?php echo '<a href="' . cd_webpath . 'plug/qqhl/index.php"><img src="' . cd_webpath . 'plug/qqhl/qq.png"></a>';?>
</div>
</div>
<?php }?>
<script type="text/javascript">
$ = function(em){	return document.getElementById(em);	};
$F = function(em){	return document.getElementById(em).value;	};
function editMe(o){
	o.style.border="1px #febb00 solid"
	o.style.backgroundColor="#fffbe7";
}
function blurMe(o){
	o.style.border="1px solid #cccccc"
	o.style.backgroundColor="transparent";
}
function doLength(intS,intB,oTd,strReg){
	if(oTd.value.length < intS || oTd.value.length > intB){
		$(strReg).innerHTML=" <img src='image/check_error.gif'/>&nbsp;&nbsp;<span class='alert'>长度在 "+intS+"-"+intB+" 个字符之间</span>";
		editMe(oTd);
	}			
}
function doEmpty(oTd,strReg,msg){
	if(oTd.value==""){
		$(strReg).innerHTML=msg;
		editMe(oTd);
	}	
}
doRe=function(strReg,intType,oTd){
	if(intType==1){
		if(oTd.value==""){
			editMe(oTd);
			$(strReg).innerHTML=" <img src='image/check_error.gif'/>&nbsp;&nbsp;<span class='alert'>验证码不允许为空</span>";
		}else{
			if(oTd.value.length < 1 || oTd.value.length > 4){
				editMe(oTd);
				$(strReg).innerHTML=" <img src='image/check_error.gif'/>&nbsp;&nbsp;<span class='alert'>长度在 1-4 个字符之间</span>";
			}else{
				var xmlHttp;
				if(window.ActiveXObject){
					xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
				}else if(window.XMLHttpRequest){
					xmlHttp = new XMLHttpRequest();
				}
				xmlHttp.open("get","source/ajax.php?ac=checkccode&seccode="+oTd.value);
				xmlHttp.onreadystatechange=function(){
					if(xmlHttp.readyState == 4){
						if(xmlHttp.status == 200){
							$("Re_1").innerHTML = unescape(xmlHttp.responseText);
						}else;
					}
				}
				xmlHttp.send(null);
				blurMe(oTd);
			}
		}
	}
	if(intType==2){
		$(strReg).innerHTML=" <img src='image/check_right.gif'/>";
		blurMe(oTd);
		doLength(2,30,oTd,strReg);
		doEmpty(oTd,strReg," <img src='image/check_error.gif'/>&nbsp;&nbsp;<span class='alert'>用户名不允许为空</span>");
	}
	if(intType==3){
		$(strReg).innerHTML=" <img src='image/check_right.gif'/>";
		blurMe(oTd);
		doLength(5,16,oTd,strReg);
		doEmpty(oTd,strReg," <img src='image/check_error.gif'/>&nbsp;&nbsp;<span class='alert'>密码不允许为空</span>");
	}
}
function isEmail(a){
	 var i=a.length;
	 var temp = a.indexOf('@');
	 var tempd = a.indexOf('.');
	 if (temp > 1) {
		if ((i-temp) > 3){
			if (tempd!=-1){
				 return 1;
			}
		}
	 }
	 return 0;
}	
doErrTest=function(){
	var t = true;
	for(i=1;i<3;i++){
		try{
			doRe("Re_"+i,i,$("ReI_"+i));
		}catch(e){}
		if((($("Re_"+i).innerHTML).indexOf("right"))<0) t = false;
	}
	return t;
}
</script>
<?php
$refer=SafeRequest("refer","get");
if(!IsNul($refer)){$refer=$_SERVER['HTTP_REFERER'];}

?>

<form id="loginform" name="loginform" action="do.php?ac=login" method="post" onsubmit="return doErrTest()" class="c_form">
<div class="name">
       验证密码：<input type="text" name="seccode" id="ReI_1" onblur="doRe('Re_1',1,this)" class="input1" value="" tabindex="1" style="width: 100px;"/> <script>seccode();</script><a href="javascript:updateseccode()">更换</a>
<span id="Re_1" style="color: #808080; margin-left: 5px; font-size: 12px;"></span>
      </div><br />	
<div class="name">
       邮箱帐号：<input type="text" name="username" id="ReI_2" onblur="doRe('Re_2',2,this)" class="input1" value="" tabindex="2" style="width: 200px;"/>
<span id="Re_2" style="color: #808080; margin-left: 5px; font-size: 12px;"></span>
      </div><br />
	  
<div class="name">
       登录密码：<input type="password" name="password" id="ReI_3" onblur="doRe('Re_3',3,this)" class="input1" value="" tabindex="3" style="width: 200px;"/>
<span id="Re_3" style="color: #808080; margin-left: 5px; font-size: 12px;"></span>
      </div><br />  
<input type="hidden" name="refer" value="<?php echo $refer ?>" />
<div style="color: #808080;margin: 20px 60px; font-size: 12px;">
<input type="submit" id="loginsubmit" name="loginsubmit" value="登 录" class="button square red" tabindex="5" />	  
&nbsp;&nbsp;<a href="lostpasswd.php">忘记密码?</a>|<a href="reg.php">注册帐号</a>
</div>
</form>	  

</div>

<?php include_once('source/space_footer.php');?>