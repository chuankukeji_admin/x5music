<?php
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
error_reporting(0);
include "../function_common.php";
admincheck(6);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head> 
  <meta http-equiv="Content-Type" content="text/html; charset=gb2312" /> 
  <meta name="renderer" content="webkit" /> 
  <title>x5Music 管理后台</title> 
  <link rel="stylesheet" href="../css/add.css" type="text/css" media="screen" /> 
<style type="text/css">
.table2 {
width: 100%;
max-width: 100%;
border-collapse: collapse;
border-spacing: 0;
}
.table2 td {
font-size: 12px;
border-top: 1px solid #ddd;
padding: 8px;
vertical-align: top;
}
.table2 tr:hover {
	text-decoration: none;
	background-color: #e6f2fb;
}
.table2 th {
border-bottom: 2px solid #ddd;
vertical-align: bottom;
padding: 2px;
text-align: left;
}
.table3 {
width: 100%;
max-width: 100%;
border-collapse: collapse;
border-spacing: 0;
}
.table3 tr:hover {
	text-decoration: none;
	background-color: #e6f2fb;
}
.table3 td {
font-size: 12px;
line-height:25px;
border-top: 1px solid #ddd;
padding: 5px;
vertical-align: top;
border-right: solid 1px #ddd;
}
.table3 th {
border-bottom: 2px solid #ddd;
vertical-align: bottom;
padding: 2px;
text-align: left;
}
</style>
 </head> 
 <body> 
  <div class="contents"> 
   <div class="panel"> 
    <div class="panel-head">
     <strong>定制插件 请联系QQ：196859961</strong>
    </div> 
    <table class="table2" id="dellist"> 
     <tbody>
      <tr bgcolor="#FAFBF7">
       <td align="left">插件名称</td>
       <td align="left">版本</td>
	   <td align="left">开发者</td>
	   <td align="left">联系QQ</td>
	   <td width="100" align="left">操作</td>
      </tr> 
      <tr>
       <td align="left">QQ互联登录注册</td>
       <td align="left">v1.0</td>
	   <td align="left"><a href="http://x5mp3.com" target="_blank">x5Music</a></td>
	   <td align="left"><a href="http://wpa.qq.com/msgrd?v=3&uin=196859961&site=qq&menu=yes" target="_blank">196859961</a></td>
	   <td width="100" align="left"><a href="qqhl/index.php" class="btn">设置</a></td>
      </tr> 
	  
      <tr>
       <td align="left">上传框架管理</td>
       <td align="left">v1.0</td>
	   <td align="left"><a href="http://x5mp3.com" target="_blank">x5Music</a></td>
	   <td align="left"><a href="http://wpa.qq.com/msgrd?v=3&uin=196859961&site=qq&menu=yes" target="_blank">196859961</a></td>
	   <td width="100" align="left"><a href="up/index.php" class="btn">设置</a></td>
      </tr> 
	  
      <tr>
       <td align="left">x5Music采集器</td>
       <td align="left">v1.0</td>
	   <td align="left"><a href="http://x5mp3.com" target="_blank">x5Music</a></td>
	   <td align="left"><a href="http://wpa.qq.com/msgrd?v=3&uin=196859961&site=qq&menu=yes" target="_blank">196859961</a></td>
	   <td width="100" align="left"><a href="caiji/index.php" class="btn">设置</a></td>
      </tr>
	  
      <tr>
       <td align="left">七牛云储存上传</td>
       <td align="left">v1.0</td>
	   <td align="left"><a href="http://x5mp3.com" target="_blank">x5Music</a></td>
	   <td align="left"><a href="http://wpa.qq.com/msgrd?v=3&uin=196859961&site=qq&menu=yes" target="_blank">196859961</a></td>
	   <td width="100" align="left"><a href="up_qiniu/index.php" class="btn">设置</a></td>
      </tr>
	  
      <tr>
       <td align="left">阿里云OSS云储存上传</td>
       <td align="left">v1.0</td>
	   <td align="left"><a href="http://x5mp3.com" target="_blank">x5Music</a></td>
	   <td align="left"><a href="http://wpa.qq.com/msgrd?v=3&uin=196859961&site=qq&menu=yes" target="_blank">196859961</a></td>
	   <td width="100" align="left"><a href="up_alioss/index.php" class="btn">设置</a></td>
      </tr>
	  
      <tr>
       <td align="left">聚龙网盘上传</td>
       <td align="left">v1.0</td>
	   <td align="left"><a href="http://x5mp3.com" target="_blank">x5Music</a></td>
	   <td align="left"><a href="http://wpa.qq.com/msgrd?v=3&uin=196859961&site=qq&menu=yes" target="_blank">196859961</a></td>
	   <td width="100" align="left"><a href="up_julong/index.php" class="btn">设置</a></td>
      </tr>
	  
     </tbody>
    </table>  
   </div> 
  </div>  
 </body>
</html>