<?php
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
error_reporting(0);
include 'x5music.config.php';
$webscan_white_directory=webscan_white_directory;
function webscan_white($webscan_white_name)
{
    $url_path=$_SERVER['SCRIPT_NAME'];
    $url_var=$_SERVER['QUERY_STRING'];
    if(preg_match("/" . $webscan_white_name . "/is", $url_path) == 1 && !empty($webscan_white_name)) {
        return false;
    }
    foreach($webscan_white_url as $key=>$value) {
        if(!empty($url_var) && !empty($value)) {
            if(stristr($url_path, $key) && stristr($url_var, $value)) {
                return false;
            }
        } elseif(empty($url_var) && empty($value)) {
            if(stristr($url_path, $key)) {
                return false;
            }
        }

    }

    return true;
}

function customError($errno, $errstr, $errfile, $errline)
{
    echo "<b>Error number:</b> [$errno],error on line $errline in $errfile<br />";
    die();
}
set_error_handler("customError", E_ERROR);
$getfilter="'|(and|or)\\b.+?(>|<|=|in|like)|\\/\\*.+?\\*\\/|<\\s*script\\b|\\bEXEC\\b|UNION.+?SELECT|UPDATE.+?SET|INSERT\\s+INTO.+?VALUES|(SELECT|DELETE).+?FROM|(CREATE|ALTER|DROP|TRUNCATE)\\s+(TABLE|DATABASE)";
$postfilter="\\b(and|or)\\b.{1,6}?(=|>|<|\\bin\\b|\\blike\\b)|\\/\\*.+?\\*\\/|<\\s*script\\b|\\bEXEC\\b|UNION.+?SELECT|UPDATE.+?SET|INSERT\\s+INTO.+?VALUES|(SELECT|DELETE).+?FROM|(CREATE|ALTER|DROP|TRUNCATE)\\s+(TABLE|DATABASE)";
$cookiefilter="\\b(and|or)\\b.{1,6}?(=|>|<|\\bin\\b|\\blike\\b)|\\/\\*.+?\\*\\/|<\\s*script\\b|\\bEXEC\\b|UNION.+?SELECT|UPDATE.+?SET|INSERT\\s+INTO.+?VALUES|(SELECT|DELETE).+?FROM|(CREATE|ALTER|DROP|TRUNCATE)\\s+(TABLE|DATABASE)";
function StopAttack($StrFiltKey, $StrFiltValue, $ArrFiltReq)
{
    if(is_array($StrFiltValue)) {
        $StrFiltValue=implode($StrFiltValue);
    }
    if(preg_match("/" . $ArrFiltReq . "/is", $StrFiltValue) == 1) {
		define("cd_webpath","/");
		$error="您输入的内容存在危险字符，已被本站拦截！";
		include 'error/content_error.php';
        exit();
    }
}
if(1 && webscan_white($webscan_white_directory)) {
    foreach($_GET as $key=>$value) {
        StopAttack($key, $value, $getfilter);
    }
    foreach($_POST as $key=>$value) {
        StopAttack($key, $value, $postfilter);
    }
    foreach($_COOKIE as $key=>$value) {
        StopAttack($key, $value, $cookiefilter);
    }
}
?>