<?php
error_reporting(0);
include "../../include/x5music.conn.php";
include('conf.inc.php');
if(qqapp_open == "no"){
    $error="本站已关闭QQ互联登录，请通过普通方式登录本站！";
    exit(include _x5music_root_ . "/include/error/content_error.php");
}
if(qqapp_id == "") {
    $error="缺少qqapp_id";
    exit(include _x5music_root_ . "/include/error/content_error.php");
}
if(qqapp_secret == "") {
    $error="缺少qqapp_secret";
    exit(include _x5music_root_ . "/include/error/content_error.php");
}
if(qqapp_pcredirect == "") {
    $error="缺少PC回调地址";
    exit(include _x5music_root_ . "/include/error/content_error.php");
}
if(qqapp_mobileredirect == "") {
    $error="缺少mobile回调地址";
    exit(include _x5music_root_ . "/include/error/content_error.php");
}
echo "<script>window.location.href='https://graph.qq.com/oauth2.0/authorize?response_type=code&client_id=" . qqapp_id . "&redirect_uri=" . qqapp_pcredirect . "';</script>";
?>