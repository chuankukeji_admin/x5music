<?php
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
error_reporting(0);
if (!eregi($_SERVER['SERVER_NAME'], $_SERVER['HTTP_REFERER'])) {exit();}
include "../x5music.conn.php";
include "../x5music.inc.php";
if($userlogined == false) {exit('很抱歉，您的登录已过期，请重新登录！');}
$f=SafeRequest("f", "get");
$action=SafeRequest("ac", "get");
$randdir=rand(2, pow(2, 24));
$time=date('YmdHis', time());
$randnum=$time . $randdir;
switch ($action) {
	case 'malbum':
		$targetFiles="upload/special/";
		$m=cd_webpath . "upload/special";
		$fileexts=cd_uppictype;
		break;
	case 'videopic':
		$targetFiles="upload/videopic/";
		$m=cd_webpath . "upload/videopic";
		$fileexts=cd_uppictype;
		break;
	case 'dancepic':
		$targetFiles="upload/dancepic/";
		$m=cd_webpath . "upload/dancepic";
		$fileexts=cd_uppictype;
		break;
	default:exit("参数错误！");break;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<title>本地上传</title>
<link href="uploadify.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="swfobject.js"></script>
<script type="text/javascript" src="jquery.uploadify.v2.1.0.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#uploadify").uploadify({
		'uploader' : 'uploadify.swf?<?php echo date('YmdHis');?>',
		'script' : 'uploads.php',
		'cancelImg' : 'cancel.png',
		'folder' : 'UploadFile',
		'method' : 'GET',
		'scriptData' : {'is':'<?php echo $randdir; ?>','id':'<?php echo $randnum; ?>','ac':'<?php echo $action;?>','uid':'<?php echo $x5music_com_userid; ?>','uname':'<?php echo base64_encode($x5music_com_username); ?>'},
		'buttonText' : 'Upload',
		'buttonImg' : 'up.png',
		'width' : '110',
		'height' : '30',
		'queueID' : 'fileQueue',
		'auto' : true,
		'multi' : false,
		'fileExt' : '<?php echo $fileexts;?>',
		'fileDesc' : '请选择<?php echo $fileexts;?>文件',
		'sizeLimit' : <?php echo (cd_uppicsize*1024*1024); ?>,
		'onError' : function (a, b, c, d) {
			if (d.status == 404){
				ReturnError("上传出错，请重试！");
			}else if (d.type === "HTTP"){
				ReturnError("error "+d.type+" : "+d.status);
			}else if (d.type === "File Size"){
				ReturnError("请上传小于的<?php echo formatsize(cd_uppicsize*1024*1024)?>的图片文件！");
			}else{
				ReturnError("error "+d.type+" : "+d.text);
			}
		},
		'onComplete' : function (event, queueID, fileObj, response, data) {
			if (response == 0){
				ReturnValue('<?php echo $targetFiles."".$randnum; ?>'+fileObj.type);
			}else{
				ReturnError(response);
			}
		}
	});
});
</script>
<script type="text/javascript">
function ReturnValue(reimg){
	this.parent.document.<?php echo $f;?>.value=reimg;
	this.parent.layer.closeAll();
}
function ReturnError(msg){
	alert(msg);
	this.parent.layer.closeAll();
}
</script>
</head>
<body>
<div id="fileQueue"></div>
<input type="file" name="uploadify" id="uploadify" />
</body>
</html>