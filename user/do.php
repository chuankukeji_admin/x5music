<?php
error_reporting(0);
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
if(!eregi($_SERVER['SERVER_NAME'], $_SERVER['HTTP_REFERER'])) {
	exit('你想干什么？');
}
include "../include/x5music.conn.php";
include "../include/x5music.inc.php";
include "source/function_common.php";
ob_start();
$ac=SafeRequest("ac", "get");
$op=SafeRequest("op", "get");
$id=SafeRequest("id", "get");
if(!IsNumID($id)) {exit();}
if(!IsNul($ac)) {
	$ac="home";
}
if(!IsNul($x5music_com_username)) {
	$x5music_com_username="游客";
}
include_once('source/space_header.php');
?>

<?php
//验证登陆
if($ac=="login") {
	if(!submitcheck('loginsubmit')) {
		showmessage("出错了，请不要提交非法参数。", "login.php", 0);
	}
	$seccode=SafeRequest("seccode", "post");
	$cookietime=SafeRequest("cookietime", "post");
	$refer=SafeRequest("refer", "post");
	$cd_seccode=SafeRequest("seccode", "post");
	$cd_username=SafeRequest("username", "post");
	$cd_password=SafeRequest("password", "post");
	$cd_logintime=date('Y-m-d H:i:s');
	$cd_loginip=$_SERVER['SERVER_ADDR'];
	if(!IsNul($refer)) {
		$refer="space.php?do=home";
	}
	if(!IsNul($cookietime)) {
		$cookietime='' . cd_userlogintime . '';
	}
	if($cd_username=="") {
		showmessage("对不起，用户名不能为空，请重新登录！", "login.php", 0);
	} elseif($cd_password=="") {
		showmessage("对不起，密码不能为空，请重新登录！", "login.php", 0);
	} else {
		if($seccode!=$_SESSION["codes"]) {
			showmessage("出错了，验证码不正确，请重新填写。", "login.php", 0);
		}
		$_SESSION["codes"]=substr(md5(date('' . rand(1, 9) . 'msds' . rand(1, 9) . 'Hs' . rand(1, 9) . 's')), 8, 16);
		$cd_password=substr(md5($cd_password), 8, 16);
		global $db;
		$sql="select cd_id from " . tname('user') . " where cd_name='" . $cd_username . "' and cd_password='" . $cd_password . "'";
		$cd_id=$db->Getone($sql);
		if($cd_id) {
			$db->query("update " . tname('user') . " set cd_loginnum=cd_loginnum+1,cd_loginip='" . $cd_loginip . "',cd_logintime='" . $cd_logintime . "' where cd_id='" . $cd_id . "'");
			$row=$db->Getrow("select cd_id,cd_lock,cd_name,cd_password from " . tname('user') . " where cd_id='" . $cd_id . "'");
			if($row["cd_lock"]==1) {
				showmessage("对不起，此账号已被锁定，请和管理员联系！", "login.php", 0);
			} elseif($row["cd_lock"]==2) {
				showmessage("对不起，此账号尚未通过审核验证，请稍后再登录！", "login.php", 0);
			} else {
				$cd_ids=$db->Getone("select cd_id from " . tname('session') . " where cd_uid='$cd_id'");
				if($cd_ids) {
					updatetable('session', array(
						'cd_logintime'=>time()
					), array(
						'cd_id'=>$cd_ids
					));
				} else {
					$setarr=array(
						'cd_uid'=>$cd_id,
						'cd_uname'=>$cd_username,
						'cd_uip'=>getonlineip(),
						'cd_logintime'=>time()
					);
					inserttable('session', $setarr, 1);
				}
				setcookie("cd_id", $row["cd_id"], time() + $cookietime, cd_cookiepath);
				setcookie("cd_name", $row["cd_name"], time() + $cookietime, cd_cookiepath);
				setcookie("cd_password", $row["cd_password"], time() + $cookietime, cd_cookiepath);
				showmessage("登录成功，现在引导您进入会员中心！", "space.php?do=home", 1);
			}
		} else {
			showmessage("对不起，用户名或密码错误，请重新登录！", "login.php", 0);
		}
	}
	//退出登陆
} elseif($ac=="logout") {
	global $db;
	$db->query("delete from " . tname('session') . " where cd_uid=$x5music_com_userid");
	setcookie("cd_id", "", time() - 1, cd_cookiepath);
	setcookie("cd_name", "", time() - 1, cd_cookiepath);
	setcookie("cd_password", "", time() - 1, cd_cookiepath);
	showmessage("你已安全退出了！", $_SERVER['HTTP_REFERER'], 1);
	//取回密码
} elseif($ac=="lostpasswd") {
	$cd_username=SafeRequest("username", "post");
	$cd_question=SafeRequest("question", "post");
	$cd_answer=SafeRequest("answer", "post");
	$cd_password=SafeRequest("password", "post");
	if($cd_username=="") {
		showmessage("出错了，登陆帐号不能为空。", "lostpasswd.php", 0);
	} elseif($cd_question=="") {
		showmessage("出错了，密保问题不能为空。", "lostpasswd.php", 0);
	} elseif($cd_answer=="") {
		showmessage("出错了，密保答案不能为空。", "lostpasswd.php", 0);
	} elseif($cd_password=="") {
		showmessage("出错了，新设密码不能为空。", "lostpasswd.php", 0);
	} else {
		$cd_answers=substr(md5($cd_answer), 8, 16);
		$cd_passwords=substr(md5($cd_password), 8, 16);
		global $db;
		$sql="select cd_id from " . tname('user') . " where cd_name='" . $cd_username . "'";
		$cd_id=$db->Getone($sql);
		if($cd_id) {
			$row=$db->Getrow("select * from " . tname('user') . " where cd_id='" . $cd_id . "'");
			if($cd_answers<>$row["cd_answer"]) {
				showmessage("出错了，输入的密码答案不对，请重新填写。", "lostpasswd.php", 0);
			} elseif($cd_question<>$row["cd_question"]) {
				showmessage("出错了，输入的密保问题不对，请重新填写。", "lostpasswd.php", 0);
			} else {
				$db->query("update " . tname('user') . " set cd_password='" . $cd_passwords . "' where cd_id='" . $cd_id . "'");
				showmessage("恭喜您，您的密码已经重设成功！", "login.php", 1);
			}
		} else {
			showmessage("出错了，你输入的登陆帐号不存在，请重新填写。", "lostpasswd.php", 0);
		}
	}
	//用户注册
} elseif($ac=="reg") {
	global $db;
	if(cd_userreg=="no") {
		showmessage("出错了，本站暂不开放新会员注册。", "login.php", 0);
	}
	if(!submitcheck('submitreg')) {
		showmessage("出错了，请不要提交非法参数。", "reg.php", 0);
	}
	$seccode=SafeRequest("seccode", "post");
	$x5music_username=SafeRequest("username", "post");
	$x5music_nicheng=SafeRequest("nicheng", "post");
	$x5music_password=SafeRequest("password", "post");
	$x5music_password=substr(md5($x5music_password), 8, 16);
	$x5music_question=SafeRequest("question", "post");
	$x5music_answer=SafeRequest("answer", "post");
	$x5music_answer=substr(md5($x5music_answer), 8, 16);
	$x5music_email=SafeRequest("email", "post");
	$x5music_sex=SafeRequest("sex", "post");
	$x5music_birthprovince=SafeRequest("birthprovince", "post");
	$x5music_birthcity=SafeRequest("birthcity", "post");
	$x5music_year=SafeRequest("b_year", "post");
	$x5music_month=SafeRequest("b_month", "post");
	$x5music_day=SafeRequest("b_day", "post");
	$x5music_birthday=$x5music_year . "-" . $x5music_month . "-" . $x5music_day;
	$x5music_addtime=date('Y-m-d H:i:s');
	if($seccode!=$_SESSION["codes"]) {
		showmessage("出错了，验证码不正确，请重新填写。", "reg.php", 0);
	} else {
		$_SESSION["codes"]=substr(md5(date('' . rand(1, 9) . 'msds' . rand(1, 9) . 'Hs' . rand(1, 9) . 's')), 8, 16);
		$userid=$db->getOne("select cd_id from " . tname('user') . " where cd_name='" . $x5music_username . "'");
		if($userid) {
			showmessage("出错了，用户已存在不能重复申请。", "reg.php", 0);
		} else {
			$setarr=array(
				'cd_name'=>$x5music_username,
				'cd_nicheng'=>$x5music_nicheng,
				'cd_password'=>$x5music_password,
				'cd_question'=>$x5music_question,
				'cd_answer'=>$x5music_answer,
				'cd_email'=>$x5music_email,
				'cd_sex'=>$x5music_sex,
				'cd_birthprovince'=>$x5music_birthprovince,
				'cd_birthcity'=>$x5music_birthcity,
				'cd_birthday'=>$x5music_birthday,
				'cd_regdate'=>$x5music_addtime,
				'cd_logintime'=>$x5music_addtime,
				'cd_loginnum'=>0,
				'cd_points'=>cd_userpoints,
				'cd_grade'=>0,
				'cd_lock'=>0,
				'cd_hidden'=>0,
				'cd_hits'=>0,
				'cd_djmub'=>0,
				'cd_ufav'=>0,
				'cd_ufans'=>0,
				'cd_isbest'=>0,
				'cd_money'=>0,
				'cd_friendnum'=>0,
				'cd_rank'=>0,
				'cd_credit'=>0,
				'cd_weblock'=>0
			);
			inserttable('user', $setarr, 1);
			$db->query('update ' . tname('system') . " set cd_usermub=cd_usermub+1"); //全局数据统计
			if(cd_userlocalpm=="yes") {
				//发消息通知
				$rowlast=$db->getrow("select cd_id from " . tname('user') . " where cd_name='$x5music_username' order by cd_id desc");
				$cd_lastid=$rowlast['cd_id'];
				$setarrs=array(
					'cd_uid'=>0,
					'cd_uname'=>'系统消息',
					'cd_uids'=>$cd_lastid,
					'cd_unames'=>$x5music_username,
					'cd_title'=>'欢迎注册',
					'cd_content'=>cd_userlocalpmtxt,
					'cd_class'=>1,
					'cd_dela'=>1,
					'cd_delb'=>0,
					'cd_addtime'=>date('Y-m-d H:i:s')
				);
				inserttable('message', $setarrs, 1);
			}
			showmessage("注册成功了，现在引导您进入登录页面。", "login.php", 1);
		}
	}
	//更新会员头像
} elseif($ac=="profileupload") {
	if(!$userlogined==true) {
		showmessage("出错了，请登陆后再操作。", "login.php", 0);
	}
	$randdir=rand(2, pow(2, 24));
	$time=date('YmdHis', time());
	$id=$time . $randdir;
	$targetDir="../upload/headpic/";
	$targetDirW="upload/headpic/";
	$targetDirM=$x5music_com_userid . "." . fileext($_FILES['attach']['name']);
	$targetDirL=rand(10, 100) . $id . "." . fileext($_FILES['attach']['name']) . "";
	$fileexts=cd_uppictype;
	$filetypes="上传头像";
	if(strpos($fileexts, fileext($_FILES['attach']['name']))!==false) {
	} else {
		@unlink($_FILES['attach']['tmp_name']);
		showmessage("请上传" . $fileexts . "格式的图片！", $_SERVER['HTTP_REFERER'], 0);
	}
	if($_FILES['attach']['size']>cd_uppicsize * 1024 * 1024) {
		@unlink($_FILES['attach']['tmp_name']);
		showmessage('请上传小于的' . formatsize(cd_uppicsize * 1024 * 1024) . '的图片文件！', $_SERVER['HTTP_REFERER'], 0);
	}
	if(isImage($_FILES['attach']['tmp_name'])===false) {
		@unlink($_FILES['attach']['tmp_name']);
		showmessage('请上传正确的图片文件！', $_SERVER['HTTP_REFERER'], 0);
	}
	if(move_uploaded_file($_FILES['attach']['tmp_name'], $targetDir . $targetDirL)===false) {
		@unlink($_FILES['attach']['tmp_name']);
		showmessage('写入文件失败，请重试！', $_SERVER['HTTP_REFERER'], 0);
	}
	@chmod($targetDir . $targetDirL, 0644);
	$img_info=getimagesize(_x5music_root_ . $targetDirW . $targetDirL);
	resizeImage(_x5music_root_ . $targetDirW . $targetDirL, $img_info[0], $img_info[1], $targetDir . $targetDirM);
	@unlink(_x5music_root_ . $targetDirW . $targetDirL);
	@unlink($_FILES['attach']['tmp_name']);
	if(!file_exists(_x5music_root_ . $targetDirW . $targetDirM)) {
		showmessage('文件格式错误，请重新上传！', $_SERVER['HTTP_REFERER'], 0);
	} else {
		//记录上传信息
		$setarrss=array(
			'cd_userid'=>$x5music_com_userid,
			'cd_username'=>'' . $x5music_com_username . '',
			'cd_userip'=>$_SERVER["REMOTE_ADDR"],
			'cd_filetype'=>$filetypes,
			'cd_filename'=>"" . $id . "." . fileext($_FILES['attach']['name']) . "",
			'cd_filesize'=>$_FILES['attach']["size"],
			'cd_fileurl'=>$targetDirW . $targetDirM,
			'cd_filetime'=>time()
		);
		inserttable('upload', $setarrss, 1);
	}
	$db->query("update " . tname('user') . " set cd_photo ='upload/headpic/" . $x5music_com_userid . ".jpg' where cd_id='$x5music_com_userid'");
	showmessage("恭喜您，更新头像成功。", $_SERVER['HTTP_REFERER'], 1);
	//修改密码
} elseif($ac=="password") {
	if(!$userlogined==true) {
		showmessage("出错了，请登陆后再操作。", "login.php", 0);
	}
	if(!submitcheck('submitpassword')) {
		showmessage("出错了，请不要提交非法参数。", $_SERVER['HTTP_REFERER'], 0);
	}
	$cd_password=SafeRequest("password", "post");
	$cd_newpwd=SafeRequest("new_pwd", "post");
	$cd_affirmpwd=SafeRequest("affirm_pwd", "post");
	if(!IsNul($cd_password)) {
		showmessage("出错了，请填写旧密码。", $_SERVER['HTTP_REFERER'], 0);
	}
	if(!IsNul($cd_newpwd)) {
		showmessage("出错了，请填写新密码。", $_SERVER['HTTP_REFERER'], 0);
	}
	if(!IsNul($cd_affirmpwd)) {
		showmessage("出错了，请填写确认密码。", $_SERVER['HTTP_REFERER'], 0);
	}
	if($cd_newpwd<>$cd_affirmpwd) {
		showmessage("出错了，两次密码不一样。", $_SERVER['HTTP_REFERER'], 0);
	}
	$cd_password=substr(md5($cd_password), 8, 16);
	if($cd_password<>$x5music_com_password) {
		showmessage("出错了，旧密码错误。", $_SERVER['HTTP_REFERER'], 0);
	}
	$sql="update " . tname('user') . " set cd_password='" . substr(md5($cd_affirmpwd), 8, 16) . "' where cd_id='$x5music_com_userid'";
	if($db->query($sql)) {
		setcookie("cd_password", substr(md5($cd_affirmpwd), 8, 16), time() + 315360000, cd_cookiepath);
		showmessage("恭喜您，修改密码成功。", $_SERVER['HTTP_REFERER'], 1);
	} else {
		showmessage("出错了，修改密码失败。", $_SERVER['HTTP_REFERER'], 2);
	}
	//修改密保
} elseif($ac=="protect") {
	if(!$userlogined==true) {
		showmessage("出错了，请登陆后再操作。", "login.php", 0);
	}
	if(!submitcheck('submitprotect')) {
		showmessage("出错了，请不要提交非法参数。", $_SERVER['HTTP_REFERER'], 0);
	}
	$cd_password=SafeRequest("password", "post");
	$cd_question=SafeRequest("question", "post");
	$cd_answer=SafeRequest("answer", "post");
	if(!IsNul($cd_password)) {
		showmessage("出错了，请填写登陆密码。", $_SERVER['HTTP_REFERER'], 0);
	}
	if(!IsNul($cd_question)) {
		showmessage("出错了，请填写密保问题。", $_SERVER['HTTP_REFERER'], 0);
	}
	if(!IsNul($cd_answer)) {
		showmessage("出错了，请填写密保答案。", $_SERVER['HTTP_REFERER'], 0);
	}
	if($cd_question==$cd_answer) {
		showmessage("出错了，问题和答案不能一样。", $_SERVER['HTTP_REFERER'], 0);
	}
	$cd_password=substr(md5($cd_password), 8, 16);
	if($cd_password<>$x5music_com_password) {
		showmessage("出错了，旧密码错误。", $_SERVER['HTTP_REFERER'], 0);
	}
	$sql="update " . tname('user') . " set cd_question=$cd_question,cd_answer='" . substr(md5($cd_answer), 8, 16) . "' where cd_id='$x5music_com_userid'";
	if($db->query($sql)) {
		showmessage("恭喜您，修改密保成功。", $_SERVER['HTTP_REFERER'], 1);
	} else {
		showmessage("出错了，修改密保失败。", $_SERVER['HTTP_REFERER'], 2);
	}
	//个人资料
} elseif($ac=="base") {
	if(!$userlogined==true) {
		showmessage("出错了，请登陆后再操作。", "login.php", 0);
	}
	if(!submitcheck('submitbase')) {
		showmessage("出错了，请不要提交非法参数。", $_SERVER['HTTP_REFERER'], 0);
	}
	$cd_nicheng=SafeRequest("nicheng", "post");
	$cd_year=SafeRequest("b_year", "post");
	$cd_month=SafeRequest("b_month", "post");
	$cd_day=SafeRequest("b_day", "post");
	$cd_birthday=$cd_year . "-" . $cd_month . "-" . $cd_day;
	$cd_birthprovince=SafeRequest("birthprovince", "post");
	$cd_birthcity=SafeRequest("birthcity", "post");
	$cd_resideprovince=SafeRequest("resideprovince", "post");
	$cd_residecity=SafeRequest("residecity", "post");
	$cd_qq=SafeRequest("qq", "post");
	$cd_email=SafeRequest("email", "post");
	$cd_sign=SafeRequest("sign", "post");
	$sql="update " . tname('user') . " set cd_nicheng='$cd_nicheng',cd_birthday='$cd_birthday',cd_birthprovince='$cd_birthprovince',cd_birthcity='$cd_birthcity',cd_resideprovince='$cd_resideprovince',cd_residecity='$cd_residecity',cd_qq='$cd_qq',cd_email='$cd_email',cd_sign='$cd_sign' where cd_id='$x5music_com_userid'";
	if($db->query($sql)) {
		$cd_id=$db->Getone("select cd_id from " . tname('feed') . " where cd_title='更新了个人资料' and cd_uid='$x5music_com_userid'");
		if($cd_id) {
			//更新动态
			updatetable('feed', array(
				'cd_addtime'=>date('Y-m-d H:i:s')
			), array(
				'cd_id'=>$cd_id
			));
		} else {
			//产生动态
			$setarr=array(
				'cd_uid'=>$x5music_com_userid,
				'cd_username'=>'' . $x5music_com_username . '',
				'cd_icon'=>'profile',
				'cd_title'=>'更新了个人资料',
				'cd_data'=>'',
				'cd_image'=>'',
				'cd_imagelink'=>'',
				'cd_dataid'=>0,
				'cd_addtime'=>date('Y-m-d H:i:s')
			);
			inserttable('feed', $setarr, 1);
		}
		showmessage("恭喜您，修改个人资料成功。", $_SERVER['HTTP_REFERER'], 1);
	} else {
		showmessage("出错了，修改个人资料失败。", $_SERVER['HTTP_REFERER'], 2);
	}
} elseif($ac=="pic") {
	if(!$userlogined==true) {
		showmessage("出错了，请登陆后再操作。", "login.php", 0);
	}
	//添加
	if($op=="upload") {
		if(!submitcheck('submitpic')) {
			showmessage("出错了，请不要提交非法参数。", $_SERVER['HTTP_REFERER'], 0);
		}
		$cd_title=SafeRequest("cd_title", "post");
		$cd_hidden=SafeRequest("cd_hidden", "post");
		$cd_style=SafeRequest("cd_style", "post");
		if(!IsNul($cd_title)) {
			$cd_title=time();
		}
		$randdir=rand(2, pow(2, 24));
		$time=date('YmdHis', time());
		$id=$time . $randdir;
		$targetDir="../upload/pic/";
		$targetDirW="upload/pic/";
		$targetDirM=$id . "." . fileext($_FILES['attach']['name']);
		$targetDirL=rand(10, 100) . $id . "." . fileext($_FILES['attach']['name']) . "";
		$fileexts=cd_uppictype;
		$filetypes="相册图片";
		if(strpos($fileexts, fileext($_FILES['attach']['name']))!==false) {
		} else {
			@unlink($_FILES['attach']['tmp_name']);
			showmessage("请上传" . $fileexts . "格式的图片！", $_SERVER['HTTP_REFERER'], 0);
		}
		if($_FILES['attach']['size']>cd_uppicsize * 1024 * 1024) {
			@unlink($_FILES['attach']['tmp_name']);
			showmessage('请上传小于的' . formatsize(cd_uppicsize * 1024 * 1024) . '的图片文件！', $_SERVER['HTTP_REFERER'], 0);
		}
		if(isImage($_FILES['attach']['tmp_name'])===false) {
			@unlink($_FILES['attach']['tmp_name']);
			showmessage('请上传正确的图片文件！', $_SERVER['HTTP_REFERER'], 0);
		}
		if(move_uploaded_file($_FILES['attach']['tmp_name'], $targetDir . $targetDirL)===false) {
			@unlink($_FILES['attach']['tmp_name']);
			showmessage('写入文件失败，请重试！', $_SERVER['HTTP_REFERER'], 0);
		}
		@chmod($targetDir . $targetDirL, 0644);
		$img_info=getimagesize(_x5music_root_ . $targetDirW . $targetDirL);
		resizeImage(_x5music_root_ . $targetDirW . $targetDirL, $img_info[0], $img_info[1], $targetDir . $targetDirM);
		@unlink(_x5music_root_ . $targetDirW . $targetDirL);
		@unlink($_FILES['attach']['tmp_name']);
		if(!file_exists(_x5music_root_ . $targetDirW . $targetDirM)) {
			showmessage('文件格式错误，请重新上传！', $_SERVER['HTTP_REFERER'], 0);
		} else {
			//记录上传信息
			$setarrss=array(
				'cd_userid'=>$x5music_com_userid,
				'cd_username'=>'' . $x5music_com_username . '',
				'cd_userip'=>$_SERVER["REMOTE_ADDR"],
				'cd_filetype'=>$filetypes,
				'cd_filename'=>"" . $id . "." . fileext($_FILES['attach']['name']) . "",
				'cd_filesize'=>$_FILES['attach']["size"],
				'cd_fileurl'=>$targetDirW . $targetDirM,
				'cd_filetime'=>time()
			);
			inserttable('upload', $setarrss, 1);
		}
		//入库
		$setarrss=array(
			'cd_uid'=>$x5music_com_userid,
			'cd_uname'=>'' . $x5music_com_username . '',
			'cd_uip'=>getonlineip(),
			'cd_title'=>$cd_title,
			'cd_style'=>$cd_style,
			'cd_url'=>$targetDirW . $targetDirM,
			'cd_hidden'=>$cd_hidden,
			'cd_hits'=>0,
			'cd_click1'=>0,
			'cd_click2'=>0,
			'cd_click3'=>0,
			'cd_click4'=>0,
			'cd_click5'=>0,
			'cd_addtime'=>time()
		);
		inserttable('pic', $setarrss, 1);
		//产生动态
		$rowlast=$db->getrow("select cd_id from " . tname('pic') . " where cd_uid='$x5music_com_userid' order by cd_id desc");
		$cd_lastid=$rowlast['cd_id'];
		$setarr=array(
			'cd_uid'=>$x5music_com_userid,
			'cd_username'=>'' . $x5music_com_username . '',
			'cd_icon'=>'pic',
			'cd_title'=>'上传了新图片 《' . $cd_title . '》',
			'cd_data'=>'<a href="space.php?do=pic&view=album&id=' . $cd_lastid . '" target="pic">查看图片</a>',
			'cd_image'=>'source/ajax.php?ac=getpic&id=' . $cd_lastid,
			'cd_imagelink'=>'space.php?do=pic&view=album&id=' . $cd_lastid,
			'cd_dataid'=>$cd_lastid,
			'cd_addtime'=>date('Y-m-d H:i:s')
		);
		inserttable('feed', $setarr, 1);
		$timea=date('Y', time());
		$timeb=date('m', time());
		$timec=date('d', time());
		$timed=mktime(0, 0, 0, $timeb, $timec, $timea);
		$picnum=$db->num_rows($db->query("select cd_id from " . tname('pic') . " where cd_uid='" . $x5music_com_userid . "' and cd_addtime >= '" . $timed . "'"));
		if(($picnum * cd_useruppicpoints)<cd_useruppicceiling) {
			$db->query("update " . tname('user') . " set cd_points=cd_points+" . cd_useruppicpoints . ",cd_rank=cd_rank+" . cd_useruppicpoints . " where cd_id='$x5music_com_userid'");
		}
		showmessage("恭喜您，上传图片成功。", "space.php?do=pic", 1);
		//编辑
	} elseif($op=="edit") {
		global $db;
		$cd_id=SafeRequest("cd_id", "post");
		$cd_title=SafeRequest("cd_title", "post");
		$cd_hidden=SafeRequest("cd_hidden", "post");
		$cd_style=SafeRequest("cd_style", "post");
		if(!submitcheck('submitepic')) {
			showmessage("出错了，请不要提交非法参数。", $_SERVER['HTTP_REFERER'], 0);
		} else {
			$cd_id=$db->Getone("select cd_id from " . tname('pic') . " where cd_uid='$x5music_com_userid' and cd_id='$cd_id'");
			if(!$cd_id) {
				showmessage("出错了，请不要试图修改别人的数据。", $_SERVER['HTTP_REFERER'], 2);
			}
			updatetable('pic', array(
				'cd_title'=>$cd_title,
				'cd_hidden'=>$cd_hidden,
				'cd_style'=>$cd_style,
				'cd_addtime'=>time()
			), array(
				'cd_id'=>$cd_id
			));
			showmessage("恭喜您，编辑图片成功。", rewrite_url("space.php?do=pic&view=album&id=" . $cd_id), 1);
		}
		//删除
	} elseif($op=="del") {
		$cd_id=$db->getrow("select * from " . tname('pic') . " where cd_uid='$x5music_com_userid' and cd_id='$id'");
		if($cd_id['cd_uid']!==$x5music_com_userid) {
			showmessage("出错了，请不要试图删除别人的数据。", $_SERVER['HTTP_REFERER'], 2);
		}
		if(delunlink($cd_id['cd_url'])==true) {
			@unlink(_x5music_root_ . $cd_id['cd_url']);
		}
		$db->query("delete from " . tname('pic') . " where cd_id='$id'");
		$db->query("delete from " . tname('feed') . " where cd_icon='pic' and cd_dataid='$id'");
		showmessage("恭喜您，删除图片成功。", "space.php?do=pic", 1);
	}
	//空间
} elseif($ac=="web") {
	if(!$userlogined==true) {
		showmessage("出错了，请登陆后再操作。", "login.php", 0);
	}
	//主页命名
	if($op=="mingm") {
		if(!submitcheck('submitwebmingm')) {
			showmessage("出错了，请不要提交非法参数。", $_SERVER['HTTP_REFERER'], 0);
		}
		$cd_title=SafeRequest("cd_title", "post");
		updatetable('web', array(
			'cd_title'=>str_encode($cd_title)
		), array(
			'cd_uid'=>$x5music_com_userid
		));
		showmessage("恭喜您，编辑空间名称成功。", $_SERVER['HTTP_REFERER'], 1);
		//切换空间风格
	} elseif($op=="theme") {
		$dir=SafeRequest("dir", "get");
		if(!IsNul($dir)) {
			showmessage("出错了，请不要提交非法参数。", $_SERVER['HTTP_REFERER'], 0);
		} else {
			updatetable('web', array(
				'cd_skin'=>$dir
			), array(
				'cd_uid'=>$x5music_com_userid
			));
			showmessage("恭喜您，切换空间风格成功。", $_SERVER['HTTP_REFERER'], 1);
		}
		//上传图片
	} elseif($op=="upload") {
		include_once(_x5music_root_ . 'user/source/function_upload.php');
		if(submitcheck('submitwebbanner')) {
			$randdir=rand(2, pow(2, 24));
			$time=date('YmdHis', time());
			$id=$time . $randdir;
			$targetDir="../upload/web/";
			$targetDirW="upload/web/";
			$targetDirM=$x5music_com_userid . "_banner." . fileext($_FILES['cd_banner']['name']);
			$targetDirL=rand(10, 100) . $id . "." . fileext($_FILES['cd_banner']['name']) . "";
			$fileexts=cd_uppictype;
			$filetypes="空间标题图片";
			if(strpos($fileexts, fileext($_FILES['cd_banner']['name']))!==false) {
			} else {
				@unlink($_FILES['cd_banner']['tmp_name']);
				showmessage("请上传" . $fileexts . "格式的图片！", $_SERVER['HTTP_REFERER'], 0);
			}
			if($_FILES['cd_banner']['size']>cd_uppicsize * 1024 * 1024) {
				@unlink($_FILES['cd_banner']['tmp_name']);
				showmessage('请上传小于的' . formatsize(cd_uppicsize * 1024 * 1024) . '的图片文件！', $_SERVER['HTTP_REFERER'], 0);
			}
			if(isImage($_FILES['cd_banner']['tmp_name'])===false) {
				@unlink($_FILES['cd_banner']['tmp_name']);
				showmessage('请上传正确的图片文件！', $_SERVER['HTTP_REFERER'], 0);
			}
			if(move_uploaded_file($_FILES['cd_banner']['tmp_name'], $targetDir . $targetDirL)===false) {
				@unlink($_FILES['cd_banner']['tmp_name']);
				showmessage('写入文件失败，请重试！', $_SERVER['HTTP_REFERER'], 0);
			}
			@chmod($targetDir . $targetDirL, 0644);
			$img_info=getimagesize(_x5music_root_ . $targetDirW . $targetDirL);
			resizeImage(_x5music_root_ . $targetDirW . $targetDirL, $img_info[0], $img_info[1], $targetDir . $targetDirM);
			@unlink(_x5music_root_ . $targetDirW . $targetDirL);
			@unlink($_FILES['cd_banner']['tmp_name']);
			if(!file_exists(_x5music_root_ . $targetDirW . $targetDirM)) {
				showmessage('文件格式错误，请重新上传！', $_SERVER['HTTP_REFERER'], 0);
			} else {
				//记录上传信息
				$setarrss=array(
					'cd_userid'=>$x5music_com_userid,
					'cd_username'=>'' . $x5music_com_username . '',
					'cd_userip'=>$_SERVER["REMOTE_ADDR"],
					'cd_filetype'=>$filetypes,
					'cd_filename'=>$targetDirM,
					'cd_filesize'=>$_FILES['cd_banner']["size"],
					'cd_fileurl'=>$targetDirW . $targetDirM,
					'cd_filetime'=>time()
				);
				inserttable('upload', $setarrss, 1);
			}
			$db->query("update " . tname('web') . " set cd_banner ='upload/web/" . $x5music_com_userid . "_banner." . fileext($_FILES['cd_banner']['name']) . "' where cd_uid='$x5music_com_userid'");
			showmessage("恭喜您，上传标题图片成功。", $_SERVER['HTTP_REFERER'], 1);
		} elseif(submitcheck('submitwebbg')) {
			$randdir=rand(2, pow(2, 24));
			$time=date('YmdHis', time());
			$id=$time . $randdir;
			$targetDir="../upload/web/";
			$targetDirW="upload/web/";
			$targetDirM=$x5music_com_userid . "_bg." . fileext($_FILES['cd_bg']['name']);
			$targetDirL=rand(10, 100) . $id . "." . fileext($_FILES['cd_bg']['name']) . "";
			$fileexts=cd_uppictype;
			$filetypes="空间背景图片";
			if(strpos($fileexts, fileext($_FILES['cd_bg']['name']))!==false) {
			} else {
				@unlink($_FILES['cd_bg']['tmp_name']);
				showmessage("请上传" . $fileexts . "格式的图片！", $_SERVER['HTTP_REFERER'], 0);
			}
			if($_FILES['cd_bg']['size']>cd_uppicsize * 1024 * 1024) {
				@unlink($_FILES['cd_bg']['tmp_name']);
				showmessage('请上传小于的' . formatsize(cd_uppicsize * 1024 * 1024) . '的图片文件！', $_SERVER['HTTP_REFERER'], 0);
			}
			if(isImage($_FILES['cd_bg']['tmp_name'])===false) {
				@unlink($_FILES['cd_bg']['tmp_name']);
				showmessage('请上传正确的图片文件！', $_SERVER['HTTP_REFERER'], 0);
			}
			if(move_uploaded_file($_FILES['cd_bg']['tmp_name'], $targetDir . $targetDirL)===false) {
				@unlink($_FILES['cd_bg']['tmp_name']);
				showmessage('写入文件失败，请重试！', $_SERVER['HTTP_REFERER'], 0);
			}
			@chmod($targetDir . $targetDirL, 0644);
			$img_info=getimagesize(_x5music_root_ . $targetDirW . $targetDirL);
			resizeImage(_x5music_root_ . $targetDirW . $targetDirL, $img_info[0], $img_info[1], $targetDir . $targetDirM);
			@unlink(_x5music_root_ . $targetDirW . $targetDirL);
			@unlink($_FILES['cd_bg']['tmp_name']);
			if(!file_exists(_x5music_root_ . $targetDirW . $targetDirM)) {
				showmessage('文件格式错误，请重新上传！', $_SERVER['HTTP_REFERER'], 0);
			} else {
				//记录上传信息
				$setarrss=array(
					'cd_userid'=>$x5music_com_userid,
					'cd_username'=>'' . $x5music_com_username . '',
					'cd_userip'=>$_SERVER["REMOTE_ADDR"],
					'cd_filetype'=>$filetypes,
					'cd_filename'=>$targetDirM,
					'cd_filesize'=>$_FILES['cd_bg']["size"],
					'cd_fileurl'=>$targetDirW . $targetDirM,
					'cd_filetime'=>time()
				);
				inserttable('upload', $setarrss, 1);
			}
			$db->query("update " . tname('web') . " set cd_bg ='upload/web/" . $x5music_com_userid . "_bg." . fileext($_FILES['cd_bg']['name']) . "' where cd_uid='$x5music_com_userid'");
			showmessage("恭喜您，上传背景图片成功。", $_SERVER['HTTP_REFERER'], 1);
		} else {
			showmessage("出错了，请不要提交非法参数。", $_SERVER['HTTP_REFERER'], 0);
		}
	}
	//发短消息
} elseif($ac=="pm") {
	if($op=="send") {
		if(!$userlogined==true) {
			showmessage("出错了，请登陆后再操作。", "login.php", 0);
		}
		if(!submitcheck('pmsubmit')) {
			showmessage("出错了，请不要提交非法参数。", $_SERVER['HTTP_REFERER'], 0);
		}
		$cd_uid=SafeRequest("uid", "post");
		$cd_uname=SafeRequest("uname", "post");
		$cd_title=SafeRequest("title", "post");
		$cd_content=SafeRequest("message", "post");
		$cd_addtime=date('Y-m-d H:i:s');
		if(!IsNul($cd_content)) {
			showmessage("出错了，内容不能为空。", $_SERVER['HTTP_REFERER'], 0);
		}
		//入库
		$setarr=array(
			'cd_uid'=>$x5music_com_userid,
			'cd_uname'=>'' . $x5music_com_username . '',
			'cd_uids'=>$cd_uid,
			'cd_unames'=>$cd_uname,
			'cd_title'=>$cd_title,
			'cd_content'=>$cd_content,
			'cd_class'=>0,
			'cd_dela'=>1,
			'cd_delb'=>0,
			'cd_addtime'=>date('Y-m-d H:i:s')
		);
		inserttable('message', $setarr, 1);
		showmessage("恭喜您，回复消息成功。", "space.php?do=pm", 1);
	} else {
		if(!$userlogined==true) {
			showmessage("出错了，请登陆后再操作。", "login.php", 0);
		}
		if(!submitcheck('submitpm')) {
			showmessage("出错了，请不要提交非法参数。", $_SERVER['HTTP_REFERER'], 0);
		}
		$cd_refer=SafeRequest("refer", "post");
		$cd_unames=SafeRequest("username", "post");
		$cd_title=SafeRequest("title", "post");
		$cd_content=SafeRequest("message", "post");
		$cd_addtime=date('Y-m-d H:i:s');
		if($cd_unames==$x5music_com_username) {
			showmessage("出错了，不能给自己发消息。", $_SERVER['HTTP_REFERER'], 2);
		}
		global $db;
		$sql="select cd_id from " . tname('user') . " where cd_name='$cd_unames'";
		$cd_id=$db->Getone($sql);
		if(!$cd_id) {
			showmessage("出错了，你输入的收件人不存在，请重新填写。", $_SERVER['HTTP_REFERER'], 0);
		}
		if(!IsNul($cd_title)) {
			showmessage("出错了，标题不能为空。", $_SERVER['HTTP_REFERER'], 2);
		}
		if(!IsNul($cd_content)) {
			showmessage("出错了，内容不能为空。", $_SERVER['HTTP_REFERER'], 2);
		}
		//入库
		$setarr=array(
			'cd_uid'=>$x5music_com_userid,
			'cd_uname'=>'' . $x5music_com_username . '',
			'cd_uids'=>GetAlias("x5music_user", "cd_id", "cd_name", $cd_unames),
			'cd_unames'=>$cd_unames,
			'cd_title'=>$cd_title,
			'cd_content'=>$cd_content,
			'cd_class'=>0,
			'cd_dela'=>1,
			'cd_delb'=>0,
			'cd_addtime'=>date('Y-m-d H:i:s')
		);
		inserttable('message', $setarr, 1);
		showmessage("恭喜您，发布消息成功。", $cd_refer, 1);
	}
	//添加好友
} elseif($ac=="friend") {
	global $db;
	if(!$userlogined==true) {
		showmessage("出错了，请登陆后再操作。", "login.php", 0);
	}
	$uid=SafeRequest("uid", "get");
	if($op=="ignore") {
		$db->query("delete from " . tname('friend') . " where cd_uid=$uid and cd_uids='$x5music_com_userid' and cd_lock=1");
		showmessage("恭喜您，进行的操作完成了。", $_SERVER['HTTP_REFERER'], 1);
	} elseif($op=="add") {
		updatetable('friend', array(
			'cd_lock'=>0
		), array(
			'cd_uids'=>$x5music_com_userid,
			'cd_uid'=>$uid
		));
		$db->query("delete from " . tname('friend') . " where cd_uid='$x5music_com_userid' and cd_uids='$uid' and cd_lock=0");
		$sql="select cd_id,cd_name,cd_nicheng from " . tname('user') . " where cd_id='$uid'";
		if($row=$db->Getrow($sql)) {
			$cd_uids=$row['cd_id'];
			$cd_unames=$row['cd_name'];
			$cd_unicheng=$row['cd_nicheng'];
			//入库
			$setarr=array(
				'cd_uid'=>$x5music_com_userid,
				'cd_uname'=>'' . $x5music_com_username . '',
				'cd_uids'=>$cd_uids,
				'cd_unames'=>$cd_unames,
				'cd_lock'=>0,
				'cd_addtime'=>date('Y-m-d H:i:s')
			);
			inserttable('friend', $setarr, 1);
		}
		//产生动态
		$setarrs=array(
			'cd_uid'=>$x5music_com_userid,
			'cd_username'=>'' . $x5music_com_username . '',
			'cd_icon'=>'friend',
			'cd_title'=>'和 <a target="web" href="' . linkweburl($cd_uids, $cd_unames) . '">' . $cd_unicheng . '</a> 成为了好友',
			'cd_data'=>'',
			'cd_image'=>'',
			'cd_imagelink'=>'',
			'cd_dataid'=>0,
			'cd_addtime'=>date('Y-m-d H:i:s')
		);
		inserttable('feed', $setarrs, 1);
		//通知对方
		$setarr1=array(
			'cd_uid'=>0,
			'cd_uname'=>'系统消息',
			'cd_uids'=>$cd_uids,
			'cd_unames'=>$cd_unames,
			'cd_title'=>'' . $x5music_com_nicheng . '和你成为了好友！',
			'cd_content'=>'嗨~我们现在已经是好友了，没事去我空间踩踩吧！<a href="' . linkweburl($x5music_com_userid, $x5music_com_username) . '" target="_blank">' . linkweburl($x5music_com_userid, $x5music_com_username) . '</a>',
			'cd_class'=>1,
			'cd_dela'=>1,
			'cd_delb'=>0,
			'cd_addtime'=>date('Y-m-d H:i:s')
		);
		inserttable('message', $setarr1, 1);
		showmessage("恭喜您，你和 $cd_unicheng 成为好友了。", $_SERVER['HTTP_REFERER'], 1);
	} else {
		if($uid==$x5music_com_userid) {
			showmessage("出错了，您不能加自己为好友。", $_SERVER['HTTP_REFERER'], 2);
		}
		$sql="select cd_id,cd_name from " . tname('user') . " where cd_id='$uid'";
		if($row=$db->Getrow($sql)) {
			$cd_uids=$row['cd_id'];
			$cd_unames=$row['cd_name'];
			//检测是否已申请
			$cd_id=$db->Getone("select cd_id from " . tname('friend') . " where cd_uid='$x5music_com_userid' and cd_uids='$cd_uids' and cd_lock=1");
			if($cd_id) {
				showmessage("正在等待对方验证中。", $_SERVER['HTTP_REFERER'], 0);
			}
			//检测是否已是好友
			$cd_id=$db->Getone("select cd_id from " . tname('friend') . " where cd_uid='$x5music_com_userid' and cd_uids='$cd_uids' and cd_lock=0");
			if($cd_id) {
				showmessage("出错了，你们已经是好友了。", $_SERVER['HTTP_REFERER'], 0);
			}
			//入库
			$setarr=array(
				'cd_uid'=>$x5music_com_userid,
				'cd_uname'=>'' . $x5music_com_username . '',
				'cd_uids'=>$cd_uids,
				'cd_unames'=>$cd_unames,
				'cd_lock'=>1,
				'cd_addtime'=>date('Y-m-d H:i:s')
			);
			inserttable('friend', $setarr, 1);
			//通知对方
			$setarr1=array(
				'cd_uid'=>0,
				'cd_uname'=>'系统消息',
				'cd_uids'=>$cd_uids,
				'cd_unames'=>$cd_unames,
				'cd_title'=>'' . $x5music_com_nicheng . '请求添加好友！',
				'cd_content'=>'嗨~我是' . $x5music_com_nicheng . '，我加你为好友，赶快接受吧。 <a href="space.php?do=friend&view=request">查看请求...</a>',
				'cd_class'=>1,
				'cd_dela'=>1,
				'cd_delb'=>0,
				'cd_addtime'=>date('Y-m-d H:i:s')
			);
			inserttable('message', $setarr1, 1);
			showmessage("好友请求已经发送，请等待对方验证中。", $_SERVER['HTTP_REFERER'], 1);
		} else {
			showmessage("出错了，你添加的好友不存在。", $_SERVER['HTTP_REFERER'], 0);
		}
	}
	//申请VIP
} elseif($ac=="vip") {
	if(!$userlogined==true) {
		showmessage("出错了，请登陆后再操作。", "login.php", 0);
	}
	if(!submitcheck('vipsubmit')) {
		showmessage("出错了，请不要提交非法参数。", $_SERVER['HTTP_REFERER'], 0);
	}
	if($x5music_com_grade==1) {
		showmessage("出错了，您已经是VIP会员了，请不要重复申请。", "space.php?do=pay&view=vip", 0);
	}
	if($x5music_com_points>=cd_vippoints) {
		$cd_vippoints=cd_vippoints;
		$cd_viptime=cd_viptime;
		$cd_vipindate=date('Y-m-d H:i:s');
		$tomorrow=mktime(date("H"), date("i"), date("s"), date("m"), date("d") + $cd_viptime, date("Y"));
		$cd_vipenddate=date("Y-m-d H:i:s", $tomorrow);
		$db->query("update " . tname('user') . " set cd_points=cd_points-'$cd_vippoints',cd_grade=1,cd_vipindate='$cd_vipindate',cd_vipenddate='$cd_vipenddate' where cd_id='$x5music_com_userid'");
		//产生动态
		$setarr=array(
			'cd_uid'=>$x5music_com_userid,
			'cd_username'=>'' . $x5music_com_username . '',
			'cd_icon'=>'pay',
			'cd_title'=>'升级成为VIP会员',
			'cd_data'=>'',
			'cd_image'=>'',
			'cd_imagelink'=>'',
			'cd_dataid'=>0,
			'cd_addtime'=>date('Y-m-d H:i:s')
		);
		inserttable('feed', $setarr, 1);
		//记录积分动态
		$setarr=array(
			'cd_uid'=>$x5music_com_userid,
			'cd_username'=>'' . $x5music_com_username . '',
			'cd_icon'=>'jifen',
			'cd_title'=>'升级成为VIP会员',
			'cd_data'=>'-' . $cd_vippoints . '',
			'cd_image'=>'',
			'cd_imagelink'=>'',
			'cd_dataid'=>0,
			'cd_addtime'=>date('Y-m-d H:i:s')
		);
		inserttable('feed', $setarr, 1);
		//消息通知
		$setarrs=array(
			'cd_uid'=>0,
			'cd_uname'=>'管理员',
			'cd_uids'=>$x5music_com_userid,
			'cd_unames'=>'' . $x5music_com_username . '',
			'cd_title'=>'VIP会员申请成功',
			'cd_content'=>cd_vipktxt,
			'cd_class'=>1,
			'cd_dela'=>1,
			'cd_delb'=>0,
			'cd_addtime'=>date('Y-m-d H:i:s')
		);
		inserttable('message', $setarrs, 1);
		showmessage("恭喜您，申请成VIP会员成功，有效期为 " . cd_viptime . " 天。扣除" . cd_vippoints . "个金币", $_SERVER['HTTP_REFERER'], 0);
	} else {
		showmessage("出错了，您的条件不满足，还无法申请成为本站VIP会员。", $_SERVER['HTTP_REFERER'], 0);
	}
	//-------------音乐操作-------------//
	//音乐
} elseif($ac=="music") {
	global $db;
	//if(!$userlogined==true){showmessage("出错了，请登陆后再操作。","login.php",0);}
	if(!submitcheck('submitmusic')) {
		showmessage("出错了，请不要提交非法参数。", $_SERVER['HTTP_REFERER'], 0);
	}
	$seccode=SafeRequest("seccode", "post");
	$CD_Name=SafeRequest("CD_Name", "post");
	$CD_Singer=SafeRequest("CD_Singer", "post");
	$CD_ClassID=SafeRequest("CD_ClassID", "post");
	$CD_Url=SafeRequest("CD_Url", "post");
	$CD_Points=SafeRequest("CD_Points", "post");
	$CD_Word=SafeRequest("CD_Word", "post");
	$CD_Lrc=SafeRequest("CD_Lrc", "post");
	$CD_Server=SafeRequest("CD_Server", "post");
	$CD_Tag=SafeRequest("CD_Tag", "post");
	$CD_from=SafeRequest("CD_from", "post");
	$CD_siz=SafeRequest("CD_siz", "post");
	$CD_md5=SafeRequest("CD_md5", "post");
	$CD_uid=SafeRequest("CD_uid", "post");
	$CD_Pic=SafeRequest("CD_Pic", "post");
	$CD_Grade=SafeRequest("CD_Grade", "post");
	$muban='play.html';
	if($seccode!=$_SESSION["codes"])
		exit(showmessage("抱歉,验证码不正确，请返回重新填写!", $_SERVER['HTTP_REFERER'], 3));
	$_SESSION["codes"]=substr(md5(date('' . rand(1, 9) . 'msds' . rand(1, 9) . 'Hs' . rand(1, 9) . 's')), 8, 16);
	//判断参数是否为空
	if($CD_ClassID=="") {
		$CD_ClassID='8';
	} //分类
	if($CD_Server=="") {
		$CD_Server='2';
	} //服务器
	if($CD_from=="") {
		$CD_from='zl';
	} //来源
	if($CD_siz=="") {
		$CD_siz="" . rand(1, 9) . "." . rand(1, 9) . rand(1, 9) . " MB";
	} //文件大小
	if($CD_md5=="") {
		$CD_md5='0';
	} //MD5
	if($CD_uid=="") {
		$CD_uid='0';
	} //UID
	if($CD_ClassID==7) { //判断是否选择了视频分类
		$muban='mvplay.html';
		$typename="视频";
	} else {
		$typename="音乐";
	}
	if(strpos("" . $CD_Url . "", ".")===false) {
		$CD_Url=base64_decode($CD_Url);
	} //判断是否本地上传
	//过滤一些特殊符号
	$CD_Name=str_replace("'", '', $CD_Name);
	$CD_Name=str_replace("\"", '', $CD_Name);
	$CD_Lrc=strip_tags($CD_Lrc, "<p><b><br>");
	$CD_Lrc=preg_replace("/<a[^>]+>(.+?)<\/a>/i", "", $CD_Lrc);
	$CD_Word=preg_replace("/<a[^>]+>(.+?)<\/a>/i", "", $CD_Word);
	$CD_Word=str_replace('我爱歌词网', '音乐外链吧', $CD_Word);
	$CD_Word=str_replace('www.5ilrc.com', 'x5mp3.com', $CD_Word);
	$CD_Word=str_replace("\n", '<br>', $CD_Word);
	$CD_Word=str_replace(" ", '&nbsp;', $CD_Word);
	$CD_Word=strip_tags($CD_Word, "<p><b><br>");
	//入库
	$setarr=array(
		'CD_Name'=>$CD_Name,
		'CD_ClassID'=>$CD_ClassID,
		'CD_SpecialID'=>0,
		'CD_Singer'=>$CD_Singer,
		'CD_User'=>'' . $x5music_com_username . '',
		'CD_Pic'=>'',
		'CD_Url'=>$CD_Url,
		'CD_DownUrl'=>$CD_Url,
		'CD_Tag'=>$CD_Tag,
		'CD_From'=>$CD_from,
		'CD_Siz'=>$CD_siz,
		'CD_Md5'=>$CD_md5,
		'CD_Uid'=>$CD_uid,
		'CD_Lrc'=>$CD_Lrc,
		'CD_Word'=>$CD_Word,
		'CD_Pic'=>$CD_Pic,
		'CD_Hits'=>0,
		'CD_DownHits'=>0,
		'CD_FavHits'=>0,
		'CD_uHits'=>0,
		'CD_dHits'=>0,
		'CD_DayHits'=>0,
		'CD_WeekHits'=>0,
		'CD_MonthHits'=>0,
		'CD_Server'=>$CD_Server,
		'CD_Deleted'=>0,
		'CD_IsBest'=>0,
		'CD_Error'=>0,
		'CD_Passed'=>'' . cd_upmusicpassed . '',
		'CD_Points'=>$CD_Points,
		'CD_Grade'=>$CD_Grade,
		'CD_Skin'=>$muban,
		'CD_LastHitTime'=>'2014-08-24 13:06:10',
		'CD_AddTime'=>date('Y-m-d H:i:s')
	);
	inserttable('dj', $setarr, 1);
	//动态
	$rowlast=$db->getrow("select CD_ID,CD_ClassID from " . tname('dj') . " where CD_User='$x5music_com_username' order by CD_ID desc");
	if($x5music_com_userid=="") {
		$x5music_com_userid='2';
	}
	$setarrs=array(
		'cd_uid'=>$x5music_com_userid,
		'cd_username'=>'' . $x5music_com_username . '',
		'cd_icon'=>'jifen',
		'cd_title'=>'上传了一首' . $typename . '  <a href="' . LinkUrl("dj", $rowlast['CD_ClassID'], 1, $rowlast['CD_ID']) . '" target="play">' . $CD_Name . '</a>',
		'cd_data'=>'+' . cd_userupmusicpoints . '',
		'cd_image'=>'',
		'cd_imagelink'=>'',
		'cd_dataid'=>LinkUrl("dj", $rowlast['CD_ClassID'], 1, $rowlast['CD_ID']),
		'cd_addtime'=>date('Y-m-d H:i:s')
	);
	inserttable('feed', $setarrs, 1);
	$db->query('update ' . tname('user') . " set cd_points=cd_points+'" . cd_userupmusicpoints . "',cd_djmub=cd_djmub+1 where cd_name='" . $x5music_com_username . "'"); //加金币vs增加分享数量
	$db->query('update ' . tname('system') . " set cd_djmub=cd_djmub+1"); //全局数据统计
	if($x5music_com_username=="游客") {
		$zurl="" . rewrite_url('space.php?do=home') . "";
	} else {
		$zurl="" . rewrite_url('space.php?do=music') . "";
	}
	if(cd_upmusicpassed=="1") {
		showmessage("恭喜您，" . $typename . "发布成功，请等待管理员审核。正在转跳中......", $zurl, 1);
	} else {
		showmessage("恭喜您，" . $typename . "发布成功，正在转跳中......", $zurl, 1);
	}
	//收藏
} elseif($ac=="fav") {
	if(!$userlogined==true) {
		showmessage("出错了，请登陆后再操作。", "login.php", 0);
	}
	if(!IsNul($id)) {
		showmessage("出错了，请不要提交非法参数。", $_SERVER['HTTP_REFERER'], 0);
	}
	//检测歌曲是否存在
	$sql="select CD_ID,CD_ClassID,CD_Name from " . tname('dj') . " where CD_ID='$id'";
	$cd_lastid="" . LinkUrl("dj", $row['CD_ClassID'], 1, $row['CD_ID']) . "";
	if($row=$db->Getrow($sql)) {
		$cd_ids=$db->Getone("select cd_id from " . tname('fav') . " where cd_uid='$x5music_com_userid' and cd_musicid=$id");
		if($cd_ids) {
			showmessage("出错了，你要收藏的歌曲已存在。", rewrite_url('space.php?do=music&view=fav'), 1);
		} else {
			$db->query("update " . tname('dj') . " set CD_FavHits=CD_FavHits+1 where CD_ID='$id'");
			//入库
			$setarr=array(
				'cd_type'=>'fav',
				'cd_uid'=>$x5music_com_userid,
				'cd_uname'=>'' . $x5music_com_username . '',
				'cd_musicid'=>$row['CD_ID'],
				'cd_musicname'=>$row['CD_Name'],
				'cd_addtime'=>time()
			);
			inserttable('fav', $setarr, 1);
			$setarrs=array(
				'cd_uid'=>$x5music_com_userid,
				'cd_username'=>$x5music_com_username,
				'cd_icon'=>'music',
				'cd_title'=>'收藏了音乐',
				'cd_data'=>'<a href="' . $cd_lastid . '" target="play">' . $row['CD_Name'] . '</a>',
				'cd_image'=>'',
				'cd_imagelink'=>'',
				'cd_dataid'=>$row['CD_ID'],
				'cd_addtime'=>date('Y-m-d H:i:s')
			);
			inserttable('feed', $setarrs, 1);
			showmessage("恭喜您，收藏 " . $row['CD_Name'] . " 成功。", rewrite_url('space.php?do=music&view=fav'), 1);
		}
	} else {
		showmessage("出错了，你要收藏的歌曲不存在。", $_SERVER['HTTP_REFERER'], 0);
	}
} elseif($ac=="special") {
	if(!$userlogined==true) {
		showmessage("出错了，请登陆后再操作。", "login.php", 0);
	}
	//添加
	if($op=="add") {
		if(!submitcheck('submitspecial')) {
			showmessage("出错了，请不要提交非法参数。", $_SERVER['HTTP_REFERER'], 0);
		}
		$CD_Name=SafeRequest("CD_Name", "post");
		$CD_ClassID=SafeRequest("CD_ClassID", "post");
		$CD_YuYan=SafeRequest("CD_YuYan", "post");
		$CD_GongSi=SafeRequest("CD_GongSi", "post");
		$CD_Singer=SafeRequest("CD_Singer", "post");
		$CD_Pic=SafeRequest("CD_Pic", "post");
		$CD_Intro=SafeRequest("CD_Intro", "post");
		$CD_Intro=str_replace("'", '', $CD_Intro);
		$CD_Intro=str_replace("\"", '', $CD_Intro);
		$CD_Intro=str_replace("\n", '<br>', $CD_Intro);
		$CD_Intro=str_replace(" ", '&nbsp;', $CD_Intro);
		//写入数据库
		$setarr=array(
			'CD_ClassID'=>$CD_ClassID,
			'CD_Name'=>$CD_Name,
			'CD_User'=>'' . $x5music_com_username . '',
			'CD_Pic'=>$CD_Pic,
			'CD_Singer'=>$CD_Singer,
			'CD_GongSi'=>$CD_GongSi,
			'CD_YuYan'=>$CD_YuYan,
			'CD_Intro'=>$CD_Intro,
			'CD_Hits'=>0,
			'CD_IsBest'=>0,
			'CD_Passed'=>1,
			'CD_AddTime'=>date('Y-m-d H:i:s')
		);
		inserttable('special', $setarr, 1);
		//动态
		$rowlast=$db->getrow("select CD_ID from " . tname('special') . " where CD_User='$x5music_com_username' order by CD_ID desc");
		$cd_lastid="" . LinkUrl("special", 2, 1, $rowlast['CD_ID']) . "";
		$setarrs=array(
			'cd_uid'=>$x5music_com_userid,
			'cd_username'=>'' . $x5music_com_username . '',
			'cd_icon'=>'special',
			'cd_title'=>'制作了一张专辑',
			'cd_data'=>'<a href="' . $cd_lastid . '" target="special">' . $CD_Name . '</a>',
			'cd_image'=>'',
			'cd_imagelink'=>'',
			'cd_dataid'=>$cd_lastid,
			'cd_addtime'=>date('Y-m-d H:i:s')
		);
		inserttable('feed', $setarrs, 1);
		showmessage("恭喜您，制作专辑成功，请等待管理员审核。", rewrite_url('space.php?do=special'), 1);
		//设置歌曲
	} elseif($op=="music") {
		$zjid=SafeRequest("zjid", "post");
		$djlist=RequestBox("djlist");
		$zjlist=RequestBox("zjlist");
		if(submitcheck('submitadd')) {
			$db->query("update " . tname('dj') . " set CD_SpecialID='$zjid' where CD_ID in ($djlist)");
			header("Location:" . $_SERVER['HTTP_REFERER']);
		} elseif(submitcheck('submitdll')) {
			$db->query("update " . tname('dj') . " set CD_SpecialID=0 where CD_ID in ($zjlist)");
			header("Location:" . $_SERVER['HTTP_REFERER']);
		} else {
			showmessage("出错了，请不要提交非法参数。", $_SERVER['HTTP_REFERER'], 0);
		}
	}
	//-------------数据操作/删除-------------//
	//删除个人动态
} elseif($ac=="delfeed") {
	if(!$userlogined==true) {
		showmessage("出错了，请登陆后再操作。", "login.php", 0);
	}
	$row=$db->getrow("Select cd_username from " . tname('feed') . " where cd_id='" . $id . "'");
	if($row['cd_username']==$x5music_com_username) {
		$db->query("delete from " . tname('feed') . " where cd_id='" . $id . "'");
		showmessage("您的动态信息已经删除。", $_SERVER['HTTP_REFERER'], 0);
	} else {
		showmessage("请不要试图删除别人的数据。", $_SERVER['HTTP_REFERER'], 0);
	}
	//删除fav动态记录
} elseif($ac=="delfav") {
	if(!$userlogined==true) {
		showmessage("出错了，请登陆后再操作。", "login.php", 0);
	}
	$row=$db->getrow("Select cd_uname from " . tname('fav') . " where cd_id='" . $id . "'");
	if($row['cd_uname']==$x5music_com_username) {
		$db->query("delete from " . tname('fav') . " where cd_id='" . $id . "'");
		showmessage("您的动态信息已经删除。", $_SERVER['HTTP_REFERER'], 0);
	} else {
		showmessage("请不要试图删除别人的数据。", $_SERVER['HTTP_REFERER'], 0);
	}
	//删除个人音乐
} elseif($ac=="delmusic") {
	if(!$userlogined==true) {
		showmessage("出错了，请登陆后再操作。", "login.php", 0);
	}
	if(strstr($x5music_com_points, "-")) {
		showmessage("账户金币不足，无法删除。删除需要扣除" . cd_userdeletemusicpoints . "个金币，您的账户金币余额：" . $x5music_com_points . "。", rewrite_url('space.php?do=music'), 2);
		exit();
	}
	$row=$db->getrow("Select cd_name,CD_User from " . tname('dj') . " where cd_id='" . $id . "'");
	if($row['CD_User']==$x5music_com_username) {
		$db->query('update ' . tname('user') . " set cd_points=cd_points-" . cd_userdeletemusicpoints . ",cd_djmub=cd_djmub-1 where cd_name='" . $row['CD_User'] . "'"); //扣除金币
		updatetable('dj', array(
			'CD_User'=>'游客',
			'CD_Deleted'=>'1'
		), array(
			'cd_id'=>$id
		)); //改变作者
		//记录积分变化
		$setarrs=array(
			'cd_uid'=>$x5music_com_userid,
			'cd_username'=>'' . $x5music_com_username . '',
			'cd_icon'=>'jifen',
			'cd_title'=>'删除了一首音乐',
			'cd_data'=>'-' . cd_userdeletemusicpoints . '',
			'cd_image'=>'',
			'cd_imagelink'=>'',
			'cd_dataid'=>$cd_lastid,
			'cd_addtime'=>date('Y-m-d H:i:s')
		);
		inserttable('feed', $setarrs, 1);
		showmessage("[" . $row['cd_name'] . "]已成功删除，并且扣除了个" . cd_userdeletemusicpoints . "金币。您的账户金币余额：" . $x5music_com_points . "", rewrite_url('space.php?do=music'), 2);
	} else {
		showmessage("请不要试图删除别人的数据。", rewrite_url('space.php?do=music'), 2);
	}
	//删除个人短消息
} elseif($ac=="delpm") {
	if(!$userlogined==true) {
		showmessage("出错了，请登陆后再操作。", "login.php", 0);
	}
	$db->query("delete from " . tname('message') . " where cd_id='" . $id . "'");
	showmessage("短消息已经删除。", $_SERVER['HTTP_REFERER'], 0);
	//删除个人全部短消息
} elseif($ac=="delpms") {
	if(!$userlogined==true) {
		showmessage("出错了，请登陆后再操作。", "login.php", 0);
	}
	$query=$db->query("Select cd_id from " . tname('message') . " where cd_uids='" . $x5music_com_userid . "'");
	while($row=$db->fetch_array($query)) {
		$db->query("delete from " . tname('message') . " where cd_id='" . $row['cd_id'] . "'");
	}
	showmessage("短消息已经全部删除。", $_SERVER['HTTP_REFERER'], 0);
	//报错
} elseif($ac=="error") {
	if(!submitcheck('errorsubmit')) {
		showmessage("出错了，请不要提交非法参数。", $_SERVER['HTTP_REFERER'], 0);
	}
	updatetable('dj', array(
		'CD_Error'=>1
	), array(
		'CD_ID'=>$id
	));
	showmessage("恭喜您，报告错误成功。", cd_webpath, 0);
}
?>