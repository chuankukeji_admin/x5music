<?php
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
error_reporting(0);
include "../../include/x5music.conn.php";
include "../function_common.php";
admincheck(11);
$action=SafeRequest("action", "get");
?>
<!DOCTYPE html>
<html>
 <head> 
  <meta charset="gbk" /> 
  <meta name="renderer" content="webkit" /> 
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" /> 
  <title>x5Music 后台管理中心 -x5mp3.com</title> 
  <link href="../css/add.css" rel="stylesheet" /> 
  <style type="text/css">
.table2{width: 100%;max-width: 100%;border-collapse: collapse;border-spacing: 0;}
.table2 td{font-size: 12px;border-top: 1px solid #ddd;padding: 8px;vertical-align: top;}
.table2 tr:hover{text-decoration: none;background-color: #e6f2fb;}
.table2 th{border-bottom: 2px solid #ddd;vertical-align: bottom;padding: 2px;text-align: left;}
.table3{width: 100%;max-width: 100%;border-collapse: collapse;border-spacing: 0;}
.table3 tr:hover{text-decoration: none;background-color: #e6f2fb;}
.table3 td{font-size: 12px;line-height:25px;border-top: 1px solid #ddd;padding: 5px;vertical-align: top;border-right: solid 1px #ddd;}
.table3 th{border-bottom: 2px solid #ddd;vertical-align: bottom;padding: 2px;text-align: left;}
</style>
 </head>   
 <body> 
<?php
switch($action) {
	case 'add':
		Add();
		break;
	case 'saveadd':
		SaveAdd();
		break;
	case 'edit':
		Edit();
		break;
	case 'saveedit':
		SaveEdit();
		break;
	case 'islock':
		IsLock();
		break;
	case 'del':
		Del();
		break;
	default:
		main();
		break;
}
?>
 </body>
</html>
<?php
Function main(){
		global $db;
		$sql="select * from ".tname('admin')."";
		$result=$db->query($sql);
		$adminnum=$db->num_rows($result);
?>
  <div class="contents"> 
   <div class="panel"> 
    <div class="panel-head">
     <strong>后台管理员</strong>
    </div> 
	<div style="padding: 8px;">
	<a href="admin.php" class="btn btn-default">管理员列表</a> 
	<a href="admin.php?action=add" class="btn btn-default">添加管理员</a>
	</div>
    <table class="table2"> 
     <tbody>
      <tr>
       <td width="30" align="left"> 序号</td>
       <td align="left">管理帐号</td>
	   <td align="left">最后登陆时间</td>
	   <td align="left">最后登录IP</td>
	   <td align="left">登录次数</td>
	   <td align="left">状态</td>
	   <td width="100" align="left">操作</td>
      </tr> 
<?php
	   if($adminnum==0) echo "<tr><td height=\"30\" colspan=\"8\" align=\"center\" bgcolor=\"#FFFFFF\" class=\"td_border\"><br><br>没有数据<br><br><br></td></tr>";		
		if($result){
			while($row=$db->fetch_array($result)){
?>
      <tr>
       <td align="left"><?php echo $row['CD_ID']?></td>
       <td align="left"><a href="?action=edit&CD_ID=<?php echo $row['CD_ID']?>"><?php echo $row['CD_AdminUserName']?></a></td>
	   <td align="left"><?php echo $row['CD_LastLogin']?></td>
	   <td align="left"><?php echo $row['CD_LoginIP']?></td>
	   <td align="left"><?php echo $row['CD_LoginNum']?></td>
	   <td align="left"><?php if($row['CD_IsLock']==1){?><a title="锁定" href="?action=islock&CD_ID=<?php echo $row['CD_ID']?>&CD_IsLock=0"><img src='../images/no.gif' border='0'></a><?php }else{?><a title="正常" href="?action=islock&CD_ID=<?php echo $row['CD_ID']?>&CD_IsLock=1"><img src='../images/yes.gif' border='0'></a><?php }?></td>
	   <td align="left"><a href="?action=edit&CD_ID=<?php echo $row['CD_ID']?>" class="btn">编辑</a>&nbsp;&nbsp;<a href="?action=del&CD_ID=<?php echo $row['CD_ID']?>" onClick="return confirm('确定要删除吗？不可恢复！');" class="btn">删除</a></td>
      </tr> 
<?php				
}
}
?>
     </tbody>
    </table> 
   </div> 
   </div> 
 </body>
</html>

<?php
}

Function EditBoard($Arr,$url){
		global $db;
		$CD_ID = SafeRequest("CD_ID","get");
		$CD_AdminUserName = $Arr[0];
		$CD_AdminPassWord = $Arr[1];
		$CD_IsLock = $Arr[2];
		$CD_Permission = $Arr[3];

?>
<script type="text/javascript">
function CheckForm(){
        if (document.form2.CD_AdminUserName.value==""){
            alert("登陆帐号不能为空！");
            document.form2.CD_AdminUserName.focus();
            return false;
        }
}
  function HideError() {
  return true;
}
</script>
  <div class="contents"> 
   <div class="panel"> 
    <div class="panel-head">
     <strong>
	 <?php 
	 $action=SafeRequest("action","get");
	 if($action==""){echo "后台管理员";
	 }elseif($action=="edit"){echo "编辑管理员";
	 }elseif($action=="add"){echo "添加管理员";
	 }else{
	 echo "后台管理员";
	 }
	?>
	 </strong>
    </div> 
	<div style="padding: 8px;">
	<a href="admin.php" class="btn btn-default">管理员列表</a> 
	<a href="admin.php?action=add" class="btn btn-default">添加管理员</a>
	</div>
	<form name="form2" method="post" action="<?php echo $url?>">
    <table class="table3"> 
     <tbody>
      <tr>
       <td width="60" align="left">登陆帐号</td>
       <td align="left"><input class="input length_4" name="CD_AdminUserName" type="text" id="CD_AdminUserName" value="<?php echo $CD_AdminUserName?>"/></td>
      </tr> 
      <tr>
       <td width="60" align="left">登陆密码</td>
       <td align="left"><input class="input length_4" name="CD_AdminPassWord" type="text" id="CD_AdminPassWord" value=""/> 　* 不修改请留空</td>
      </tr> 
      <tr>
       <td width="60" align="left">确认密码</td>
       <td align="left"><input class="input length_4" name="CD_AdminPassWord1" type="text" id="CD_AdminPassWord1" value=""/>　* 不修改请留空</td>
      </tr> 
      <tr>
       <td width="60" align="left">是否开启</td>
       <td align="left">
	   <input type="radio" name="CD_IsLock" id="CD_IsLock" class="checkbox" value="0"<?php if($CD_IsLock==0){echo " checked"; }?> />开启&nbsp;&nbsp;
       <input type="radio" name="CD_IsLock" id="CD_IsLock" class="checkbox" value="1"<?php if($CD_IsLock==1){echo " checked"; }?> />禁用
	   </td>
      </tr> 
      <tr>
       <td width="60" align="left">管理权限</td>
       <td align="left">
       <label class="mr20"><input type="checkbox" name="CD_Permission[]" checked value="1"  class="checkbox" />首页</label>&nbsp;
       <label class="mr20"><input type="checkbox" name="CD_Permission[]" <?php if(checkpermission($CD_Permission,2)=="1"){echo "checked";}?> value="2"  class="checkbox" />全局</label>&nbsp;
       <label class="mr20"><input type="checkbox" name="CD_Permission[]" <?php if(checkpermission($CD_Permission,3)=="1"){echo "checked";}?> value="3"  class="checkbox" />内容</label>&nbsp;
       <label class="mr20"><input type="checkbox" name="CD_Permission[]" <?php if(checkpermission($CD_Permission,4)=="1"){echo "checked";}?> value="4"  class="checkbox" />会员</label>&nbsp;
       <label class="mr20"><input type="checkbox" name="CD_Permission[]" <?php if(checkpermission($CD_Permission,5)=="1"){echo "checked";}?> value="5"  class="checkbox" />文章</label>&nbsp;
       <label class="mr20"><input type="checkbox" name="CD_Permission[]" <?php if(checkpermission($CD_Permission,6)=="1"){echo "checked";}?> value="6"  class="checkbox" />插件</label>&nbsp;<br />
       <label class="mr20"><input type="checkbox" name="CD_Permission[]" <?php if(checkpermission($CD_Permission,7)=="1"){echo "checked";}?> value="7"  class="checkbox" />模板</label>&nbsp;
       <label class="mr20"><input type="checkbox" name="CD_Permission[]" <?php if(checkpermission($CD_Permission,8)=="1"){echo "checked";}?> value="8"  class="checkbox" />广告</label>&nbsp;
       <label class="mr20"><input type="checkbox" name="CD_Permission[]" <?php if(checkpermission($CD_Permission,9)=="1"){echo "checked";}?> value="9"  class="checkbox" />工具</label>&nbsp;
       <label class="mr20"><input type="checkbox" name="CD_Permission[]" <?php if(checkpermission($CD_Permission,10)=="1"){echo "checked";}?> value="10"  class="checkbox" />生成</label>&nbsp;
	   <label class="mr20"><input type="checkbox" name="CD_Permission[]" <?php if(checkpermission($CD_Permission,11)=="1"){echo "checked";}?> value="11"  class="checkbox" />本页权限</label>&nbsp;
	   </td>
      </tr> 
      <tr>
       <td height="35" colspan="2" align="left" bgcolor="#FAFBF7" class="td_border"><button type="submit" id="adddance" class="btn btn_submit J_ajax_submit_btn" style="margin-left: 80px;">确定提交</button></td>
      </tr>
     </tbody>
    </table> 
	</form>  
   </div> 
   </div> 
 </body>
</html>	
<?php
}
	function checkpermission($perm,$value){
		$permarr=explode(',',$perm);
		for($i=0;$i<count($permarr);$i++){
			if($permarr[$i]==$value){
				return "1";
				break;
				}
			}
		}

	//保存编辑数据
	function SaveEdit(){
		global $db;
		$CD_ID=SafeRequest("CD_ID","get");
		$CD_AdminUserName=SafeRequest("CD_AdminUserName","post");
		$CD_AdminPassWord=SafeRequest("CD_AdminPassWord","post");
		$CD_AdminPassWord1=SafeRequest("CD_AdminPassWord1","post");
		$CD_IsLock=SafeRequest("CD_IsLock","post");
		$CD_AdminCheck=RequestBox("CD_Permission");
		if($CD_AdminUserName==""){showmessage("出错了，登陆帐号不能为空！","admin.php",1);}
		if($CD_AdminPassWord<>$CD_AdminPassWord1){showmessage("出错了，两次密码不一样！","admin.php",1);}
		if($CD_AdminPassWord1){
			$sql="update ".tname('admin')." set CD_AdminUserName='".$CD_AdminUserName."',CD_AdminPassWord='".md5($CD_AdminPassWord)."',CD_IsLock=".$CD_IsLock.",CD_Permission='".$CD_AdminCheck."' where CD_ID='$CD_ID'";
		}else{
			$sql="update ".tname('admin')." set CD_AdminUserName='".$CD_AdminUserName."',CD_Permission='".$CD_AdminCheck."',CD_IsLock=".$CD_IsLock." where CD_ID='$CD_ID'";

		}

		if($db->query($sql)){
			showmessage("恭喜您，编辑成功！","admin.php",1);
		}else{
			showmessage("出错了，编辑失败！","admin.php",1);
		}	
	}

	//编辑数据
	function Edit(){
		global $db;
		$CD_ID=SafeRequest("CD_ID","get");
		$sql="Select * from ".tname('admin')." where CD_ID=".$CD_ID."";
		if($row=$db->Getrow($sql)){
			$Arr=array($row['CD_AdminUserName'],$row['CD_AdminPassWord'],$row['CD_IsLock'],$row['CD_Permission']);
			}
		EditBoard($Arr,"?action=saveedit&CD_ID=".$CD_ID."");
	}

	//删除数据
	function Del(){
		global $db;
		$CD_ID=SafeRequest("CD_ID","get");
		$sql="delete from ".tname('admin')." where CD_ID='".$CD_ID."'";
		if($db->query($sql)){
			echo "<script>window.location='admin.php';</script>";
			}
		}

	//保存添加数据
	function SaveAdd(){
		global $db;
		$CD_AdminUserName = SafeRequest("CD_AdminUserName","post");
		$CD_AdminPassWord = SafeRequest("CD_AdminPassWord","post");
		$CD_AdminPassWord1 = SafeRequest("CD_AdminPassWord1","post");
		$CD_IsLock = SafeRequest("CD_IsLock","post");
		$CD_AdminCheck=RequestBox("CD_Permission");
		if($CD_AdminUserName==""){showmessage("出错了，登陆帐号不能为空！","admin.php",1);}
		if($CD_AdminPassWord==""){showmessage("出错了，登陆密码不能为空！","admin.php",1);}
		if($CD_AdminPassWord<>$CD_AdminPassWord1){showmessage("出错了，两次密码不一样！","admin.php",1);}
		$AdminID=$db->getOne("select CD_ID from ".tname('admin')." where CD_AdminUserName='".$CD_AdminUserName."'");
		if($AdminID){
			showmessage("出错了，该管理员帐号已经存在！","admin.php",1);
		}else{
			$sql="Insert ".tname('admin')." (CD_AdminUserName,CD_AdminPassWord,CD_LoginNum,CD_IsLock,CD_Permission) values ('".$CD_AdminUserName."','".md5($CD_AdminPassWord1)."',0,".$CD_IsLock.",'".$CD_AdminCheck."')";
			if($db->query($sql)){
				showmessage("恭喜您，添加成功！","admin.php",0);
			}else{
				showmessage("出错了，添加失败！","admin.php",0);
			}
		}	
		}

	//添加数据
	function Add(){
		$Arr=array("","","","","","","","","","","","","","","","","","","","","");
		EditBoard($Arr,"?action=saveadd");
		}

	//更新状态
	function IsLock(){
		global $db;
		$CD_ID = SafeRequest("CD_ID","get");
		$CD_IsLock = SafeRequest("CD_IsLock","get");
		$sql="update ".tname('admin')." set CD_IsLock=".$CD_IsLock." where CD_ID=".$CD_ID."";
		if($db->query($sql)){
			echo "<script>window.location='".$_SERVER['HTTP_REFERER']."'</script>";
			}	
		}

?>