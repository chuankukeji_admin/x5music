function get_cookie(_name){
	var Res=eval('/'+_name+'=([^;]+)/').exec(document.cookie); return Res==null?'':unescape(Res[1]);
};
var DomainUrl = top.location.hostname; 
function SetCookie(name, value){
	var expdate = new Date();
	var argv = SetCookie.arguments;
	var argc = SetCookie.arguments.length;
	var expires = (argc > 2) ? argv[2] : null;
	var path = (argc > 3) ? argv[3] : null;
	var domain = (argc > 4) ? argv[4] : null;
	var secure = (argc > 5) ? argv[5] : false;
	if(expires!=null) expdate.setTime(expdate.getTime() + ( expires * 1000 ));
	document.cookie = name + "=" + escape (value) +((expires == null) ? "" : ("; expires="+ expdate.toGMTString()))+((path == null) ? "" : ("; path=" + path)) +((domain == null) ? "" : ("; domain=" + domain))+((secure == true) ? "; secure" : "");
};

function set_history(_url, _name, _id){
	var current_str = _id + "#" + _name + "#" + _url + "*ylmv*";
	var cookie_info = get_cookie("ylmvHistory");
	var deal_cookie="";
	if( "" != cookie_info){
		cookie_info = cookie_info.split("*ylmv*");
		var N = (cookie_info.length > 18) ? 18 : cookie_info.length - 1;
		for(var i=0; i<N; i++){
			if(current_str != cookie_info[i] + "*ylmv*" && '' != cookie_info[i]) deal_cookie += (cookie_info[i] + "*ylmv*");
		}
	}
	current_str = ('' == deal_cookie) ? current_str : current_str + deal_cookie;
	SetCookie("ylmvHistory", current_str, 48 * 3600, "/", DomainUrl, false);
};

function get_history(){
	var cookie_info=get_cookie("ylmvHistory").split("*ylmv*");
	var N = cookie_info.length - 1;
	var history_list = "", infos, s;
	for(var i=0; i<N; i++){
		var ii = i+1;
		infos = cookie_info[i].split("#");
		history_list += '<li><span class="check"><input name="check" type="checkbox" value="'+infos[0]+'"></span>&nbsp;<span class="title"><a title="'+infos[1]+'" href="'+infos[2]+'" target="_self">'+infos[1]+'</a></span></li>';
	}
	//history_list += '<div align="center"><iframe src="" width="300" height="250" scrolling="no" frameborder="0" style="display:block;margin:0"></iframe></div><br>';
	$A("music_list").innerHTML = history_list;
	$A("list_do").innerHTML = '<a href="javascript:void(0)" onclick="del_history();return false;" class="his">清除记录</a>';
};

function del_history(){
	SetCookie("ylmvHistory", "", null, "/", DomainUrl, false);
	$A("music_list").innerHTML = "<li class='selected'>&nbsp;清除试听记录成功!</li>";
};

function over_bg(t){ t.style.background = '#FFFFFF'; };
function out_bg(t){ t.style.background = '#FFFFFF'; };

set_history(location.href, music_name ,music_id);

//分类
function get_listtop(_classid){
	document.getElementById("music_list").innerHTML = '<li class="selected" style="height: 35px;"><img src="'+web_skin+'images/loadingx.gif" />&nbsp;玩命加载中...</li>';
	var theHttpRequest = getHttpObject();
	theHttpRequest.onreadystatechange = function() {processAJAX();};
	theHttpRequest.open("GET", web_skin+"player.php?action=class&id="+_classid, true);
	theHttpRequest.send(null);
	function processAJAX(){
		if (theHttpRequest.readyState == 4) {
			if (theHttpRequest.status == 200) {
				document.getElementById("music_list").innerHTML = unescape(theHttpRequest.responseText);
        			document.getElementById("list_do").innerHTML = '';
			} else {
				document.getElementById("music_list").innerHTML = "您请求的页面出现异常错误";
 			}
		}
	}
}
//分类推荐
function get_goodtop(_classid){
	document.getElementById("music_list").innerHTML = '<li class="selected" style="height: 35px;"><img src="'+web_skin+'images/loadingx.gif" />&nbsp;玩命加载中...</li>';
	var theHttpRequest = getHttpObject();
	theHttpRequest.onreadystatechange = function() {processAJAX();};
	theHttpRequest.open("GET", web_skin+"player.php?action=goodtop&id="+_classid, true);
	theHttpRequest.send(null);
	function processAJAX(){
		if (theHttpRequest.readyState == 4) {
			if (theHttpRequest.status == 200) {
				document.getElementById("music_list").innerHTML = unescape(theHttpRequest.responseText);
        			document.getElementById("list_do").innerHTML = '';
			} else {
				document.getElementById("music_list").innerHTML = "您请求的页面出现异常错误";
 			}
		}
	}
}
//分类今日人气
function get_newtop(_classid){
	document.getElementById("music_list").innerHTML = '<li class="selected" style="height: 35px;"><img src="'+web_skin+'images/loadingx.gif" />&nbsp;玩命加载中...</li>';
	var theHttpRequest = getHttpObject();
	theHttpRequest.onreadystatechange = function() {processAJAX();};
	theHttpRequest.open("GET", web_skin+"player.php?action=dayhitstop&id="+_classid, true);
	theHttpRequest.send(null);
	function processAJAX(){
		if (theHttpRequest.readyState == 4) {
			if (theHttpRequest.status == 200) {
				document.getElementById("music_list").innerHTML = unescape(theHttpRequest.responseText);
        			document.getElementById("list_do").innerHTML = '';
			} else {
				document.getElementById("music_list").innerHTML = "您请求的页面出现异常错误";
 			}
		}
	}
}
//最近更新
function get_new(){
	document.getElementById("music_list").innerHTML = '<li class="selected" style="height: 35px;"><img src="'+web_skin+'images/loadingx.gif" />&nbsp;玩命加载中...</li>';
	var theHttpRequest = getHttpObject();
	theHttpRequest.onreadystatechange = function() {processAJAX();};
	theHttpRequest.open("GET", web_skin+"player.php?action=new", true);
	theHttpRequest.send(null);
	function processAJAX(){
		if (theHttpRequest.readyState == 4) {
			if (theHttpRequest.status == 200) {
				document.getElementById("music_list").innerHTML = unescape(theHttpRequest.responseText);
        			document.getElementById("list_do").innerHTML = '';
			} else {
				document.getElementById("music_list").innerHTML = "您请求的页面出现异常错误";
 			}
		}
	}
}
//我收藏的
function get_myfav(){
	document.getElementById("music_list").innerHTML = '<li class="selected" style="height: 35px;"><img src="'+web_skin+'images/loadingx.gif" />&nbsp;玩命加载中...</li>';
	var theHttpRequest = getHttpObject();
	theHttpRequest.onreadystatechange = function() {processAJAX();};
	theHttpRequest.open("GET", web_skin+"player.php?action=myfav", true);
	theHttpRequest.send(null);
	function processAJAX(){
		if (theHttpRequest.readyState == 4) {
			if (theHttpRequest.status == 200) {
				document.getElementById("music_list").innerHTML = unescape(theHttpRequest.responseText);
        			document.getElementById("list_do").innerHTML = '';
			} else {
				document.getElementById("music_list").innerHTML = "您请求的页面出现异常错误";
 			}
		}
	}
}
//播放登录
function x5music_playlogadd() {
	var name = $("#cd_pname").val();
	var pass = $("#cd_ppass").val();
	if (name == '' || pass == "") {
		asyncbox.tips("请输入帐号密码!", "wait", 1500);
	} else {
		$.getJSON(web_skin + "login.php?action=dologin&name=" + name + "&pass=" + pass + "&random=" + Math.random() + "&callback=?", function (data) {
			if (data['error'] == '10001') { //用户名为空
				asyncbox.tips("帐号不能为空!", "wait", 1500);
			} else if (data['error'] == '10002') { //密码为空
				asyncbox.tips("密码不能为空!", "wait", 1500);
			} else if (data['error'] == '10003') { //帐号不存在
				asyncbox.tips("帐号或密码错误!", "wait", 1500);
			} else if (data['error'] == '10004') { //密码错误
				asyncbox.tips("密码错误!", "wait", 1000);
			} else if (data['error'] == '10005') { //帐号被锁定
				asyncbox.tips("对不起，该帐号已经被锁定!", "wait", 1500);
			} else if (data['error'] == '10007') { //邮件未激活
				asyncbox.tips("您的帐号未通过审核!", "wait", 1500);
			} else if (data['error'] == '10008') { //登录成功
				getlogin();
				asyncbox.tips("登录成功!", "success", 1500);
				get_myfav();
			} else if (data['error'] == '10009') { //登录成功
				asyncbox.tips("本站已关闭帐号登录！!", "error", 1500);
			} else {
				asyncbox.tips("您请求的页面出现异常错误！", "error", 3000);
			}
		});
	}
}
function set_tab(_current, _n){
	for(i=1;i<=_n;i++){
		$A('title_' + i).className = (i == _current) ? 'ico this' : '';
	}
	switch( _current ){
		case 1 : get_history(); break;
		case 2 : get_listtop(music_classid);break;
		case 3 : get_goodtop(music_classid);break;
		case 4 : get_newtop(music_classid); break;
		case 5 : get_new(); break;
		case 6 : get_myfav(); break;
	}
};