<?php
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
error_reporting(0);
include "../../include/x5music.conn.php";
include "../function_common.php";
$f=SafeRequest("f", "auto");
$activepath=SafeRequest("activepath", "auto");
if(!eregi($_SERVER['SERVER_NAME'], $_SERVER['HTTP_REFERER'])) {
	echo "<br><p align=center><font color='red' style='font-size:9pt'>对不起，为了系统安全，不允许直接输入地址访问本系统的后台管理页面。</font></p>";
	exit();
}
?>
<html>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=gb2312'>
<title>模板管理器</title>
<style>
.linerow {border-bottom: 1px solid #FFF;}
</style>
</head>
<body leftmargin='0' topmargin='0'>
<SCRIPT language='JavaScript'>
function ReturnValue(reimg){
this.parent.document.<?php echo $f?>.value=reimg;
this.parent.layer.closeAll();
}
</SCRIPT>
<table width='100%' border='0' cellpadding='0' cellspacing='1' bgcolor='#ddd' align="center">
<tr bgcolor='#FFFFFF'>
<td colspan='3'>
<!-- 开始文件列表  -->
<table width='100%' border='0' cellspacing='0' cellpadding='2'>
<tr bgcolor="#CCCCCC">
<td width="55%" align="center" background="../images/wbg.gif" class='linerow'><strong>点击名称选择文件</strong></td>
<td width="15%" align="center" background="../images/wbg.gif" class='linerow'><strong>文件大小</strong></td>
<td width="30%" align="center" background="../images/wbg.gif" class='linerow'><strong>最后修改时间</strong></td>
</tr>

<tr>
<td colspan='3' class='linerow'>&nbsp;<img src=../images/dir2.gif border=0 width=16 height=16 align=absmiddle>当前目录: <?php echo cd_webpath.cd_templateurl?></td>
</tr>
<?php
$path = "../../".cd_templateurl;
if(file_exists($path) && is_dir($path))
{
$d = dir($path); 
while(false !== ($v = $d->read()))
{
if($v == "." || $v == "..")
{
continue; 
}
$file = $d->path."\\".$v; 
if(is_dir($file))
{
//echo "<a href='?action=templist&path=".$path."&dir=$v'>[$v]</a><br />";
}
}
$d->rewind();

while(false !== ($v = $d->read()))
{

if($v == "." || $v == "..")
{
continue; 
}
$file = $d->path . $v; 
if(is_dir($file))
{
continue;
}
if(is_file($file))
{
?>
<tr>
<td class='linerow' bgcolor='#ddd'>&nbsp;<a href="javascript:ReturnValue('<?php echo $v?>');"><?php echo $v?></a></td>
<td class='linerow' bgcolor='#ddd'><?php echo round(filesize($file)/1204,2)."Kb"?></td>
<td align='center' class='linerow' bgcolor='#ddd'><?php echo date('Y-m-d H:i:s',filemtime($file))?></td>
</tr>
<?php					
}
}
?>
</table>
<?php	
$d->close();
}
?>
</td>
</tr>
</table>
</body>
</html>