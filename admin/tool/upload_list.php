<?php
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
error_reporting(0);
include "../../include/x5music.conn.php";
include "../function_common.php";
admincheck(9);
$action=SafeRequest("action","get");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head> 
  <meta http-equiv="Content-Type" content="text/html; charset=gb2312" /> 
  <meta name="renderer" content="webkit" /> 
  <title>x5Music 管理后台</title> 
  <link rel="stylesheet" href="../css/add.css" type="text/css" media="screen" /> 
  <style type="text/css">
.table2{width: 100%;max-width: 100%;border-collapse: collapse;border-spacing: 0;}
.table2 td{font-size: 12px;border-top: 1px solid #ddd;padding: 8px;vertical-align: top;}
.table2 tr:hover{text-decoration: none;background-color: #e6f2fb;}
.table2 th{border-bottom: 2px solid #ddd;vertical-align: bottom;padding: 2px;text-align: left;}
.table3{width: 100%;max-width: 100%;border-collapse: collapse;border-spacing: 0;}
.table3 tr:hover{text-decoration: none;background-color: #e6f2fb;}
.table3 td{font-size: 12px;line-height:25px;border-top: 1px solid #ddd;padding: 5px;vertical-align: top;border-right: solid 1px #ddd;}
.table3 th{border-bottom: 2px solid #ddd;vertical-align: bottom;padding: 2px;text-align: left;}
</style> 
 </head> 
 <body> 
<?php
switch($action){
	case 'keyword':
		$key=SafeRequest("key","get");
		$sql='select * from '.tname('upload')." where cd_filename like '%".$key."%' or cd_username like '%".$key."%' or cd_filetype like '%".$key."%' order by cd_filetime desc";
		logs($sql,20);
		break;
	default:
		$sql="select * from ".tname('upload')." order by cd_filetime desc";
		logs($sql,20);
		break;
	}
?>
</body>
</html>
<?php
function logs($sql,$size){
	global $db;
	$Arr=getpagerow($sql,$size);
	$result=$db->query($Arr[2]);
	$upnum=$db->num_rows($result);
?>
<div class="contents"> 
   <div class="panel"> 
    <div class="panel-head"> 
     <strong> 文件上传记录　作用：查看是否有异常文件</strong> 
    </div> 
  <form method="get" action=""> 
   <div class="search_type cc mb10"> 
    <div class="ul_wrap"> 
     <ul class="cc"> 
      <li> <label>关键字：</label> <input type="hidden" name="action" value="keyword" /> <input name="key" id="key" value="" type="text" class="input length_3" placeholder="支持文件名，会员名，类型" /> &nbsp;&nbsp;<button class="btn mr20" type="submit">搜索</button> </li> 
     </ul> 
    </div> 
   </div> 
  </form>
     <table class="table2" id="dellist"> 
      <tbody> 
       <tr> 
        <td align="left"> 文件名</td> 
        <td align="left">上传会员</td> 
        <td align="center">大小</td> 
		<td align="center">类型</td> 
		<td align="center">格式</td>
		<td align="center">上传时间</td>
       </tr> 
<?php
if($upnum==0){
	echo "<tr><td height='30' colspan='6' align='center' bgcolor='#FFFFFF' class='td_border'><br><br>没有数据<br><br><br></td></tr>";
}
if($result) {
	while($row=$db->fetch_array($result)) {
?>
       <tr> 
        <td align="left"><a href="<?php echo cd_webpath.$row['cd_fileurl']; ?>" target="_blank" class="act"><?php echo $row['cd_filename']; ?></a></td> 
        <td align="left"><a href="?action=keyword&key=<?php echo $row['cd_username']; ?>" class="act"><?php echo ReplaceStr($row['cd_username'],SafeRequest("key","get"),"<font color='red'>".SafeRequest("key","get")."</font>"); ?></a></td> 
        <td align="center"><?php echo formatsize($row['cd_filesize']); ?></td> 
		<td align="center"><?php echo fileext($row['cd_fileurl']); ?></td> 
		<td align="center"><a href="?action=keyword&key=<?php echo $row['cd_filetype']; ?>" class="act"><?php echo ReplaceStr($row['cd_filetype'],SafeRequest("key","get"),"<font color='red'>".SafeRequest("key","get")."</font>"); ?></a></td>
		<td align="center"><?php if(date("Y-m-d",$row['cd_filetime'])==date('Y-m-d')){ echo "<font color='red'>".date("Y-m-d H:i:s",$row['cd_filetime'])."</font>"; }else{ echo date("Y-m-d H:i:s",$row['cd_filetime']); } ?></td>
       </tr> 
<?php
}
}
?>
      </tbody> 
     </table> 
   </div> 
  </div> 
  <div class="p10" style="margin-bottom: 0px;"> 
   <div class="pages"><?php echo $Arr[0];?></div>
  </div> 
<?php } ?>