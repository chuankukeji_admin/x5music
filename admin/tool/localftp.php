<?php
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
error_reporting(0);
include "../../include/x5music.conn.php";
include "../function_common.php";
admincheck(3);
include_once(_x5music_root_ . 'data/data_setting.php');
$ac=SafeRequest("ac", "get");
if($ac=="hard") {
	$f=SafeRequest("f", "auto");
	$activepath=SafeRequest("activepath", "auto");
?>
<html>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=gb2312'>
<title>扫描本地硬盘音乐</title>
<link href="../css/add.css" rel="stylesheet" />
<style type="text/css">
*{font-size: 14px;font-family: "宋体";}
body{margin-top:5px;font-size: 14px;line-height: 1.5;font-family: "宋体";}
form,h1,h2,h3,ul,ol,div{margin: 0;padding:0;}
td,th,div{word-break:break-all;word-wrap:break-word;}
b,strong{color:#333;}
li,dd{list-style-type:none;margin:0px;padding:0px;}
h1{color:#171B16;font-size:130%;font-weight:bold;}
h2{color:#171B16;font-size:115%;font-weight:bold;}
h3{color:#171B16;font-size:100%;font-weight:bold;}
a:link{font-size: 9pt;color: #000000;text-decoration: none;font-family: "宋体";}
a:visited{font-size: 9pt;color: #000000;text-decoration: none;font-family: "宋体";}
a:hover{color: red;font-family: "宋体";}
a img{border-style:none;}
b a{color:#666600;}
strong a{color:#666600;}
a b{color:#666600;}
a strong{color:#666600;}
.np{border:none;}
.linerow{border-bottom: 1px solid #ACACAC;}
.coolbg{border-right: 1px solid #ACACAC;border-bottom: 1px solid #ACACAC;background-color: #F1F8B4;padding:2px;padding-top:5px;padding-right:5px;padding-left:5px;background:#EFF7D0;cursor:pointer;}
.length_3{width: 170px;}
.length_2{width: 170px;}
.length_1{width: 40px;}
.length_4{width: 170px;}
.length_5{width: 170px;}
.length_6{width: 270px;}
.input{height: 28px;}
.input, textarea, select{padding: 4px 4px;font-size: 100%;line-height: 18px;border: 1px solid #ccc;background-color: #fff;box-shadow: 2px 2px 2px #f0f0f0 inset;border-radius: 1px;vertical-align: middle;margin: 0;font-family: inherit;}
.btn_submit:hover{background-position: 0 -160px;}
.btn_error, .btn_error:hover, .btn_success, .btn_success:hover, .btn_submit, .btn_submit:hover{color: #ffffff !important;}
.btn:hover{background-position: 0 -40px;color: #333;text-decoration: none;}
input.btn, button.btn{;}
.btn_submit{background-position: 0 -120px;background-color: #1b75b6;text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);border-color: #106bab #106bab #0d68a9;}
.btn_error, .btn_error:hover, .btn_success, .btn_success:hover, .btn_submit, .btn_submit:hover{color: #423030 !important;}
.btn{color: #333;background: #e6e6e6 url(../images/btn.png);border: 1px solid #c4c4c4;border-radius: 2px;text-shadow: 0 1px 1px rgba(255, 255, 255, 0.75);padding: 4px 10px;display: inline-block;cursor: pointer;font-size: 100%;line-height: normal;text-decoration: none;overflow: visible;vertical-align: middle;text-align: center;zoom: 1;white-space: nowrap;font-family: inherit;_position: relative;}
</style>
</head>
<body leftmargin='0' topmargin='0'>
<SCRIPT language='JavaScript'>
function nullLink(){
	return;
}
function ReturnValue(reimg){
	this.parent.document.form1.CD_Path.value=reimg;
	this.parent.layer.closeAll();
}
</SCRIPT>
<table width='100%' border='0' cellpadding='0' cellspacing='1' bgcolor='#ddd'>
   <tr bgcolor='#FFFFFF'>
      <td colspan='3'>
<!-- 开始文件列表  -->
           <table width='100%' border='0' cellspacing='0' cellpadding='2'>
                <tr>
			<td colspan='3' class='linerow'>&nbsp;<img src=../images/dir2.gif border=0 width=16 height=16 align=absmiddle>&nbsp;当前目录: <?php echo replacestr($activepath,"../../","/");?></td>
			<td class='linerow'><a href="javascript:history.back(1);">返回上一页</a>&nbsp;</td>

                </tr>
                <tr bgcolor="#CCCCCC">
			<td width="10%" align="center" background="../images/wbg.gif" class='linerow'><strong>选择</strong></td>
			<td width="50%" background="../images/wbg.gif" class='linerow'><strong>目录名称</strong></td>
			<td width="15%" background="../images/wbg.gif" class='linerow'>&nbsp;</td>
			<td width="25%" background="../images/wbg.gif" class='linerow'>&nbsp;</td>
                </tr>
                <?php
		$path = $activepath;
		if(file_exists($path) && is_dir($path)){
			$d = dir($path);
			while(false !== ($v = $d->read())){
				if($v == "." || $v == ".."){
					continue; 
				}
				$file = $d->path."\\".$v; 
				if(is_dir($file)){
					$paths	= replacestr($activepath,"../../","/")."/".$v."/";
					echo "<tr>";
					echo "<td align='center' class='linerow' bgcolor='#F9FBF0'><input type='checkbox' value='0' class='checkbox' onClick=\"ReturnValue('".replacestr($paths,"//","/")."');\" /></td>";
					echo "<td class='linerow' bgcolor='#F9FBF0'><img src='../images/dir.gif'>&nbsp;<a href='localftp.php?ac=hard&activepath=".$activepath.$v."/'>".$v."</a></td>";
					echo "<td class='linerow' bgcolor='#F9FBF0'>&nbsp;</td>";
					echo "<td class='linerow' bgcolor='#F9FBF0'>".date('Y-m-d H:i:s',filemtime($file))."</td>";
					echo "</tr>";

   					//echo "<a href='?action=templist&path=".$path."&dir=$v'>[$v]</a><br />";
				}
			}
			$d->rewind();

			while(false !== ($v = $d->read())){
				if($v == "." || $v == ".."){
					continue; 
				}
				$file = $d->path . $v; 
				if(is_dir($file)){
					continue;
				}
				if(is_file($file)){
					echo "<tr>";
					echo "<td align='center' class='linerow' bgcolor='#F9FBF0'><input type='checkbox' value='0' class='checkbox' disabled /></td>";
					echo "<td class='linerow' bgcolor='#F9FBF0'><img src='../images/htm.gif'>&nbsp;".$v."</td>";
					echo "<td class='linerow' bgcolor='#F9FBF0'>". formatsize(filesize($file))."</td>";
					echo "<td class='linerow' bgcolor='#F9FBF0'>".date('Y-m-d H:i:s',filemtime($file))."</td>";
					echo "</tr>";					
				}
			}
			$d->close();
		}
                ?>
           </table>
      </td>
   </tr>
</table>
</body>
</html>
<?php }else{ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head> 
  <meta http-equiv="Content-Type" content="text/html; charset=gb2312" /> 
  <meta name="renderer" content="webkit" /> 
  <title>x5Music 管理后台</title> 
  <link rel="stylesheet" href="../css/add.css" type="text/css" media="screen" /> 
  <script type="text/javascript" src="<?php echo cd_webpath?>user/static/space/layer/jquery.js"></script> 
  <script type="text/javascript" src="<?php echo cd_webpath?>user/static/space/layer/lib.js"></script> 
  <script type="text/javascript">
var pop = {
	up : function (text, url, width, height, top) {
		$.layer({
			type : 2,
			maxmin : true,
			title : text,
			iframe : {
				src : url
			},
			area : [width, height],
			offset : [top, '50%'],
			shade : [0]
		});
	}
}
function CheckForm(){
        if (document.form1.CD_ClassID.value=="0"){
            alert("请先选择所属栏目！");
            document.form1.CD_ClassID.focus();
            return false;
        }
        if (document.form1.CD_Singer.value==""){
            alert("所属歌手不能为空！");
            document.form1.CD_Singer.focus();
            return false;
        }
        if (document.form1.CD_User.value==""){
            alert("添加会员不能为空！");
            document.form1.CD_User.focus();
            return false;
        }
        if (document.form1.CD_Skin.value==""){
            alert("播放页面风格模板不能为空！");
            document.form1.CD_Skin.focus();
            return false;
        }
}
</script> 
  <style type="text/css">
.table2{width: 100%;max-width: 100%;border-collapse: collapse;border-spacing: 0;}
.table2 td{font-size: 12px;border-top: 1px solid #ddd;padding: 8px;vertical-align: top;}
.table2 tr:hover{text-decoration: none;background-color: #e6f2fb;}
.table2 th{border-bottom: 2px solid #ddd;vertical-align: bottom;padding: 2px;text-align: left;}
.table3{width: 100%;max-width: 100%;border-collapse: collapse;border-spacing: 0;}
.table3 tr:hover{text-decoration: none;background-color: #e6f2fb;}
.table3 td{font-size: 12px;line-height:25px;border-top: 1px solid #ddd;padding: 5px;vertical-align: top;border-right: solid 1px #ddd;}
.table3 th{border-bottom: 2px solid #ddd;vertical-align: bottom;padding: 2px;text-align: left;}
</style> 
 </head> 
 <body> 
<?php
$action=SafeRequest("do","post");
switch($action){
	case 'add':
		add(1);
		break;
	case 'hard':
		add(2);
		break;
	default:
		main();
		break;
}
?>
</body>
</html>
<?php } ?>
<?php function main(){ ?>
  <div class="contents"> 
   <div class="panel"> 
    <div style="padding: 8px;"> 
     <a href="localftp.php" class="btn btn_success">扫描本地硬盘音乐</a> 
     <a href="localftp_video.php" class="btn">扫描本地硬盘视频</a>
     <a href="ftp.php" class="btn">扫描远程FTP音乐</a>
     <a href="ftp_video.php" class="btn">扫描远程FTP视频</a>
    </div> 
   </div> 
  </div>
  <div class="contents"> 
   <div class="panel"> 
    <div class="panel-head"> 
     <strong>扫描本地硬盘音乐</strong> 
    </div> 
    <table class="table3"> 
     <tbody> 
     <form name="form1" action="" method="post" target='stafrm'>
	 <input type="hidden" name="do" value="add" />
      <tr> 
       <td width="100" align="left">音乐选项</td> 
       <td align="left">
       <select name="CD_ClassID">  
       <option value="0">所属栏目</option>
                <?php
                global $db;
		$sqlclass="select * from ".tname('class')." where CD_FatherID=0 and CD_ID<>7";
		$results=$db->query($sqlclass);
		if($results){
			while ($row3=$db->fetch_array($results)){
				echo "<option value='".$row3['CD_ID']."'>".$row3['CD_Name']."</option>";
			}
		}
                ?>
       </select>　
       <select name="CD_IsBest">
       <option value="" selected>推荐星级</option>
       <option value="0">不推荐</option>
       <option value="1">一星级</option>
       <option value="2">二星级</option>
       <option value="3">三星级</option>
       <option value="4">四星级</option>
       <option value="5">五星级</option>
       </select>　
       <select name="CD_Color">
       <option value="" selected>标题颜色</option>
       <option style="background-color:#00FF00;color: #00FF00" value="#00FF00">绿色</option>
       <option style="background-color:#0000CC;color: #0000CC" value="#0000CC">深蓝</option>
       <option style="background-color:#FFFF00;color: #FFFF00" value="#FFFF00">黄色</option>
       <option style="background-color:#FF33CC;color: #FF33CC" value="#FF33CC">粉红</option>
       <option style="background-color:#FF0000;color: #FF0000" value="#FF0000">红色</option>
       <option style="background-color:#660099;color: #660099" value="#660099">紫色</option>
       <option style="background-color:#FFFFFF;color: #FFFFFF" value="">无色</option>
       </select>
       </td> 
      </tr> 
      <tr> 
       <td align="left">所属歌手</td> 
       <td align="left"><input name="CD_Singer" type="text" id="CD_Singer" class="input input_hd length_3" value="" size="20"></td> 
      </tr> 
      <tr> 
       <td align="left">服 务 器</td> 
       <td align="left">
       <select name="CD_Server" size="1">
       <option value="0">不设置</option>
          <?php
          $sqlclass="select * from ".tname('server')."";
          $results=$db->query($sqlclass);
          if($results){
          	while ($row3=$db->fetch_array($results)){
			echo "<option value='".$row3['CD_ID']."' >".$row3['CD_Name']."</option>";			
		}
          }
          ?>
       </select>
       </td> 
      </tr> 
      <tr> 
       <td align="left">下载权限</td> 
       <td align="left">
        <select name="CD_Grade" id="CD_Grade">
        <option value="0">游客下载</option>
        <option value="1" selected>普通用户</option>
        <option value="2">VIP 用户</option>
        </select>&nbsp;下载扣除金币：<input type="text" name="CD_Points" class="input input_hd length_1" size="5" value="0"> *数字整数
       </td> 
      </tr> 
      <tr> 
       <td align="left">所属会员</td> 
       <td align="left"><input name="CD_User" type="text" id="CD_User" class="input input_hd length_3" value="" size="30"><font color="#d01f3c">&nbsp;&nbsp;必须是站内已存在的用户帐号！不能乱写!</font></td> 
      </tr> 
      <tr> 
       <td align="left">所属专辑ID</td> 
       <td align="left"><input name="CD_SpecialID" type="text" id="CD_SpecialID" class="input input_hd length_1" value="0" size="30"><font color="#d01f3c">&nbsp;&nbsp;填写专辑的ID序号</font></td> 
      </tr>
      <tr> 
       <td align="left">扫描路径</td> 
       <td align="left"><input name="CD_Path" type="text" id="CD_Path" class="input input_hd length_6" value="/upload/song/" size="30"><button type="button" onclick="pop.up('浏览硬盘目录', 'localftp.php?ac=hard&activepath=../../', '550px', '400px', '140px');" class="btn">浏览...</button></td> 
      </tr>
      <tr> 
       <td align="left">播放模板</td> 
       <td align="left"><input name="CD_Skin" type="text" id="CD_Skin" value="play.html" size="30" class="input input_hd length_2"><button type="button" onclick="pop.up('浏览模板目录', '../inc/select_templets.php?f=form1.CD_Skin', '550px', '400px', '140px');" class="btn">浏览...</button></td> 
      </tr>
      <tr> 
       <td align="left">扫描的文件后缀名</td> 
       <td align="left"><input name="CD_Suffix" type="text" id="CD_Suffix" value="mp3|wma" size="30" class="input input_hd length_3">&nbsp;只扫描这里设置的格式 | 隔开</td> 
      </tr>
       <tr> 
        <td height="35" colspan="2" align="left" bgcolor="#FAFBF7" class="td_border"><input name="b112" type="submit" class="btn btn_submit mr10 J_ajax_submit_btn" value="开始扫描" onClick="return CheckForm();" style="margin-left: 110px;"> </td> 
       </tr> 
	 </form>
     </tbody>  
    </table> 
   </div> 
  </div> 
  <div class="contents"> 
   <div class="panel"> 
    <div class="panel-head" id="mtd"> 
     <strong>进行状态：<span style="color:#ff0000;">在没看到提示完成的情况下不要刷新或关闭此页面</span></strong> 
    </div> 
    <div id="mdv" style="width:100%;height:200px;"> 
     <iframe name="stafrm" frameborder="0" id="stafrm" width="100%" height="100%"></iframe>
    </div> 
   </div> 
  </div>  
<?php } ?>

<?php
function add($cid){
	echo "<style type='text/css'><!--body,td,th {background-color: #FFFFFF;font-size: 9pt;}a {font-size: 9pt;}a:link {text-decoration: none;color:#3F628C;}a:visited {text-decoration: none;color:#3F628C;}a:hover {text-decoration: none;color:#3F628C;}a:active {text-decoration: none;color:#3F628C;}--></style>";
	if($cid==1) {
		$CD_Path=SafeRequest("CD_Path", "post");
		$CD_ClassID=SafeRequest("CD_ClassID", "post");
		$CD_IsBest=SafeRequest("CD_IsBest", "post");
		$CD_Color=SafeRequest("CD_Color", "post");
		$CD_Singer=SafeRequest("CD_Singer", "post");
		$CD_Server=SafeRequest("CD_Server", "post");
		$CD_Grade=SafeRequest("CD_Grade", "post");
		$CD_Points=SafeRequest("CD_Points", "post");
		$CD_User=SafeRequest("CD_User", "post");
		$CD_Path=SafeRequest("CD_Path", "post");
		$CD_Skin=SafeRequest("CD_Skin", "post");
		$CD_Suffix=SafeRequest("CD_Suffix", "post");
		$CD_SpecialID=SafeRequest("CD_SpecialID", "post");
		global $db;
		//判断用户是否存在
		$result=$db->query("select * from " . tname(user) . " where CD_Name='" . $CD_User . "'");
		if($row=$db->fetch_array($result)) {
			$path="../.." . $CD_Path;
			if(file_exists($path)&&is_dir($path)) {
				$d=dir($path);
				$d->rewind();
				while(false!==($v=$d->read())) {
					if($v=="."||$v=="..") {
						continue;
					}
					$file=$d->path . $v;
					if(is_dir($file)) {
						continue;
					}
					if(is_file($file)) {
						$menuarr=explode("|", $CD_Suffix);
						for($i=0; $i<count($menuarr); $i++) {
							if(fileext($v)==$menuarr[$i]) {
								$a=$a+1;
								$CD_Name=explode(".", $v);
								//判断音乐是否存在
								$result=$db->query("select * from " . tname('dj') . " where CD_Name='" . $CD_Name[0] . "'");
								if($row=$db->fetch_array($result)) {
									echo "&nbsp;<font style=font-size:10pt;>".$a.".音乐：<font color=red>" . $v . "</font>&nbsp;已存在，不入库</font><br/>";
								} else {
									//******提交参数********//
									$CD_Name=$CD_Name[0]; //标题
									$CD_Singer=$CD_Singer; //歌手
									$CD_Pic=""; //封面图片
									$CD_Url=$CD_Path . $v; //播放ID
									$CD_DownUrl=$CD_Url; //下载地址
									$CD_Siz=$CD_siz[1]; //文件大小
									$CD_Md5=''; //文件MD5
									$CD_From="BFTP"; //数据来源
									$CD_Uid=""; //备用ID
									$CD_SpecialID=$CD_SpecialID; //专辑ID
									$CD_User=$CD_User; //会员帐号
									$CD_Tag=""; //标签
									$CD_Word=""; //简介
									$CD_Lrc=""; //歌词
									$CD_Hits=0;
									$CD_DownHits=0;
									$CD_FavHits=0;
									$CD_uHits=0;
									$CD_dHits=0;
									$CD_DayHits=0;
									$CD_WeekHits=0;
									$CD_MonthHits=0;
									$CD_AddTime=date('Y-m-d H:i:s');
									$CD_LastHitTime=date('Y-m-d H:i:s');
									$CD_Deleted=0;
									$CD_IsBest=$CD_IsBest;
									$CD_Error=0;
									$CD_Passed=0;
									$CD_Points=$CD_Points;
									$CD_Grade=$CD_Grade;
									$CD_Color=$CD_Color;
									$CD_Skin=$CD_Skin;
									//******写入数据库********//
									global $db;
									$sql="Insert " . tname('dj') . " (CD_ClassID,CD_SpecialID,CD_Name,CD_Singer,CD_User,CD_Pic,CD_From,CD_Siz,CD_Uid,CD_Md5,CD_Tag,CD_Url,CD_DownUrl,CD_Word,CD_Lrc,CD_Hits,CD_DownHits,CD_FavHits,CD_uHits,CD_dHits,CD_DayHits,CD_WeekHits,CD_MonthHits,CD_AddTime,CD_LastHitTime,CD_Server,CD_Deleted,CD_IsBest,CD_Error,CD_Passed,CD_Points,CD_Grade,CD_Color,CD_Skin) values ('" . $CD_ClassID . "','" . $CD_SpecialID . "','" . $CD_Name . "','" . $CD_Singer . "','" . $CD_User . "','" . $CD_Pic . "','" . $CD_From . "','" . $CD_Siz . "','" . $CD_Uid . "','" . $CD_Md5 . "','" . $CD_Tag . "','" . $CD_Url . "','" . $CD_DownUrl . "','" . $CD_Word . "','" . $CD_Lrc . "','" . $CD_Hits . "','" . $CD_DownHits . "','" . $CD_FavHits . "','" . $CD_uHits . "','" . $CD_dHits . "','" . $CD_DayHits . "','" . $CD_WeekHits . "','" . $CD_MonthHits . "','" . $CD_AddTime . "','" . $CD_LastHitTime . "','" . $CD_Server . "','" . $CD_Deleted . "','" . $CD_IsBest . "','" . $CD_Error . "','" . $CD_Passed . "','" . $CD_Points . "','" . $CD_Grade . "','" . $CD_Color . "','" . $CD_Skin . "')";
									$db->query($sql);
									$db->query('update ' . tname('system') . " set cd_djmub=cd_djmub+1"); //全局数据统计
									echo "&nbsp;<font style=font-size:10pt;>".$a."音乐：<font color=red>" . $v . "</font>&nbsp;入库成功</font><br/>";
								}
							}
						}
					}
					ob_end_flush();
				}
				$d->close();
				die("<script language='javascript'>alert('所有音乐已扫描完毕！');</script>");
			}
		} else {
			die("<script language='javascript'>alert('出错了，站内没有 " . $CD_User . " 这个会员！');</script>");
		}
	}
}
?>