<?php
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
error_reporting(0);
include "../include/x5music.conn.php";
include "function_common.php";
admincheck(3);
$action=SafeRequest("action", "get");
?>
<!DOCTYPE html>
<html>
 <head> 
  <meta charset="gbk" /> 
  <meta name="renderer" content="webkit" /> 
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" /> 
  <title>x5Music 后台管理中心 -x5mp3.com</title> 
  <link href="css/add.css" rel="stylesheet" /> 
  <style type="text/css">
.table2{width: 100%;max-width: 100%;border-collapse: collapse;border-spacing: 0;}
.table2 td{font-size: 12px;border-top: 1px solid #ddd;padding: 8px;vertical-align: top;}
.table2 tr:hover{text-decoration: none;background-color: #e6f2fb;}
.table2 th{border-bottom: 2px solid #ddd;vertical-align: bottom;padding: 2px;text-align: left;}
.table3{width: 100%;max-width: 100%;border-collapse: collapse;border-spacing: 0;}
.table3 tr:hover{text-decoration: none;background-color: #e6f2fb;}
.table3 td{font-size: 12px;line-height: 25px;border-top: 1px solid #ddd;padding: 5px;vertical-align: top;border-right: solid 1px #ddd;}
.table3 th{border-bottom: 2px solid #ddd;vertical-align: bottom;padding: 2px;text-align: left;}
</style> 
<script language="javascript">
function CheckAll(form) {
	for (var i = 0; i < form.elements.length; i++) {
		var e = form.elements[i];
		if (e.name != 'chkall')
			e.checked = form.chkall.checked;
	}
}

function exchange_type(theForm) {
	if (theForm.CD_SystemID.value == '2') {
		alert("请在 英文别名 处填写跳转网址！");
		document.form1.CD_AliasName.focus();
		return false;
	}
}
</script> 
 </head> 
 <body> 
  <div class="contents"> 
   <div class="panel"> 
    <div style="padding: 8px;"> 
     <a href="class.php" class="btn btn_success">分类栏目管理</a> 
    </div> 
   </div> 
  </div> 
<?php
switch($action) {
	case 'del':
		Del();
		break;
	case 'edit':
		Edit();
		break;
	case 'saveseoAdd':
		SaveseoAdd();
		break;
	case 'editishide':
		EditIsHide();
		break;
	case 'editsave':
		EditSave();
		break;
	case 'saveadd':
		SaveAdd();
		break;
	case 'unite':
		Unite();
		break;
	default:
		main();
		break;
}
?> 
 </body>
</html>
<?php
function main() {
	global $db;
	$sql="select * from " . tname('class') . " order by cd_id asc";
	$result=$db->query($sql);
	$classnum=$db->num_rows($result);
?>
  <div class="contents"> 
   <div class="panel"> 
    <div class="panel-head"> 
     <strong>分类栏目管理</strong> 
    </div> 
    <form name="form" method="post" action="?action=editsave"> 
     <table class="table2"> 
      <tbody> 
       <tr> 
        <td width="45" align="left"> 序号</td> 
        <td align="left">栏目名称</td> 
        <td align="left">栏目属性</td> 
        <td align="left">英文别名</td> 
        <td align="left">栏目模板</td> 
        <td align="left">排序</td> 
        <td align="left">显示</td> 
        <td width="100" align="left">操作</td> 
       </tr> 
<?php
if($classnum==0)
	echo "<tr><td height=\"30\" colspan=\"8\" align=\"center\" bgcolor=\"#FFFFFF\" class=\"td_border\"><br><br>没有数据<br><br><br></td></tr>";
if($result) {
	while($row=$db->fetch_array($result)) {
?>
       <tr> 
        <td><input type="checkbox" name="CD_ID[]" id="CD_ID" value="<?php echo $row['CD_ID']?>" class="checkbox" /><?php echo $row['CD_ID']?></td> 
        <td><input type="text" class="classinput k100" name="CD_Name<?php echo $row['CD_ID']?>" value="<?php echo $row['CD_Name']?>" /></td> 
        <td><?php if($row['CD_SystemID']==1){?>内部栏目<?php }else{?><font color=red>跳转网址</font><?php }?></td> 
        <td><input type="text" class="classinput k100" name="CD_AliasName<?php echo $row['CD_ID']?>" value="<?php echo $row['CD_AliasName']?>"/></td> 
        <td><input type="text" name="CD_Template<?php echo $row['CD_ID']?>" value="<?php echo $row['CD_Template']?>" class="classinput"/></td>
        <td align="left"><input type="text" class="classinput k20" name="CD_TheOrder<?php echo $row['CD_ID']?>" value="<?php echo $row['CD_TheOrder']?>" onkeyup="this.value=this.value.replace(/[^\d]/g,'')" onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/[^\d]/g,''))"/></td> 
        <td align="left"><?php if($row['CD_IsHide']==1){?><a href="?action=editishide&CD_ID=<?php echo $row['CD_ID']?>&CD_IsHide=0"><img src='images/no.gif' border='0'></a><?php }else{?><a href="?action=editishide&CD_ID=<?php echo $row['CD_ID']?>&CD_IsHide=1"><img src='images/yes.gif' border='0'></a><?php }?></td> 
        <td align="left">
		<input type="button" value="预览" onclick="window.open('<?php if($row['CD_SystemID']==2){ echo $row['CD_AliasName']; }else{?>../play/index.php?0,<?php echo $row['CD_ID']?>,1<?php }?>','list')" class="btn" style="font-size: 12px;padding: 3px 8px;"> 
		<a onclick="return confirm('确定删除吗？不可恢复！');" href="?action=del&CD_ID=<?php echo $row['CD_ID']?>" class="btn" style="font-size: 12px;padding: 3px 8px;">删除</a>
        </td>
       </tr> 
<?php				
}
}
?>
       <tr> 
        <td height="35" colspan="8" align="left" bgcolor="#FAFBF7" class="td_border"><label for="chkall"><input type="checkbox" id="chkall" onclick="CheckAll(this.form)" class="checkbox" />&nbsp;全选</label>&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; <input type="submit" name="Submit" value="修改所选栏目" class="btn btn_submit mr10 J_ajax_submit_btn" /></td> 
       </tr> 
      </tbody> 
     </table> 
    </form> 
   </div> 
  </div> 
<!--添加栏目-->
  <div class="contents"> 
   <div class="panel"> 
    <div class="panel-head"> 
     <strong>添加分类栏目 注意：选跳转网址的请在英文别名填上http地址</strong> 
    </div> 
    <form id="form1" name="form1" method="post" action="?action=saveadd"> 
     <table width="100%" border="0" cellpadding="0" cellspacing="0"> 
      <tbody> 
       <tr> 
        <td height="30" align="center" bgcolor="#FFFFFF" class="td_border">栏目名称</td> 
        <td align="center" bgcolor="#FFFFFF" class="td_border">英文别名</td> 
        <td align="center" bgcolor="#FFFFFF" class="td_border">栏目属性</td> 
        <td align="center" bgcolor="#FFFFFF" class="td_border">栏目模板</td> 
        <td align="center" bgcolor="#FFFFFF" class="td_border">排序</td> 
        <td align="center" bgcolor="#FFFFFF" class="td_border">状态</td> 
        <td align="center" bgcolor="#FFFFFF" class="td_border">&nbsp;</td> 
       </tr> 
       <tr> 
        <td height="30" align="center"><input type="text" class="classinput k100" name="CD_Name" id="CD_Name" value="" /></td> 
        <td height="30" align="center"><input type="text" class="classinput k100" name="CD_AliasName" id="CD_AliasName" value="" size="50" /></td> 
        <td height="30" align="center"><select name="CD_SystemID" id="CD_SystemID" onchange="exchange_type(form1)"><option value="1">内部栏目</option><option value="2">跳转网址</option></select></td> 
        <td height="30" align="center"><input name="CD_Template" type="text" class="classinput k100" id="CD_Template" value="play_list.html" /></td> 
        <td height="30" align="center"><input type="text" name="CD_TheOrder" class="classinput k20" id="CD_TheOrder" value="0" size="5" onkeyup="this.value=this.value.replace(/[^\d]/g,'')" onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/[^\d]/g,''))" /></td> 
        <td height="30" align="center"><select name="CD_IsHide"><option value="0">显示</option><option value="1">隐藏</option></select></td> 
        <td align="center" class="td_border"><input type="submit" name="button" id="addvc" value="提 交" class="btn btn_submit mr10 J_ajax_submit_btn" /></td> 
       </tr> 
       <tr> 
        <td height="3" colspan="7">&nbsp;</td> 
       </tr> 
      </tbody> 
     </table> 
    </form> 
   </div> 
  </div> 
<!--转移栏目数据-->
  <div class="contents"> 
   <div class="panel"> 
    <div class="panel-head"> 
     <strong>转移栏目数据 注意：操作前请先备份数据库，程序将前面的栏目数据合并到后面栏目后将不能再恢复原始栏目，请一定要慎重操作!</strong> 
    </div> 
    <form action="?action=unite" name="unite" id="unite" method="post"> 
     <table width="100%"> 
      <tbody>
       <tr> 
        <td height="50" bgcolor="#FFFFFF" class="td_border">&nbsp;将栏目： 
		<select size="1" name="Type_1">
		<option value="">选择栏目</option> 
<?php
$sqlclass="select * from " . tname('class') . " where CD_SystemID=1";
$results=$db->query($sqlclass);
if($results) {
	while($row3=$db->fetch_array($results)) {
?>
		<option value="<?php echo $row3['CD_ID']?>" ><?php echo $row3['CD_Name']?></option>
<?php
}
}
?>						
		</select> 的数据转移到 
		<select size="1" name="Type_2">
		<option value="">选择栏目</option>
<?php
$sqlclass="select * from " . tname('class') . " where CD_SystemID=1";
$results=$db->query($sqlclass);
if($results) {
	while($row3=$db->fetch_array($results)) {
?>
		<option value="<?php echo $row3['CD_ID']?>" ><?php echo $row3['CD_Name']?></option>
<?php
}
}
?>
		</select> 
		<input type="submit" value="确定转移" name="submit" class="btn btn_submit mr10 J_ajax_submit_btn" /></td> 
       </tr> 
       <tr> 
       </tr> 
      </tbody>
     </table> 
    </form> 
   </div> 
  </div>
<?php
}
//执行保存
function SaveseoAdd() {
	global $db;
	$CD_ID=SafeRequest("CD_ID", "post");
	$CD_Name=SafeRequest("CD_Name", "post");
	$CD_AliasName=SafeRequest("CD_AliasName", "post");
	$CD_Template=SafeRequest("CD_Template", "post");
	$sql="update " . tname('class') . " set CD_Name='" . $CD_Name . "',CD_AliasName='" . $CD_AliasName . "',CD_Template='" . $CD_Template . "' where CD_ID=" . $CD_ID . "";
	$db->query($sql);
	showmessage("恭喜您，编辑栏目成功！", $_SERVER['HTTP_REFERER'], 0);
}

function Unite() {
	global $db;
	$classa=SafeRequest("Type_1", "post");
	$classb=SafeRequest("Type_2", "post");
	if(!IsNul($classa)|!IsNul($classb)) {
		showmessage("出错了，请选择要转移的栏目！", "class.php", 0);
	}
	$sql="select * from " . tname('dj') . " where CD_ClassID=" . $classa . "";
	$result=$db->query($sql);
	if($result) {
		while($row=$db->fetch_array($result)) {
			$sqlstr="update " . tname('dj') . " set CD_ClassID=" . $classb . " where CD_ID=" . $row['CD_ID'] . "";
			$db->query($sqlstr);
		}
		showmessage("恭喜您，数据转移成功！", "class.php", 0);
	}
}

function SaveAdd() {
	global $db;
	$CD_Name=SafeRequest("CD_Name", "post");
	$CD_AliasName=SafeRequest("CD_AliasName", "post");
	$CD_TheOrder=SafeRequest("CD_TheOrder", "post");
	$CD_Template=SafeRequest("CD_Template", "post");
	$CD_IsHide=SafeRequest("CD_IsHide", "post");
	$CD_SystemID=SafeRequest("CD_SystemID", "post");
	if(!IsNul($CD_Name)) {
		showmessage("出错了，栏目名称不能为空！", "class.php", 1);
	}
	if(!IsNul($CD_AliasName)) {
		showmessage("出错了，英文别名不能为空！", "class.php", 1);
	}
	if(!IsNul($CD_Template)) {
		showmessage("出错了，栏目模板不能为空！", "class.php", 1);
	}
	if(!IsNum($CD_TheOrder)) {
		showmessage("出错了，排序不能为空！", "class.php", 1);
	}
	$sql="Insert " . tname('class') . " (CD_Name,CD_AliasName,CD_FatherID,CD_TheOrder,CD_Template,CD_IsHide,CD_SystemID) values ('" . $CD_Name . "','" . $CD_AliasName . "',0," . $CD_TheOrder . ",'" . $CD_Template . "',0," . $CD_SystemID . ")";
	$db->query($sql);
	showmessage("恭喜您，添加栏目成功！", "class.php", 0);
}

function EditSave() {
	global $db;
	$CD_ID=RequestBox("CD_ID");
	if($CD_ID=="0") {
		showmessage("出错了，请选择要编辑的栏目！", "class.php", 1);
	} else {
		$ID=explode(",", $CD_ID);

		for($i=0; $i<count($ID); $i++) {
			$CD_Name=SafeRequest("CD_Name" . $ID[$i] . "", "post");
			$CD_AliasName=SafeRequest("CD_AliasName" . $ID[$i] . "", "post");
			$CD_Template=SafeRequest("CD_Template" . $ID[$i] . "", "post");
			$CD_TheOrder=SafeRequest("CD_TheOrder" . $ID[$i] . "", "post");
			if(!IsNul($CD_Name)) {
				showmessage("出错了，栏目名称不能为空！", "class.php", 1);
			}
			if(!IsNul($CD_AliasName)) {
				showmessage("出错了，英文别名不能为空！", "class.php", 1);
			}
			if(!IsNul($CD_Template)) {
				showmessage("出错了，栏目模板不能为空！", "class.php", 1);
			}
			if(!IsNum($CD_TheOrder)) {
				showmessage("出错了，排序不能为空！", "class.php", 1);
			}
			$sql="update " . tname('class') . " set CD_Name='" . $CD_Name . "',CD_AliasName='" . $CD_AliasName . "',CD_Template='" . $CD_Template . "',CD_TheOrder=" . $CD_TheOrder . " where CD_ID=" . $ID[$i] . "";
			$db->query($sql);
		}
		showmessage("恭喜您，编辑栏目成功！", "class.php", 0);
	}

}

function EditIsHide() {
	global $db;
	$CD_ID=SafeRequest("CD_ID", "get");
	$CD_IsHide=SafeRequest("CD_IsHide", "get");
	$sql="update " . tname('class') . " set CD_IsHide=" . $CD_IsHide . " where CD_ID=" . $CD_ID . "";
	if($db->query($sql)) {
		echo "<script>window.location='class.php'</script>";
	}
}

function del() {
	global $db;
	$CD_ID=SafeRequest("CD_ID", "get");
	$array=array('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17');
	if(in_array($CD_ID, $array)) {
		showmessage("删除失败,系统自带栏目您只可以编辑不可删除。", "class.php", 0);
	}
	$sql="delete from " . tname('class') . " where CD_ID='" . $CD_ID . "'";
	if($db->query($sql)) {
		die("<script>window.location='class.php'</script>");
	}
}
?>