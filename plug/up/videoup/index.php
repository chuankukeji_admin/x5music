<?php
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
error_reporting(0);
if(!eregi($_SERVER['SERVER_NAME'], $_SERVER['HTTP_REFERER'])) {echo'<meta charset="gbk" />';exit('来路错误！');}
include "../../../include/x5music.conn.php";
include "../../../include/x5music.inc.php";
if(cd_userupload=="0"){echo'<meta charset="gbk" />';exit('您没有权限访问此页面！');}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<title>upload</title>
<link href="<?php echo cd_webpath?>plug/up/upload/default.css" rel="stylesheet" type="text/css" />
<link href="<?php echo cd_webpath?>plug/up/upload/uploadify.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo cd_webpath?>plug/up/upload/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="<?php echo cd_webpath?>plug/up/upload/swfobject.js"></script>
<script type="text/javascript" src="<?php echo cd_webpath?>plug/up/upload/jquery.uploadify.v2.1.0.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#uploadify").uploadify({
		'uploader': '<?php echo cd_webpath?>plug/up/upload/uploadify.swf',
		'script': 'uploads.php',
		'cancelImg': '<?php echo cd_webpath?>plug/up/upload/cancel.png',
		'folder': 'upload',
		'method': 'GET',
		'scriptData' : {'uid':'<?php echo $x5music_com_userid; ?>','uname':'<?php echo base64_encode($x5music_com_username); ?>'},
		'queueID': 'fileQueue',
		'auto': true,
		'multi': false,
		'simUploadLimit': 1,
		'queueSizeLimit': 1,
		'removeCompleted': true,
		'buttonImg': '<?php echo cd_webpath?>plug/up/upload/up.png',
		'width': '75',
		'height': '28',
		'wmode': 'transparent',
		'displayData': 'percentage',
		'fileTypeDesc': 'Image Files',
		'fileDesc': '请选择<?php echo cd_upvideotype?>文件',
		'fileExt': '<?php echo cd_upvideotype?>',
		'sizeLimit': <?php echo (cd_upvideosize*1024*1024); ?>,
		'onError' : function (a, b, c, d) {
		if (d.status == 404){
		alert('出错了，文件404，请查看up及下属目录是否有权限！');document.location.reload();
		}else if (d.type === "HTTP"){
		alert('error '+d.type+": "+d.status);
		}else if (d.type ==="File Size"){
		alert('抱歉，只允许上传<?php echo cd_upvideosize?>以内的文件,请重新选择');document.location.reload();
		}else{
		alert('error '+d.type+": "+d.text);
		}   
		},
		'onComplete': function(event, queueID, fileObj, response, data) {
		var value = response ;
		window.location.href="ok.php"+ value;
		}
	});
});
function uploadFile() {
	jQuery('#uploadify').uploadifyUpload();
}
</script>
<div class="uploadbox" style="width:595px; margin:20px auto;padding: 3px !important;border: 0px;!important;height:30px !important;position:relative;overflow:hide;background-image: url(<?php echo cd_webpath?>plug/up/upload/bg.jpg);">
	<div class="blue" id="swfupload" style="float:left;width:434px;height:26px;line-height:26px;">
		<ul class="attachment-list" id="fileQueue"></ul>
	</div>
	<div class="addnew" id="addnew" style="float:left;">
		<input type="file" name="uploadify" id="uploadify"/>
	</div>
	<div style="float:left;">
		<input type="button" id="btupload" value="开始上传" onclick="uploadFile()" />
		<div id="btnCancel"></div>
	</div>
</div>