<?php if(!$userlogined==true){showmessage("出错了，请登陆后再操作。","login.php",0);}?>
<div class="user">
<div class="user_center">
<?php include_once('source/space_menu.php');?>
<div class="user_main">
<div class="uMain_content">
<div class="main_nav">
<ul>
<li class="skin"><a <?php if($view==""){ echo' class="on"'; } ?> href="<?php echo rewrite_url('space.php?do=web');?>">主页风格</a></li>
<li class="modify"><a <?php if($view=="mingm"){ echo' class="on"'; } ?>href="<?php echo rewrite_url('space.php?do=web&view=mingm');?>">主页命名</a></li>
<li class="certify"><a <?php if($view=="bg"){ echo' class="on"'; } ?>href="<?php echo rewrite_url('space.php?do=web&view=bg');?>">主页背景</a></li>
</ul>
</div>


<?php if($view=="mingm"){?>
<div class="main_nav2">
<ul>
<font color="red">输入自己想要的主页名（不要超过16个汉字）。</font>
</ul>
</div>
<div id="notification" class="minHeight500">
<div class="Ignore02">
</div>
<div class="notification">
<ul>
<form name="form2" method="post" action="do.php?ac=web&op=mingm">
<?php $rowlast=$db->getrow("select cd_title from ".tname('web')." where cd_uid=$x5music_com_userid"); ?>
<div class="name">
主页名称：<input type="txt" name="cd_title" value="<?php echo $rowlast['cd_title']; ?>" size="50" maxlength="16" class="input_normal" />
</div><br />


<input type="submit" name="submitwebmingm" value="确认修改" class="button square lv" />

</form>
</div> 

</ul>
</div>
<?php }elseif($view=="bg"){?>

<div class="main_nav2">
<ul>
<font color="red">上传头部图片和背景图片能DIY出不一样感觉的空间。</font>
</ul>
</div>
<div id="notification" class="minHeight500">
<div class="Ignore02">
</div>
<div class="notification">
<ul>

<script language="javascript" type="text/javascript">
   function CheckImgCss(o, img) {
        var filePath = o.value;
        var fileExt = filePath.substring(filePath.lastIndexOf('.') + 1, filePath.length);
        fileExt = fileExt.toLowerCase();
        if (fileExt != "jpg" && fileExt != "gif") {
        //if (!/\.((jpg)|(gif))$/ig.test(o.value)) {
             alert('只能上传jpg,gif格式图片');
        }else {
             document.getElementById(img).src = o.value;
        }
   }

   function previewTitle(newuserid, tempID, imgTitle) {
        window.open("../singer/index.php?id=" + newuserid + "&picbanner=" + imgTitle.value, "_blank");
        //document.getElementById("hrefTitle").target = "_blank";
        //document.getElementById("hrefTitle").href = "../singer/index.php?id=" + newuserid + "&picbanner=" + encodeURIComponent(imgTitle.value);
   }

   function previewBG(newuserid, tempID, imgBG) {
        window.open("../singer/index.php?id=" + newuserid + "&picbg=" + imgBG.value, "_blank");
        //document.getElementById("hrefBG").target = "_blank";
        //document.getElementById("hrefBG").href = "../singer/index.php?id=" + newuserid + "&picbg=" + encodeURIComponent(imgBG.value);
   }
</script>
<?php $rowlast=$db->getrow("select cd_banner,cd_bg from ".tname('web')." where cd_uid=$x5music_com_userid"); ?>

<div style="padding: 5px; color: #808080; font-size: 18px;">
<b>头部/顶部图片</b>
</div>建议头部图片限定200KB内，jpg或gif,png格式。建议像素大小为1900px*350px<br /><br />


<form name="form2" method="post" action="do.php?ac=web&op=upload" enctype="multipart/form-data" class="c_form">

<form name="form2" method="post" action="do.php?ac=web&op=top" enctype="multipart/form-data">

<div class="name">
头部顶部图片：<input type="file" id="cd_banner" name="cd_banner" onchange="CheckImgCss(this, 'imgTitle');" size="50" class="input_normal" />
</div>
<br /><br />
<div class="name">效果预览：
<img src="../<?php echo $rowlast['cd_banner']; ?>" id="imgTitle" width="480" height="50" />
</div>
<br /><br />
<input type="submit" name="submitwebbanner" value="确认修改" class="button square lv" />

<div class="main_nav2"></div>

<div style="padding: 5px; color: #808080; font-size: 18px;">
<b>主页背景图片</b>
</div>建议背景图片限定20KB内，jpg或gif,png格式。建议像素大小为1px*1px<br /><br />

<div class="name">主页背景图片：
<input type="file" id="cd_bg" name="cd_bg" onchange="CheckImgCss(this, 'imgBG');" size="50" class="input_normal" />
</div>
<br /><br />
<div class="name">效果预览：
<img src="../<?php echo $rowlast['cd_bg']; ?>" id="imgBG" width="140" height="50"/>
</div>
<br /><br />
<input type="submit" name="submitwebbg" value="确认修改" class="button square lv" />
</form>
</div>
</ul>
</div>
<?php }else{?>
<div class="main_nav2">
<ul>
<font color="red">选择一款合适的配色方案，点击图片可以预览效果。</font>
</ul>
</div>
<div id="notification" class="minHeight500">
<form id="list">
<div class="image_me">
<ul>
<?php
$cd_skin = GetAlias("x5music_web","cd_skin","cd_uid",$x5music_com_userid);
$path = "../singer/theme/";
$i=0;
if(file_exists($path) && is_dir($path)){
$d = dir($path); 
while(false !== ($v = $d->read())){
if($v == "." || $v == ".."){
continue; 
}
$file = $d->path."\\".$v; 


echo "      <li>\n";
echo "      <div class=\"pic\">\n";
echo "       <a href=\"../singer/index.php?id=$x5music_com_userid&theme=$v\" target=\"_blank\"><img src=\"../singer/theme/$v/preview.jpg\"></a>\n";
echo "      </div>\n";
echo "      <div class=\"option\">\n";
if($cd_skin==$v){
echo "<font size=2 color=red>正在使用</font>";
}else{
echo "<FONT size=2><a href='do.php?ac=web&op=theme&dir=$v'>启用</a></FONT>";
}
echo "      </div></li>\n";
if($i%10==9){;}
$i++;

}
$d->rewind();
}
?>
</ul>
</div>
<?php } ?>
</div>
</div>
</div> 
</div>
</div>
</div>
</div>