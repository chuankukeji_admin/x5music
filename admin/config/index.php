<?php
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
error_reporting(0);
include "../../include/x5music.conn.php";
include "../function_common.php";
// 获取文件夹大小
function getDirSize($dir) {
	$handle=opendir($dir);
	while(false!==($FolderOrFile=readdir($handle))) {
		if($FolderOrFile!="." && $FolderOrFile!="..") {
			if(is_dir("$dir/$FolderOrFile")) {
				$sizeResult+=getDirSize("$dir/$FolderOrFile");
			} else {
				$sizeResult+=filesize("$dir/$FolderOrFile");
			}
		}
	}
	closedir($handle);
	return $sizeResult;
}
// 单位自动转换函数
function getRealSize($size) {
	$kb=1024; // Kilobyte
	$mb=1024*$kb; // Megabyte
	$gb=1024*$mb; // Gigabyte
	$tb=1024*$gb; // Terabyte

	if($size<$kb) {
		return $size . " B";
	} else if($size<$mb) {
		return round($size/$kb, 2) . " KB";
	} else if($size<$gb) {
		return round($size/$mb, 2) . " MB";
	} else if($size<$tb) {
		return round($size/$gb, 2) . " GB";
	} else {
		return round($size/$tb, 2) . " TB";
	}
}
//服务器信息
function getOS() {
	$os='';
	$Agent=$_SERVER['HTTP_USER_AGENT'];
	if(eregi('win', $Agent) && strpos($Agent, '95')) {
		$os='Windows 95';
	} elseif(eregi('win 9x', $Agent) && strpos($Agent, '4.90')) {
		$os='Windows ME';
	} elseif(eregi('win', $Agent) && ereg('98', $Agent)) {
		$os='Windows 98';
	} elseif(eregi('win', $Agent) && eregi('nt 5.0', $Agent)) {
		$os='Windows 2000';
	} elseif(eregi('win', $Agent) && eregi('nt 6.0', $Agent)) {
		$os='Windows Vista';
	} elseif(eregi('win', $Agent) && eregi('nt 6.1', $Agent)) {
		$os='Windows 7';
	} elseif(eregi('win', $Agent) && eregi('nt 5.1', $Agent)) {
		$os='Windows XP';
	} elseif(eregi('win', $Agent) && eregi('nt', $Agent)) {
		$os='Windows NT';
	} elseif(eregi('win', $Agent) && ereg('32', $Agent)) {
		$os='Windows 32';
	} elseif(eregi('linux', $Agent)) {
		$os='Linux';
	} elseif(eregi('unix', $Agent)) {
		$os='Unix';
	} else if(eregi('sun', $Agent) && eregi('os', $Agent)) {
		$os='SunOS';
	} elseif(eregi('ibm', $Agent) && eregi('os', $Agent)) {
		$os='IBM OS/2';
	} elseif(eregi('Mac', $Agent) && eregi('PC', $Agent)) {
		$os='Macintosh';
	} elseif(eregi('PowerPC', $Agent)) {
		$os='PowerPC';
	} elseif(eregi('AIX', $Agent)) {
		$os='AIX';
	} elseif(eregi('HPUX', $Agent)) {
		$os='HPUX';
	} elseif(eregi('NetBSD', $Agent)) {
		$os='NetBSD';
	} elseif(eregi('BSD', $Agent)) {
		$os='BSD';
	} elseif(ereg('OSF1', $Agent)) {
		$os='OSF1';
	} elseif(ereg('IRIX', $Agent)) {
		$os='IRIX';
	} elseif(eregi('FreeBSD', $Agent)) {
		$os='FreeBSD';
	} elseif($os=='') {
		$os='Unknown';
	}
	return $os;
}
function _get_client_ip() {
	$ip=$_SERVER['REMOTE_ADDR'];
	if(isset($_SERVER['HTTP_CLIENT_IP']) && preg_match('/^([0-9]{1,3}\.){3}[0-9]{1,3}$/', $_SERVER['HTTP_CLIENT_IP'])) {
		$ip=$_SERVER['HTTP_CLIENT_IP'];
	} elseif(isset($_SERVER['HTTP_X_FORWARDED_FOR']) AND preg_match_all('#\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}#s', $_SERVER['HTTP_X_FORWARDED_FOR'], $matches)) {
		foreach($matches[0] AS $xip) {
			if(!preg_match('#^(10|172\.16|192\.168)\.#', $xip)) {
				$ip=$xip;
				break;
			}
		}
	}
	return $ip;
}
mysql_connect("localhost", "mysql_user", "mysql_pass");
$mysqlinfo=mysql_get_server_info();
if(function_exists("gd_info")) {
	$gd=gd_info();
	$gdinfo=$gd['GD Version'];
} else {
	$gdinfo="未知";
}
$errors=ini_get("display_errors") ? "<font color=red>未关闭</font> => 建议关闭报错,避免程序发生错误. <a href=\"http://www.x5mp3.com/help/10.html\" target=\"_blank\">关闭教程</a>" : "已关闭";
$max_upload=ini_get("file_uploads") ? ini_get("upload_max_filesize") : "Disabled";
$freetype=$gd["FreeType Support"] ? "支持" : "<font color=red>不支持</font> 可能导致验证码无法显示";
$post_max_size=ini_get("post_max_size") . "";
date_default_timezone_set("Etc/GMT-8");
$systemtime=date("Y-m-d H:i:s", time());
$cururl = $_SERVER["REQUEST_URI"];
if(eregi('/admin/',$cururl)){
$cadmin= '  <div class="content"> 
   <div class="panel"> 
    <div class="panel-head" style="border-bottom: solid 1px #DF7F7F;background-color: #DF7F7F;color: #FCF8F8;">
     <strong>重要提醒：</strong>
    </div> 
	<div style="padding: 10px;">※系统检测到您的后台管理目录为默认目录，强烈建议重命名管理目录，避免安全隐患!在重命名之前，请先到全局防注入设置中填写后台目录名称白名单。</div>
   </div> 
  </div> ';
}
if (file_exists(_x5music_root_.'install/data.txt')) {
$cinstall= '  <div class="content"> 
   <div class="panel"> 
    <div class="panel-head" style="border-bottom: solid 1px #DF7F7F;background-color: #DF7F7F;color: #FCF8F8;">
     <strong>重要提醒：</strong>
    </div> 
	<div style="padding: 10px;">※系统检测到您的install安装目录没有删除，建议您立即手动删除【install】目录。避免安全隐患!</div>
   </div> 
  </div> ';
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head> 
  <meta http-equiv="Content-Type" content="text/html; charset=gb2312" /> 
  <meta name="renderer" content="webkit" /> 
  <title>x5Music 管理后台</title> 
  <link rel="stylesheet" href="../css/add.css" type="text/css" media="screen" /> 
 </head> 
 <body> 
<?php echo $cadmin.$cinstall?>
  <div class="content"> 
   <div class="panel"> 
    <div class="panel-head">
     <strong>服务器信息</strong>
    </div> 
    <table class="table"> 
     <tbody>
      <tr>
       <td align="left">操作系统：<?php echo getOS();?></td>
       <td align="left">服务器标识字串：<?php echo $_SERVER["SERVER_SOFTWARE"]; ?></td>
      </tr> 
      <tr>
       <td align="left">域名/IP地址：<?php echo $_SERVER['SERVER_NAME'];?>(<?php echo _get_client_ip();?>)</td>
       <td align="left">服务器主机名：<?php echo $_SERVER['SERVER_NAME']; ?></td>
      </tr>
      <tr>
       <td align="left">PHP版本：<?php echo phpversion();?></td>
       <td align="left">MySQL版本：<?php echo $mysqlinfo;?></td>
      </tr>
      <tr>
       <td align="left">脚本超时时间：<?php echo ini_get("max_execution_time");?> 秒 (max_execution_time)</td>
       <td align="left">系统报错：<?php echo $errors;?></td>
      </tr>
      <tr>
       <td align="left">上传文件最大限制：<?php echo $max_upload?> (upload_max_filesize)</td>
       <td align="left">FreeType：<?php echo $freetype?></td>
      </tr>
      <tr>
       <td align="left">POST提交最大限制：<?php echo $post_max_size?> (post_max_size)</td>
       <td align="left">服务器时间：<?php echo $systemtime?></td>
      </tr>
     </tbody>
    </table> 
   </div> 
<br />
   <div class="panel"> 
    <div class="panel-head">
     <strong>系统信息</strong>
    </div> 
    <table class="table"> 
     <tbody>
      <tr>
       <td align="left">系统类型：免费版</td>
       <td align="left">开发团队：x5Music</td>
      </tr> 
      <tr>
       <td align="left">本地版本：<?php echo $CD_Version;?>&nbsp;&nbsp;|&nbsp;&nbsp;官网版本：</td>
       <td align="left">程序官网：<a href="http://www.x5mp3.com/" target="_blank">www.x5mp3.com</a>　<a href="http://bbs.x5mp3.com/">BBS交流论坛</a></td>
      </tr>
      <tr>
       <td align="left">更新时间：<?php echo $CD_Vtime?></td>
       <td align="left">程序技术/BUG反馈：<a href="http://wpa.qq.com/msgrd?v=3&uin=196859961&site=qq&menu=yes" target="_blank">QQ：196859961 (点击交谈)</a></td>
      </tr>
      <tr>
       <td align="left" colspan="2">程序声明：<font color=red>本软件为免费版本，未经允许不得向任何第三方提供出售本软件系统，您可以随意二次开发，但请保留版权信息！</font></td>
      </tr>
      <tr>
       <td align="left" colspan="2">支持作者：开源软件的每一行代码和文档都凝聚着我的心血和汗水！如果您认可我的劳动成果，如果我的软件曾经给您或您的公司带来了收益，那么请考虑以捐赠的方式支持一下。<br> 您的捐赠将有助于我适当减少迫于生计而参与的商业项目的开发，从而投入更多时间完善x5Music免费版及开发新的项目。&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font><br>捐赠时如需在后台挂赞助商链接，请拍淘宝时，备注站名和网址。<a style="float: right;" href="https://item.taobao.com/item.htm?id=525741743975" target="_blank"><font color=red>点我打赏</font></a></td>
      </tr>
     </tbody>
    </table> 
   </div>  
<br />
   <div class="panel"> 
    <div class="panel-head">
     <strong>鸣谢以下赞助商</strong>
    </div> 
    <div style="padding: 8px;font-size: 12px;"><?php Admin_Footer(1);?></div>
   </div> 
  </div>  
 </body>
</html>