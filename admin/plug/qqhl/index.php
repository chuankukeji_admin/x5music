<?php
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
error_reporting(0);
include '../../../include/x5music.conn.php';
include "../../function_common.php";
admincheck(6);
include "../../../plug/qqhl/conf.inc.php";
$action=SafeRequest("action", "get");
$ac=SafeRequest("ac", "get");
$version="v1.0";
if($action=="config") {
	$qqapp_open=SafeRequest("qqapp_open", "post");
	$qqapp_mail=SafeRequest("qqapp_mail", "post");
	$qqapp_id=SafeRequest("qqapp_id", "post");
	$qqapp_secret=SafeRequest("qqapp_secret", "post");
	$qqapp_pcredirect=SafeRequest("qqapp_pcredirect", "post");
	$qqapp_mobileredirect=SafeRequest("qqapp_mobileredirect", "post");
	$strs="<?php" . "\n";
	$strs=$strs . "define(\"qqapp_open\",\"" . $qqapp_open . "\");\n";
	$strs=$strs . "define(\"qqapp_mail\",\"" . $qqapp_mail . "\");\n";
	$strs=$strs . "define(\"qqapp_id\",\"" . $qqapp_id . "\");\n";
	$strs=$strs . "define(\"qqapp_secret\",\"" . $qqapp_secret . "\");\n";
	$strs=$strs . "define(\"qqapp_pcredirect\",\"" . $qqapp_pcredirect . "\");\n";
	$strs=$strs . "define(\"qqapp_mobileredirect\",\"" . $qqapp_mobileredirect . "\");\n";
	$strs=$strs . "?>";
	if(!$fp=fopen('../../../plug/qqhl/conf.inc.php', 'w')) {
		showmessage("出错了，文件 ../../../plug/qqhl/conf.inc.php 没有写入权限！", $_SERVER['HTTP_REFERER'], 0);
	}
	$ifile=new iFile('../../../plug/qqhl/conf.inc.php', 'w');
	$ifile->WriteFile($strs, 3);
	showmessage("恭喜您，保存设置成功！", $_SERVER['HTTP_REFERER'], 0);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head> 
  <meta http-equiv="Content-Type" content="text/html; charset=gb2312" /> 
  <meta name="renderer" content="webkit" /> 
  <title>x5Music 管理后台</title> 
  <link rel="stylesheet" href="../../css/add.css" type="text/css" media="screen" /> 
<style type="text/css">
.table2{width: 100%;max-width: 100%;border-collapse: collapse;border-spacing: 0;}
.table2 td{font-size: 12px;border-top: 1px solid #ddd;padding: 8px;vertical-align: top;}
.table2 tr:hover{text-decoration: none;background-color: #e6f2fb;}
.table2 th{border-bottom: 2px solid #ddd;vertical-align: bottom;padding: 2px;text-align: left;}
.table3{width: 100%;max-width: 100%;border-collapse: collapse;border-spacing: 0;}
.table3 tr:hover{text-decoration: none;background-color: #e6f2fb;}
.table3 td{font-size: 12px;line-height:25px;border-top: 1px solid #ddd;padding: 5px;vertical-align: top;border-right: solid 1px #ddd;}
.table3 th{border-bottom: 2px solid #ddd;vertical-align: bottom;padding: 2px;text-align: left;}
</style>
 </head> 
 <body> 
  <div class="contents"> 
   <div class="panel"> 
    <div class="panel-head">
     <strong>QQ互联登录插件</strong>
    </div> 
    <form action="?action=config" method="post" name="form2" >
    <table class="table3" id="dellist"> 
     <tbody>
	 <tr>
       <td width="110" align="left">版本信息</td>
       <td align="left"><?php echo $version;?>　<a href="http://www.x5mp3.com/free/5.html">查看更新</a></td>
      </tr>
	 <tr>
       <td width="120" align="left">是否启用</td>
       <td align="left">
<label class="mr20"><input name="qqapp_open" type="radio" value="yes" <?php if(qqapp_open=="yes"){echo "checked";} ?>>&nbsp;启用&nbsp;</label>
<label class="mr20"><input type="radio" name="qqapp_open" value="no" <?php if(qqapp_open=="no"){echo "checked";} ?>>&nbsp;关闭&nbsp;</label>
	   </td>
      </tr>
	 <tr>
       <td width="120" align="left">邮箱后缀名</td>
       <td align="left"><input name="qqapp_mail" id="qqapp_mail" value="<?php echo qqapp_mail?>" type="text" class="input input_hd length_2"></td>
      </tr>
      <tr>
       <td width="60" align="left">APP ID</td>
       <td align="left"><input name="qqapp_id" id="qqapp_id" value="<?php echo qqapp_id?>" type="text" class="input input_hd length_3"></td>
      </tr>
      <tr>
       <td width="60" align="left">APP KEY</td>
       <td align="left"><input name="qqapp_secret" id="qqapp_secret" value="<?php echo qqapp_secret?>" type="text" class="input input_hd length_6"></td>
      </tr>
      <tr>
       <td width="60" align="left">PC 电脑端回调地址</td>
       <td align="left"><input name="qqapp_pcredirect" id="qqapp_pcredirect" value="<?php echo qqapp_pcredirect?>" type="text" class="input input_hd length_6">*请将sqdemo.x5mp3.com改成你的域名,并且与QQ互联一致</td>
      </tr>
      <tr>
       <td width="60" align="left">mobile 手机回调地址</td>
       <td align="left"><input name="qqapp_mobileredirect" id="qqapp_mobileredirect" value="<?php echo qqapp_mobileredirect?>" type="text" class="input input_hd length_6">　*请将sqdemo.x5mp3.com改成你的域名</td>
      </tr>
      <tr>
       <td width="60" align="left">友情提醒</td>
       <td align="left">如需要修改QQ登录图标，替换<?php echo cd_webpath?>plug/qqhl/qq.png</td>
      </tr>
      <tr>
       <td height="35" colspan="2" align="left" bgcolor="#FAFBF7" class="td_border"><button type="submit" id="adddance" class="btn btn_submit J_ajax_submit_btn" style="margin-left: 80px;">保存设置</button></td>
      </tr>
     </tbody>
    </table> 
   </div> 
  </div>  
 </body>
</html>