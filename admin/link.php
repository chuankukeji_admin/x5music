<?php
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
error_reporting(0);
include "../include/x5music.conn.php";
include "admin_check.php";
set_time_limit(0);
admincheck(6);
$action=SafeRequest("action","get");
$CD_ClassID=SafeRequest("CD_ClassID","get");
?>
<!doctype html>
<html>
<head>
<meta charset="gbk">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>x5music</title>
<link href="css/admin_style.css" rel="stylesheet" />
<link href="css/webox.css" rel="stylesheet" />
<script type="text/javascript" src="js/ajax.js"></script>
<script language="javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/webox.js"></script>
<script language="javascript"> 
function del(_id){ 
if (confirm("确定删除吗？")){
window.location='?ac=del&id='+_id;
return true;
}else{
return false;
}		
}

</script>
</head>
<body>
<?php
switch($action){
case 'add':
Add();
break;
case 'saveadd':
SaveAdd();
break;
case 'edit':
Edit();
break;
case 'saveedit':
SaveEdit();
break;
case 'del':
Del();
break;
case 'editisindex':
editisindex();
break;
case 'alleditsave':
alleditsave();
break;
default:
main("select * from ".tname('link')." order by cd_theorder asc",30);
break;
}
Admin_Footer(0);
?>
<div class="wrap">
<?php
Function EditBoard($Arr,$url,$name){
$cd_name = $Arr[0];
$cd_url = $Arr[1];
$cd_pic = $Arr[2];
$cd_classid = $Arr[3];
$cd_isindex = $Arr[4];
$cd_input = $Arr[5];
if(!IsNul($cd_classid)){$cd_classid=1;}
if(!IsNul($cd_isindex)){$cd_isindex=0;}
?>
<div class="nav">
<ul class="cc">
<li class="current"><a href="link.php">链接管理</a></li>
<li><a href="?action=add">添加链接</a></li>
</ul>
</div>

<div class="h_a"><?php echo $name?>链接</div>
<div class="table_full">
<table width="100%" class="J_check_wrap">
<col class="th" />
<col width="400" />
<col />
<form action="<?php echo $url?>" method="post" name="form2" >

<tr>
        <td width="11%" height="30" class="td_border" align="right">&nbsp;网站名称：&nbsp;</td>
        <td width="89%" class="td_border"><input class="input input_hd length_3" type="text" name="cd_name" id="cd_name" value="<?php echo $cd_name?>" size="35" onfocus="this.style.borderColor='#F93'" onblur="this.style.borderColor='#CCC'" />&nbsp;<font color="#FF0000">*</font></td>
</tr>
<tr>	
        <td height="30" class="td_border" align="right">&nbsp;网站地址：&nbsp;</td>
        <td class="td_border"><input type="text" class="input input_hd length_5" id="cd_url" name="cd_url" value="<?php echo $cd_url?>" size="60" onfocus="this.style.borderColor='#F93'" onblur="this.style.borderColor='#CCC'" />&nbsp;<font color="#FF0000">*</font></td>
</tr>
<tr>	
        <td height="30" class="td_border" align="right">&nbsp;网站Logo地址：&nbsp;</td>
        <td class="td_border"><input class="input input_hd length_5" type="text" id="cd_pic" name="cd_pic" value="<?php echo $cd_pic?>" size="60" onfocus="this.style.borderColor='#F93'" onblur="this.style.borderColor='#CCC'" /></td>
</tr>
<tr>
        <td height="30" class="td_border" align="right">&nbsp;友情链接类型：&nbsp;</td>
        <td class="td_border">
        <input type="radio" name="cd_classid" value="1"<?php if($cd_classid==1){echo " checked";} ?>>&nbsp;文字
        &nbsp;&nbsp;<input type="radio" name="cd_classid" value="2"<?php if($cd_classid==2){echo " checked";} ?>>&nbsp;图片
        </td>
</tr>
<tr>
        <td height="30" class="td_border" align="right">&nbsp;是否首页显示：&nbsp;</td>
        <td class="td_border">
        <input type="radio" name="cd_isindex" value="0"<?php if($cd_isindex==0){echo " checked";} ?>>&nbsp;是
        &nbsp;&nbsp;<input type="radio" name="cd_isindex" value="1"<?php if($cd_isindex==1){echo " checked";} ?>>&nbsp;否
        </td>
</tr>
<tr>
        <td height="30" class="td_border" align="right">&nbsp;网站说明：&nbsp;</td>
        <td class="td_border"><input class="input input_hd length_5" name="cd_input" type="text" id="cd_input" value="<?php echo $cd_input?>" size="50" onfocus="this.style.borderColor='#F93'" onblur="this.style.borderColor='#CCC'" /></td>
</tr>
<tr>
        <td height="40" colspan="2" align="center" class="td_border"> &nbsp;<input type="hidden" name="CD_HttpUrl" value="<?php echo $_SERVER['HTTP_REFERER'] ?>">
        <input type="submit" name="Submit" value="确定提交" class="btn btn_submit" onClick="return CheckForm();"/>
        &nbsp;&nbsp; <input type="reset" name="reset" value="清除重置" class="btn btn_submit"/></td>
</tr>
</form>  
</table>
</div></div>

<?php
}

Function main($sql,$size){
global $db;
$Arr=getpagerow($sql,$size);//sql,每页显示条数
$result=$db->query($Arr[2]);
$videonum=$db->num_rows($result);
?>
<!-- start -->
<div class="nav">
<ul class="cc">
<li class="current"><a href="link.php">链接管理</a></li>
<li><a href="?action=add">添加链接</a></li>
</ul>
</div>
<form method="post" class="J_ajaxForm" action="?ac=edit" data-role="list">
<div class="table_list">
<table width="100%" id="J_table_list" style="table-layout:fixed;">
<colgroup>
<col width="30">
<col width="280">
<col width="280">
<col width="80">
<col>
</colgroup>
<thead>
<tr>
<td></td>
<td>[ID] 站点名称</td>
<td>站点地址</td>
<td>链接内型</td>
<td class="tac">显示</td>
<td>操作</td>
</tr>
</thead>
<tbody>
<?php
if($videonum==0) echo "<tr><td height='30' colspan='9' align='center' bgcolor='#FFFFFF' class='td_border'><br><br>没有数据<br><br><br></td></tr>";
if($result){
while ($row = $db ->fetch_array($result)){
?>
<tr>
<td><span class="zero_icon"></span></td>
<td>
<input name="CS_ID[]" type="text" class="input length_0 mr10" value="<?php echo $row['cd_id']?>">
<input name="cd_name<?php echo $row['cd_id']?>" type="text" class="input length_3 mr5" value="<?php echo $row['cd_name']?>">
</td>
<td><input name="cd_url<?php echo $row['cd_id']?>" type="text" class="input length_4" value="<?php echo $row['cd_url']?>"></td>
<td><?php if($row['cd_classid']==1){ echo "文字类型"; }else{ echo "图片类型"; }?></td>
<td class="tac"><?php if($row['cd_isindex']==1){?><a href="?action=editisindex&cd_isindex=0&cd_id=<?php echo $row['cd_id']?>"><img src='images/no.gif' border='0'></a><?php }else{ ?><a href="?action=editisindex&cd_isindex=1&cd_id=<?php echo $row['cd_id']?>"><img src='images/yes.gif' border='0'></a><?php } ?></td>
<td>
<a href="?action=edit&cd_id=<?php echo $row['cd_id']?>">编辑</a> <a href="?action=del&cd_id=<?php echo $row['cd_id']?>" onClick="return confirm('确定将链接删除吗?');">删除</a>
</td>
</tr>


<?php
}
}
?> 
</tbody>
</table>
</div>

</div>	
</form>
<!-- end -->
</div>
<?php			
}
//删除
function Del(){
global $db;
$cd_id=SafeRequest("cd_id","get");
$sql="delete from ".tname('link')." where cd_id=$cd_id";
if($db->query($sql)){
die("<script>window.location='".$_SERVER['HTTP_REFERER']."'</script>");
}
}

//编辑
function Edit(){
global $db;
$cd_id=SafeRequest("cd_id","get");
$sql="Select * from ".tname('link')." where cd_id=$cd_id";
if($row=$db->Getrow($sql)){
$Arr=array($row['cd_name'],$row['cd_url'],$row['cd_pic'],$row['cd_classid'],$row['cd_isindex'],$row['cd_input']);
}
EditBoard($Arr,"?action=saveedit&cd_id=".$cd_id."","编辑");
}

//添加数据
function Add(){
$Arr=array("","","","","","");
EditBoard($Arr,"?action=saveadd","添加");
}

//执行保存
function SaveAdd(){
global $db;
$cd_name = SafeRequest("cd_name","post");
$cd_url = SafeRequest("cd_url","post");
$cd_pic = SafeRequest("cd_pic","post");
$cd_classid = SafeRequest("cd_classid","post");
$cd_input = SafeRequest("cd_input","post");
$cd_isindex = SafeRequest("cd_isindex","post");
$setarr = array(
'cd_name' => $cd_name,
'cd_url' => $cd_url,
'cd_pic' => $cd_pic,
'cd_classid' => $cd_classid,
'cd_input' => $cd_input,
'cd_isverify' => 0,
'cd_isindex' => $cd_isindex,
'cd_theorder' => 0
);
inserttable('link', $setarr, 1);
AdminAlert("恭喜您，添加友情链接成功！","link.php",0);	
}

//保存编辑
function SaveEdit(){
global $db;
$cd_id = SafeRequest("cd_id","get");
$cd_name = SafeRequest("cd_name","post");
$cd_url = SafeRequest("cd_url","post");
$cd_pic = SafeRequest("cd_pic","post");
$cd_classid = SafeRequest("cd_classid","post");
$cd_input = SafeRequest("cd_input","post");
$cd_isindex = SafeRequest("cd_isindex","post");
$setarr = array(
'cd_name' => $cd_name,
'cd_url' => $cd_url,
'cd_pic' => $cd_pic,
'cd_classid' => $cd_classid,
'cd_input' => $cd_input,
'cd_isindex' => $cd_isindex
);
updatetable('link', $setarr, array('cd_id'=>$cd_id));
AdminAlert("恭喜您，编辑友情链接成功！","link.php",0);	
}

function editisindex(){
global $db;
$cd_id = SafeRequest("cd_id","get");
$cd_isindex = SafeRequest("cd_isindex","get");
$sql="update ".tname('link')." set cd_isindex=".$cd_isindex." where cd_id=".$cd_id."";
if($db->query($sql)){
echo "<script>window.location='".$_SERVER['HTTP_REFERER']."'</script>";
}	
}

function alleditsave(){
global $db;
$cd_id = RequestBox("cd_id");
if($cd_id=="0"){
AdminAlert("出错了，请选择要编辑的友情链接！","link.php",1);
}else{
$ID=explode(",",$cd_id);
for($i=0;$i<count($ID);$i++){
$cd_name=SafeRequest("cd_name".$ID[$i]."","post");
$cd_url=SafeRequest("cd_url".$ID[$i]."","post");
$cd_theorder=SafeRequest("cd_theorder".$ID[$i]."","post");
if(!IsNul($cd_name)){AdminAlert("出错了，网站名称不能为空！","link.php",1);}
if(!IsNul($cd_url)){AdminAlert("出错了，网站地址不能为空！","link.php",1);}
if(!IsNum($cd_theorder)){AdminAlert("出错了，排序不能为空！","link.php",1);}
$setarr = array(
'cd_name' => $cd_name,
'cd_url' => $cd_url,
'cd_theorder' => $cd_theorder
);
updatetable('link', $setarr, array('cd_id'=>$ID[$i]));
}
AdminAlert("恭喜您，编辑友情链接成功！","link.php",0);
}
}
?>
</body>
</html>