<?php
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
error_reporting(0);
include '../include/x5music.conn.php';
include '../include/x5music.inc.php';
$cd_id=SafeRequest("id", "get");
$ac=SafeRequest("action", "get");
if(cd_webhtml==1 or cd_webhtml==2 or cd_webhtml==3) {
	if(!IsNumID($cd_id)) {
		$error="ID错误，请检查Url地址是否正确！";
		exit(include _x5music_root_ . "/include/error/content_error.php");
	}
	if(!IsNum($cd_id)) {
		$error="缺少ID，请检查Url地址是否正确！";
		exit(include _x5music_root_ . "/include/error/content_error.php");
	}
	$cache_id="playdown_" . $cd_id;
	if(!($cache_opt->start($cache_id))) {
		echo SpanDjDown($cd_id, $userlogined);
		$cache_opt->end();
	}
}
?>