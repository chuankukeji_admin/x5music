<?php
error_reporting(0);
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
include "../include/config.inc.php";
include "../include/x5music.function.php";

//程序目录
define('S_ROOT', substr(dirname(__FILE__), 0, -7));
$theurl = 'index.php';
$_SC[charset] = 'gbk';
$sqlfile = S_ROOT.'./install/sql-dftables.txt';
$formhash = formhash();

//变量
$step = empty($_GET['step'])?0:intval($_GET['step']);
$action = empty($_GET['action'])?'':trim($_GET['action']);
$nowarr = array('','','','','','','');

if(!file_exists($sqlfile)) {
	show_msg('./install目录下的数据结构文件[sql-dftables.txt]不存在，请上传后在执行安装。', 999);
}
$configfile = S_ROOT.'./include/config.inc.php';

//检查config.inc.php是否可写
if(!@$fp = fopen($configfile, 'a')) {
	show_msg("文件 $configfile 读写权限设置错误，请设置为可写，再执行安装程序");
} else {
	@fclose($fp);
}

$lockfile = S_ROOT.'./data/install.lock';
if(file_exists($lockfile)) {
	show_msg('<b>警告!您已经安装过x5Music <br><br>
		为了保证数据安全，请立即手动删除 install文件夹<br><br>
		如果您想重新安装x5Music ，请删除 data/install.lock 文件</b>');
}

if(empty($step)) {

	show_header();

	//检查权限设置
	$checkok = true;
	$perms = array();
	if(!checkfdperm(S_ROOT.'./include/config.inc.php', 1)) {
		$perms['configinc'] = '<font color=red><strong>×</strong></font>';
		$checkok = false;
	} else {
		$perms['configinc'] = '<font color=green><strong>√</strong></font>';
	}
	if(!checkfdperm(S_ROOT.'./include/x5music.config.php', 1)) {
		$perms['x5musicconfig'] = '<font color=red><strong>×</strong></font>';
		$checkok = false;
	} else {
		$perms['x5musicconfig'] = '<font color=green><strong>√</strong></font>';
	}
	if(!checkfdperm(S_ROOT.'./data/')) {
		$perms['cache'] = '<font color=red><strong>×</strong></font>';
		$checkok = false;
	} else {
		$perms['cache'] = '<font color=green><strong>√</strong></font>';
	}
	if(!checkfdperm(S_ROOT.'./play/')) {
		$perms['html'] = '<font color=red><strong>×</strong></font>';
		$checkok = false;
	} else {
		$perms['html'] = '<font color=green><strong>√</strong></font>';
	}

	//安装阅读
	print<<<END
<div class="box musicBus mt bgWrite" style="margin-top: 20px;"> 
<div style="color: #808080; margin: 10px 0px; font-size: 18px;">
<h3>x5Music  安装向导</h3><br />
程序介绍:<br />
x5Music 是一套面向千万级数据量设计的开源音乐产品！<br />

基于php+mysql语言开发、前台支持用户系统，多模板模式，丰富的DIY标签有利于站长二次开发与数据调用！<br />

后台支持模板下载，插件下载，数据库备份还原，歌曲批量管理等功能。<br /><br />
官方网站：http://x5mp3.com/<br />
QQ交流群：343319601<br />
</div>  
END;

	if(!$checkok) {
		echo "<table><tr><td><b>出现问题:系统检测到以上目录或文件权限没有正确设置，强烈建议正常设置权限后再刷新本页面以便继续安装， [<a href=\"$theurl?step=1\">强制继续</a>]</td></tr></table>";
	} else {
		$ucapi = empty($_POST['ucapi'])?'/':$_POST['ucapi'];
		$ucfounderpw = empty($_POST['ucfounderpw'])?'':$_POST['ucfounderpw'];
		print <<<END
		<div style="float:right;">
		<form id="theform" method="post" action="$theurl?step=1">
			<a class="btn btn-primary" href="$theurl?step=1">同意协议，并开始安装！</a>
			<input type="hidden" name="ucapi" value="$ucapi" />
			<input type="hidden" name="ucfounderpw" value="$ucfounderpw" />
			<input type="hidden" name="formhash" value="$formhash">
		</form>
		</div><br /><br />
		</div>
END;
	}

	print<<<END
	<div class="row" id="tbl_readme" style="display:none;"><div class="panel"><div class="panel-heading"><h3>产品协议</h3></div><div class="panel-body"><pre><p>免费产品，随便开发，随便折腾。官方网站：http://x5mp3.com/  联系QQ：196859961</p></pre></div></div></div>
END;

	show_footer();

} elseif($step == 1) {

	show_header();
	//设置数据库配置
	print<<<END
<div class="box musicBus mt bgWrite" style="margin-top: 20px;"> 
<div style="color: #808080; margin: 10px 0px; font-size: 22px;">
<h3>创建数据库</h3><br />
<form class="form-horizontal" id="theform" method="post" action="$theurl?step=2">
数据库服务器：<input type="text" name="sqlserver" value="localhost" style="width: 175px; margin: 10px 0px;" class="input1"><br />
数据库用户名：<input type="text" name="sqlname" value="" style="width: 175px; margin: 10px 0px;" class="input1"><br />
数据库密码：<input type="password" name="sqlpass" value="" style="width: 175px; margin: 10px 23px;" class="input1"><br />
数据库名：<input type="text" name="sqldbname" value="" style="width: 175px;margin: 10px 45px;" class="input1"><br />
数据库表名：<input type="text" name="sqltablename" value="x5music_" style="width: 175px; margin: 10px 23px;" class="input1"><br />
数据库字符集：GBK<br /><br />
<button type="submit" style="margin: 10px 123px;" class="btn btn-primary">创建数据库</button>
</form>
</div>
</div>
END;
} elseif($step == 2) {
	$path=$_SERVER['PHP_SELF'];
	$path=ReplaceStr(strtolower($path),"install/index.php","");
	$server=SafeRequest("sqlserver","post");
	$dbname=SafeRequest("sqldbname","post");
	$id=SafeRequest("sqlname","post");
	$pwd=SafeRequest("sqlpass","post");
	$sqltablename=SafeRequest("sqltablename","post");

	//判断数据库
	$havedata = false;
	if(!@mysql_connect($server, $id, $pwd)) {
		show_msg('数据库连接信息填写错误，请确认！');
	}

	$config=file_get_contents("../include/config.inc.php");
	$config=preg_replace('/"cd_sqlservername","(.*?)"/','"cd_sqlservername","'.$server.'"',$config);
	$config=preg_replace('/"cd_sqldbname","(.*?)"/','"cd_sqldbname","'.$dbname.'"',$config);
	$config=preg_replace('/"cd_sqluserid","(.*?)"/','"cd_sqluserid","'.$id.'"',$config);
	$config=preg_replace('/"cd_sqlpwd","(.*?)"/','"cd_sqlpwd","'.$pwd.'"',$config);
	$config=preg_replace('/"cd_webpath","(.*?)"/','"cd_webpath","'.$path.'"',$config);
	$config=preg_replace('/"cd_tablename","(.*?)"/','"cd_tablename","'.$sqltablename.'"',$config);
	$config=preg_replace('/"cd_cookiepath","(.*?)"/','"cd_cookiepath","'.$path.'"',$config);
	$ifile = new iFile('../include/config.inc.php','w');
	$ifile->WriteFile($config,3);

	if(mysql_select_db($dbname)) {
		if(mysql_query("SELECT COUNT(*) FROM ".$sqltablename."dj")) {
			$havedata = true;
		}
	} else {
		if(!mysql_query("CREATE DATABASE `".$dbname."`")) {
show_msg('设定的数据库无权限操作，请先手工操作后，再执行安装程序');
		}
	}

	if($havedata) {
		show_msg('危险!指定的数据库已有数据，如果继续将会清空原有数据!', ($step+1));
	} else {
		show_msg('数据库配置成功，进入下一步操作', ($step+1), 1);
	}

} elseif($step == 3) {
	$lnk=mysql_connect(cd_sqlservername,cd_sqluserid,cd_sqlpwd)
	or show_msg('数据库连接信息填写错误，请确认');
mysql_select_db(cd_sqldbname,$lnk)
	or show_msg('数据库连接信息填写错误，请确认');
	mysql_query("SET NAMES gb2312", $lnk);
	$sql=file_get_contents("sql-dftables.txt");
	$sql=ReplaceStr($sql,"x5music_",cd_tablename);
	$sqlarr=explode(";",$sql);
	$datasql=file_get_contents("sql-dfdata.txt");
	$datasql=str_replace('localhost',$_SERVER['HTTP_HOST'],$datasql);
	$datasql=ReplaceStr($datasql,"x5music_",cd_tablename);
	$dataarr=explode("-x5music-",$datasql);

	for($i=0;$i<count($sqlarr)-1;$i++){
		mysql_query($sqlarr[$i]);
		$intablepost=strpos($sqlarr[$i],"(");
		$intable=substr($sqlarr[$i],0,$intablepost);
	}
	for($i=0;$i<count($dataarr);$i++){
		mysql_query($dataarr[$i]);
	}
	show_msg('数据表已经全部安装完成，进入下一步操作', ($step+1), 1);

} elseif ($step == 4) {
	$msg = <<<EOF
	<script type="text/javascript">
	function $(id) {
		return document.getElementById(id);
	}
	function showmessage(message) {
		document.getElementById('notice').innerHTML += message + '<br />';
	}
	function CheckForm(){
        	if (document.form2.username.value==""){
        		alert("后台用户不能为空！");
        		document.form2.username.focus();
        		return false;
        	}
        	if (document.form2.password.value==""){
        		alert("后台密码不能为空！");
        		document.form2.password.focus();
        		return false;
        	}
        	if (document.form2.password1.value==""){
        		alert("确认密码不能为空！");
        		document.form2.password1.focus();
        		return false;
        	}
        	if (document.form2.password1.value!==document.form2.password.value){
        		alert("两次密码不一样！");
        		return false;
        	}
        	if (document.form2.checkcode.value==""){
        		alert("认证码不能为空！");
        		document.form2.checkcode.focus();
        		return false;
        	}
	}
	</script>
<h3>创建管理员帐号</h3><br />
<form class="form-horizontal" name="form2" method="post" action="$theurl?step=5">
后台用户：<input type="text" name="username" value="" style="width: 175px; margin: 10px 0px;" class="input1"><br />
后台密码：<input type="password" name="password" value="" style="width: 175px; margin: 10px 0px;" class="input1"><br />
确认密码：<input type="password" name="password1" value="" style="width: 175px; margin: 10px 0px;" class="input1"><br />
<div style="display:none">后台验证码<input type="text" name="checkcode" value="x5mp3.com" style="width: 175px; margin: 10px 0px;" class="input1" readonly>*默认验证码不可修改，安装后请到后台全局中修改验证码。<br /></div>
<input type="submit" class="btn btn-primary" style="margin: 10px 123px;" name="opensubmit" value="建立后台管理员" onClick="return CheckForm();">
	</form>
EOF;
	show_msg($msg, 999);
} elseif ($step == 5) {

	$username=SafeRequest("username","post");
	$password=SafeRequest("password","post");
	$password1=SafeRequest("password1","post");
	$checkcode=SafeRequest("checkcode","post");
	if($password<>$password1){show_msg('出错了，两次密码不一样！');}
	$lnk=mysql_connect(cd_sqlservername,cd_sqluserid,cd_sqlpwd)
	or show_msg('数据库连接信息填写错误，请确认');
	mysql_select_db(cd_sqldbname,$lnk)
	or show_msg('数据库连接信息填写错误，请确认');	
	mysql_query("SET NAMES gb2312", $lnk);

	$config=file_get_contents("../include/x5music.config.php");
	$config=preg_replace('/"CD_WebCodeB","(.*?)"/','"CD_WebCodeB","'.$checkcode.'"',$config);
	$config=preg_replace('/"CD_TempLateUrl","(.*?)"/','"CD_TempLateUrl","skin/index/html/"',$config);
	fwrite(fopen("../include/x5music.config.php","wb"),$config);
	$sql="insert into `".tname('admin')."` (CD_AdminUserName,CD_AdminPassWord,CD_Permission,CD_LoginNum,CD_IsLock) values ('".$username."','".md5($password1)."','1,2,3,4,5,6,7,8,9,10,11','0','0')";
	if(mysql_query($sql)){
		if(mysql_query($sqls)){}
		fwrite(fopen("../data/install.lock","wb"),"");
		show_msg('
                	<div class="alert alert-success">
                	    <p>恭喜，系统已安装成功！为了安全起见，请立即删除install文件夹！</p>
                        <p><a href="../">访问前台</a></p><a href="../">
                        </a><p><a href="../admin/"></a><a href="../admin/">访问后台</a></p><a href="../admin/">
                	</a></div><a href="../admin/">
                	</a><a class="btn btn-primary pull-right" href="../">完成创建</a>
                </div>', 999);
	}else{
		echo mysql_error();
	}
}

//页面头部
function show_header() {
	global $_SGLOBAL, $nowarr, $step, $theurl, $_SC;

	$nowarr[$step] = ' class="current"';
	print<<<END
<!DOCTYPE html>
<html>
 <head>
  <meta charset="gbk" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
  <meta name="description" content="" />
  <meta name="author" content="" />
  <meta name="renderer" content="webkit|ie-comp|ie-stand" />
  <title>x5Music 安装</title>
  <style type="text/css">
body {
margin: 0;
background: no-repeat #E9E9E9;
}
body, button, input, select {
font: 14px/1.5 arial, sans-serif;
color: #fff;
}
body, h1, h2, h3, h4, h5, h6, hr, p, blockquote, dl, dt, dd, ul, ol, li, pre, form, fieldset, legend, button, input, textarea, th, td {
margin: 0;
padding: 0;
}
a {
color: #428bca;
text-decoration: none;
}
user agent stylesheetbody {
display: block;
margin: 8px;
}
.container {
margin: 0px auto;
width: 960px;
}
.box {
padding: 10px;
background-color: #FFFFFF;
border-radius: 5px 5px 5px 5px;
-moz-border-radius: 5px 5px 5px 5px;
box-shadow: 0 2px 5px rgba(0, 0, 0, 0.2);
-moz-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.2);
}
.btn-primary:hover, .btn-primary:focus, .btn-primary:active, .btn-primary.active, .open .dropdown-toggle.btn-primary {
color: #fff;
background-color: #3276b1;
border-color: #285e8e;
}
.btn:hover, .btn:focus {
color: #fff;
text-decoration: none;
}
a:hover, a:focus {
color: #2a6496;
text-decoration: underline;
}
a:active, a:hover {
outline: 0;
}
.pull-right {
float: right!important;
}
.btn-primary {
color: #fff;
background-color: #428bca;
border-color: #357ebd;
}
.btn {
display: inline-block;
margin-bottom: 0;
font-weight: 400;
text-align: center;
vertical-align: middle;
cursor: pointer;
background-image: none;
border: 1px solid transparent;
white-space: nowrap;
padding: 6px 12px;
font-size: 14px;
line-height: 1.42857143;
border-radius: 4px;
-webkit-user-select: none;
-moz-user-select: none;
-ms-user-select: none;
user-select: none;
}
input.input1{border:1px solid #ccc;border-radius:2px;  color:#666;padding:7px 8px;padding:8px 8px 6px 8px\9; height:23px; line-height:23px; font-family:Tahoma, Geneva, sans-serif; font-weight:bold;
font-size:14px;width:200px;position: relative;}

    </style>
 </head>
 <body>
<div class="container">
END;
}

//页面顶部
function show_footer() {
	print<<<END
</div>  
</body>
</html>
END;
}

//显示
function show_msg($message, $next=0, $jump=0) {
	global $theurl;

	$nextstr = '';
	$backstr = '';

	if(empty($next)) {
		$backstr .= "<a href=\"javascript:history.go(-1);\">返回上一步</a>";
	} elseif ($next == 999) {
	} else {
		$url_forward = "$theurl?step=$next";
		if($jump) {
			$nextstr .= "<a href=\"$url_forward\">请稍等...</a><script>setTimeout(\"window.location.href ='$url_forward';\", 1000);</script>";
		} else {
			$nextstr .= "<a href=\"$url_forward\">继续下一步</a>";
			$backstr .= "<a href=\"javascript:history.go(-1);\">返回上一步</a>";
		}
	}

	show_header();
	print<<<END
		<div class="box musicBus mt bgWrite" style="margin-top: 20px;"> 
<div style="color: #808080; margin: 10px 0px; font-size: 22px;">
$message<br /><br />
$backstr $nextstr
</div></div>

END;
	show_footer();
	exit();
}

//检查权限
function checkfdperm($path, $isfile=0) {
	if($isfile) {
		$file = $path;
		$mod = 'a';
	} else {
		$file = $path.'./install_tmptest.data';
		$mod = 'w';
	}
	if(!@$fp = fopen($file, $mod)) {
		return false;
	}
	if(!$isfile) {
		//是否可以删除
		fwrite($fp, ' ');
		fclose($fp);
		if(!@unlink($file)) {
			return false;
		}
		//检测是否可以创建子目录
		if(is_dir($path.'./install_tmpdir')) {
			if(!@rmdir($path.'./install_tmpdir')) {
				return false;
			}
		}
		if(!@mkdir($path.'./install_tmpdir')) {
			return false;
		}
		//是否可以删除
		if(!@rmdir($path.'./install_tmpdir')) {
			return false;
		}
	} else {
		fclose($fp);
	}
	return true;
}

//产生form防伪码
function formhash() {
	global $_SGLOBAL, $_SCONFIG;

	if(empty($_SGLOBAL['formhash'])) {
		$hashadd = defined('IN_ADMINCP') ? 'Only For UCenter Home AdminCP' : '';
		$_SGLOBAL['formhash'] = substr(md5(substr($_SGLOBAL['timestamp'], 0, -7).'|'.$_SGLOBAL['supe_uid'].'|'.md5($_SCONFIG['sitekey']).'|'.$hashadd), 8, 8);
	}
	return $_SGLOBAL['formhash'];
}

//判断提交是否正确
function submitcheck($var) {
	if(!empty($_POST[$var]) && $_SERVER['REQUEST_METHOD'] == 'POST') {
		if((empty($_SERVER['HTTP_REFERER']) || preg_replace("/https?:\/\/([^\:\/]+).*/i", "\\1", $_SERVER['HTTP_REFERER']) == preg_replace("/([^\:]+).*/", "\\1", $_SERVER['HTTP_HOST'])) && $_POST['formhash'] == formhash()) {
			return true;
		} else {
			showmessage('submit_invalid');
		}
	} else {
		return false;
	}
}
?>