
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*数据表 `x5music_admin` 的表结构*/

DROP TABLE IF EXISTS `x5music_admin`;

CREATE TABLE `x5music_admin` (
  `CD_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CD_AdminUserName` varchar(64) NOT NULL,
  `CD_AdminPassWord` varchar(64) NOT NULL,
  `CD_LoginIP` varchar(128) DEFAULT NULL,
  `CD_LoginNum` int(11) DEFAULT '0',
  `CD_LastLogin` datetime DEFAULT NULL,
  `CD_IsLock` int(11) NOT NULL DEFAULT '0',
  `CD_Permission` varchar(128) NOT NULL,
  PRIMARY KEY (`CD_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=gbk;


/*数据表 `x5music_class` 的表结构*/

DROP TABLE IF EXISTS `x5music_class`;

CREATE TABLE `x5music_class` (
  `CD_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CD_Name` varchar(255) NOT NULL,
  `CD_AliasName` varchar(64) NOT NULL,
  `CD_Template` varchar(64) NOT NULL,
  `CD_FatherID` int(11) NOT NULL DEFAULT '0',
  `CD_TieID` varchar(255) DEFAULT NULL,
  `CD_SystemID` int(11) NOT NULL DEFAULT '1',
  `CD_TheOrder` int(11) NOT NULL DEFAULT '0',
  `CD_IsHide` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`CD_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=gbk;


/*数据表 `x5music_dj` 的表结构*/

DROP TABLE IF EXISTS `x5music_dj`;

CREATE TABLE `x5music_dj` (
  `CD_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `CD_Name` varchar(255) NOT NULL,
  `CD_ClassID` int(11) NOT NULL,
  `CD_SpecialID` int(11) NOT NULL,
  `CD_Singer` varchar(50) DEFAULT NULL,
  `CD_User` varchar(50) DEFAULT NULL,
  `CD_Tag` text,
  `CD_Pic` text,
  `CD_Url` text,
  `CD_DownUrl` text,
  `CD_Word` text,
  `CD_Lrc` text,
  `CD_From` text,
  `CD_Siz` text,
  `CD_Uid` text,
  `CD_Md5` text,
  `CD_Hits` int(11) NOT NULL,
  `CD_DownHits` int(11) NOT NULL,
  `CD_FavHits` int(11) NOT NULL,
  `CD_uHits` int(11) NOT NULL,
  `CD_dHits` int(11) NOT NULL,
  `CD_DayHits` int(11) NOT NULL,
  `CD_WeekHits` int(11) NOT NULL,
  `CD_MonthHits` int(11) NOT NULL,
  `CD_LastHitTime` datetime DEFAULT NULL,
  `CD_AddTime` datetime DEFAULT NULL,
  `CD_Server` int(11) NOT NULL,
  `CD_Deleted` int(11) NOT NULL,
  `CD_IsBest` int(11) NOT NULL,
  `CD_Error` int(11) NOT NULL,
  `CD_Passed` int(11) NOT NULL,
  `CD_Points` int(11) NOT NULL,
  `CD_Grade` int(11) NOT NULL,
  `CD_Color` varchar(20) DEFAULT NULL,
  `CD_Skin` varchar(50) NOT NULL,
  PRIMARY KEY (`CD_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=gbk;


/*数据表 `x5music_fans` 的表结构*/

DROP TABLE IF EXISTS `x5music_fans`;

CREATE TABLE `x5music_fans` (
  `cd_id` int(11) NOT NULL AUTO_INCREMENT,
  `cd_uid` int(11) NOT NULL,
  `cd_uname` varchar(50) NOT NULL,
  `cd_uids` int(11) NOT NULL,
  `cd_unames` varchar(50) NOT NULL,
  `cd_lock` int(11) NOT NULL,
  `cd_addtime` datetime NOT NULL,
  PRIMARY KEY (`cd_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=gbk;

/*数据表 `x5music_fav` 的表结构*/

DROP TABLE IF EXISTS `x5music_fav`;

CREATE TABLE `x5music_fav` (
  `cd_id` int(11) NOT NULL AUTO_INCREMENT,
  `cd_type` varchar(10) NOT NULL,
  `cd_uid` int(11) NOT NULL,
  `cd_uname` varchar(50) NOT NULL,
  `cd_musicid` int(11) NOT NULL,
  `cd_musicname` varchar(255) NOT NULL,
  `cd_addtime` int(11) NOT NULL,
  PRIMARY KEY (`cd_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=gbk;

/*数据表 `x5music_feed` 的表结构*/

DROP TABLE IF EXISTS `x5music_feed`;

CREATE TABLE `x5music_feed` (
  `cd_id` int(11) NOT NULL AUTO_INCREMENT,
  `cd_uid` int(11) NOT NULL,
  `cd_username` varchar(50) DEFAULT NULL,
  `cd_icon` varchar(20) NOT NULL,
  `cd_title` text NOT NULL,
  `cd_data` text NOT NULL,
  `cd_image` varchar(255) DEFAULT NULL,
  `cd_imagelink` varchar(255) DEFAULT NULL,
  `cd_dataid` int(11) NOT NULL,
  `cd_addtime` datetime NOT NULL,
  PRIMARY KEY (`cd_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=gbk;

/*数据表 `x5music_friend` 的表结构*/

DROP TABLE IF EXISTS `x5music_friend`;

CREATE TABLE `x5music_friend` (
  `cd_id` int(11) NOT NULL AUTO_INCREMENT,
  `cd_uid` int(11) NOT NULL,
  `cd_uname` varchar(50) NOT NULL,
  `cd_uids` int(11) NOT NULL,
  `cd_unames` varchar(50) NOT NULL,
  `cd_lock` int(11) NOT NULL,
  `cd_addtime` datetime NOT NULL,
  PRIMARY KEY (`cd_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=gbk;

/*数据表 `x5music_message` 的表结构*/

DROP TABLE IF EXISTS `x5music_message`;

CREATE TABLE `x5music_message` (
  `cd_id` int(11) NOT NULL AUTO_INCREMENT,
  `cd_uid` int(11) NOT NULL,
  `cd_uname` varchar(50) NOT NULL,
  `cd_uids` int(11) NOT NULL,
  `cd_unames` varchar(50) NOT NULL,
  `cd_title` text NOT NULL,
  `cd_content` text NOT NULL,
  `cd_class` int(11) NOT NULL,
  `cd_dela` int(11) NOT NULL,
  `cd_delb` int(11) NOT NULL,
  `cd_addtime` datetime NOT NULL,
  PRIMARY KEY (`cd_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=gbk;

/*数据表 `x5music_mold` 的表结构*/

DROP TABLE IF EXISTS `x5music_mold`;

CREATE TABLE `x5music_mold` (
  `CD_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CD_Name` varchar(255) NOT NULL,
  `CD_TempPath` text NOT NULL,
  `CD_TheOrder` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`CD_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=gbk;

/*数据表 `x5music_news` 的表结构*/

DROP TABLE IF EXISTS `x5music_news`;

CREATE TABLE `x5music_news` (
  `CD_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CD_ClassID` int(11) NOT NULL,
  `CD_Name` varchar(255) NOT NULL,
  `CD_Intro` text,
  `CD_Hits` int(11) NOT NULL,
  `CD_IsIndex` int(11) NOT NULL,
  `CD_IsBest` int(11) NOT NULL,
  `CD_Color` varchar(20) NOT NULL,
  `CD_AddTime` datetime NOT NULL,
  PRIMARY KEY (`CD_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=gbk;

/*数据表 `x5music_newsclass` 的表结构*/

DROP TABLE IF EXISTS `x5music_newsclass`;

CREATE TABLE `x5music_newsclass` (
  `CD_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CD_Name` varchar(255) CHARACTER SET gbk NOT NULL,
  `CD_TheOrder` int(11) NOT NULL,
  `CD_IsIndex` int(11) NOT NULL,
  PRIMARY KEY (`CD_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=gbk;

/*数据表 `x5music_pic` 的表结构*/

DROP TABLE IF EXISTS `x5music_pic`;

CREATE TABLE `x5music_pic` (
  `cd_id` int(11) NOT NULL AUTO_INCREMENT,
  `cd_uid` int(11) NOT NULL,
  `cd_uname` varchar(50) NOT NULL,
  `cd_uip` varchar(50) DEFAULT NULL,
  `cd_title` varchar(100) NOT NULL,
  `cd_style` text NOT NULL,
  `cd_url` varchar(200) NOT NULL,
  `cd_hidden` int(11) NOT NULL,
  `cd_hits` int(11) NOT NULL,
  `cd_click1` int(11) NOT NULL,
  `cd_click2` int(11) NOT NULL,
  `cd_click3` int(11) NOT NULL,
  `cd_click4` int(11) NOT NULL,
  `cd_click5` int(11) NOT NULL,
  `cd_addtime` int(11) NOT NULL,
  PRIMARY KEY (`cd_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=gbk;

/*数据表 `x5music_server` 的表结构*/

DROP TABLE IF EXISTS `x5music_server`;

CREATE TABLE `x5music_server` (
  `CD_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CD_Name` varchar(100) NOT NULL,
  `CD_Url` varchar(255) NOT NULL,
  `CD_DownUrl` varchar(255) DEFAULT NULL,
  `CD_Yes` int(11) NOT NULL,
  PRIMARY KEY (`CD_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=gbk;

/*数据表 `x5music_session` 的表结构*/

DROP TABLE IF EXISTS `x5music_session`;

CREATE TABLE `x5music_session` (
  `cd_id` int(11) NOT NULL AUTO_INCREMENT,
  `cd_uid` int(11) NOT NULL,
  `cd_uname` varchar(30) NOT NULL,
  `cd_uip` varchar(50) NOT NULL,
  `cd_logintime` int(11) NOT NULL,
  PRIMARY KEY (`cd_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=gbk;

/*数据表 `x5music_special` 的表结构*/

DROP TABLE IF EXISTS `x5music_special`;

CREATE TABLE `x5music_special` (
  `CD_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CD_ClassID` int(11) NOT NULL,
  `CD_Name` varchar(255) DEFAULT NULL,
  `CD_User` varchar(50) DEFAULT NULL,
  `CD_Pic` varchar(255) DEFAULT NULL,
  `CD_Singer` varchar(50) DEFAULT NULL,
  `CD_GongSi` varchar(50) DEFAULT NULL,
  `CD_YuYan` varchar(50) DEFAULT NULL,
  `CD_Intro` text,
  `CD_Hits` int(11) NOT NULL,
  `CD_IsBest` int(11) NOT NULL,
  `CD_Passed` int(11) NOT NULL,
  `CD_AddTime` datetime NOT NULL,
  PRIMARY KEY (`CD_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=gbk;


/*数据表 `x5music_upload` 的表结构*/

DROP TABLE IF EXISTS `x5music_upload`;

CREATE TABLE `x5music_upload` (
  `cd_id` int(11) NOT NULL AUTO_INCREMENT,
  `cd_userid` int(11) NOT NULL,
  `cd_username` varchar(30) NOT NULL,
  `cd_userip` varchar(50) NOT NULL,
  `cd_filetype` varchar(10) NOT NULL,
  `cd_filename` varchar(200) NOT NULL,
  `cd_filesize` int(11) NOT NULL,
  `cd_fileurl` varchar(200) NOT NULL,
  `cd_filetime` int(11) NOT NULL,
  PRIMARY KEY (`cd_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=gbk;

/*数据表 `x5music_user` 的表结构*/

DROP TABLE IF EXISTS `x5music_user`;

CREATE TABLE `x5music_user` (
  `cd_id` int(11) NOT NULL AUTO_INCREMENT,
  `cd_name` varchar(30) DEFAULT NULL,
  `cd_nicheng` varchar(30) DEFAULT NULL,
  `cd_password` varchar(50) DEFAULT NULL,
  `cd_question` varchar(50) DEFAULT NULL,
  `cd_answer` varchar(50) DEFAULT NULL,
  `cd_email` varchar(100) DEFAULT NULL,
  `cd_sex` varchar(10) DEFAULT NULL,
  `cd_regdate` datetime DEFAULT NULL,
  `cd_loginip` varchar(30) DEFAULT NULL,
  `cd_loginnum` int(11) DEFAULT NULL,
  `cd_qq` varchar(15) DEFAULT NULL,
  `cd_logintime` datetime DEFAULT NULL,
  `cd_grade` int(11) DEFAULT NULL,
  `cd_lock` int(11) DEFAULT NULL,
  `cd_points` int(11) DEFAULT NULL,
  `cd_photo` varchar(100) DEFAULT NULL,
  `cd_sign` text,
  `cd_hidden` int(11) DEFAULT NULL,
  `cd_birthprovince` varchar(20) DEFAULT NULL,
  `cd_birthcity` varchar(20) DEFAULT NULL,
  `cd_resideprovince` varchar(20) DEFAULT NULL,
  `cd_residecity` varchar(20) DEFAULT NULL,
  `cd_birthday` varchar(20) DEFAULT NULL,
  `cd_vipindate` datetime DEFAULT NULL,
  `cd_vipenddate` datetime DEFAULT NULL,
  `cd_djmub` int(11) DEFAULT NULL,
  `cd_ufav` int(11) DEFAULT NULL,
  `cd_ufans` int(11) DEFAULT NULL,
  `cd_hits` int(11) DEFAULT NULL,
  `cd_isbest` int(11) DEFAULT NULL,
  `cd_money` int(11) DEFAULT NULL,
  `cd_weblock` int(11) DEFAULT NULL,
  `cd_skin` varchar(50) DEFAULT NULL,
  `cd_friendnum` int(11) DEFAULT NULL,
  `cd_creditname` text,
  `cd_rank` int(11) DEFAULT NULL,
  `cd_credit` int(11) DEFAULT NULL,
  `cd_qqopenid` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`cd_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=gbk;

/*数据表 `x5music_web` 的表结构*/

DROP TABLE IF EXISTS `x5music_web`;

CREATE TABLE `x5music_web` (
  `cd_id` int(11) NOT NULL AUTO_INCREMENT,
  `cd_uid` int(11) NOT NULL,
  `cd_uname` varchar(50) NOT NULL,
  `cd_title` varchar(200) NOT NULL,
  `cd_skin` varchar(50) NOT NULL,
  `cd_hits` int(11) NOT NULL,
  `cd_lock` int(11) NOT NULL,
  `cd_banner` varchar(200) DEFAULT NULL,
  `cd_bg` varchar(200) DEFAULT NULL,
  `cd_addtime` int(11) NOT NULL,
  PRIMARY KEY (`cd_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=gbk;


/*数据表 `x5music_link` 的表结构*/

DROP TABLE IF EXISTS `x5music_link`;

CREATE TABLE `x5music_link` (
  `cd_id` int(11) NOT NULL AUTO_INCREMENT,
  `cd_name` varchar(50) NOT NULL,
  `cd_url` varchar(100) NOT NULL,
  `cd_pic` varchar(150) NOT NULL,
  `cd_classid` int(11) NOT NULL,
  `cd_input` varchar(255) NOT NULL,
  `cd_isverify` int(11) NOT NULL,
  `cd_isindex` int(11) NOT NULL,
  `cd_theorder` int(11) NOT NULL,
  PRIMARY KEY (`cd_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=gbk;

/*数据表 `x5music_system` 的表结构*/

DROP TABLE IF EXISTS `x5music_system`;

CREATE TABLE `x5music_system` (
  `cd_djmub` int(11) NOT NULL AUTO_INCREMENT,
  `cd_usermub` int(11) NOT NULL,
  `cd_cnzz` text,
  `cd_message` text,
  `gg960_90` text,
  `gg960_60` text,
  `gg728_90` text,
  `gg640_60` text,
  `gg580_90` text,
  `gg468_60` text,
  `gg760_90` text,
  `gg468_15` text,
  `gg500_200` text,
  `gg160_600` text,
  `gg120_600` text,
  `gg120_240` text,
  `gg300_250` text,
  `gg250_250` text,
  `gg200_200` text,
  `gg336_280` text,
  `gg360_300` text,
  `gg125_125` text,
  `gg180_150` text,
  `gg728_15` text,
  `gg234_60` text,
  `gg480_160` text,
  `gg460_60` text,
  `gg120_90` text,
  `gg200_90` text,
  `gg160_90` text,
  `gg180_90` text,
  `ggself_defined` text,
  PRIMARY KEY (`cd_djmub`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=gbk;