<?php
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
error_reporting(0);
include "../include/x5music.conn.php";
include "function_common.php";
admincheck(3);
$action=SafeRequest("action", "get");
?>
<!DOCTYPE html>
<html>
 <head> 
  <meta charset="gbk" /> 
  <meta name="renderer" content="webkit" /> 
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" /> 
  <title>x5Music 后台管理中心 -x5mp3.com</title> 
  <link href="css/add.css" rel="stylesheet" /> 
  <style type="text/css">
.table2{width: 100%;max-width: 100%;border-collapse: collapse;border-spacing: 0;}
.table2 td{font-size: 12px;border-top: 1px solid #ddd;padding: 8px;vertical-align: top;}
.table2 tr:hover{text-decoration: none;background-color: #e6f2fb;}
.table2 th{border-bottom: 2px solid #ddd;vertical-align: bottom;padding: 2px;text-align: left;}
.table3{width: 100%;max-width: 100%;border-collapse: collapse;border-spacing: 0;}
.table3 tr:hover{text-decoration: none;background-color: #e6f2fb;}
.table3 td{font-size: 12px;line-height:25px;border-top: 1px solid #ddd;padding: 5px;vertical-align: top;border-right: solid 1px #ddd;}
.table3 th{border-bottom: 2px solid #ddd;vertical-align: bottom;padding: 2px;text-align: left;}
</style> 
  <script language="javascript">
function SelectAll() {
	for (var i = 0; i < document.myform.CD_ClassID.length; i++) {
		document.myform.CD_ClassID.options[i].selected = true;
	}
}
function UnSelectAll() {
	for (var i = 0; i < document.myform.CD_ClassID.length; i++) {
		document.myform.CD_ClassID.options[i].selected = false;
	}
}
function $(vId) {
	return document.all ? document.all[vId] : document.getElementById(vId);
}
function switchSupport(vTribe, vFlag) {
	vTribes = {
		'ModifyClass' : '0',
		'ModifyPassed' : '1',
		'ModifyIsbest' : '2',
		'ModifyUser' : '3',
		'ModifySinger' : '4',
		'ModifyGongSi' : '5'
	};
	
	for (var i = 0; i < 11; i++) {
		try {
			$('supportTribe' + vTribes[vTribe]).style.color = vFlag ? 'red' : '#000';
			$('supportTitle' + vTribes[vTribe]).style.color = vFlag ? 'black' : '#000';
		} catch (e) {}
		try {
			$('myform').elements['support[' + vTribe + '][soldier][' + i + ']'].disabled = vFlag ? false : true;
			$('myform').elements['support[' + vTribe + '][soldier][' + i + ']'].style.borderColor = vFlag ? '#F93' : '#CCC';
		} catch (e) {}
		try {
			$('myform').elements['support[' + vTribe + '][level][' + i + ']'].disabled = vFlag ? false : true;
		} catch (e) {}
	}
}

</script> 
 </head> 
 <body>
<?php
switch($action) {
	case 'edit':
		Edit();
		break;
	default:
		Main();
		break;
}
?>
</body>
</html>
<?php
Function Main() {
	global $db;
	$id=SafeRequest("id", "get");
?> 
  <div class="contents"> 
   <div class="panel"> 
    <div style="padding: 8px;"> 
     <a href="malbum.php" class="btn">所有音乐专辑</a> 
     <a href="malbum.php?action=add" class="btn">添加专辑</a> 
     <a href="malbum.php?action=pass" class="btn">审核专辑</a> 
     <a href="malbum.php?action=isbest" class="btn">推荐专辑</a> 
    </div> 
   </div> 
  </div> 
  <div class="contents"> 
   <div class="panel"> 
    <div class="panel-head"> 
     <strong>若要批量修改某个属性的值，请先选中其左侧的复选框，然后再设定属性值。 </strong> 
    </div> 
    <form method="post" id="myform" name="myform" action="?action=edit"> 
     <table class="table3"> 
      <tbody> 
       <tr> 
        <td align="left"><input type='radio' id='BatchType1' name='BatchType' value='1' <?php if($id<>''){echo ' checked';}?>></td> 
        <td width="120" align="left"><label for="BatchType1">修改指定音乐专辑ID</label></td> 
        <td align="left"><input type="text" id="CD_ID" name="CD_ID" class="input input_hd length_6" value="<?php echo $id;?>"></td>
       </tr> 
       <tr> 
        <td width="30" align="center"><input type="checkbox" name="ModifyUser" value="Yes" id="support[ModifyUser][is_able]" onclick="switchSupport('ModifyUser', this.checked)" /></td> 
        <td width="100" align=" left" id="supportTribe3"><label for="support[ModifyUser][is_able]">所属会员</label></td> 
        <td><input class="input length_3" name="CD_User" id="support[ModifyUser][soldier][0]" type="text" size="5" value="" disabled="" /><font color="#d01f3c">&nbsp;必须是站内已存在的用户帐号！不能乱写!</font></td> 
       </tr> 
       <tr> 
        <td width="30" align="center"><input type="checkbox" name="ModifySinger" value="Yes" id="support[ModifySinger][is_able]" onclick="switchSupport('ModifySinger', this.checked)" /></td> 
        <td width="100" align=" left" id="supportTribe4"><label for="support[ModifySinger][is_able]">所属歌手</label></td> 
        <td><input class="input length_2" name="CD_Singer" id="support[ModifySinger][soldier][0]" type="text" size="5" value="" disabled="" /><font color="#d01f3c"></font></td> 
       </tr> 
       <tr> 
        <td width="30" align="center"><input type="checkbox" name="ModifyGongSi" value="Yes" id="support[ModifyGongSi][is_able]" onclick="switchSupport('ModifyGongSi', this.checked)" /></td> 
        <td width="100" align=" left" id="supportTribe5"><label for="support[ModifyGongSi][is_able]">发行公司</label></td> 
        <td><input class="input length_2" name="CD_GongSi" id="support[ModifyGongSi][soldier][0]" type="text" size="5" value="" disabled="" /><font color="#d01f3c"></font></td> 
       </tr> 
       <tr> 
        <td width="30" align="center"><input type="checkbox" name="ModifyClass" value="Yes" id="support[ModifyClass][is_able]" onclick="switchSupport('ModifyClass', this.checked)" /></td> 
        <td width="100" align=" left" id="supportTribe0"><label for="support[ModifyClass][is_able]">移动到分类</label></td> 
        <td><select name="CD_Class" id="support[ModifyClass][soldier][0]" disabled=""> <option value="0" selected="">选择分类</option> <?php
$sqlclass='select * from ' . tname('class') . ' where CD_FatherID=0 ';
$results=$db->query($sqlclass);
if ($results) {
while ($row3=$db->fetch_array($results)) {
echo "<option value='" . $row3['CD_ID'] . "' >" . $row3['CD_Name'] . '</option>';
}
}
?></select></td> 
       </tr> 
       <tr> 
        <td width="30" align="center"><input type="checkbox" name="ModifyPassed" value="Yes" id="support[ModifyPassed][is_able]" onclick="switchSupport('ModifyPassed', this.checked)" /></td> 
        <td width="100" align=" left" id="supportTribe1"><label for="support[ModifyPassed][is_able]">审核专辑</label></td> 
        <td> <input name="CD_Passed" type="radio" id="support[ModifyPassed][soldier][0]" value="0" checked="" disabled="" />&nbsp;通过&nbsp; <input name="CD_Passed" type="radio" id="support[ModifyPassed][level][0]" value="1" disabled="" />&nbsp;取消</td> 
       </tr> 
       <tr> 
        <td width="30" align="center"><input type="checkbox" name="ModifyIsbest" value="Yes" id="support[ModifyIsbest][is_able]" onclick="switchSupport('ModifyIsbest', this.checked)" /></td> 
        <td width="100" align=" left" id="supportTribe3"><label for="support[ModifyIsbest][is_able]">推荐专辑</label></td> 
        <td> <input name="CD_Isbest" type="radio" id="support[ModifyIsbest][soldier][0]" value="0" checked="" disabled="" />&nbsp;推荐&nbsp; <input name="CD_Isbest" type="radio" id="support[ModifyIsbest][level][0]" value="1" disabled="" />&nbsp;取消</td> 
       </tr> 
       <tr> 
        <td width="30" align="center"><input type="checkbox" name="ModifyDeleted" value="Yes" id="support[ModifyDeleted][is_able]" onclick="switchSupport('ModifyDeleted', this.checked)" /></td> 
        <td width="100" align=" left" id="supportTribe11"><label for="support[ModifyDeleted][is_able]">删除专辑</label></td> 
        <td> &nbsp;&nbsp;<font color="#d01f3c">注意：批量删除不可恢复，请慎重！</font> </td> 
       </tr> 
       <tr> 
        <td height="35" colspan="3" align="left" bgcolor="#FAFBF7" class="td_border"> <button type="submit" id="adddance" class="btn btn_submit J_ajax_submit_btn" style="margin-left: 10px;">确定提交</button> &nbsp;&nbsp;<font color="#d01f3c">注意：批量设置只对该表进行设置，不与积分,奖惩,动态挂钩！</font> </td> 
       </tr> 
      </tbody> 
     </table> 
    </form>
   </div> 
  </div> 
<?php
}
Function Edit() {
	global $db;
	$x5music_Com_CD_ID=SafeRequest('CD_ID', 'post');
	$x5music_Com_BatchType=SafeRequest('BatchType', 'post');
	$x5music_Com_Isbest=SafeRequest('CD_Isbest', 'post');
	$x5music_Com_Passed=SafeRequest('CD_Passed', 'post');
	$x5music_Com_Class=SafeRequest('CD_Class', 'post');
	$x5music_Com_Deleted=SafeRequest('CD_Deleted', 'post');
	$x5music_Com_User=SafeRequest('CD_User', 'post');
	$x5music_Com_GongSi=SafeRequest('CD_GongSi', 'post');
	$x5music_Com_ModifyIsbest=SafeRequest('ModifyIsbest', 'post');
	$x5music_Com_ModifyPassed=SafeRequest('ModifyPassed', 'post');
	$x5music_Com_ModifyClass=SafeRequest('ModifyClass', 'post');
	$x5music_Com_ModifyDeleted=SafeRequest('ModifyDeleted', 'post');
	$x5music_Com_ModifyUser=SafeRequest('ModifyUser', 'post');
	$x5music_Com_ModifySinger=SafeRequest('ModifySinger', 'post');
	$x5music_Com_ModifyGongSi=SafeRequest('ModifyGongSi', 'post');
	$x5music_Com_Singer=SafeRequest('CD_Singer', 'post');
	$x5music_Com_ModifySinger=SafeRequest('ModifySinger', 'post');
	If($x5music_Com_BatchType=='1') {
		$sql1="CD_ID in ($x5music_Com_CD_ID)";
	}
	If($x5music_Com_ModifyClass=='Yes') {
		if($db->query($sql='update ' . tname('special') . ' set CD_ClassID=' . $x5music_Com_Class . ' where ' . $sql1 . '')) {
		}
	}
	If($x5music_Com_ModifyPassed=='Yes') {
		if($db->query($sql='update ' . tname('special') . ' set CD_Passed=' . $x5music_Com_Passed . ' where ' . $sql1 . '')) {
		}
	}
	If($x5music_Com_ModifyIsbest=='Yes') {
		if($db->query($sql='update ' . tname('special') . ' set CD_Isbest=' . $x5music_Com_Isbest . ' where ' . $sql1 . '')) {
		}
	}
	If($x5music_Com_ModifyUser=='Yes') {
		if($db->query($sql='update ' . tname('special') . " set CD_User='" . $x5music_Com_User . "' where " . $sql1 . '')) {
		}
	}
	If($x5music_Com_ModifyGongSi=='Yes') {
		if($db->query($sql='update ' . tname('special') . " set CD_GongSi='" . $x5music_Com_GongSi . "' where " . $sql1 . '')) {
		}
	}
	If($x5music_Com_ModifySinger=='Yes') {
		if($db->query($sql='update ' . tname('special') . " set CD_Singer='" . $x5music_Com_Singer . "' where " . $sql1 . '')) {
		}
	}
	If($x5music_Com_ModifyDeleted=='Yes') {
		$query=$db->query("select CD_Pic from " . tname('special') . " where CD_ID in ($x5music_Com_CD_ID)");
		while($row=$db->fetch_array($query)) {
			if(delunlink($row['CD_Pic'])==true) {
				@unlink(_x5music_root_ . $row['CD_Pic']);
			}
		}
		if($db->query($sql='delete from ' . tname('special') . ' where ' . $sql1 . '')) {
		}
	}
	showmessage('恭喜您，批量设置数据成功！', 'malbum.php', 0);
}
?>