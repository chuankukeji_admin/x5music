<?php
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
error_reporting(0);
include "../include/x5music.conn.php";
include "function_common.php";
admincheck(3);
$action=SafeRequest("action", "get");
$cd=SafeRequest("CD_ClassID", "get");
?>
<!DOCTYPE html>
<html>
 <head> 
  <meta charset="gbk" /> 
  <meta name="renderer" content="webkit" /> 
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" /> 
  <title>x5Music 后台管理中心 -x5mp3.com</title> 
  <link href="css/add.css" rel="stylesheet" /> 
   <script type="text/javascript" src="js/ajax.js"></script> 
  <script type="text/javascript" src="<?php echo cd_webpath?>user/static/space/layer/jquery.js"></script> 
  <script type="text/javascript" src="<?php echo cd_webpath?>user/static/space/layer/lib.js"></script> 
  <script type="text/javascript">
var pop = {
	up : function (text, url, width, height, top) {
		$.layer({
			type : 2,
			maxmin : true,
			title : text,
			iframe : {
				src : url
			},
			area : [width, height],
			offset : [top, '50%'],
			shade : [0]
		});
	}
}
function MM_jumpMenu(targ, selObj, restore) { //v3.0
	eval(targ + ".location='" + selObj.options[selObj.selectedIndex].value + "'");
	if (restore)
		selObj.selectedIndex = 0;
}
</script> 
  <style type="text/css">
.table2{width: 100%;max-width: 100%;border-collapse: collapse;border-spacing: 0;}
.table2 td{font-size: 12px;border-top: 1px solid #ddd;padding: 8px;vertical-align: top;}
.table2 tr:hover{text-decoration: none;background-color: #e6f2fb;}
.table2 th{border-bottom: 2px solid #ddd;vertical-align: bottom;padding: 2px;text-align: left;}
.table3{width: 100%;max-width: 100%;border-collapse: collapse;border-spacing: 0;}
.table3 tr:hover{text-decoration: none;background-color: #e6f2fb;}
.table3 td{font-size: 12px;line-height:25px;border-top: 1px solid #ddd;padding: 5px;vertical-align: top;border-right: solid 1px #ddd;}
.table3 th{border-bottom: 2px solid #ddd;vertical-align: bottom;padding: 2px;text-align: left;}
</style> 
 </head> 
 <body> 
<?php
if($action=="") {
	$x5='btn_success';
} elseif($action=="class") {
	$x51='btn_success';
} elseif($action=="newsadd") {
	$x52='btn_success';
} elseif($cd=="1") {
	$x53='btn_success';
} elseif($action=="newsggadd") {
	$x54='btn_success';
} else {
	$x5='btn_success';
}
?>
  <div class="contents"> 
   <div class="panel"> 
    <div style="padding: 8px;"> 
     <a href="newsclass.php" class="btn <?php echo $x51;?>">栏目管理</a> 
     <a href="news.php" class="btn <?php echo $x5;?>">文章管理</a> 
     <a href="news.php?action=newsadd" class="btn <?php echo $x52;?>">添加文章</a> 
     <a href="news.php?action=classc&CD_ClassID=1" class="btn <?php echo $x53;?>">公告管理</a> 
     <a href="news.php?action=newsggadd&bid=1" class="btn <?php echo $x54;?>">添加公告</a> 
    </div> 
   </div> 
  </div> 
<?php
switch($action) {
	case 'editsave':
		EditSave();
		break;
	case 'class':
		ClassMain();
		break;
	case 'editisindex':
		EditIsIndex();
		break;
	case 'del':
		Del();
		break;
	case 'classadd':
		ClassAdd();
		break;
	case 'newsadd':
		NewsAdd();
		break;
	case 'newsggadd':
		NewsAdd();
		break;
	case 'saveadd':
		SaveAdd();
		break;
	case 'newsisindex':
		NewsIsIndex();
		break;
	case 'newsdel':
		NewsDel();
		break;
	case 'newsalldel':
		NewsAllDel();
		break;
	case 'edit':
		Edit();
		break;
	case 'saveedit':
		SaveEdit();
		break;
	case 'keyword':
		$key=SafeRequest("key", "get");
		$sql="select * from " . tname('news') . " where CD_Name like '%" . $key . "%' order by CD_ID desc";
		main($sql, 20);
		break;
	case 'classc':
		$CD_ClassID=SafeRequest("CD_ClassID", "get");
		$sql="select * from " . tname('news') . " where CD_ClassID=" . $CD_ClassID . " order by CD_ID desc";
		main($sql, 20);
		break;
	default:
		main("select * from " . tname('news') . " where CD_ClassID<>1 order by CD_ID desc", 20);
		break;
}
?>
 </body>
</html>
<?php					
Function main($sql, $size) {
	global $db;
	$Arr=getpagerow($sql, $size); //sql,每页显示条数
	$result=$db->query($Arr[2]);
	$videonum=$db->num_rows($result);
?>
  <div class="contents"> 
   <div class="panel"> 
    <div class="panel-head"> 
     <strong> <?php 
$action=SafeRequest("action","get");
$CD_ClassID=SafeRequest("CD_ClassID","get");
if($action==""){echo "文章管理";
}elseif($CD_ClassID=="1"){echo "公告管理";
}else{
echo "文章管理";
}
?></strong> 
    </div> 
    <form method="get" action="news.php"> 
     <div class="search_type cc mb10"> 
      <div class="ul_wrap"> 
       <ul class="cc"> 
        <li> <label>关键字：</label> <input type="hidden" name="action" value="keyword" /> <input name="key" id="key" value="" type="text" class="input length_3" placeholder="搜索文章标题" /> &nbsp;&nbsp;<button class="btn mr20" type="submit">搜索</button> </li> 
        <li> <label>按分类：</label><select onchange="window.location.href=''+this.options[this.selectedIndex].value+'';"> <?php
$sqlclass="select * from " . tname('newsclass') . "";
$results=$db->query($sqlclass);
if($results) {
	while($row3=$db->fetch_array($results)) {
		if(SafeRequest("CD_ClassID", "get")==$row3['CD_ID']) {
			echo "<option value='?action=classc&CD_ClassID=" . $row3['CD_ID'] . "' selected='selected'>" . $row3['CD_Name'] . "</option>";
		} else {
			echo "<option value='?action=classc&CD_ClassID=" . $row3['CD_ID'] . "' >" . $row3['CD_Name'] . "</option>";
		}

	}
}
?> </select> </li> 
       </ul> 
      </div> 
     </div> 
    </form> 
    <form name="form" method="post" action="?action=newsalldel"> 
     <table class="table2" id="dellist"> 
      <tbody> 
       <tr> 
        <td width="100" align="left"> 序号</td> 
        <td align="left">文章标题</td> 
        <td align="left">分类栏目</td> 
        <td align="left">显示</td> 
        <td align="left">更新时间</td> 
        <td width="110" align="left">操作</td> 
       </tr> 
<?php
if($videonum==0)
	echo "<tr><td height='30' colspan='9' align='center' bgcolor='#FFFFFF' class='td_border'><br><br>没有数据<br><br><br></td></tr>";
if($result) {
	while($row=$db->fetch_array($result)) {
?>
       <tr> 
        <td width="100" align="left"><input type="checkbox" name="CD_ID[]" id="CD_ID" value="<?php echo $row['CD_ID']?>" class="checkbox"/><?php echo $row['CD_ID']?></td>
        <td align="left"><a href="../play/index.php?3,<?php echo $row['CD_ID']?>" target="_blank"><font color="<?php echo $row['CD_Color']?>"><?php echo ReplaceStr($row['CD_Name'],SafeRequest("key","get"),"<font color=red>".SafeRequest("key","get")."</font>")?></font></a></td>
        <td align="left"><?php
$res=$db->getrow("select CD_ID,CD_Name from " . tname('newsclass') . " where CD_ID=" . $row['CD_ClassID'] . "");
if($res) {
	echo "<a href='?action=classc&CD_ClassID=" . $res['CD_ID'] . "'>" . $res['CD_Name'] . "</a>";
} else {
	echo "暂无分类";
}
?></td> 
        <td align="left"><?php if($row['CD_IsIndex']==1){?><a title="隐藏" href="?action=newsisindex&CD_ID=<?php echo $row['CD_ID']?>&CD_IsIndex=0"><img src='images/no.gif' border='0'></a><?php }else{?><a title="显示" href="?action=newsisindex&CD_ID=<?php echo $row['CD_ID']?>&CD_IsIndex=1"><img src='images/yes.gif' border='0'></a><?php }?></td>
        <td align="left"><?php if(date("Y-m-d",strtotime($row['CD_AddTime']))==date('Y-m-d')){ echo "<font color=red>".date("Y-m-d",strtotime($row['CD_AddTime']))."</font>"; }else{ echo date("Y-m-d",strtotime($row['CD_AddTime'])); } ?></td>
        <td width="60" align="left"><a href="?action=edit&CD_ID=<?php echo $row['CD_ID']?>" class="btn">编辑</a> <a href="?action=newsdel&CD_ID=<?php echo $row['CD_ID']?>" onClick="return confirm('确定要删除吗？不可恢复！');" class="btn">删除</a></td>
       </tr> 
<?php
}
}
?>
       <tr> 
        <td height="35" colspan="12" align="left" bgcolor="#FAFBF7" class="td_border"> <label for="chkall"><input type="checkbox" id="chkall" onclick="CheckAll(this.form)" class="checkbox" />&nbsp;全选&nbsp;&nbsp; &nbsp;</label>
		<input name="button2" type="button" class="btn btn_submit J_ajax_submit_btn" value="批量设置" onclick="EditInfo(this.form,'news_pl.php')" /> 
		</td>
       </tr> 
      </tbody> 
     </table> 
    </form> 
   </div> 
  </div> 
  <div class="p10" style="margin-bottom: 0px;"> 
   <div class="pages"><?php echo $Arr[0];?></div>
  </div> 
<?php			
}
Function EditBoard($Arr, $ActionUrl, $ActionName) {
	global $db;
	$CD_ID=SafeRequest("CD_ID", "get");
	$CD_ClassID=$Arr[0];
	$CD_Name=$Arr[1];
	$CD_Intro=$Arr[2];
	$CD_IsIndex=$Arr[3];
	$CD_Color=$Arr[4];
	$CD_Time=$Arr[5];
	if(!IsNum($CD_IsIndex)) {
		$CD_IsIndex=0;
	}
?>
  <div class="contents"> 
   <div class="panel"> 
    <div class="panel-head"> 
     <strong> <?php
$action=SafeRequest("action", "get");
$bid=SafeRequest("bid", "get");
if($action=="") {
	echo "文章管理";
} elseif($action=="edit") {
	echo "编辑文章";
} elseif($action=="newsadd") {
	echo "添加文章";
} elseif($action=="newsggadd") {
	echo "添加公告";
} else {
	echo "文章管理";
}
if($bid=="1") {
	$CD_ClassID=1;
}
?></strong> 
    </div> 
    <form action="<?php echo $ActionUrl?>" method="post" name="form2"> 
     <table class="table3" id="dellist"> 
      <tbody> 
       <tr> 
        <td width="60" align="left">文章名称</td> 
        <td align="left"><input type="text" class="input input_hd length_6" name="CD_Name" id="CD_Name" value="<?php echo $CD_Name?>" /></td> 
       </tr> 
       <tr> 
        <td align="left">标题颜色</td> 
        <td align="left"> <select name="CD_Color"> <option value=""></option><option style="background-color:#00FF00;color: #00FF00" value="#00FF00"<?php if($CD_Color=="#00FF00"){echo " selected";} ?>>绿色</option><option style="background-color:#0000CC;color: #0000CC" value="#0000CC"<?php if($CD_Color=="#0000CC"){echo " selected";} ?>>深蓝</option><option style="background-color:#FFFF00;color: #FFFF00" value="#FFFF00"<?php if($CD_Color=="#FFFF00"){echo " selected";} ?>>黄色</option><option style="background-color:#FF33CC;color: #FF33CC" value="#FF33CC"<?php if($CD_Color=="#FF33CC"){echo " selected";} ?>>粉红</option><option style="background-color:#FF0000;color: #FF0000" value="#FF0000"<?php if($CD_Color=="#FF0000"){echo " selected";} ?>>红色</option><option style="background-color:#660099;color: #660099" value="#660099"<?php if($CD_Color=="#660099"){echo " selected";} ?>>紫色</option><option style="background-color:#FFFFFF;color: #FFFFFF" value="">无色</option> </select> </td> 
       </tr> 
       <tr> 
        <td align="left">所属栏目</td> 
        <td align="left"> <select name="CD_ClassID"><option value="0" selected>请选择栏目</option><?php
$sqlclass="select * from " . tname('newsclass') . " order by CD_ID asc";
$results=$db->query($sqlclass);
if($results) {
	while($row3=$db->fetch_array($results)) {
		if($CD_ClassID==$row3['CD_ID']) {
			echo "<option value='" . $row3['CD_ID'] . "' selected='selected'>" . $row3['CD_Name'] . "</option>";
		} else {
			echo "<option value='" . $row3['CD_ID'] . "' >" . $row3['CD_Name'] . "</option>";
		}

	}
}
?>
</select> </td> 
       </tr> 
       <tr> 
        <td align="left">是否显示</td> 
        <td align="left"> <input type="radio" name="CD_IsIndex" value="0"<?php if($CD_IsIndex==0){echo " checked";} ?>>&nbsp;显示&nbsp;&nbsp;<input type="radio" name="CD_IsIndex" value="1"<?php if($CD_IsIndex==1){echo " checked";} ?>>&nbsp;隐藏　<input type="checkbox" name="edittime" class="checkbox" id="edittime" value="1"  checked /><label for="edittime">更新时间</label></td> 
       </tr> 
       <tr> 
        <td align="left">文章内容</td> 
        <td align="left"> <script type="text/javascript" charset="gbk" src="editor/kindeditor.js"></script> <script type="text/javascript"> KE.show({
id : 'CD_Intro',
resizeMode : 1,
allowPreviewEmoticons : false,
allowUpload : true,
imageUploadJson : '../../news.php',
items : ['source', '|', 'fullscreen', 'undo', 'redo', 'print', 'cut', 'copy', 'paste',
'plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 'justifyright',
'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript',
'superscript', '|', 'selectall', '-',
'title', 'fontname', 'fontsize', '|', 'textcolor', 'bgcolor', 'bold',
'italic', 'underline', 'strikethrough', 'removeformat', '|', 'image',
'flash', 'media', 'advtable', 'hr', 'emoticons', 'link', 'unlink', '|', 'about']
});
</script><textarea style="width:620px; height:300px;" name="CD_Intro"><?php echo $CD_Intro?></textarea> </td> 
       </tr> 
       <tr> 
        <td height="35" colspan="2" align="left" bgcolor="#FAFBF7" class="td_border"><button type="submit" id="adddance" class="btn btn_submit J_ajax_submit_btn" style="margin-left: 80px;">确定提交</button></td> 
       </tr> 
      </tbody> 
     </table> 
    </form> 
   </div> 
  </div> 
<?php
}
function Edit() {
	global $db;
	$CD_ID=SafeRequest("CD_ID", "get");
	$sql="Select * from " . tname('news') . " where CD_ID=" . $CD_ID . "";
	if($row=$db->Getrow($sql)) {
		$Arr=array(
			$row['CD_ClassID'],
			$row['CD_Name'],
			$row['CD_Intro'],
			$row['CD_IsIndex'],
			$row['CD_Color'],
			$row['CD_AddTime']
		);
	}
	EditBoard($Arr, "?action=saveedit&CD_ID=" . $CD_ID . "", "编辑");
}

Function NewsAllDel() {
	global $db;
	$CD_ID=RequestBox("CD_ID");
	$sql="delete from " . tname('news') . " where CD_ID in ($CD_ID)";
	if($CD_ID=="0") {
		showmessage("出错了，请选择要删除的文章！", "", 1);
	} else {
		if($db->query($sql)) {
			showmessage("恭喜您，删除文章成功！", "news.php", 0);
		}
	}
}

Function NewsDel() {
	global $db;
	$CD_ID=SafeRequest("CD_ID", "get");
	$sql="delete from " . tname('news') . " where CD_ID='" . $CD_ID . "'";
	$db->query("delete from " . tname('comment') . " where cd_channel=6 and cd_dataid='" . $CD_ID . "'");
	if($db->query($sql)) {
		showmessage("恭喜您，删除文章成功！", "news.php", 0);
	}
}

//保存编辑
function SaveEdit() {
	global $db;
	$CD_ID=SafeRequest("CD_ID", "get");
	$CD_Name=SafeRequest("CD_Name", "post");
	$CD_ClassID=SafeRequest("CD_ClassID", "post");
	$CD_IsIndex=SafeRequest("CD_IsIndex", "post");
	$CD_Intro=SafeRequest("CD_Intro", "post");
	$CD_Color=SafeRequest("CD_Color", "post");
	$edittime=SafeRequest("edittime", "post");
	$CD_Time=SafeRequest("CD_Time", "post");
	$CD_HttpUrl=SafeRequest("CD_HttpUrl", "post");
	if($edittime==1) {
		$CD_AddTime=date('Y-m-d H:i:s');
	} else {
		$CD_AddTime=$CD_Time;
	}
	$sql="update " . tname('news') . " set CD_ClassID=" . $CD_ClassID . ",CD_Name='" . $CD_Name . "',CD_Intro='" . $CD_Intro . "',CD_IsIndex=" . $CD_IsIndex . ",CD_Color='" . $CD_Color . "',CD_AddTime='" . $CD_AddTime . "' where CD_ID=" . $CD_ID . "";
	if($db->query($sql)) {
		showmessage("恭喜您，编辑文章成功！", $_SERVER['HTTP_REFERER'], 0);
	} else {
		showmessage("出错了，编辑文章失败！", $_SERVER['HTTP_REFERER'], 0);
	}
}

function SaveAdd() {
	global $db;
	$CD_Name=SafeRequest("CD_Name", "post");
	$CD_ClassID=SafeRequest("CD_ClassID", "post");
	$CD_IsIndex=SafeRequest("CD_IsIndex", "post");
	$CD_Intro=SafeRequest("CD_Intro", "post");
	$CD_Color=SafeRequest("CD_Color", "post");
	$CD_AddTime=date('Y-m-d H:i:s');

	$sql="Insert " . tname('news') . " (CD_ClassID,CD_Name,CD_Intro,CD_Hits,CD_IsIndex,CD_IsBest,CD_Color,CD_AddTime) values (" . $CD_ClassID . ",'" . $CD_Name . "','" . $CD_Intro . "',0," . $CD_IsIndex . ",0,'" . $CD_Color . "','" . $CD_AddTime . "')";
	if($db->query($sql)) {
		if($CD_ClassID=="1"){
			showmessage("恭喜您，添加公告成功！", "news.php?action=classc&CD_ClassID=1", 0);
		}else{
			showmessage("恭喜您，添加文章成功！", "news.php", 0);
		}
	} else {
		if($CD_ClassID=="1"){
			showmessage("出错了，添加公告失败！", "news.php?action=classc&CD_ClassID=1", 0);
		}else{
			showmessage("出错了，添加文章失败！", "news.php", 0);
		}
	}
}

function NewsAdd() {
	$Arr=array("","","","","","","","","","","","","","","","","","","","","");
	EditBoard($Arr, "?action=saveadd", "添加");
}

Function ClassAdd() {
	global $db;
	$CD_Name=SafeRequest("CD_Name", "post");
	$CD_TheOrder=SafeRequest("CD_TheOrder", "post");
	if(!IsNul($CD_Name)) {
		showmessage("栏目名称不能为空！", "", 1);
	}
	if(!IsNum($CD_TheOrder)) {
		showmessage("排序不能为空！", "", 1);
	}
	$sql="Insert " . tname('newsclass') . " (CD_Name,CD_TheOrder,CD_IsIndex) values ('" . $CD_Name . "'," . $CD_TheOrder . ",0)";
	$db->query($sql);
	showmessage("恭喜您，添加栏目成功！", "?action=class", 0);
}

Function EditSave() {
	global $db;
	$CD_ID=RequestBox("CD_ID");
	if($CD_ID=="0") {
		showmessage("请选择要编辑的栏目！", "", 1);
	} else {
		$ID=explode(",", $CD_ID);
		for($i=0; $i<count($ID); $i++) {
			$CD_Name=SafeRequest("CD_Name" . $ID[$i] . "", "post");
			$CD_TheOrder=SafeRequest("CD_TheOrder" . $ID[$i] . "", "post");
			if(!IsNul($CD_Name)) {
				showmessage("分类名称不能为空！", "", 1);
			}
			if(!IsNum($CD_TheOrder)) {
				showmessage("排序不能为空！", "", 1);
			}
			$sql="update " . tname('newsclass') . " set CD_Name='" . $CD_Name . "',CD_TheOrder=" . $CD_TheOrder . " where CD_ID=" . $ID[$i] . "";
			$db->query($sql);
		}
		showmessage("恭喜您，编辑栏目成功！", "?action=class", 0);
	}
}

Function NewsIsIndex() {
	global $db;
	$CD_ID=SafeRequest("CD_ID", "get");
	$CD_IsIndex=SafeRequest("CD_IsIndex", "get");
	$sql="update " . tname('news') . " set CD_IsIndex=" . $CD_IsIndex . " where CD_ID=" . $CD_ID . "";
	if($db->query($sql)) {
		echo "<script>window.location='" . $_SERVER['HTTP_REFERER'] . "'</script>";
	}
}

Function EditIsIndex() {
	global $db;
	$CD_ID=SafeRequest("CD_ID", "get");
	$CD_IsIndex=SafeRequest("CD_IsIndex", "get");
	$sql="update " . tname('newsclass') . " set CD_IsIndex=" . $CD_IsIndex . " where CD_ID=" . $CD_ID . "";
	if($db->query($sql)) {
		echo "<script>window.location='" . $_SERVER['HTTP_REFERER'] . "'</script>";
	}
}

Function del() {
	global $db;
	$CD_ID=SafeRequest("CD_ID", "get");
	$array=array(
		'1'
	);
	if(in_array($CD_ID, $array)) {
		showmessage("删除失败,系统自带栏目您只可以编辑不可删除。", "?action=class", 0);
	}
	$sql="delete from " . tname('newsclass') . " where CD_ID='" . $CD_ID . "'";
	if($db->query($sql)) {
		die("<script>window.location='" . $_SERVER['HTTP_REFERER'] . "'</script>");
	}
}
?>
