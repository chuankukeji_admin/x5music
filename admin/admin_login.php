<?php
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
error_reporting(0);
include "../include/x5music.conn.php";
include "function_common.php";
$action=SafeRequest("action", "get");
if($action=="login") {
	$CD_Name=SafeRequest("CD_AdminUserName", "post");
	$CD_Pass=md5(SafeRequest("CD_AdminPassWord", "post"));
	$CD_Code=SafeRequest("CD_CheckCode", "post");
	$logtime=date('Y-m-d H:i:s');
	global $db;
	if(cd_webcodea=='yes') {
		if($CD_Code!=cd_webcodeb) {
			showmessage("登录失败,认证码错误！", "admin_login.php", 0);
		}
	}
	$sql="Select CD_ID from " . tname('admin') . " where CD_AdminUserName='" . $CD_Name . "' and CD_AdminPassWord='" . $CD_Pass . "' and CD_IsLock=0";
	$CD_ID=$db->Getone($sql); //从数据库中返回1
	if($CD_ID) {
		$db->query("update " . tname('admin') . " set CD_LoginNum =CD_LoginNum +1,CD_LoginIP='" . $_SERVER['SERVER_ADDR'] . "',CD_LastLogin ='" . $logtime . "' where CD_ID='" . $CD_ID . "'");
		$row=$db->Getrow("Select * from " . tname('admin') . " where CD_ID='" . $CD_ID . "' ");
		setcookie("CD_AdminID", $row['CD_ID']);
		setcookie("CD_AdminUserName", $row['CD_AdminUserName']);
		setcookie("CD_AdminPassWord", md5($row['CD_AdminPassWord']));
		setcookie("CD_Permission", $row['CD_Permission']);
		setcookie("CD_Login", md5($row['CD_ID'] . $row['CD_AdminUserName'] . md5($row['CD_AdminPassWord']) . $row['CD_Permission']));
		//showmessage("成功登录，正在转向后台管理主页！", "admin_index.php", 0);
		echo'<script language="javascript">window.parent.location.href="admin_index.php";</script>';
	} else {
		showmessage("登录失败请确认输入的是正确信息以及帐号是否开启!", "admin_login.php", 0);
	}
} elseif($action=="logout") {
	setcookie("CD_AdminID", "", time()-1);
	setcookie("CD_AdminUserName", "", time()-1);
	setcookie("CD_AdminPassWord", "", time()-1);
	setcookie("CD_Permission", "", time()-1);
	setcookie("CD_Login", "", time()-1);
	showmessage("您已经安全退出管理中心！", "admin_login.php", 0);
}
//if($_COOKIE['CD_AdminID']<>""){echo"<script>window.location.href='admin_index.php';</script>";}
?>
<!DOCTYPE html>
<html>
 <head> 
  <meta charset="gbk" /> 
  <meta name="renderer" content="webkit" /> 
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" /> 
  <title>登录x5Music后台管理系统</title> 
  <link href="css/add.css" rel="stylesheet" /> 
  <style type="text/css">
body {background-image: url(images/bg.png);background-size: cover, 100%;background-attachment: fixed;background-position: 50% 50%;}
.table3{width: 100%;max-width: 100%;border-collapse: collapse;border-spacing: 0;}
.table3 tr:hover{text-decoration: none;background-color: #e6f2fb;}
.table3 td{font-size: 12px;line-height:25px;border-top: 1px solid #ddd;padding: 5px;vertical-align: top;border-right: solid 1px #ddd;background-color: #fff;}
.table3 th{border-bottom: 2px solid #ddd;vertical-align: bottom;padding: 2px;text-align: left;}
</style>
<script type="text/javascript">
function CheckLogin(){
	if(document.form.CD_AdminUserName.value==""){
		alert("请输入登录帐号！");
		document.form.CD_AliasName.focus();
		return false;
	}
	else if(document.form.CD_AdminPassWord.value==""){
		alert("请输入登录密码！");
		document.form.CD_AdminPassWord.focus();
		return false;
	}
	<?php if(cd_webcodea=='yes'){ ?>
	else if(document.form.CD_CheckCode.value==""){
		alert("后台认证码！");
		document.form.CD_CheckCode.focus();
		return false;
	}
	<?php } ?>
	else {
	        return true;
	}
}
</script> 
 </head> 
 <body> 
  <div class="content" style="width: 400px;padding-top: 120px;"> 
   <div class="panel"> 
    <div class="panel-head"> 
     <strong>登录x5Music后台管理系统</strong> 
    </div> 
    <table class="table3"> 
     <tbody> 
      <form name="form" action="?action=login" method="post">
      <tr> 
       <td width="65" align="left">登录帐号</td> 
       <td align="left"><input name="CD_AdminUserName" type="text" id="admin_name" class="input input_hd length_3" value="" />&nbsp;&nbsp;*请输入管理员帐号</td> 
      </tr> 
      <tr> 
       <td align="left">登录密码</td> 
       <td align="left"><input name="CD_AdminPassWord" type="password" id="admin_pwd" class="input input_hd length_3" value="" />&nbsp;&nbsp;*请输入密码</td> 
      </tr> 
	  <?php 
	  if(cd_webcodea=="yes"){
		echo '<tr> 
       <td align="left">后台认证码</td> 
       <td align="left"><input name="CD_CheckCode" type="password" id="admin_code" class="input input_hd length_2" value="" />&nbsp;&nbsp;*请输入后台认证码</td> 
      </tr> ';  
	  }
	  ?>
      <tr> 
       <td height="35" colspan="2" align="left" bgcolor="#FAFBF7" class="td_border">
	   <input name="b112" type="submit" class="btn btn_submit mr10 J_ajax_submit_btn" value="立即登录后台" style="margin-left: 100px;" onclick="return CheckLogin();"/> 
	   <a style="font-size: 12px;float: right;color: #000;" target="_blank" href="http://x5mp3.com">官方网站</a>
	   <a style="font-size: 12px;float: right;color: #000;" target="_blank" href="../">首页　|　</a>
	   </td> 
      </tr>  
	  </form> 
     </tbody> 
    </table> 
   </div> 
   <a style="padding-top: 10px;font-size: 12px;float: right;color: #999;" target="_blank" href="http://x5mp3.com">基于x5Music框架构建</a>
  </div>  
 </body>
</html>