<?php if(!$userlogined==true){showmessage("出错了，请登陆后再操作。","login.php",0);}?>
<div class="user">
<div class="user_center">
<?php include_once('source/space_menu.php');?>
<div class="user_main">
<div class="uMain_content">
<div class="main_nav">
<ul>
<li class="recommend"><a href="<?php echo rewrite_url('space.php?do=music');?>">我上传的</a></li>
<li class="like"><a href="<?php echo rewrite_url('space.php?do=music&view=fav');?>">我收藏的</a></li>
<li class="download"><a href="<?php echo rewrite_url('space.php?do=music&view=down');?>">我下载的</a></li>
<li class="me_query"><a <?php if($view=="" and $orderby==""){ echo ' class="on"'; } ?> href="<?php echo rewrite_url('space.php?do=special');?>">我的专辑</a></li>
<li class="message"><a <?php if($view=="add"){ echo ' class="on"'; } ?> <?php if($view=="music"){ echo ' class="on"'; } ?>href="<?php echo rewrite_url('space.php?do=special&view=add');?>">新建专辑</a></li>
</ul>
</div>




<?php if($view=="add"){ if(!$userlogined==true){showmessage("出错了，请登陆后再操作。","space.php?do=login&refer=".urlencode("space.php?do=special&view=add"),0);}?>
<div class="main_nav2">
<ul>
<span style='color:red;'>一张精心挑选的专辑，能够感动无数人。赶快新建一张吧！</span> 
</ul>
</div>
<div id="favoritesList" class="minHeight500">
<div class="private_dance_list">

<form name="form1" method="post" action="do.php?ac=special&op=add" enctype="multipart/form-data" onsubmit="return doErrTest()">
<div class="name">
专辑名称：<input type="text" name="CD_Name" id="ReI_1" onblur="doRe('Re_1',1,this)" value="" class="input_normal" style="width: 250px;" />
<span id="Re_1" style="color: #808080; margin-left: 5px; font-size: 12px;"></span>
</div><br />
<div class="name">
所属歌手：<input type="text" name="CD_Singer" id="ReI_2" onblur="doRe('Re_2',2,this)" value="" class="input_normal" style="width: 100px;" />
<span id="Re_2" style="color: #808080; margin-left: 5px; font-size: 12px;"></span>
</div><br />
<div class="name">
制作发行：<input type="text" name="CD_GongSi" id="ReI_3" onblur="doRe('Re_3',3,this)" value="" class="input_normal" style="width: 100px;" />
<span id="Re_3" style="color: #808080; margin-left: 5px; font-size: 12px;"></span>
</div><br />	  
<div class="name">
所属类型：<select name="CD_ClassID" id="CD_ClassID" id="ReI_4"><option value="0">选择类型</option>
<?php
$sqlclass="select * from ".tname('class')." where CD_FatherID=0 and CD_IsHide=0";
$results=$db->query($sqlclass);
if($results){
while ($row3=$db->fetch_array($results)){
echo "<option value='".$row3['CD_ID']."' >".$row3['CD_Name']."</option>";
}
}?></select>&nbsp;&nbsp;&nbsp;所属语言：<select name="CD_YuYan"><option value="">选择语言</option>
<option value="国语">国语</option>
<option value="粤语">粤语</option>
<option value="英文">英文</option>
<option value="日韩">日韩</option>
<option value="韩文">韩文</option>
<option value="日语">日语</option>
<option value="国/粤语">国/粤语</option>
<option value="中/英文">中/英文</option>
<option value="中/日文">中/日文</option>
<option value="中/韩文">中/韩文</option>
</select>&nbsp;&nbsp;&nbsp;
<span id="Re_4" style="color: #808080; margin-left: 5px; font-size: 12px;"></span>
</div><br />	
<div class="name">
<span style="float: left;">专辑封面：<input type="text" name="CD_Pic" value="" class="input_normal" style="width: 250px;" id="ReI_5"/></span><div class="input_msg"> <span class="button2-main"><span><button type="button" onclick="pop.up('上传封面', '<?php echo cd_webpath?>include/upload/?ac=malbum&f=form1.CD_Pic', '406px', '180px', '140px');">上传封面</button></span></span> </div>
<span id="Re_5" style="color: #808080; margin-left: 5px; font-size: 12px;"></span>
</div>
<div class="name" style="margin-top: 40px;">
专辑简介：<span id="Re_6" style="color: #808080; margin-left: 5px; font-size: 12px;"></span><br /> 
<textarea name="CD_Intro" id="ReI_6" onblur="doRe('Re_6',6,this)" class="input_normal" style="width: 400px; height: 100px;margin-top: 5px;"></textarea>
</div><br />	
<div class="name">
添加会员：<?php echo $x5music_com_username?>
</div><br />	
<input type="submit" name="submitspecial" value="添加专辑" class="button square lv" />
</form>
</div> 

</div>
<script type="text/javascript">
var $id = function (id) {
    return typeof id == "string" ? document.getElementById(id) : id;  
};
function editMe(o) {
  o.style.border = "1px #febb00 solid"
    o.style.backgroundColor = "#fffbe7";
}
function blurMe(o) {
  o.style.border = "1px solid #cccccc"
    o.style.backgroundColor = "transparent";
}
function doLength(intS, intB, oTd, strReg) {
  if (oTd.value.length < intS || oTd.value.length > intB) {
    $id(strReg).innerHTML = " <img src='image/check_error.gif'/>&nbsp;&nbsp;<span class='alert'>长度在 " + intS + "-" + intB + " 个字符之间</span>";
    editMe(oTd);
  }
}
function doEmpty(oTd, strReg, msg) {
  if (oTd.value == "") {
    $id(strReg).innerHTML = msg;
    editMe(oTd);
  }
}
doRe = function (strReg, intType, oTd) {
  if (intType == 1) {
    $id(strReg).innerHTML = " <img src='image/check_right.gif'/>";
    blurMe(oTd);
    doLength(3, 30, oTd, strReg);
    doEmpty(oTd, strReg, " <img src='image/check_error.gif'/>&nbsp;&nbsp;<span class='alert'>专辑名称不允许为空</span>");
  }
  if (intType == 2) {
    $id(strReg).innerHTML = " <img src='image/check_right.gif'/>";
    blurMe(oTd);
    doLength(3, 30, oTd, strReg);
    doEmpty(oTd, strReg, " <img src='image/check_error.gif'/>&nbsp;&nbsp;<span class='alert'>歌手不允许为空</span>");
  }
    if (intType == 3) {
    $id(strReg).innerHTML = " <img src='image/check_right.gif'/>";
    blurMe(oTd);
    doLength(3, 30, oTd, strReg);
    doEmpty(oTd, strReg, " <img src='image/check_error.gif'/>&nbsp;&nbsp;<span class='alert'>制作发行不允许为空</span>");
  }
  if (intType == 4) {
    $id(strReg).innerHTML = " <img src='image/check_right.gif'/>";
    blurMe(oTd);
    doLength(1, 10, oTd, strReg);
    doEmpty(oTd, strReg, " <img src='image/check_error.gif'/>&nbsp;&nbsp;<span class='alert'>请选择所属类型</span>");
  }
  if (intType == 5) {
    $id(strReg).innerHTML = " <img src='image/check_right.gif'/>";
    blurMe(oTd);
    doLength(1, 200, oTd, strReg);
    doEmpty(oTd, strReg, " <img src='image/check_error.gif'/>&nbsp;&nbsp;<span class='alert'>请上传封面图片</span>");
  }
  if (intType == 6) {
    $id(strReg).innerHTML = " <img src='image/check_right.gif'/>";
    blurMe(oTd);
    doLength(1, 500, oTd, strReg);
    doEmpty(oTd, strReg, " <img src='image/check_error.gif'/>&nbsp;&nbsp;<span class='alert'>请填写专辑简介</span>");
  }
}

doErrTest = function () {
  var t = true;
  for (i = 1; i < 7; i++) {
    try {
      doRe("Re_" + i, i, $id("ReI_" + i));
    } catch (e) {}
    if ((($id("Re_" + i).innerHTML).indexOf("right")) < 0)
      t = false;
  }
  return t;
}

</script>
<?php }elseif($view=="music"){ ?>
<script type="text/javascript">
function checkdjlist() {
if (document.form1.djlist.value==""){
alert("请选择要加入的歌曲");
document.form1.djlist.focus();
return false;
}

}
function checkzjlist() {
if (document.form1.zjlist.value==""){
alert("请选择要删除的歌曲");
document.form1.zjlist.focus();
return false;
}

}
</script>
<?php
$sql="select * from ".tname('special')." where CD_ID='$id'";
if($row=$db->Getrow($sql)){
$CD_Name=$row['CD_Name'];
$CD_User=$row['CD_User'];
if($CD_User==$x5music_com_username){
?>
<div class="main_nav2">
<ul>
<span style='color:red;'><?php echo $CD_Name;?></span>  - 编辑歌曲：编辑后可能要等管理员审核更新才能看到最新的歌曲列表。
</ul>
</div>
<div id="favoritesList" class="minHeight500">
<div class="private_dance_list">



<form name="form1" method="post" action="do.php?ac=special&op=music" class="c_form">
<input type="hidden" name="zjid" value="<?php echo $id ;?>" />
<h3 class="l_status"></h3>
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td vAlign="top"><table cellSpacing="0" cellPadding="0" width="90%" border="0">
<tr align="center">
<td height="35">我上传的歌曲列表（<font color='red'>只显示尚未加入专辑的歌曲</font>）</td>
<td style="LINE-HEIGHT: 24px">&nbsp;</td>
<td>专辑拥有歌曲列表</td>
</tr>
<tr align="center">
<td width="42%" height="113"><select name="djlist[]" id="djlist" style="WIDTH: 320px; HEIGHT: 350px" multiple="multiple" size="1">
<?php
$sqlclass="select CD_ID,CD_Name from ".tname('dj')." where CD_User='$x5music_com_username' and CD_Deleted=0 and CD_Passed=0 and CD_SpecialID=0 order by CD_AddTime desc";
$results=$db->query($sqlclass);
if($results){
while ($row1=$db->fetch_array($results)){
	echo "<option value='".$row1['CD_ID']."' >".$row1['CD_Name']."</option>";
}
}?>                                                                                                                                                                                                                                          </option>
</select></td>
<td width="16%">&nbsp;<input type="submit" name="submitadd" class="button square lv" type="button" value="加入=>" onClick="return checkdjlist();">&nbsp;
<br><br>&nbsp;<input type="submit" name="submitdll" type="button" class="button square lv" value="<=删除" onClick="return checkzjlist();">&nbsp;<br><br><font color='red'>按住Shift键可以多选</font></td>
<td width="42%"><select name="zjlist[]" id="zjlist" style="WIDTH: 320px; HEIGHT: 350px" multiple="multiple" size="1">
<?php
$sqlclass="select CD_ID,CD_Name from ".tname('dj')." where CD_SpecialID='$id' and CD_Deleted=0 and CD_Passed=0 order by CD_AddTime desc";
$results=$db->query($sqlclass);
if($results){
while ($row2=$db->fetch_array($results)){
	echo "<option value='".$row2['CD_ID']."' >".$row2['CD_Name']."</option>";
}
}?>
</select></td>
</tr></table>
</td>
</tr>
</table>
</form>
</div></div>
<?php
}else{
echo "<div class='c_form'>出错了，您不能编辑别人的专辑。</div>";
}
}else{ 
echo "<div class='c_form'>出错了，您要查看的专辑不存在。</div>";
} ?>
<?php }elseif($view=="all"){ ?>

<?php }else{ ?>
<!--我的音乐专辑列表-->
<div class="main_nav2">
<ul>
必须有上传过作品到本站的才能制做专辑，点击 <span style='color:red;'>编辑</span> 可以编辑歌曲列表。编辑后要等待管理员审核更新才能看到最新的歌曲列表。
</ul>
</div>
<div id="favoritesList" class="minHeight500">
<div class="private_dance_list">
<ul id="list">
<li class="title">
<div class="song">
专辑名称
</div>
<div class="time">
制作时间
</div>
<div class="file">
语言
</div>
<div class="add">
审核
</div>
<div class="down">
人气
</div>
<div class="deleting">
编辑
</div></li>

<?
global $db;
$sql="select * from ".tname('special')." where CD_User='$x5music_com_username' order by CD_AddTime desc";
$Arr=getuserpage($sql,15);//sql,每页显示条数
$result=$db->query($Arr[2]);
$num=$db->num_rows($result);
if($num==0) echo "<div class=\"private_dance_list\"><div class=\"nothing\">没有相关专辑, 赶快制作一张吧! ^_^</div></div>";
if($result){
while ($row = $db ->fetch_array($result)){
$a=$a+1;

echo "<li>\n";
echo "     <div class=\"song\">\n";
echo "      <div class=\"aleft\">\n";
echo "	  <img <img width=\"20\" height=\"20\" src=\"".LinkPicUrl($row['CD_Pic'])."\" onerror=\"this.onerror=null;this.src='image/nopic.png'\">\n";
echo "       <a class=\"mname\" href=\"".LinkUrl("special",2,1,$row['CD_ID'])."\" target=\"p\">".$a.".&nbsp;&nbsp;".$row["CD_Name"]."</a>\n";
echo "      </div>\n";
echo "     </div>\n";
echo "     <div class=\"time\">\n";
echo "      ".datetime($row['CD_AddTime'])."\n";
echo "     </div>\n";
echo "     <div class=\"file\">\n";
if($row["CD_YuYan"]){echo "      ".$row["CD_YuYan"]."\n";}else{echo "未知";}
echo "     </div>\n";
echo "     <div class=\"add\">\n";
if($row["CD_Passed"]==1){ echo "<font color='red'> 审核中</font></div>"; }else{ echo " 已审核</div>"; }
echo "     <div class=\"down\">\n";
echo "      ".$row["CD_Hits"]."\n";
echo "     </div>\n";
echo "     <div class=\"action\">\n";
echo "      <a class=\"edit\" title=\"编辑内容\" href=\"".rewrite_url('space.php?do=special&view=music&id='.$row["CD_ID"])."\"></a>\n";
echo "     </div>\n";
echo "</li>\n";

}
}
?>

</ul>
</div>

<div class="page"> 
<div class="pages">
<?php echo $Arr[0];?>
</div>
</div> 

</div>
<?php } ?>
</div>
</div>
</div>
</div>