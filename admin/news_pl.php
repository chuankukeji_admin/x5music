<?php
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
error_reporting(0);
include "../include/x5music.conn.php";
include "function_common.php";
admincheck(3);
if(Smallfivesolution()==$_SERVER['SERVER_NAME']) {
} else {
	echo Smallfivesq();
	exit();
}
$action=SafeRequest("action", "get");
?>
<!DOCTYPE html>
<html>
 <head> 
  <meta charset="gbk" /> 
  <meta name="renderer" content="webkit" /> 
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" /> 
  <title>x5Music 后台管理中心 -x5mp3.com</title> 
  <link href="css/add.css" rel="stylesheet" /> 
  <style type="text/css">
.table2{width: 100%;max-width: 100%;border-collapse: collapse;border-spacing: 0;}
.table2 td{font-size: 12px;border-top: 1px solid #ddd;padding: 8px;vertical-align: top;}
.table2 tr:hover{text-decoration: none;background-color: #e6f2fb;}
.table2 th{border-bottom: 2px solid #ddd;vertical-align: bottom;padding: 2px;text-align: left;}
.table3{width: 100%;max-width: 100%;border-collapse: collapse;border-spacing: 0;}
.table3 tr:hover{text-decoration: none;background-color: #e6f2fb;}
.table3 td{font-size: 12px;line-height:25px;border-top: 1px solid #ddd;padding: 5px;vertical-align: top;border-right: solid 1px #ddd;}
.table3 th{border-bottom: 2px solid #ddd;vertical-align: bottom;padding: 2px;text-align: left;}
</style> 
  <script language="javascript">
function SelectAll() {
	for (var i = 0; i < document.myform.CD_ClassID.length; i++) {
		document.myform.CD_ClassID.options[i].selected = true;
	}
}
function UnSelectAll() {
	for (var i = 0; i < document.myform.CD_ClassID.length; i++) {
		document.myform.CD_ClassID.options[i].selected = false;
	}
}
function $(vId) {
	return document.all ? document.all[vId] : document.getElementById(vId);
}
function switchSupport(vTribe, vFlag) {
	vTribes = {
		'ModifyColor' : '1',
		'ModifyClass' : '2',
		'ModifyIsIndex' : '3',
		'ModifyDeleted' : '4'
	};
	
	for (var i = 0; i < 11; i++) {
		try {
			$('supportTribe' + vTribes[vTribe]).style.color = vFlag ? 'red' : '#000';
			$('supportTitle' + vTribes[vTribe]).style.color = vFlag ? 'black' : '#000';
		} catch (e) {}
		try {
			$('myform').elements['support[' + vTribe + '][soldier][' + i + ']'].disabled = vFlag ? false : true;
			$('myform').elements['support[' + vTribe + '][soldier][' + i + ']'].style.borderColor = vFlag ? '#F93' : '#CCC';
		} catch (e) {}
		try {
			$('myform').elements['support[' + vTribe + '][level][' + i + ']'].disabled = vFlag ? false : true;
		} catch (e) {}
	}
}

</script> 
 </head> 
 <body>
<?php
switch($action) {
	case 'edit':
		Edit();
		break;
	default:
		Main();
		break;
}
?>
</body>
</html>
<?php
Function Main() {
	global $db;
	$id=SafeRequest("id", "get");
?> 
  <div class="contents"> 
   <div class="panel"> 
    <div style="padding: 8px;"> 
     <a href="newsclass.php" class="btn">栏目管理</a> 
     <a href="news.php" class="btn ">文章管理</a> 
     <a href="news.php?action=newsadd" class="btn ">添加文章</a> 
     <a href="news.php?action=classc&CD_ClassID=1" class="btn ">公告管理</a> 
     <a href="news.php?action=newsggadd&bid=1" class="btn ">添加公告</a> 
    </div> 
   </div> 
  </div>
  <div class="contents"> 
   <div class="panel"> 
    <div class="panel-head"> 
     <strong>若要批量修改某个属性的值，请先选中其左侧的复选框，然后再设定属性值。 </strong> 
    </div> 
    <form method="post" id="myform" name="myform" action="?action=edit"> 
     <table class="table3"> 
      <tbody> 
       <tr> 
        <td align="left"><input type='radio' id='BatchType1' name='BatchType' value='1' <?php if($id<>''){echo ' checked';}?>></td>
        <td width="120" align="left"><label for="BatchType1">修改指定文章ID</label></td> 
        <td align="left"><input type="text" id="CD_ID" name="CD_ID" class="input input_hd length_6" value="<?php echo $id;?>"></td>
       </tr> 
       <tr> 
        <td width="30" align="center"><input type="checkbox" name="ModifyColor" value="Yes" id="support[ModifyColor][is_able]" onclick="switchSupport('ModifyColor', this.checked)" /></td> 
        <td width="100" align=" left" id="supportTribe1"><label for="support[ModifyColor][is_able]">标题颜色</label></td> 
        <td><select name="CD_Color" id="support[ModifyColor][soldier][0]" disabled=""> 
        <option value=""></option> 
        <option style="background-color:#00FF00;color: #00FF00" value="#00FF00">绿色</option>
        <option style="background-color:#0000CC;color: #0000CC" value="#0000CC">深蓝</option>
        <option style="background-color:#FFFF00;color: #FFFF00" value="#FFFF00">黄色</option>
        <option style="background-color:#FF33CC;color: #FF33CC" value="#FF33CC">粉红</option>
        <option style="background-color:#FF0000;color: #FF0000" value="#FF0000">红色</option>
        <option style="background-color:#660099;color: #660099" value="#660099">紫色</option>
        <option style="background-color:#FFFFFF;color: #FFFFFF" value="">无色</option> 
        </select>
        </td> 
       </tr> 
       <tr> 
        <td width="30" align="center"><input type="checkbox" name="ModifyClass" value="Yes" id="support[ModifyClass][is_able]" onclick="switchSupport('ModifyClass', this.checked)" /></td> 
        <td width="100" align=" left" id="supportTribe2"><label for="support[ModifyClass][is_able]">移动栏目</label></td> 
        <td><select name="CD_Class" id='support[ModifyClass][soldier][0]' disabled>
<?php
$sqlclass="select * from " . tname('newsclass') . "";
$results=$db->query($sqlclass);
if ($results) {
while ($row3=$db->fetch_array($results)) {
if ($x5music_CD_ClassID==$row3['CD_ID']) {
echo "<option value='" . $row3['CD_ID'] . "' selected='selected'>" . $row3['CD_Name'] . "</option>";
} else {
echo "<option value='" . $row3['CD_ID'] . "' >" . $row3['CD_Name'] . "</option>";
}
}
}
?>
</select></td> 
       </tr> 
       <tr> 
        <td width="30" align="center"><input type="checkbox" name="ModifyIsIndex" value="Yes" id="support[ModifyIsIndex][is_able]" onclick="switchSupport('ModifyIsIndex', this.checked)" /></td> 
        <td width="100" align=" left" id="supportTribe3"><label for="support[ModifyIsIndex][is_able]">是否显示</label></td> 
        <td> <input name="CD_IsIndex" type="radio" id="support[ModifyIsIndex][soldier][0]" value="0" checked="" disabled="" />&nbsp;显示&nbsp; <input name="CD_IsIndex" type="radio" id="support[ModifyIsIndex][level][0]" value="1" disabled="" />&nbsp;隐藏</td> 
       </tr> 
       <tr> 
        <td width="30" align="center"><input type="checkbox" name="ModifyDeleted" value="Yes" id="support[ModifyDeleted][is_able]" onclick="switchSupport('ModifyDeleted', this.checked)" /></td> 
        <td width="100" align=" left" id="supportTribe4"><label for="support[ModifyDeleted][is_able]">删除文章</label></td> 
        <td> &nbsp;&nbsp;<font color="#d01f3c">注意：批量删除不可恢复，请慎重！</font> </td> 
       </tr> 
       <tr> 
        <td height="35" colspan="3" align="left" bgcolor="#FAFBF7" class="td_border"> <button type="submit" id="adddance" class="btn btn_submit J_ajax_submit_btn" style="margin-left: 10px;">确定提交</button>  </td> 
       </tr> 
      </tbody> 
     </table> 
    </form>
   </div> 
  </div> 
<?php
}
Function Edit() {
	global $db;
	$x5music_Com_CD_ID=SafeRequest('CD_ID', 'post');
	$x5music_Com_BatchType=SafeRequest('BatchType', 'post');
	$x5music_Com_Color=SafeRequest('CD_Color', 'post');
	$x5music_Com_Class=SafeRequest('CD_Class', 'post');
	$x5music_Com_IsIndex=SafeRequest('CD_IsIndex', 'post');
	$x5music_Com_ModifyColor=SafeRequest('ModifyColor', 'post');
	$x5music_Com_ModifyClass=SafeRequest('ModifyClass', 'post');
	$x5music_Com_ModifyDeleted=SafeRequest('ModifyDeleted', 'post');
	$x5music_Com_ModifyIsIndex=SafeRequest('ModifyIsIndex', 'post');
	If($x5music_Com_BatchType=='1') {
		$sql1="CD_ID in ($x5music_Com_CD_ID)";
	}
	If($x5music_Com_ModifyClass=='Yes') {
		if($db->query($sql='update ' . tname('news') . " set CD_ClassID='" . $x5music_Com_Class . "' where " . $sql1 . '')) {
		}
	}
	If($x5music_Com_ModifyColor=='Yes') {
		if($db->query($sql='update ' . tname('news') . " set CD_Color='" . $x5music_Com_Color . "' where " . $sql1 . '')) {
		}
	}
	If($x5music_Com_ModifyIsIndex=='Yes') {
		if($db->query($sql='update ' . tname('news') . ' set CD_IsIndex=' . $x5music_Com_IsIndex . ' where ' . $sql1 . '')) {
		}
	}
	If($x5music_Com_ModifyDeleted=='Yes') {
		if($db->query($sql='delete from ' . tname('news') . ' where ' . $sql1 . '')) {
		}
	}
	showmessage('恭喜您，批量设置数据成功！', 'news.php', 0);
}
?>