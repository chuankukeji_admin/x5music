<?php
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
error_reporting(0);
include "../include/x5music.conn.php";
include "function_common.php";
admincheck(3);
$action=SafeRequest("action", "get");
$CD_ClassID=SafeRequest("CD_ClassID", "get");
?>
<!DOCTYPE html>
<html>
 <head> 
  <meta charset="gbk" /> 
  <meta name="renderer" content="webkit" /> 
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" /> 
  <title>x5Music 后台管理中心 -x5mp3.com</title> 
  <link href="css/add.css" rel="stylesheet" /> 
  <script type="text/javascript" src="js/ajax.js"></script> 
  <script type="text/javascript" src="<?php echo cd_webpath?>user/static/space/layer/jquery.js"></script> 
  <script type="text/javascript" src="<?php echo cd_webpath?>user/static/space/layer/lib.js"></script> 
  <script type="text/javascript">
var pop = {
	up : function (text, url, width, height, top) {
		$.layer({
			type : 2,
			maxmin : true,
			title : text,
			iframe : {
				src : url
			},
			area : [width, height],
			offset : [top, '50%'],
			shade : [0]
		});
	}
}
function MM_jumpMenu(targ, selObj, restore) { //v3.0
	eval(targ + ".location='" + selObj.options[selObj.selectedIndex].value + "'");
	if (restore)
		selObj.selectedIndex = 0;
}
</script> 
  <style type="text/css">
.table2{width: 100%;max-width: 100%;border-collapse: collapse;border-spacing: 0;}
.table2 td{font-size: 12px;border-top: 1px solid #ddd;padding: 8px;vertical-align: top;}
.table2 tr:hover{text-decoration: none;background-color: #e6f2fb;}
.table2 th{border-bottom: 2px solid #ddd;vertical-align: bottom;padding: 2px;text-align: left;}
.table3{width: 100%;max-width: 100%;border-collapse: collapse;border-spacing: 0;}
.table3 tr:hover{text-decoration: none;background-color: #e6f2fb;}
.table3 td{font-size: 12px;line-height:25px;border-top: 1px solid #ddd;padding: 5px;vertical-align: top;border-right: solid 1px #ddd;}
.table3 th{border-bottom: 2px solid #ddd;vertical-align: bottom;padding: 2px;text-align: left;}
</style> 
 </head> 
 <body> 
<?php
if($action=="") {
	$x5='btn_success';
} elseif($action=="add") {
	$x51='btn_success';
} elseif($action=="pass") {
	$x52='btn_success';
} elseif($action=="isbest") {
	$x53='btn_success';
} else {
	$x5='btn_success';
}
?>
  <div class="contents"> 
   <div class="panel"> 
    <div style="padding: 8px;"> 
     <a href="malbum.php" class="btn <?php echo $x5;?>">所有音乐专辑</a> 
     <a href="malbum.php?action=add" class="btn <?php echo $x51;?>">添加专辑</a> 
     <a href="malbum.php?action=pass" class="btn <?php echo $x52;?>">审核专辑</a> 
     <a href="malbum.php?action=isbest" class="btn <?php echo $x53;?>">推荐专辑</a> 
    </div> 
   </div> 
  </div> 
<?php
switch($action) {
	case 'add':
		Add();
		break;
	case 'saveadd':
		SaveAdd();
		break;
	case 'edit':
		Edit();
		break;
	case 'saveedit':
		SaveEdit();
		break;
	case 'del':
		Del();
		break;
	case 'alldel':
		AllDel();
		break;
	case 'editisbest':
		EditIsBest();
		break;
	case 'editpassed':
		EditPassed();
		break;
	case 'class':
		$CD_ClassID=SafeRequest("CD_ClassID", "get");
		main("select * from " . tname('special') . " where CD_ClassID=" . $CD_ClassID . " order by CD_AddTime desc", 20);
		break;
	case 'keyword':
		$key=SafeRequest("key", "get");
		main("select * from " . tname('special') . " where CD_Name like '%" . $key . "%' or CD_User like '%" . $key . "%' order by CD_AddTime desc", 20);
		break;
	case 'pass':
		main("select * from " . tname('special') . " where CD_Passed=1 order by CD_AddTime desc", 20);
		break;
	case 'isbest':
		main("select * from " . tname('special') . " where CD_IsBest=0 order by CD_AddTime desc", 20);
		break;
	case 'singer':
		$CD_SingerID=SafeRequest("CD_SingerID", "get");
		main("select * from " . tname('special') . " where CD_Singer=" . $CD_SingerID . " order by CD_AddTime desc", 20);
		break;
	default:
		main("select * from " . tname('special') . " order by CD_AddTime desc", 20);
		break;
}
?>
 </body>
</html>
<?php
function EditBoard($Arr, $ActionUrl, $ActionName) {
	global $db;
	$CD_ClassID=$Arr[0];
	$CD_Name=$Arr[1];
	$CD_User=$Arr[2];
	$CD_Pic=$Arr[3];
	$CD_GongSi=$Arr[4];
	$CD_YuYan=$Arr[5];
	$CD_Intro=$Arr[6];
	$CD_IsBest=$Arr[7];
	$CD_Time=$Arr[8];
	$CD_Passed=$Arr[9];
	$CD_Hits=$Arr[10];
	$CD_Singer=$Arr[11];
	if(!IsNul($CD_User)) {
		$CD_User=$_COOKIE['CD_AdminUserName'];
	}
	if(!IsNum($CD_Hits)) {
		$CD_Hits=0;
	}
?> 
  <div class="contents"> 
   <div class="panel"> 
    <div class="panel-head"> 
     <strong><?php $action=SafeRequest("action","get"); if($action=="add"){echo "添加音乐专辑";}elseif($action=="edit"){echo "编辑音乐专辑";}?></strong> 
    </div> 
    <form action="<?php echo $ActionUrl; ?>" method="post" name="form2"> 
     <table class="table3"> 
      <tbody> 
       <tr> 
        <td width="70" align="left">专辑名称</td> 
        <td align="left"><input type="text" class="input input_hd length_3" value="<?php echo $CD_Name; ?>" name="CD_Name" id="CD_Name" /></td> 
       </tr> 
       <tr> 
        <td width="70" align="left">所属歌手</td> 
        <td align="left"><input type="text" class="input input_hd length_3" value="<?php echo $CD_Singer; ?>" name="CD_Singer" id="CD_Singer" /></td> 
       </tr> 
       <tr> 
        <td width="70" align="left">所属会员</td> 
        <td align="left"><input type="text" class="input input_hd length_2" value="<?php echo $CD_User; ?>" name="CD_User" id="CD_User" /><font color="#d01f3c">&nbsp;&nbsp;必须是站内已存在的用户帐号！不能乱写!</font></td> 
       </tr> 
       <tr> 
        <td width="70" align="left">分类栏目</td> 
        <td align="left"><select name="CD_ClassID" id="CD_ClassID"> <option value="0">选择栏目</option> <?php
$sqlclass="select * from " . tname('class') . " where CD_FatherID=0 ";
$results=$db->query($sqlclass);
if($results) {
	while($row3=$db->fetch_array($results)) {
		if($CD_ClassID==$row3['CD_ID']) {
			echo "<option value='" . $row3['CD_ID'] . "' selected='selected'>" . $row3['CD_Name'] . "</option>";
		} else {
			echo "<option value='" . $row3['CD_ID'] . "' >" . $row3['CD_Name'] . "</option>";
		}
	}
}
?></select></td> 
       </tr> 
       <tr> 
        <td width="70" align="left">所属语言</td> 
        <td align="left">
<select name="CD_YuYan">
<option value="国语"<?php if($CD_YuYan=="国语"){echo " selected";} ?>>国语</option>
<option value="粤语"<?php if($CD_YuYan=="粤语"){echo " selected";} ?>>粤语</option>
<option value="英文"<?php if($CD_YuYan=="英文"){echo " selected";} ?>>英文</option>
<option value="日韩"<?php if($CD_YuYan=="日韩"){echo " selected";} ?>>日韩</option>
<option value="韩文"<?php if($CD_YuYan=="韩文"){echo " selected";} ?>>韩文</option>
<option value="日语"<?php if($CD_YuYan=="日语"){echo " selected";} ?>>日语</option>
<option value="国/粤语"<?php if($CD_YuYan=="国/粤语"){echo " selected";} ?>>国/粤语</option>
<option value="中/英文"<?php if($CD_YuYan=="中/英文"){echo " selected";} ?>>中/英文</option>
<option value="中/日文"<?php if($CD_YuYan=="中/日文"){echo " selected";} ?>>中/日文</option>
<option value="中/韩文"<?php if($CD_YuYan=="中/韩文"){echo " selected";} ?>>中/韩文</option>
</select>
		</td> 
       </tr> 
       <tr> 
        <td width="70" align="left">专辑人气</td> 
        <td align="left"><input type="text" class="input input_hd length_1" value="<?php echo $CD_Hits; ?>" name="CD_Hits" id="CD_Hits" onkeyup="this.value=this.value.replace(/[^\d]/g,'')" onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/[^\d]/g,''))" /></td> 
       </tr> 
       <tr> 
        <td width="70" align="left">发行公司</td> 
        <td align="left"><input type="text" class="input input_hd length_2" value="<?php echo $CD_GongSi; ?>" name="CD_GongSi" id="CD_GongSi" /></td> 
       </tr> 
       <tr> 
        <td width="70" align="left">专辑封面</td> 
        <td align="left"><input type="text" class="input input_hd length_6" value="<?php echo $CD_Pic; ?>" name="CD_Pic" id="CD_Pic" /> <button type="button" onclick="pop.up('上传封面', 'upload/?ac=malbum&f=form2.CD_Pic', '406px', '180px', '140px');" class="btn">上传封面</button> <label class="mr20"><input type="checkbox" name="delpic" class="checkbox" id="delpic" value="1">删除封面图片</label></td> 
       </tr> 
       <tr> 
        <td width="70" align="left">专辑介绍</td> 
        <td align="left"> <script type="text/javascript" charset="gbk" src="editor/kindeditor.js"></script> <script type="text/javascript"> KE.show({
id : 'CD_Intro',
resizeMode : 1,
allowPreviewEmoticons : false,
allowUpload : true,
imageUploadJson : '../../malbum.php',
items : ['source', '|', 'fullscreen', 'undo', 'redo', 'print', 'cut', 'copy', 'paste',
'plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 'justifyright',
'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript',
'superscript', '|', 'selectall', '-',
'title', 'fontname', 'fontsize', '|', 'textcolor', 'bgcolor', 'bold',
'italic', 'underline', 'strikethrough', 'removeformat', '|', 'image',
'flash', 'media', 'advtable', 'hr', 'emoticons', 'link', 'unlink', '|', 'about']
});
</script><textarea style="width:620px; height:300px;" name="CD_Intro"><?php echo $CD_Intro;?></textarea> </td> 
       </tr> 
       <tr> 
        <td height="35" colspan="2" align="left" bgcolor="#FAFBF7" class="td_border"> <input type="hidden" name="CD_HttpUrl" value="<?php echo $_SERVER['HTTP_REFERER']; ?>" /> <input type="hidden" name="CD_Time" value="<?php echo $CD_Time; ?>"> <input class="checkbox" type="checkbox" name="CD_EditTime" id="CD_EditTime" value="1" checked /> <label for="CD_EditTime">更新时间</label><input class="checkbox" type="checkbox" name="CD_IsBest" id="CD_IsBest" value="1"<?php if($CD_IsBest==1){echo " checked";} ?> /> <label for="CD_IsBest">推荐</label> <input class="checkbox" type="checkbox" name="CD_Passed" id="CD_Passed" value="1"<?php if($CD_Passed==0){echo " checked";} ?> /> <label for="CD_Passed">审核</label> <button type="submit" name="form2" class="btn btn_submit J_ajax_submit_btn" style="margin-left: 10px;" onclick="return CheckForm();">确定提交</button> </td> 
       </tr> 
      </tbody> 
     </table> 
    </form>
   </div> 
  </div> 
<?php
}
function main($sql, $size) {
	global $db, $action;
	$Arr=getpagerow($sql, $size);
	$result=$db->query($Arr[2]);
	$videonum=$db->num_rows($result);
?>
<script type="text/javascript">
function CheckAll(form) {
	for (var i = 0; i < form.elements.length; i++) {
		var e = form.elements[i];
		if (e.name != 'chkall')
			e.checked = form.chkall.checked;
	}
}
</script>
  <div class="contents"> 
   <div class="panel"> 
    <div class="panel-head"> 
     <strong> <?php
$action=SafeRequest("action", "get");
$key=SafeRequest("key", "get");
$CD_SingerID=SafeRequest("CD_SingerID", "get");
$CD_SpecialID=SafeRequest("CD_SpecialID", "get");
if($action=="") {
	echo "所有音乐专辑列表";
} elseif($action=="pass") {
	echo "等待审核的音乐专辑";
} elseif($action=="isbest") {
	echo "已被推荐的音乐专辑";
} elseif($action=="class") {
	echo "按音乐专辑分类查看";
} elseif($action=="singer") {
	echo "《" . GetSingerAlias("x5music_singer", "CD_Name", "CD_ID", $CD_SingerID) . "》的相关专辑";
} elseif($action=="keyword") {
	echo "搜索关键词　$key";
} else {
	echo "所有视频专辑列表";
}
?></strong> 
    </div> 
    <form method="get" action="malbum.php"> 
     <div class="search_type cc mb10"> 
      <div class="ul_wrap"> 
       <ul class="cc"> 
        <li> <label>关键字：</label> <input type="hidden" name="action" value="keyword" /> <input name="key" id="search" value="" type="text" class="input length_3" /> &nbsp;&nbsp;<button class="btn mr20" type="submit">搜索</button> </li> 
        <li> <label>按分类：</label><select class="select_3" onchange="window.location.href=''+this.options[this.selectedIndex].value+'';"> <option value="malbum.php">选择分类</option> <?php
$sqlclass="select * from " . tname('class') . " where CD_FatherID=0 ";
$results=$db->query($sqlclass);
if($results) {
	while($row3=$db->fetch_array($results)) {
		if(SafeRequest("CD_ClassID", "get")==$row3['CD_ID']) {
			echo "<option value='?action=class&CD_ClassID=" . $row3['CD_ID'] . "' selected='selected'>" . $row3['CD_Name'] . "</option>";
		} else {
			echo "<option value='?action=class&CD_ClassID=" . $row3['CD_ID'] . "' >" . $row3['CD_Name'] . "</option>";
		}
	}
}
?>  </select> </li> 
       </ul> 
      </div> 
     </div> 
    </form> 
    <form name="form" method="post"> 
     <table class="table2" id="dellist"> 
      <tbody> 
       <tr> 
        <td width="100" align="left"> 序号</td> 
        <td align="left">专辑名称</td> 
        <td align="left">所属会员</td> 
        <td align="center">音乐统计</td> 
        <td align="left">分类栏目</td> 
        <td align="left">推荐</td> 
        <td align="left">审核</td> 
        <td align="left">更新时间</td> 
        <td width="110" align="left">操作</td> 
       </tr> 
<?php
if($videonum==0) {
	echo "<tr><td height='30' colspan='10' align='center' bgcolor='#FFFFFF' class='td_border'><br><br>没有数据<br><br><br></td></tr>";
}
if($result) {
	while($row=$db->fetch_array($result)) {
?>
       <tr> 
        <td align="left"> <input class="checkbox" type="checkbox" name="CD_ID[]" id="CD_ID" value="<?php echo $row['CD_ID']; ?>"> <?php echo $row['CD_ID']; ?></td> 
        <td align="left"><a href="../play/index.php?2,<?php echo $row['CD_ID']?>" target="_blank"><?php echo ReplaceStr($row['CD_Name'],SafeRequest("key","get"),"<font color=red>".SafeRequest("key","get")."</font>")?></a></td>
        <td align="left"><a href="?action=keyword&key=<?php echo $row['CD_User']?>"><?php echo $row['CD_User']?></a></td>
        <td align="center"><a href="dance.php?action=special&CD_SpecialID=<?php echo $row['CD_ID']; ?>">
<?php
$sqlstr="select CD_ID from ".tname('dj')." where CD_SpecialID=".$row['CD_ID'];
$res=$db -> query($sqlstr);
$nums= $db -> num_rows($res);
echo $nums;
?>
</a></td>
        <td align="left"><?php
$res=$db->getrow("select CD_ID,CD_Name from " . tname('class') . " where CD_ID=" . $row['CD_ClassID'] . "");
if($res) {
	echo "<a href='?action=class&CD_ClassID=" . $res['CD_ID'] . "'>" . $res['CD_Name'] . "</a>";
} else {
	echo "暂无栏目";
}
?></td>
        <td align="left"><?php if($row['CD_IsBest']==1){?><a title="未推荐" href="?action=editisbest&CD_ID=<?php echo $row['CD_ID']?>&CD_IsBest=0"><img src='images/no.gif' border='0'></a><?php }else{?><a title="已推荐" href="?action=editisbest&CD_ID=<?php echo $row['CD_ID']?>&CD_IsBest=1"><img src='images/yes.gif' border='0'></a><?php }?></td>
        <td align="left"><?php if($row['CD_Passed']==1){?><a title="未审核" href="?action=editpassed&CD_ID=<?php echo $row['CD_ID']?>&CD_Passed=0"><img src='images/no.gif' border='0'></a><?php }else{?><a title="已审核" href="?action=editpassed&CD_ID=<?php echo $row['CD_ID']?>&CD_Passed=1"><img src='images/yes.gif' border='0'></a><?php }?></td>
        <td align="left"><?php if(date("Y-m-d",strtotime($row['CD_AddTime']))==date('Y-m-d')){ echo "<font color=red>".date("Y-m-d",strtotime($row['CD_AddTime']))."</font>"; }else{ echo date("Y-m-d",strtotime($row['CD_AddTime'])); } ?></td>
        <td width="80" align="left"><a href="?action=edit&CD_ID=<?php echo $row['CD_ID']; ?>" class="btn">编辑</a>&nbsp;<a onClick="return confirm('确定要删除吗？不可恢复！');" href="?action=del&CD_ID=<?php echo $row['CD_ID']; ?>" class="btn">删除</a></td>
       </tr> 
<?php
}
}
?>
       <tr>
        <td height="35" colspan="10" align="left" bgcolor="#FAFBF7" class="td_border"> <label class="mr20"><input type="checkbox" class="J_check_all" id="chkall" onclick="return CheckAll(this.form)" value="on" />全选</label>
		<input name="button2" type="button" class="btn btn_submit J_ajax_submit_btn" value="批量设置" onclick="EditInfo(this.form,'malbum_pl.php')" /> 
		</td> 
       </tr> 
      </tbody> 
     </table> 
    </form> 
   </div> 
  </div> 
  <div class="p10" style="margin-bottom: 0px;"> 
   <div class="pages"><?php echo $Arr[0];?></div> 
  </div> 
<?php
}
//推荐
function EditIsBest() {
	global $db;
	$CD_ID=SafeRequest("CD_ID", "get");
	$CD_IsBest=SafeRequest("CD_IsBest", "get");
	$sql="update " . tname('special') . " set CD_IsBest=" . $CD_IsBest . " where CD_ID=" . $CD_ID;
	if($db->query($sql)) {
		echo "<script>window.location='" . $_SERVER['HTTP_REFERER'] . "'</script>";
	}
}
//审核
function EditPassed() {
	global $db;
	$CD_ID=SafeRequest("CD_ID", "get");
	$CD_Passed=SafeRequest("CD_Passed", "get");
	$sql="update " . tname('special') . " set CD_Passed=" . $CD_Passed . " where CD_ID=" . $CD_ID;
    if($db->query($sql)) {
        echo "<script>window.location='" . $_SERVER['HTTP_REFERER'] . "'</script>";
    }
}
//删除
function Del() {
	global $db;
	$CD_ID=SafeRequest("CD_ID", "get");
	$sqls="select CD_Pic from " . tname('special') . " where CD_ID=" . $CD_ID;
	if($row=$db->getrow($sqls)) {
		if(delunlink($row['CD_Pic'])==true) {
			@unlink(_x5music_root_ . $row['CD_Pic']);
		}
	}
	$sql="delete from " . tname('special') . " where CD_ID=" . $CD_ID;
	if($db->query($sql)) {
		showmessage("恭喜您，专辑删除成功！", "malbum.php", 0);
	}
}
//编辑
function Edit() {
	global $db;
	$CD_ID=SafeRequest("CD_ID", "get");
	$sql="Select * from " . tname('special') . " where CD_ID=" . $CD_ID . "";
	if($row=$db->Getrow($sql)) {
		$Arr=array(
			$row['CD_ClassID'],
			$row['CD_Name'],
			$row['CD_User'],
			$row['CD_Pic'],
			$row['CD_GongSi'],
			$row['CD_YuYan'],
			$row['CD_Intro'],
			$row['CD_IsBest'],
			$row['CD_AddTime'],
			$row['CD_Passed'],
			$row['CD_Hits'],
			$row['CD_Singer']
		);
	}
	EditBoard($Arr, "?action=saveedit&CD_ID=" . $CD_ID . "", "编辑专辑");
}
//添加数据
function Add() {
	$Arr=array("","","","","","","","","","","","","","","","","","","","","");
	EditBoard($Arr, "?action=saveadd", "新增");
}
//保存添加数据
function SaveAdd() {
	global $db;
	$CD_ClassID=SafeRequest("CD_ClassID", "post");
	$CD_Name=SafeRequest("CD_Name", "post");
	$CD_User=SafeRequest("CD_User", "post");
	$CD_Pic=SafeRequest("CD_Pic", "post");
	$CD_GongSi=SafeRequest("CD_GongSi", "post");
	$CD_YuYan=SafeRequest("CD_YuYan", "post");
	$CD_Intro=SafeRequest("CD_Intro", "post");
	$CD_IsBest=SafeRequest("CD_IsBest", "post");
	$CD_AddTime=date('Y-m-d H:i:s');
	$edittime=SafeRequest("edittime", "post");
	$CD_Time=SafeRequest("CD_Time", "post");
	$CD_HttpUrl=SafeRequest("CD_HttpUrl", "post");
	$CD_Passed=SafeRequest("CD_Passed", "post");
	$CD_Hits=SafeRequest("CD_Hits", "post");
	$CD_Singer=SafeRequest("CD_Singer", "post");
	if($CD_IsBest==1) {
		$CD_IsBest=1;
	} else {
		$CD_IsBest=0;
	}
	if($CD_Passed==1) {
		$CD_Passed=0;
	} else {
		$CD_Passed=1;
	}
	$sql="Insert " . tname('special') . " (CD_ClassID,CD_Name,CD_User,CD_Pic,CD_GongSi,CD_YuYan,CD_Intro,CD_Hits,CD_IsBest,CD_Passed,CD_AddTime,CD_Singer) values (" . $CD_ClassID . ",'" . $CD_Name . "','" . $CD_User . "','" . $CD_Pic . "','" . $CD_GongSi . "','" . $CD_YuYan . "','" . $CD_Intro . "'," . $CD_Hits . "," . $CD_IsBest . "," . $CD_Passed . ",'" . $CD_AddTime . "','" . $CD_Singer . "')";
	if($db->query($sql)) {
		showmessage("恭喜您，添加专辑成功！", "malbum.php", 0);
	} else {
		showmessage("出错了，添加专辑失败！", "malbum.php", 0);
	}
}
//保存编辑数据
function SaveEdit() {
	global $db;
	$CD_ID=SafeRequest("CD_ID", "get");
	$CD_ClassID=SafeRequest("CD_ClassID", "post");
	$CD_Name=SafeRequest("CD_Name", "post");
	$CD_User=SafeRequest("CD_User", "post");
	$CD_Pic=SafeRequest("CD_Pic", "post");
	$CD_GongSi=SafeRequest("CD_GongSi", "post");
	$CD_YuYan=SafeRequest("CD_YuYan", "post");
	$CD_Intro=SafeRequest("CD_Intro", "post");
	$CD_IsBest=SafeRequest("CD_IsBest", "post");
	$CD_EditTime=SafeRequest("CD_EditTime", "post");
	$CD_Time=SafeRequest("CD_Time", "post");
	$CD_HttpUrl=SafeRequest("CD_HttpUrl", "post");
	$CD_Passed=SafeRequest("CD_Passed", "post");
	$CD_Hits=SafeRequest("CD_Hits", "post");
	$delpic=SafeRequest("delpic", "post");
	$CD_Singer=SafeRequest("CD_Singer", "post");
	if($CD_EditTime==1) {
		$CD_AddTime=date('Y-m-d H:i:s');
	} else {
		$CD_AddTime=$CD_Time;
	}
	if($CD_IsBest==1) {
		$CD_IsBest=1;
	} else {
		$CD_IsBest=0;
	}
	if($CD_Passed==1) {
		$CD_Passed=0;
	} else {
		$CD_Passed=1;
	}
	if($delpic==1) {
		if(delunlink($CD_Pic)==true) {@unlink(_x5music_root_ . $CD_Pic);}
		$CD_Pic="";
	}
	$sql="update " . tname('special') . " set CD_ClassID=" . $CD_ClassID . ",CD_Name='" . $CD_Name . "',CD_User='" . $CD_User . "',CD_Pic='" . $CD_Pic . "',CD_GongSi='" . $CD_GongSi . "',CD_YuYan='" . $CD_YuYan . "',CD_Intro='" . $CD_Intro . "',CD_IsBest=" . $CD_IsBest . ",CD_Passed=" . $CD_Passed . ",CD_AddTime='" . $CD_AddTime . "',CD_Singer='".$CD_Singer."',CD_Hits=" . $CD_Hits . " where CD_ID=" . $CD_ID . "";
	if($db->query($sql)) {
		showmessage("恭喜您，专辑编辑成功！", "malbum.php", 0);
	} else {
		showmessage("编辑失败，所属会员不存在！", $_SERVER['HTTP_REFERER'], 0);
	}
}
?>