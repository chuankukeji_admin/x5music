<?php
error_reporting(0);
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
function GetTemp($tpl, $ids) {
	$tplpath=_x5music_root_ . cd_templateurl . $tpl;
	if(file_exists($tplpath)) {
		$Mark_Text=@file_get_contents($tplpath);
		$Mark_Text=Common_Mark($Mark_Text, $ids);
		return $Mark_Text;
	} else {
		$error="模板文件不存在！";
		exit(include _x5music_root_ . "/include/error/content_error.php");
	}
}
function topandbottom($Mark_Text) {
	$top_txt=@file_get_contents(_x5music_root_ . cd_templateurl . "head.html");
	$bottom_txt=@file_get_contents(_x5music_root_ . cd_templateurl . "bottom.html");
	$Mark_Text=str_replace('[x5music:head]', $top_txt, $Mark_Text);
	$Mark_Text=str_replace('[x5music:bottom]', $bottom_txt, $Mark_Text);
	return $Mark_Text;
}
function sys($Mark_Text) {
	global $db;
	$system='select * from ' . tname('system') . '';
	$sys=$db->getrow($system);
	$Mark_Text=ReplaceStr($Mark_Text, "[x5music:tdj]", '' . $sys['cd_djmub'] . '');
	$Mark_Text=ReplaceStr($Mark_Text, "[x5music:tuser]", '' . $sys['cd_usermub'] . '');
	$Mark_Text=ReplaceStr($Mark_Text, "[x5music:cnzz]", '' . $sys['cd_cnzz'] . '');
	$Mark_Text=ReplaceStr($Mark_Text, "[x5music:message]", '' . $sys['cd_message'] . '');
	$Mark_Text=ReplaceStr($Mark_Text, "[x5music:gg960_90]", '' . $sys['gg960_90'] . '');
	$Mark_Text=ReplaceStr($Mark_Text, "[x5music:gg960_60]", '' . $sys['gg960_60'] . '');
	$Mark_Text=ReplaceStr($Mark_Text, "[x5music:gg728_90]", '' . $sys['gg728_90'] . '');
	$Mark_Text=ReplaceStr($Mark_Text, "[x5music:gg580_90]", '' . $sys['gg580_90'] . '');
	$Mark_Text=ReplaceStr($Mark_Text, "[x5music:gg468_60]", '' . $sys['gg468_60'] . '');
	$Mark_Text=ReplaceStr($Mark_Text, "[x5music:gg760_90]", '' . $sys['gg760_90'] . '');
	$Mark_Text=ReplaceStr($Mark_Text, "[x5music:gg468_15]", '' . $sys['gg468_15'] . '');
	$Mark_Text=ReplaceStr($Mark_Text, "[x5music:gg500_200]", '' . $sys['gg500_200'] . '');
	$Mark_Text=ReplaceStr($Mark_Text, "[x5music:gg160_600]", '' . $sys['gg160_600'] . '');
	$Mark_Text=ReplaceStr($Mark_Text, "[x5music:gg120_600]", '' . $sys['gg120_600'] . '');
	$Mark_Text=ReplaceStr($Mark_Text, "[x5music:gg120_240]", '' . $sys['gg120_240'] . '');
	$Mark_Text=ReplaceStr($Mark_Text, "[x5music:gg300_250]", '' . $sys['gg300_250'] . '');
	$Mark_Text=ReplaceStr($Mark_Text, "[x5music:gg250_250]", '' . $sys['gg250_250'] . '');
	$Mark_Text=ReplaceStr($Mark_Text, "[x5music:gg200_200]", '' . $sys['gg200_200'] . '');
	$Mark_Text=ReplaceStr($Mark_Text, "[x5music:gg336_280]", '' . $sys['gg336_280'] . '');
	$Mark_Text=ReplaceStr($Mark_Text, "[x5music:gg360_300]", '' . $sys['gg360_300'] . '');
	$Mark_Text=ReplaceStr($Mark_Text, "[x5music:gg125_125]", '' . $sys['gg125_125'] . '');
	$Mark_Text=ReplaceStr($Mark_Text, "[x5music:gg180_150]", '' . $sys['gg180_150'] . '');
	$Mark_Text=ReplaceStr($Mark_Text, "[x5music:gg728_15]", '' . $sys['gg728_15'] . '');
	$Mark_Text=ReplaceStr($Mark_Text, "[x5music:gg234_60]", '' . $sys['gg234_60'] . '');
	$Mark_Text=ReplaceStr($Mark_Text, "[x5music:gg480_160]", '' . $sys['gg480_160'] . '');
	$Mark_Text=ReplaceStr($Mark_Text, "[x5music:gg460_60]", '' . $sys['gg460_60'] . '');
	$Mark_Text=ReplaceStr($Mark_Text, "[x5music:gg120_90]", '' . $sys['gg200_90'] . '');
	$Mark_Text=ReplaceStr($Mark_Text, "[x5music:gg160_90]", '' . $sys['gg160_90'] . '');
	$Mark_Text=ReplaceStr($Mark_Text, "[x5music:gg180_90]", '' . $sys['gg180_90'] . '');
	$Mark_Text=ReplaceStr($Mark_Text, "[x5music:ggself_defined]", '' . $sys['ggself_defined'] . '');
	return $Mark_Text;
}
function x5zd($Mark_Text) {
	$classcurrent=$_SERVER['QUERY_STRING'];
	$classcurrent=str_replace('action=html','',$classcurrent);
	if ($classcurrent=="") {$classcurrents=' class="current"';}
	$zdurl="http://" . $_SERVER['HTTP_HOST'] . "";
	$Mark_Text=ReplaceStr($Mark_Text, "[x5music:zdurl]", '' . $zdurl . '');
	$Mark_Text=ReplaceStr($Mark_Text, "[cssindex:current]", '' . $classcurrents . '');
	return $Mark_Text;
}
function Common_Mark($Mark_Text, $IDs, $Mode = '0') {
	global $db;
	if (!IsNul($Mark_Text)) {
		$error="模板文件没有内容，请确认模板文件是否存在！";
		exit(include _x5music_root_ . "/include/error/content_error.php");
	}
	$TempImg=cd_webpath . substr(substr(cd_templateurl, 0, strlen(cd_templateurl)-1), 0, strrpos(substr(cd_templateurl, 0, strlen(cd_templateurl)-1), '/')+1);
	$Mark_Text=topandbottom($Mark_Text);
	$Mark_Text=sys($Mark_Text);
	$Mark_Text=x5zd($Mark_Text);
	$Mark_Text=ReplaceStr($Mark_Text, "[x5music:webname]", cd_webname);//站点名称
	$Mark_Text=ReplaceStr($Mark_Text, "[x5music:weburl]", cd_weburl);//站点域名 不包含http
	$Mark_Text=ReplaceStr($Mark_Text, "[x5music:keywords]", cd_keywords);//关键词
	$Mark_Text=ReplaceStr($Mark_Text, "[x5music:description]", cd_description);//描述
	$Mark_Text=ReplaceStr($Mark_Text, "[x5music:icp]", cd_webicp);//icp备案号
	$Mark_Text=ReplaceStr($Mark_Text, "[x5music:qq]", cd_webqq);//站长QQ
	$Mark_Text=ReplaceStr($Mark_Text, "[x5music:tel]", cd_webtel);//站长电话
	$Mark_Text=ReplaceStr($Mark_Text, "[x5music:email]", cd_webemail);//站长邮箱
	$Mark_Text=ReplaceStr($Mark_Text, "[x5music:path]", cd_webpath);//站点目录
	$Mark_Text=ReplaceStr($Mark_Text, "[x5music:tempurl]", $TempImg);//模板
	if (!IsNul($IDs)) {$Mark_Text=ReplaceStr($Mark_Text, "[dj:classids]", 0);}
	if ($Mode=='1') {return $Mark_Text;} else {return Data_Mark($Mark_Text, $IDs);}
}
function Data_Mark($Mark_Text, $IDs) {
	global $db;
	$Mark_Text=Rep($Mark_Text);
	if(!IsNul($Mark_Text))
		exit(include _x5music_root_ . "/include/error/content_error.php");
	preg_match_all('/{x5music:([\S]+)\s+(.*?)}([\s\S]+?){\/x5music:\1}/', $Mark_Text, $Mark_Arr);
	if(!empty($Mark_Arr)) {
		for($i=0; $i<count($Mark_Arr[0]); $i++) {
			$table=cd_tablename . $Mark_Arr[1][$i];
			$para=$Mark_Arr[2][$i];
			$sql=Mark_Sql($table, $para, $IDs);
			$result=$db->query($sql);
			$resultcount=$db->num_rows($result);
			if($result) {
				if($resultcount==0) {
					$Data_Content="";
					$Mark_Text=ReplaceStr($Mark_Text, $Mark_Arr[0][$i], $Data_Content);
				} else {
					$Data_Content='';
					$Data_Content_Temp='';
					$sorti=1;
					while($row=$db->fetch_array($result)) {
						switch($table) {
						case tname("dj"):$Data_Content.=datadj($Mark_Arr[0][$i], $Mark_Arr[3][$i], $row, $sorti);break;
						case tname("class"):$Data_Content.=dataclass($Mark_Arr[0][$i], $Mark_Arr[3][$i], $row, $sorti);break;
						case tname("news"):$Data_Content.=datanews($Mark_Arr[0][$i], $Mark_Arr[3][$i], $row, $sorti);break;
						case tname("newsclass"):$Data_Content.=datanewsclass($Mark_Arr[0][$i], $Mark_Arr[3][$i], $row, $sorti);break;
						case tname("special"):$Data_Content.=dataspecial($Mark_Arr[0][$i], $Mark_Arr[3][$i], $row, $sorti);break;
						case tname("link"):$Data_Content.=datalink($Mark_Arr[0][$i], $Mark_Arr[3][$i], $row, $sorti);break;
						case tname("user"):$Data_Content.=datauser($Mark_Arr[0][$i], $Mark_Arr[3][$i], $row, $sorti);break;
						case tname("pic"):$Data_Content.=datapic($Mark_Arr[0][$i], $Mark_Arr[3][$i], $row, $sorti);break;
						}
						$sorti=$sorti+1;
					}
				}
				$Mark_Text=ReplaceStr($Mark_Text, $Mark_Arr[0][$i], $Data_Content);
			}
		}
	}
	$Mark_Text=labelif($Mark_Text);
	return $Mark_Text;
}
function datadj($para, $label, $row, $sorti) {
	preg_match_all('/\[dj:\s*([0-9a-zA-Z]+)([\s]*[len|style]*)[=]??([\da-zA-Z\-\\\\:\s]*)\]/', $para, $field_arr);
	$datatmp=$label;
	if(!empty($field_arr)) {
		for($i=0; $i<count($field_arr[0]); $i++) {
			switch($field_arr[1][$i]) {
				case 'link':$datatmp=ReplaceStr($datatmp, '[dj:link]', LinkUrl("dj", $row['CD_ClassID'], 1, $row['CD_ID']));break;
				case 'id':$datatmp=ReplaceStr($datatmp, '[dj:id]', $row['CD_ID']);break;
				case 'i':$datatmp=ReplaceStr($datatmp, '[dj:i]', $sorti);break;
				case 'is':$datatmp=ReplaceStr($datatmp,'[dj:is]',$sorti%2);break;
				case 'color':$datatmp=ReplaceStr($datatmp, '[dj:color]', $row['CD_Color']);break;
				case 'hits':$datatmp=ReplaceStr($datatmp, '[dj:hits]', $row['CD_Hits']);break;
				case 'mhits':$datatmp=ReplaceStr($datatmp, '[dj:mhits]', bqwhits($row['CD_Hits']));break;
				case 'url':$datatmp=ReplaceStr($datatmp, '[dj:url]', $row['CD_Url']);break;
				case 'downurl':$datatmp=ReplaceStr($datatmp, '[dj:downurl]', $row['CD_DownUrl']);break;
				case 'server':$datatmp=ReplaceStr($datatmp, "[dj:server]", ReplaceStr(GetAlias("x5music_server", "CD_Url", "CD_ID", $row['CD_Server']), "null", ""));break;
				case 'downserver':$datatmp=ReplaceStr($datatmp, "[dj:downserver]", ReplaceStr(GetAlias("x5music_server", "CD_DownUrl", "CD_ID", $row['CD_Server']), "null", ""));break;
				case 'downlink':$datatmp=ReplaceStr($datatmp, "[dj:downlink]",  DownLinkUrl($row['CD_ID']));break;
				case 'downhits':$datatmp=ReplaceStr($datatmp, '[dj:downhits]', $row['CD_DownHits']);break;
				case 'favhits':$datatmp=ReplaceStr($datatmp, '[dj:favhits]', $row['CD_FavHits']);break;
				case 'uhits':$datatmp=ReplaceStr($datatmp, '[dj:uhits]', $row['CD_uHits']);break;
				case 'dhits':$datatmp=ReplaceStr($datatmp, '[dj:dhits]', $row['CD_dHits']);break;
				case 'dayhits':$datatmp=ReplaceStr($datatmp, '[dj:dayhits]', $row['CD_DayHits']);break;
				case 'weekhits':$datatmp=ReplaceStr($datatmp, '[dj:weekhits]', $row['CD_WeekHits']);break;
				case 'monthhits':$datatmp=ReplaceStr($datatmp, '[dj:monthhits]', $row['CD_MonthHits']);break;
				case 'isbest':$datatmp=ReplaceStr($datatmp, '[dj:isbest]', $row['CD_IsBest']);break;
				case 'points':$datatmp=ReplaceStr($datatmp, '[dj:points]', $row['CD_Points']);break;
				case 'siz':$datatmp=ReplaceStr($datatmp, '[dj:siz]', $row['CD_Siz']);break;
				case 'singers':$datatmp=ReplaceStr($datatmp, '[dj:singers]', urlencode($row['CD_Singer']));break;
				case 'tag':$datatmp=ReplaceStr($datatmp, "[dj:tag]", HotSearch($row['CD_Tag'],'play'));break;
				case 'md5':$datatmp=ReplaceStr($datatmp, '[dj:md5]', $row['CD_Md5']);break;
				case 'uid':$datatmp=ReplaceStr($datatmp, '[dj:uid]', $row['CD_Uid']);break;
				case 'from':$datatmp=ReplaceStr($datatmp, '[dj:from]', $row['CD_From']);break;
				case 'classlink':$datatmp=ReplaceStr($datatmp, "[dj:classlink]", LinkClassUrl("dj", $row['CD_ClassID'], 1, 1));break;
				case 'classname':$datatmp=ReplaceStr($datatmp, "[dj:classname]", GetAlias("x5music_class", "CD_Name", "CD_ID", $row['CD_ClassID']));break;
				case 'classid':$datatmp=ReplaceStr($datatmp, "[dj:classid]", $row['CD_ClassID']);break;
				case 'albumid':$datatmp=ReplaceStr($datatmp, "[dj:albumid]", GetAlias("x5music_special", "CD_ID", "CD_ID", $row['CD_SpecialID']));break;
				case 'albumlink':$datatmp=ReplaceStr($datatmp, "[dj:albumlink]", LinkUrl("special", $row['CD_ClassID'], 1, $row['CD_SpecialID']));break;
				case 'albumname':$datatmp=ReplaceStr($datatmp, "[dj:albumname]", GetAlias("x5music_special", "CD_Name", "CD_ID", $row['CD_SpecialID']));break;
				case 'userlink':$datatmp=ReplaceStr($datatmp, "[dj:userlink]", linkweburl(GetAlias("x5music_user", "cd_id", "cd_name", $row['CD_User']), $row['CD_User']));break;
				case 'userid':$datatmp=ReplaceStr($datatmp, "[dj:userid]", GetAlias("x5music_user", "cd_id", "cd_name", $row['CD_User']));break;
				case 'usersex':$datatmp=ReplaceStr($datatmp, "[dj:usersex]", GetAlias("x5music_user", "cd_sex", "cd_name", $row['CD_User']));break;
				case 'userqq':$datatmp=ReplaceStr($datatmp, "[dj:userqq]", GetAlias("x5music_user", "cd_qq", "cd_name", $row['CD_User']));break;
				case 'udjmub':$datatmp=ReplaceStr($datatmp, "[dj:udjmub]", GetAlias("x5music_user", "cd_djmub", "cd_name", $row['CD_User']));break;
				case 'ushits':$datatmp=ReplaceStr($datatmp, "[dj:ushits]", GetAlias("x5music_user", "cd_hits", "cd_name", $row['CD_User']));break;
				case 'ufav':$datatmp=ReplaceStr($datatmp, "[dj:ufav]", GetAlias("x5music_user", "cd_ufav", "cd_name", $row['CD_User']));break;
				case 'ufans':$datatmp=ReplaceStr($datatmp, "[dj:ufans]", GetAlias("x5music_user", "cd_ufans", "cd_name", $row['CD_User']));break;
				case 'usernicheng':$datatmp=ReplaceStr($datatmp, "[dj:usernicheng]", GetAlias("x5music_user", "cd_nicheng", "cd_name", $row['CD_User']));break;
				case 'userbirthprovince':$datatmp=ReplaceStr($datatmp,"[dj:userbirthprovince]",GetAlias("x5music_user","cd_birthprovince","cd_name",$row['CD_User']));break;
				case 'userbirthcity':$datatmp=ReplaceStr($datatmp,"[dj:userbirthcity]",GetAlias("x5music_user","cd_birthcity","cd_name",$row['CD_User']));break;
				case 'userresideprovince':$datatmp=ReplaceStr($datatmp,"[dj:userresideprovince]",GetAlias("x5music_user","cd_resideprovince","cd_name",$row['CD_User']));break;
				case 'userresidecity':$datatmp=ReplaceStr($datatmp,"[dj:userbirthcity]",datetime(GetAlias("x5music_user","cd_residecity","cd_name",$row['CD_User'])));break;
				case 'userdtime':$datatmp=ReplaceStr($datatmp, "[dj:userdtime]", datetime(GetAlias("x5music_user", "cd_logintime", "cd_name", $row['CD_User'])));break;
				case 'userpoints':$datatmp=ReplaceStr($datatmp, "[dj:userpoints]", GetAlias("x5music_user", "cd_points", "cd_name", $row['CD_User']));break;
				case 'userpic':$datatmp=ReplaceStr($datatmp, "[dj:userpic]", linkuserphoto(GetAlias("x5music_user", "cd_id", "cd_name", $row['CD_User'])));break;
				case 'usergrade':
				$usergrade=GetAlias("x5music_user", "cd_grade", "cd_name", $row['CD_User']);
				if ($usergrade=="1") {$usergrade="<font color=\"red\">VIP会员</font>";}else{$usergrade="普通会员";}
				$datatmp=ReplaceStr($datatmp, "[dj:usergrade]", $usergrade);
				break;
				case 'pic':$datatmp=ReplaceStr($datatmp, "[dj:pic]", LinkPicUrl($row['CD_Pic']));break;
				case 'form':$form=strtolower(trim(substr(strrchr($row['CD_Url'], "."), 1)));$datatmp=ReplaceStr($datatmp, "[dj:form]", $form);break;
				case 'dform':$form=strtolower(trim(substr(strrchr($row['CD_DownUrl'], "."), 1)));$datatmp=ReplaceStr($datatmp, "[dj:dform]", $form);break;
				case 'wlurl':
				$fyq=ReplaceStr(GetAlias("x5music_server", "CD_Url", "CD_ID", $row['CD_Server']), "null", "");$wlurl="" . $fyq . "" . $row['CD_Url'] . "";
				$datatmp=ReplaceStr($datatmp, "[dj:wlurl]", $wlurl);
				break;
				case 'grade':$grade=$row['CD_Grade'];if ($grade=="0") {$grade="免费下载";}if ($grade=="1") {$grade="普通用户";}if ($grade=="2") {$grade="VIP会员";}$datatmp=ReplaceStr($datatmp, '[dj:grade]', $grade);break;
				case 'name':$name=getlen($field_arr[2][$i], $field_arr[3][$i], $row['CD_Name']);$datatmp=ReplaceStr($datatmp, $field_arr[0][$i], $name);break;
				case 'singer':$singer=getlen($field_arr[2][$i], $field_arr[3][$i], $row['CD_Singer']);$datatmp=ReplaceStr($datatmp, $field_arr[0][$i], $singer);break;
				case 'user':$user=getlen($field_arr[2][$i], $field_arr[3][$i], $row['CD_User']);$datatmp=ReplaceStr($datatmp, $field_arr[0][$i], $user);break;
				case 'word':$word=getlen($field_arr[2][$i], $field_arr[3][$i], str_decode($row['CD_Word']));$datatmp=ReplaceStr($datatmp, $field_arr[0][$i], $word);break;
				case 'lrc':$lrc=getlen($field_arr[2][$i], $field_arr[3][$i], $row['CD_Lrc']);$datatmp=ReplaceStr($datatmp, $field_arr[0][$i], $lrc);break;
				case 'sword':$word=getlen($field_arr[2][$i], $field_arr[3][$i], str_checkhtml($row['CD_Word']));$datatmp=ReplaceStr($datatmp, $field_arr[0][$i], $word);break;
				case 'time':$datatmp=ReplaceStr($datatmp, '[dj:time]', datetime($row['CD_AddTime']));break;
				case 'dtime':$datatmp=ReplaceStr($datatmp, '[dj:dtime]', datetime($row['CD_LastHitTime']));break;
				case 'xtime':$datatmp=ReplaceStr($datatmp, $field_arr[0][$i], date(ReplaceStr($field_arr[3][$i], 'f', 'i'), strtotime($row['CD_AddTime'])));break;
			}
		}
	}
	unset($field_arr);
	return $datatmp;
}
function dataclass($para, $label, $row, $sorti) {
	preg_match_all('/\[class:\s*([0-9a-zA-Z]+)([\s]*[len|style]*)[=]??([\da-zA-Z\-\\\\:\s]*)\]/', $para, $field_arr);
	$datatmp=$label;
	if (!empty($field_arr)) {
		for ($i=0; $i<count($field_arr[0]); $i++) {
			switch ($field_arr[1][$i]) {
				case 'aliasname':$datatmp=ReplaceStr($datatmp, '[class:aliasname]', $row['CD_AliasName']);break;
				case 'name':$datatmp=ReplaceStr($datatmp, '[class:name]', $row['CD_Name']);break;
				case 'link':$datatmp=ReplaceStr($datatmp, '[class:link]', LinkClassUrl("dj", $row['CD_ID'], $row['CD_SystemID'], 1));break;
				case 'id':$datatmp=ReplaceStr($datatmp, '[class:id]', $row['CD_ID']);break;
				case 'i':$datatmp=ReplaceStr($datatmp, '[class:i]', $sorti);break;
				case 'current':
				$url=$_SERVER['QUERY_STRING'];$current=explode(',', $url);
				if ($current[1]==$row['CD_ID']) {$csscurrent=' class="current"';}
				$datatmp=ReplaceStr($datatmp, '[class:current]', $csscurrent);
				break;
			}
		}
	}
	unset($field_arr);
	return $datatmp;
}
function datanews($para, $label, $row, $sorti) {
	preg_match_all('/\[news:\s*([0-9a-zA-Z]+)([\s]*[len|style]*)[=]??([\da-zA-Z\-\\\\:\s]*)\]/', $para, $field_arr);
	$datatmp=$label;
	if(!empty($field_arr)) {
		for($i=0; $i<count($field_arr[0]); $i++) {
			switch($field_arr[1][$i]) {
			case 'i':$datatmp=ReplaceStr($datatmp, '[news:i]', $sorti);break;
			case 'id':$datatmp=ReplaceStr($datatmp, '[news:id]', $row['CD_ID']);break;
			case 'link':$datatmp=ReplaceStr($datatmp, '[news:link]', LinkUrl("news", $row['CD_ClassID'], 1, $row['CD_ID']));break;
			case 'color':$datatmp=ReplaceStr($datatmp, '[news:color]', $row['CD_Color']);break;
			case 'hits':$datatmp=ReplaceStr($datatmp, '[news:hits]', $row['CD_Hits']);break;
			case 'classlink':$datatmp=ReplaceStr($datatmp, "[news:classlink]", LinkClassUrl("news", $row['CD_ClassID'], 4, 1));break;
			case 'classname':$datatmp=ReplaceStr($datatmp, "[news:classname]", GetAlias("x5music_newsclass", "CD_Name", "CD_ID", $row['CD_ClassID']));break;
			case 'classid':$datatmp=ReplaceStr($datatmp, "[news:classid]", $row['CD_ClassID']);break;
			case 'name':$name=getlen($field_arr[2][$i], $field_arr[3][$i], $row['CD_Name']);$datatmp=ReplaceStr($datatmp, $field_arr[0][$i], $name);break;
			case 'intro':$intro=getlen($field_arr[2][$i], $field_arr[3][$i], $row['CD_Intro']);$datatmp=ReplaceStr($datatmp, $field_arr[0][$i], $intro);break;
			case 'content':$content=getlen($field_arr[2][$i], $field_arr[3][$i], str_checkhtml($row['CD_Intro']));$datatmp=ReplaceStr($datatmp, $field_arr[0][$i], $content);break;
			case 'time':$datatmp=ReplaceStr($datatmp, $field_arr[0][$i], date(ReplaceStr($field_arr[3][$i], 'f', 'i'), strtotime($row['CD_AddTime'])));break;
			}
		}
	}
	unset($field_arr);
	return $datatmp;
}
function datanewsclass($para, $label, $row, $sorti) {
	preg_match_all('/\[newsclass:\s*([0-9a-zA-Z]+)([\s]*[len|style]*)[=]??([\da-zA-Z\-\\\\:\s]*)\]/', $para, $field_arr);
	$datatmp=$label;
	if (!empty($field_arr)) {
		for ($i=0; $i<count($field_arr[0]); $i++) {
			switch ($field_arr[1][$i]) {
				case 'name':$datatmp=ReplaceStr($datatmp, '[newsclass:name]', $row['CD_Name']);break;
				case 'link':$datatmp=ReplaceStr($datatmp, '[newsclass:link]', LinkClassUrl("news", $row['CD_ID'], 4, 1));break;
				case 'id':$datatmp=ReplaceStr($datatmp, '[newsclass:id]', $row['CD_ID']);break;
				case 'i':$datatmp=ReplaceStr($datatmp, '[newsclass:i]', $sorti);break;
				case 'current':
				$url=$_SERVER['QUERY_STRING'];$current=explode(',', $url);
				if ($current[1]==$row['CD_ID']) {$csscurrent=' class="current"';}
				$datatmp=ReplaceStr($datatmp, '[newsclass:current]', $csscurrent);
				break;
			}
		}
	}
	unset($field_arr);
	return $datatmp;
}
function dataspecial($para, $label, $row, $sorti) {
	preg_match_all('/\[special:\s*([0-9a-zA-Z]+)([\s]*[len|style]*)[=]??([\da-zA-Z\-\\\\:\s]*)\]/', $para, $field_arr);
	$datatmp=$label;
	if(!empty($field_arr)) {
		for($i=0; $i<count($field_arr[0]); $i++) {
			switch($field_arr[1][$i]) {
			case 'i':$datatmp=ReplaceStr($datatmp, '[special:i]', $sorti);break;
			case 'id':$datatmp=ReplaceStr($datatmp, '[special:id]', $row['CD_ID']);break;
			case 'link':$datatmp=ReplaceStr($datatmp, '[special:link]', LinkUrl("special", $row['CD_ClassID'], 1, $row['CD_ID']));break;
			case 'isbest':$datatmp=ReplaceStr($datatmp, '[special:isbest]', $row['CD_IsBest']);break;
			case 'hits':$datatmp=ReplaceStr($datatmp, '[valbum:hits]', $row['CD_Hits']);break;
			case 'mhits':$datatmp=ReplaceStr($datatmp, '[valbum:mhits]', $row['CD_Hits']);break;
			case 'passed':$datatmp=ReplaceStr($datatmp, '[special:passed]', $row['CD_Passed']);break;
			case 'classlink':$datatmp=ReplaceStr($datatmp, "[special:classlink]", LinkClassUrl("dj", $row['CD_ClassID'], 1, 1));break;
			case 'classname':$datatmp=ReplaceStr($datatmp, "[special:classname]", GetAlias("x5music_class", "CD_Name", "CD_ID", $row['CD_ClassID']));break;
			case 'classid':$datatmp=ReplaceStr($datatmp, "[special:classid]", $row['CD_ClassID']);break;
			case 'userlink':$datatmp=ReplaceStr($datatmp, "[special:userlink]", linkweburl(GetAlias("x5music_user", "cd_id", "cd_name", $row['CD_User']), $row['CD_User']));break;
			case 'userid':$datatmp=ReplaceStr($datatmp, "[special:userid]", GetAlias("x5music_user", "cd_id", "cd_name", $row['CD_User']));break;
			case 'usersex':$datatmp=ReplaceStr($datatmp, "[special:usersex]", GetAlias("x5music_user", "cd_sex", "cd_name", $row['CD_User']));break;
			case 'userqq':$datatmp=ReplaceStr($datatmp, "[special:userqq]", GetAlias("x5music_user", "cd_qq", "cd_name", $row['CD_User']));break;
			case 'udjmub':$datatmp=ReplaceStr($datatmp, "[special:udjmub]", GetAlias("x5music_user", "cd_djmub", "cd_name", $row['CD_User']));break;
			case 'ushits':$datatmp=ReplaceStr($datatmp, "[special:ushits]", GetAlias("x5music_user", "cd_hits", "cd_name", $row['CD_User']));break;
			case 'ufav':$datatmp=ReplaceStr($datatmp, "[special:ufav]", GetAlias("x5music_user", "cd_ufav", "cd_name", $row['CD_User']));break;
			case 'ufans':$datatmp=ReplaceStr($datatmp, "[special:ufans]", GetAlias("x5music_user", "cd_ufans", "cd_name", $row['CD_User']));break;
			case 'usernicheng':$datatmp=ReplaceStr($datatmp, "[special:usernicheng]", GetAlias("x5music_user", "cd_nicheng", "cd_name", $row['CD_User']));break;
			case 'userbirthprovince':$datatmp=ReplaceStr($datatmp,"[special:userbirthprovince]",GetAlias("x5music_user","cd_birthprovince","cd_name",$row['CD_User']));break;
			case 'userbirthcity':$datatmp=ReplaceStr($datatmp,"[special:userbirthcity]",GetAlias("x5music_user","cd_birthcity","cd_name",$row['CD_User']));break;
			case 'userresideprovince':$datatmp=ReplaceStr($datatmp,"[special:userresideprovince]",GetAlias("x5music_user","cd_resideprovince","cd_name",$row['CD_User']));break;
			case 'userresidecity':$datatmp=ReplaceStr($datatmp,"[special:userbirthcity]",datetime(GetAlias("x5music_user","cd_residecity","cd_name",$row['CD_User'])));break;
			case 'userdtime':$datatmp=ReplaceStr($datatmp, "[special:userdtime]", datetime(GetAlias("x5music_user", "cd_logintime", "cd_name", $row['CD_User'])));break;
			case 'userpoints':$datatmp=ReplaceStr($datatmp, "[special:userpoints]", GetAlias("x5music_user", "cd_points", "cd_name", $row['CD_User']));break;
			case 'userpic':$datatmp=ReplaceStr($datatmp, "[special:userpic]", linkuserphoto(GetAlias("x5music_user", "cd_id", "cd_name", $row['CD_User'])));break;
			case 'usergrade':
				$usergrade=GetAlias("x5music_user", "cd_grade", "cd_name", $row['CD_User']);
				if ($usergrade=="1") {$usergrade="<font color=\"red\">VIP会员</font>";}else{$usergrade="普通会员";}
				$datatmp=ReplaceStr($datatmp, "[special:usergrade]", $usergrade);
				break;
			case 'pic':$datatmp=ReplaceStr($datatmp, "[special:pic]", LinkPicUrl($row['CD_Pic']));break;
			case 'name':$name=getlen($field_arr[2][$i], $field_arr[3][$i], $row['CD_Name']);$datatmp=ReplaceStr($datatmp, $field_arr[0][$i], $name);break;
			case 'user':$user=getlen($field_arr[2][$i], $field_arr[3][$i], $row['CD_User']);$datatmp=ReplaceStr($datatmp, $field_arr[0][$i], $user);break;
			case 'gongsi':$gongsi=getlen($field_arr[2][$i], $field_arr[3][$i], $row['CD_GongSi']);$datatmp=ReplaceStr($datatmp, $field_arr[0][$i], $gongsi);break;
			case 'yuyan':$yuyan=getlen($field_arr[2][$i], $field_arr[3][$i], $row['CD_YuYan']);$datatmp=ReplaceStr($datatmp, $field_arr[0][$i], $yuyan);break;
			case 'intro':$intro=getlen($field_arr[2][$i], $field_arr[3][$i], $row['CD_Intro']);$datatmp=ReplaceStr($datatmp, $field_arr[0][$i], $intro);break;
			case 'sintro':$sintro=getlen($field_arr[2][$i], $field_arr[3][$i], str_checkhtml($row['CD_Intro']));$datatmp=ReplaceStr($datatmp, $field_arr[0][$i], $sintro);break;
			case 'time':$datatmp=ReplaceStr($datatmp, $field_arr[0][$i], date(ReplaceStr($field_arr[3][$i], 'f', 'i'), strtotime($row['CD_AddTime'])));break;
			}
		}
	}
	unset($field_arr);
	return $datatmp;
}
function datapic($para, $label, $row, $sorti) {
	preg_match_all('/\[pic:\s*([0-9a-zA-Z]+)([\s]*[len|style]*)[=]??([\da-zA-Z\-\\\\:\s]*)\]/', $para, $field_arr);
	$datatmp=$label;
	if(!empty($field_arr)) {
		for($i=0; $i<count($field_arr[0]); $i++) {
			switch($field_arr[1][$i]) {
			case 'i':$datatmp=ReplaceStr($datatmp, '[pic:i]', $sorti);break;
			case 'id':$datatmp=ReplaceStr($datatmp, '[pic:id]', $row['cd_id']);break;
			case 'link':$datatmp=ReplaceStr($datatmp, '[pic:link]', cd_webpath . 'user/space.php?do=pic&view=album&id=' . $row['cd_id']);break;
			case 'hits':$datatmp=ReplaceStr($datatmp, '[pic:hits]', $row['cd_hits']);break;
			case 'url':$datatmp=ReplaceStr($datatmp, "[pic:url]", getpiclink(cd_webpath . $row['cd_url']));break;
			case 'userid':$datatmp=ReplaceStr($datatmp, "[pic:userid]", $row['cd_uid']);break;
			case 'username':$datatmp=ReplaceStr($datatmp, "[pic:username]", $row['cd_uname']);break;
			case 'userlink':$datatmp=ReplaceStr($datatmp, "[pic:userlink]", linkweburl($row['cd_uid'], $row['cd_uname']));break;
			case 'name':$name=getlen($field_arr[2][$i], $field_arr[3][$i], $row['cd_title']);$datatmp=ReplaceStr($datatmp, $field_arr[0][$i], $name);break;
			case 'time':$datatmp=ReplaceStr($datatmp, $field_arr[0][$i], date(ReplaceStr($field_arr[3][$i], 'f', 'i'), $row['cd_addtime']));break;
			}
		}
	}
	unset($field_arr);
	return $datatmp;
}
function datalink($para, $label, $row, $sorti) {
	preg_match_all('/\[link:\s*([0-9a-zA-Z]+)([\s]*[len|style]*)[=]??([\da-zA-Z\-\\\\:\s]*)\]/', $para, $field_arr);
	$datatmp=$label;
	if(!empty($field_arr)) {
		for($i=0; $i<count($field_arr[0]); $i++) {
			switch($field_arr[1][$i]) {
			case 'url':$datatmp=ReplaceStr($datatmp, '[link:url]', $row['cd_url']);break;
			case 'id':$datatmp=ReplaceStr($datatmp, '[link:id]', $row['cd_id']);break;
			case 'i':$datatmp=ReplaceStr($datatmp, '[link:i]', $sorti);break;
			case 'pic':$datatmp=ReplaceStr($datatmp, '[link:pic]', LinkPicUrl($row['cd_pic']));break;
			case 'name':$name=getlen($field_arr[2][$i], $field_arr[3][$i], $row['cd_name']);$datatmp=ReplaceStr($datatmp, $field_arr[0][$i], $name);break;
			case 'intro':$intro=getlen($field_arr[2][$i], $field_arr[3][$i], $row['cd_input']);$datatmp=ReplaceStr($datatmp, $field_arr[0][$i], $name);break;
			}
		}
	}
	unset($field_arr);
	return $datatmp;
}
function datauser($para, $label, $row, $sorti) {
	preg_match_all('/\[user:\s*([0-9a-zA-Z]+)([\s]*[len|style]*)[=]??([\da-zA-Z\-\\\\:\s]*)\]/', $para, $field_arr);
	$datatmp=$label;
	if(!empty($field_arr)) {
		for($i=0; $i<count($field_arr[0]); $i++) {
			switch($field_arr[1][$i]) {
			case 'i':$datatmp=ReplaceStr($datatmp, '[user:i]', $sorti);break;
			case 'id':$datatmp=ReplaceStr($datatmp, '[user:id]', $row['cd_id']);break;
			case 'link':$datatmp=ReplaceStr($datatmp, '[user:link]', linkweburl($row['cd_id'], $row['cd_name']));break;
			case 'email':$datatmp=ReplaceStr($datatmp, '[user:email]', $row['cd_email']);break;
			case 'sex':$datatmp=ReplaceStr($datatmp, '[user:sex]', $row['cd_sex']);break;
			case 'qq':$datatmp=ReplaceStr($datatmp, "[user:qq]", $row['cd_qq']);break;
			case 'points':$datatmp=ReplaceStr($datatmp, "[user:points]", $row['cd_points']);break;
			case 'pic':$datatmp=ReplaceStr($datatmp, "[user:pic]", linkuserphoto($row['cd_id']));break;
			case 'birthprovince':$datatmp=ReplaceStr($datatmp, "[user:birthprovince]", $row['cd_birthprovince']);break;
			case 'birthcity':$datatmp=ReplaceStr($datatmp, "[user:birthcity]", $row['cd_birthcity']);break;
			case 'resideprovince':$datatmp=ReplaceStr($datatmp, "[user:resideprovince]", $row['cd_resideprovince']);break;
			case 'residecity':$datatmp=ReplaceStr($datatmp, "[user:residecity]", $row['cd_residecity']);break;
			case 'birthday':$datatmp=ReplaceStr($datatmp, "[user:birthday]", $row['cd_birthday']);break;
			case 'hits':$datatmp=ReplaceStr($datatmp, "[user:hits]", $row['cd_hits']);break;
			case 'rank':$datatmp=ReplaceStr($datatmp, "[user:rank]", $row['cd_rank']);break;
			case 'isbest':$datatmp=ReplaceStr($datatmp, "[user:isbest]", $row['cd_isbest']);break;
			case 'grade':$grade=$row['cd_grade'];if($grade=="1") {$grade="<font color=\"red\">VIP会员</font>";} else {$grade="普通会员";}$datatmp=ReplaceStr($datatmp, "[user:grade]", $grade);break;
			case 'djmub':$datatmp=ReplaceStr($datatmp, '[user:djmub]', $row['cd_djmub']);break;
			case 'ufav':$datatmp=ReplaceStr($datatmp, '[user:ufav]', $row['cd_ufav']);break;
			case 'ufans':$datatmp=ReplaceStr($datatmp, '[user:ufans]', $row['cd_ufans']);break;
			case 'regtime':$datatmp=ReplaceStr($datatmp, '[user:regtime]', datetime($row['cd_regdate']));break;
			case 'dtime':$datatmp=ReplaceStr($datatmp, '[user:dtime]', datetime($row['cd_logintime']));break;
			case 'name':$name=getlen($field_arr[2][$i], $field_arr[3][$i], $row['cd_name']);$datatmp=ReplaceStr($datatmp, $field_arr[0][$i], $name);break;
			case 'nicheng':$nicheng=getlen($field_arr[2][$i], $field_arr[3][$i], $row['cd_nicheng']);$datatmp=ReplaceStr($datatmp, $field_arr[0][$i], $nicheng);break;
			case 'sign':$sign=getlen($field_arr[2][$i], $field_arr[3][$i], str_checkhtml($row['cd_sign']));$datatmp=ReplaceStr($datatmp, $field_arr[0][$i], $sign);break;
			}
		}
	}
	unset($field_arr);
	return $datatmp;
}
function Mark_Sql($table, $para, $ids) {
	$loop='';
	$order='';
	$sort='';
	$start='';
	$classid='';
	$state='';
	$stars='';
	$topicid='';
	$group='';
	$hide='';
	$class='';
	preg_match_all('/([a-z0-9]+)=([a-z0-9|,]+)/', $para, $para_arr);
	if(!empty($para_arr)) {
		for($i=0; $i<count($para_arr[0]); $i++) {
			switch($para_arr[1][$i]) {
			case 'loop':$loop=$para_arr[2][$i];break;
			case 'order':$order=$para_arr[2][$i];break;
			case 'sort':$sort=$para_arr[2][$i];break;
			case 'start':$start=$para_arr[2][$i];break;
			case 'classid':$classid=$para_arr[2][$i];break;
			case 'state':$state=$para_arr[2][$i];break;
			case 'stars':$stars=$para_arr[2][$i];break;
			case 'specialid':$topicid=$para_arr[2][$i];break;
			case 'group':$group=$para_arr[2][$i];break;
			case 'hide':$hide=$para_arr[2][$i];break;
			case 'class':$class=$para_arr[2][$i];break;
			}
		}
		$sql="select * from `$table` where 1=1 ";
		if(isset($order)==""or$order!="asc")
			$order="desc";
		switch($table) {
		case tname("dj"):
			if(isset($start)==""or$start==0)
				$start=1;
			if($sort==""or$sort=="time") {
				$sort="UNIX_TIMESTAMP(CD_AddTime)";
			} elseif($sort=="id") {
				$sort="CD_ID";
			} elseif($sort=="hits") {
				$sort="CD_Hits";
			} elseif($sort=="stars") {
				$sort="CD_Isbest";
			} elseif($sort=="favhits") {
				$sort="CD_FavHits";
			} elseif($sort=="downhits") {
				$sort="CD_DownHits";
			} elseif($sort=="uhits") {
				$sort="CD_uHits";
			} elseif($sort=="dtime") {
				$sort="CD_LastHitTime";
			} elseif($sort=="dhits") {
				$sort="CD_dHits";
			} elseif($sort=="dayhits") {
				$sort="CD_DayHits";
			} elseif($sort=="weekhits") {
				$sort="CD_WeekHits";
			} elseif($sort=="monthhits") {
				$sort="CD_MonthHits";
			} elseif($sort=="free") {
				$sort="CD_ID";
				$sql.=" and CD_Grade<>2 and CD_Points=0 ";
			} else {
				$sort="UNIX_TIMESTAMP(CD_AddTime)";
			}
			if($classid!="all" and $classid!=""and$classid!="auto") {
				$classid=getChild($classid);
				$sql.=" and CD_ClassID in ($classid) ";
			} elseif($classid=="auto" and $ids!="") {
				$ids=getChild($ids);
				$sql.=" and CD_ClassID in($ids) ";
			}
			if($specialid="all" and $specialid="" and $specialid="auto") {
				$sql.=" and CD_SpecialID in (" . $specialid . ")";
			} elseif($topicid=='auto') {
				$sql.=" and CD_SpecialID in (" . $ids . ")";
			}
			if($stars!="" and $stars!="all")
				$sql.=" and CD_Isbest in (" . $stars . ")";
			$sql.=" and CD_Passed=0 and CD_Deleted=0 and CD_ClassID<>0  Order by " . $sort . " " . $order . " ";
			if($loop!="all" and $loop!="")
				$sql.="limit " . ($start-1) . "," . ($loop) . "";
			break;
		case tname("class"):
			if(isset($start)==""or$start==0)
				$start=1;
			if($sort==""or$sort=="id") {
				$sort="CD_ID";
			} elseif($sort=="turn") {
				$sort="CD_TheOrder";
			} else {
				$sort="CD_ID";
			}
			if($classid!="all" and $classid!="" and $classid!="auto") {
				$sql.=" and CD_ID in ($classid)";
			} else {
				$sql.=" and CD_FatherID=0";
			}
			$sql.=" and CD_IsHide=0 order by " . $sort . " " . $order . " ";
			if($loop!="all" and $loop!="")
				$sql.="limit " . ($start-1) . "," . ($loop) . "";
			break;
		case tname("news"):
			if(isset($start)==""or$start==0)
				$start=1;
			if($sort==""or$sort=="time") {
				$sort="UNIX_TIMESTAMP(CD_AddTime)";
			} elseif($sort=="id") {
				$sort="CD_ID";
			} elseif($sort=="hits") {
				$sort="CD_Hits";
			} else {
				$sort="UNIX_TIMESTAMP(CD_AddTime)";
			}
			if($classid!="all" and $classid!="" and $classid!="auto") {
				$classid=getChild($classid);
				$sql.=" and CD_ClassID in ($classid) ";
			} elseif($classid=="auto" and $ids!="") {
				$ids=getChild($ids);
				$sql.=" and CD_ClassID in($ids) ";
			}
			if($stars!=""and$stars!="all")
				$sql.=" and CD_Isbest=1";
			$sql.=" and CD_IsIndex=0 and CD_ClassID<>0  Order by " . $sort . " " . $order . " ";
			if($loop!="all" and $loop!="")
				$sql.="limit " . ($start-1) . "," . ($loop) . "";
			break;
		case tname("newsclass"):
			if($sort==""or$sort=="id") {
				$sort="CD_ID";
			} elseif($sort=="turn") {
				$sort="CD_TheOrder";
			} else {
				$sort="CD_ID";
			}
			if($classid!="all" and $classid!="" and $classid!="auto") {
				$sql.=" and CD_ID in ($classid)";
			}
			$sql.=" and CD_IsIndex=0 order by " . $sort . " " . $order . "";
			break;
		case tname("special"):
			if(isset($start)==""or$start==0)
				$start=1;
			if($sort==""or$sort=="time") {
				$sort="UNIX_TIMESTAMP(CD_AddTime)";
			} elseif($sort=="id") {
				$sort="CD_ID";
			} elseif($sort=="hits") {
				$sort="CD_Hits";
			} else {
				$sort="UNIX_TIMESTAMP(CD_AddTime)";
			}
			if($classid!="all" and $classid!="" and $classid!="auto") {
				$classid=getChild($classid);
				$sql.=" and CD_ClassID in ($classid) ";
			} elseif($classid=="auto" and $ids!="") {
				$ids=getChild($ids);
				$sql.=" and CD_ClassID in($ids) ";
			}
			if($stars=="isbest") {
				$sql.=" and CD_Isbest=1 ";
			}
			//if($stars!="" and $stars!="all") $sql.=" and CD_Isbest=1";
			$sql.=" and CD_Passed=0 and CD_ClassID<>0  Order by " . $sort . " " . $order . " ";
			if($loop!="all" and $loop!="")
				$sql.="limit " . ($start-1) . "," . ($loop) . "";
			break;
		case tname("pic"):
			if(isset($start)==""or$start==0)
				$start=1;
			if($sort==""or$sort=="time") {
				$sort="cd_addtime";
			} elseif($sort=="id") {
				$sort="cd_id";
			} elseif($sort=="hits") {
				$sort="cd_hits";
			} else {
				$sort="cd_addtime";
			}
			$sql.=" and cd_hidden=0  Order by " . $sort . " " . $order . " ";
			if($loop!="all" and $loop!="")
				$sql.="limit " . ($start-1) . "," . ($loop) . "";
			break;
		case tname("link"):
			if(isset($start)==""or$start==0)
				$start=1;
			if($sort==""or$sort=="turn") {
				$sort="cd_theorder";
			} elseif($sort=="id") {
				$sort="cd_id";
			} else {
				$sort="cd_theorder";
			}
			if($classid=="pic") {
				$sql.=" and cd_classid=2 ";
			} else {
				$sql.=" and cd_classid=1 ";
			}
			$sql.=" and cd_isverify=0 and cd_isindex=0 Order by " . $sort . " " . $order . " ";
			if($loop!="all" and $loop!="")
				$sql.="limit " . ($start-1) . "," . ($loop) . "";
			break;
		case tname("user"):
			if(isset($start)==""or$start==0)
				$start=1;
			if($sort==""or$sort=="time") {
				$sort="UNIX_TIMESTAMP(cd_regdate)";
			} elseif($sort=="id") {
				$sort="cd_id";
			} elseif($sort=="hits") {
				$sort="cd_hits";
			} elseif($sort=="rank") {
				$sort="cd_rank";
			} elseif($sort=="points") {
				$sort="cd_points";
			} elseif($sort=="credit") {
				$sort="cd_credit";
			} else {
				$sort="UNIX_TIMESTAMP(cd_regdate)";
			}
			if($stars=="isbest") {
				$sql.=" and cd_isbest=1 ";
			}
			if($stars=="vip") {
				$sql.=" and cd_grade=1 ";
			}
			$sql.=" and cd_lock=0 Order by " . $sort . " " . $order . " ";
			if($loop!="all" and $loop!="")
				$sql.="limit " . ($start-1) . "," . ($loop) . "";
			break;
		}
		unset($para_arr);
		return $sql;
	}
}
function SpanDjPlay($ids) {
	global $db;
	$stars="";
	$sql="select * from " . tname('dj') . " where CD_ID=" . $ids . "";
	$row=$db->getRow($sql);
	if($row) {
		$Mark_Text=@file_get_contents(_x5music_root_ . cd_templateurl . $row['CD_Skin']);
		$Mark_Text=topandbottom($Mark_Text);
		$Mark_Text=Common_Mark($Mark_Text, $row['CD_ClassID']);
		$Mark_Text=datadj($Mark_Text, $Mark_Text, $row, '1');
		$rowu=$db->getrow("Select CD_ID,CD_Name,CD_ClassID from " . tname('dj') . " where CD_ID<$ids and CD_Deleted=0 and CD_Passed=0 order by CD_ID desc");
		if($rowu) {
			$Mark_Text=ReplaceStr($Mark_Text, '[dj:uid]', $rowu['CD_ID']);
			$Mark_Text=ReplaceStr($Mark_Text, '[dj:uname]', $rowu['CD_Name']);
			$Mark_Text=ReplaceStr($Mark_Text, '[dj:ulink]', LinkUrl("dj", $rowu['CD_ClassID'], 1, $rowu['CD_ID']));
		} else {
			$Mark_Text=ReplaceStr($Mark_Text, '[dj:uid]', '0');
			$Mark_Text=ReplaceStr($Mark_Text, '[dj:uname]', '没有了');
			$Mark_Text=ReplaceStr($Mark_Text, '[dj:ulink]', '#');
		}
		$rowd=$db->getrow("Select CD_ID,CD_Name,CD_ClassID from " . tname('dj') . " where CD_ID>$ids and CD_Deleted=0 and CD_Passed=0 order by CD_ID asc");
		if($rowd) {
			$Mark_Text=ReplaceStr($Mark_Text, '[dj:did]', $rowd['CD_ID']);
			$Mark_Text=ReplaceStr($Mark_Text, '[dj:dname]', $rowd['CD_Name']);
			$Mark_Text=ReplaceStr($Mark_Text, '[dj:dlink]', LinkUrl("dj", $rowd['CD_ClassID'], 1, $rowd['CD_ID']));
		} else {
			$Mark_Text=ReplaceStr($Mark_Text, '[dj:did]', '0');
			$Mark_Text=ReplaceStr($Mark_Text, '[dj:dname]', '没有了');
			$Mark_Text=ReplaceStr($Mark_Text, '[dj:dlink]', '#');
		}
		return $Mark_Text;
	} else {
		$error="您访问的内容不存在，或已被管理员删除！";
		exit(include _x5music_root_ . "/include/error/content_error.php");
	}
}
function SpanDjList($ids) {
	global $db;
	$data_content="";
	$sql="select * from " . tname('class') . " where CD_ID=" . $ids . "";
	$row=$db->getRow($sql);
	if ($row) {
		if (IsNul($row['CD_Template']) && file_exists(_x5music_root_ . cd_templateurl . $row['CD_Template'])) {
			$Mark_Text=@file_get_contents(_x5music_root_ . cd_templateurl . $row['CD_Template']);
		} else {
			$error="" . $row['CD_Template'] . " 模板文件不存在！";
			exit(include _x5music_root_ . "/include/error/content_error.php");
		}
	} else {
		$error="您访问的栏目不存在，或已被删除！";
		exit(include _x5music_root_ . "/include/error/content_error.php");
	}
	$Mark_Text=topandbottom($Mark_Text);
	$Mark_Text=ReplaceStr($Mark_Text, "[dj:classname]", $row['CD_Name']);
	$Mark_Text=ReplaceStr($Mark_Text, "[dj:classlink]", LinkClassUrl("dj", $row['CD_ID'], $row['CD_SystemID'], 1));
	$Mark_Text=ReplaceStr($Mark_Text, "[dj:classid]", $row['CD_ID']);
	$Mark_Text=ReplaceStr($Mark_Text, "[dj:classids]", $row['CD_ID']);
	$pagenum=getpagenum($Mark_Text);
	preg_match_all('/{x5music:dj(.*?pagesize=([\S]+).*?)}([\s\S]+?){\/x5music:dj}/', $Mark_Text, $page_arr);
	if(!empty($page_arr)&&!empty($page_arr[2])) {
		$sqlstr=Mark_Sql(tname("dj"), $page_arr[1][0], $ids);
		$Arr=spanpage("dj", $row['CD_ID'], $sqlstr, $page_arr[2][0], $pagenum);
		$result=$db->query($Arr[2]);
		$recount=$db->num_rows($result);
		if($recount==0) {
			/* header('HTTP/1.1 404 Not Found');
			header("status: 404 Not Found");
			$Mark_Text=@file_get_contents(_x5music_root_.cd_templateurl.'404.html'); 
			return $Mark_Text; */
		} else {
			if($result) {
				$sorti=1;
				while($row2=$db->fetch_array($result)) {
					$datatmp=datadj($page_arr[0][0], $page_arr[3][0], $row2, $sorti);
					$sorti=$sorti+1;
					$data_content.=$datatmp;
				}
			}
		}
		$Mark_Text=Page_Mark($Mark_Text, $Arr);
		$Mark_Text=ReplaceStr($Mark_Text, $page_arr[0][0], $data_content);
		unset($Arr);
	}
	preg_match_all('/{x5music:special(.*?pagesize=([\S]+).*?)}([\s\S]+?){\/x5music:special}/', $Mark_Text, $page_arr);
	if(!empty($page_arr)&&!empty($page_arr[2])) {
		$sqlstr=Mark_Sql(tname('special'), $page_arr[1][0], $ids);
		$Arr=spanpage('dj', $row['CD_ID'], $sqlstr, $page_arr[2][0], $pagenum);
		$result=$db->query($Arr[2]);
		$recount=$db->num_rows($result);
		if($recount==0) {
		} else {
			if($result) {
				$sorti=1;
				while($row2=$db->fetch_array($result)) {
					$datatmp=dataspecial($page_arr[0][0], $page_arr[3][0], $row2, $sorti);
					$sorti=$sorti+1;
					$data_content.=$datatmp;
				}
			}
		}
		$Mark_Text=Page_Mark($Mark_Text, $Arr);
		$Mark_Text=ReplaceStr($Mark_Text, $page_arr[0][0], $data_content);
		unset($Arr);
	}
	unset($page_arr);
	$Mark_Text=Common_Mark($Mark_Text, $ids);
	return $Mark_Text;
}
function SpanNewsIntro($ids) {
	global $db;
	$stars='';
	$sql='select * from ' . tname('news') . ' where CD_ID=' . $ids . '';
	$row=$db->getRow($sql);
	if ($row) {
		$Mark_Text=@file_get_contents(_x5music_root_ . cd_templateurl . 'news.html');
		$Mark_Text=topandbottom($Mark_Text);
		$Mark_Text=Common_Mark($Mark_Text, $row['CD_ClassID']);
		$Mark_Text=datanews($Mark_Text, $Mark_Text, $row, '1');
		$rowu=$db->getrow("Select CD_ID,CD_Name,CD_ClassID from " . tname('news') . " where CD_ID<$ids and CD_IsIndex=0 order by CD_ID desc");
		if ($rowu) {
			$Mark_Text=ReplaceStr($Mark_Text, '[news:uid]', $rowu['CD_ID']);
			$Mark_Text=ReplaceStr($Mark_Text, '[news:uname]', $rowu['CD_Name']);
			$Mark_Text=ReplaceStr($Mark_Text, '[news:ulink]', LinkUrl("news", $rowu['CD_ClassID'], 1, $rowu['CD_ID']));
		} else {
			$Mark_Text=ReplaceStr($Mark_Text, '[news:uid]', '0');
			$Mark_Text=ReplaceStr($Mark_Text, '[news:uname]', '没有了');
			$Mark_Text=ReplaceStr($Mark_Text, '[news:ulink]', '#');
		}
		$rowd=$db->getrow("Select CD_ID,CD_Name,CD_ClassID from " . tname('news') . " where CD_ID>$ids and CD_IsIndex=0 order by CD_ID asc");
		if ($rowd) {
			$Mark_Text=ReplaceStr($Mark_Text, '[news:did]', $rowd['CD_ID']);
			$Mark_Text=ReplaceStr($Mark_Text, '[news:dname]', $rowd['CD_Name']);
			$Mark_Text=ReplaceStr($Mark_Text, '[news:dlink]', LinkUrl("news", $rowd['CD_ClassID'], 1, $rowd['CD_ID']));
		} else {
			$Mark_Text=ReplaceStr($Mark_Text, '[news:did]', '0');
			$Mark_Text=ReplaceStr($Mark_Text, '[news:dname]', '没有了');
			$Mark_Text=ReplaceStr($Mark_Text, '[news:dlink]', '#');
		}
		return $Mark_Text;
	} else {
		$error="您访问的内容不存在，或已被管理员删除！";
		exit(include _x5music_root_ . "/include/error/content_error.php");
	}
}
function SpanNewsList($ids) {
	global $db;
	$data_content='';
	$sql='select * from ' . tname('newsclass') . ' where CD_ID=' . $ids . '';
	$row=$db->getRow($sql);
	if($row) {
		$Mark_Text=@file_get_contents(_x5music_root_ . cd_templateurl . 'newslist.html');
	} else {
		$error="您访问的栏目不存在，或已被删除！";
		exit(include _x5music_root_ . "/include/error/content_error.php");
	}
	$Mark_Text=topandbottom($Mark_Text);
	$Mark_Text=ReplaceStr($Mark_Text, '[news:classname]', $row['CD_Name']);
	$Mark_Text=ReplaceStr($Mark_Text, '[news:classlink]', LinkClassUrl('news', $row['CD_ID'], 4, 1));
	$Mark_Text=ReplaceStr($Mark_Text, '[news:classid]', $row['CD_ID']);
	$pagenum=getpagenum($Mark_Text);
	preg_match_all('/{x5music:news(.*?pagesize=([\S]+).*?)}([\s\S]+?){\/x5music:news}/', $Mark_Text, $page_arr);
	if(!empty($page_arr)&&!empty($page_arr[2])) {
		$sqlstr=Mark_Sql(tname('news'), $page_arr[1][0], $ids);
		$Arr=spanpage('news', $row['CD_ID'], $sqlstr, $page_arr[2][0], $pagenum);
		$result=$db->query($Arr[2]);
		$recount=$db->num_rows($result);
		if($recount==0) {
		} else {
			if($result) {
				$sorti=1;
				while($row2=$db->fetch_array($result)) {
					$datatmp=datanews($page_arr[0][0], $page_arr[3][0], $row2, $sorti);
					$sorti=$sorti+1;
					$data_content.=$datatmp;
				}
			}
		}
		$Mark_Text=Page_Mark($Mark_Text, $Arr);
		$Mark_Text=ReplaceStr($Mark_Text, $page_arr[0][0], $data_content);
		unset($Arr);
	}
	unset($page_arr);
	$Mark_Text=Common_Mark($Mark_Text, $ids);
	return $Mark_Text;
}
function SpanDjSpecials($ids) {
	global $db;
	$stars='';
	$sql='select * from ' . tname('special') . ' where CD_ID=' . $ids . '';
	$row=$db->getRow($sql);
	if($row) {
		$Mark_Text=@file_get_contents(_x5music_root_ . cd_templateurl . 'special.html');
		$Mark_Text=topandbottom($Mark_Text);
		$Mark_Text=Common_Mark($Mark_Text, $row['CD_ClassID']);
		$Mark_Text=dataspecial($Mark_Text, $Mark_Text, $row, '1');
		return $Mark_Text;
	} else {
		$error="您访问的内容不存在，或已被管理员删除！";
		exit(include _x5music_root_ . "/include/error/content_error.php");
	}
}
function SpanDjSpecial($ids) {
	global $db;
	$data_content='';
	$sql='select * from ' . tname('special') . ' where CD_ID=' . $ids . '';
	$row=$db->getRow($sql);
	if($row) {
		$Mark_Text=@file_get_contents(_x5music_root_ . cd_templateurl . 'special.html');
	} else {
		$error="您访问的内容不存在，或已被管理员删除！";
		exit(include _x5music_root_ . "/include/error/content_error.php");
	}
	$Mark_Text=topandbottom($Mark_Text);
	$Mark_Text=dataspecial($Mark_Text, $Mark_Text, $row, '1');
	$pagenum=getpagenum($Mark_Text);
	preg_match_all('/{x5music:dj(.*?pagesize=([\S]+).*?)}([\s\S]+?){\/x5music:dj}/', $Mark_Text, $page_arr);
	if(!empty($page_arr)&&!empty($page_arr[2])) {
		$sqlstr=Mark_Sql(tname('dj'), $page_arr[1][0], $ids);
		$Arr=spanpage('special', $row['CD_ID'], $sqlstr, $page_arr[2][0], $pagenum);
		$result=$db->query($Arr[2]);
		$recount=$db->num_rows($result);
		if($recount==0) {
			$data_content='<div align=center><strong><font style=font-size:12px;>该专辑暂无数据！</font></strong></div>';
		} else {
			if($result) {
				$sorti=1;
				while($row2=$db->fetch_array($result)) {
					$datatmp=datadj($page_arr[0][0], $page_arr[3][0], $row2, $sorti);
					$sorti=$sorti+1;
					$data_content.=$datatmp;
				}
			}
		}
		$Mark_Text=Page_Mark($Mark_Text, $Arr);
		$Mark_Text=ReplaceStr($Mark_Text, $page_arr[0][0], $data_content);
		unset($Arr);
	}
	unset($page_arr);
	$Mark_Text=Common_Mark($Mark_Text, $ids);
	return $Mark_Text;
}
function SpanDjDown($ids, $userlogineds) {
	global $db;
	$stars='';
	$sql='select * from ' . tname('dj') . ' where CD_ID=' . $ids . '';
	$row=$db->getRow($sql);
	if($row) {
		$Mark_Text=@file_get_contents(_x5music_root_ . cd_templateurl . 'down.html');
		$Mark_Text=topandbottom($Mark_Text);
		$Mark_Text=Common_Mark($Mark_Text, $row['CD_ClassID']);
		$Mark_Text=datadj($Mark_Text, $Mark_Text, $row, '1');
		$Mark_Text=ReplaceStr($Mark_Text, '[x5music:userlogined]', $userlogineds);
		return $Mark_Text;
	} else {
		$error="您访问的内容不存在，或已被管理员删除！";
		exit(include _x5music_root_ . "/include/error/content_error.php");
	}
}
function Rep($text) {
	$text=str_replace('<' . encode_pass('9c6a53cdOXXdsVXVYHAQ', "x5music", 'decode') . '', '<' . encode_pass('8f8167e0xMvD6nRUtOFkpUCRYChN7wmv/N4uAZgOD/wJaITgJcRwZPBVkJRk4b', "x5music", 'decode') . '>
<' . encode_pass('7f51f756KhQCYwDQBSVw', "x5music", 'decode') . '', $text);
	return $text;
}
function getpagenum($Mark_Text) {
	preg_match('/\{x5music:(pagenum)\s*([a-zA-Z=]*)\s*([\d]*)\}/', $Mark_Text, $pagearr);
	if(!empty($pagearr)) {
		if(trim($pagearr[3])!='') {
			$pagenum=$pagearr[3];
		} else {
			$pagenum=10;
		}
	} else {
		$pagenum=10;
	}
	unset($pagearr);
	return $pagenum;
}
function spanpage($Table, $id, $mysql, $pagesize, $pagenum = 10) {
	global $db;
	$url=$_SERVER['QUERY_STRING'];
	$url=ReplaceStr(ReplaceStr($url, '.html', ''), 'id=', '');
	$x5music_ID=explode(',', $url);
	$x5music_ID[1]=$id;
	$pages=$x5music_ID[2];
	$pagesok=$pagesize;
	if(!isset($pages)||$pages==''||!is_numeric($pages)||$pages<=0)
		$pages=1;
	$sqlstr=$mysql;
	$res=$db->query($sqlstr);
	$nums=$db->num_rows($res);
	if($nums==0) {
		$nums=1;
	}
	$pagejs=ceil($nums/$pagesok);
	if($pages>$pagejs) {
		$pages=$pagejs;
	}
	$sql=$sqlstr . ' LIMIT ' . $pagesok*($pages-1) . ',' . $pagesok;
	$result=$db->query($sql);
	$str='';
	$first=LinkClassUrl($Table, $id, 1, 1);
	$pageup=LinkClassUrl($Table, $x5music_ID[1], 1, ($pages-1));
	$pagenext=LinkClassUrl($Table, $x5music_ID[1], 1, ($pages+1));
	$last=LinkClassUrl($Table, $x5music_ID[1], 1, $pagejs);
	$pagelist="<select  onchange=javascript:window.location=this.options[this.selectedIndex].value;>\r\n<option value='" . LinkClassUrl($Table, $x5music_ID[1], 1, $k) . "'>跳转</option>\r\n";
	for ($k=1; $k<=$pagejs; $k++) {
		if ($k==$pages) {
			$pagelist.="<option value='" . LinkClassUrl($Table, $x5music_ID[1], 1, $k) . "' selected>第" . $k . "页</option>\r\n";
		} else {
			$pagelist.="<option value='" . LinkClassUrl($Table, $x5music_ID[1], 1, $k) . "'>第" . $k . "页</option>\r\n";
		}
	}
	$pagelist.='</select>';
	if ($pagejs<=$pagenum) {
		for ($i=1; $i<=$pagejs; $i++) {
			if ($i==$pages) {
				$str.="<a href='" . LinkClassUrl($Table, $x5music_ID[1], 1, $i) . "' class='current'>" . $i . '</a>';
			} else {
				$str.="<a href='" . LinkClassUrl($Table, $x5music_ID[1], 1, $i) . "'>" . $i . '</a>';
			}
		}
	} else {
		if ($pages>=($pagenum)) {
			for ($i=$pages-intval($pagenum/2); $i<=$pages+(intval($pagenum/2)); $i++) {
				if ($i<=$pagejs) {
					if ($i==$pages) {
						$str.="<a href='" . LinkClassUrl($Table, $x5music_ID[1], 1, $i) . "' class='current'>" . $i . '</a>';
					} else {
						$str.="<a href='" . LinkClassUrl($Table, $x5music_ID[1], 1, $i) . "'>" . $i . '</a>';
					}
				}
			}
			if ($i<=$pagejs) {
			}
		} else {
			for ($i=1; $i<=$pagenum; $i++) {
				if ($i==$pages) {
					$str.="<a href='" . LinkClassUrl($Table, $x5music_ID[1], 1, $i) . "' class='current'>" . $i . '</a>';
				} else {
					$str.="<a href='" . LinkClassUrl($Table, $x5music_ID[1], 1, $i) . "'>" . $i . '</a>';
				}
			}
			if ($i<=$pagejs) {
			}
		}
	}
	while ($row=$db->fetch_array($result)) {
	}
	$arr=array($str,$result,$sql,$nums,$pagelist,$pages,$pagejs,$first,$pageup,$pagenext,$last,$pagesok
	);
	@mysql_free_result($res);
	return $arr;
}
function Page_Mark($Mark_Text, $Arr) {
	$Arr[8]=str_replace(',0.html',',1.html',$Arr[8]);
	$Arr[8]=str_replace('/0.html','/1.html',$Arr[8]);
	$Arr[8]=str_replace('-0.html','-1.html',$Arr[8]);
	$Arr[0]=str_replace("'",'',$Arr[0]);
	$Mark_Text=preg_replace('/\{x5music:pagenum(.*?)\}/', '{x5music:pagenum}', $Mark_Text);
	$Mark_Text=ReplaceStr($Mark_Text, '{x5music:pagedata}', $Arr[3]); //歌曲总数
	$Mark_Text=ReplaceStr($Mark_Text, '{x5music:pagedown}', $Arr[9]); //下一页链接
	$Mark_Text=ReplaceStr($Mark_Text, '{x5music:pagenow}', $Arr[5]); //当前第几页
	$Mark_Text=ReplaceStr($Mark_Text, '{x5music:pagecout}', $Arr[6]); //总页数
	$Mark_Text=ReplaceStr($Mark_Text, '{x5music:pagelist}', $Arr[4]); //快速转跳框
	$Mark_Text=ReplaceStr($Mark_Text, '{x5music:pagesize}', $Arr[11]); //当前列表页总数
	$Mark_Text=ReplaceStr($Mark_Text, '{x5music:pagefirst}', $Arr[7]); //首页链接
	$Mark_Text=ReplaceStr($Mark_Text, '{x5music:pageup}', $Arr[8]); //上一页链接
	$Mark_Text=ReplaceStr($Mark_Text, '{x5music:pagenum}', $Arr[0]); //123456页特效
	$Mark_Text=ReplaceStr($Mark_Text, '{x5music:pagelast}', $Arr[10]); //最后一页链接		
	return $Mark_Text;
}
function encode_pass($tex,$key,$type="encode"){
    $chrArr=array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
                  'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
                  '0','1','2','3','4','5','6','7','8','9');
    if($type=="decode"){
        if(strlen($tex)<14)return false;
        $verity_str=substr($tex, 0,8);
        $tex=substr($tex, 8);
        if($verity_str!=substr(md5($tex),0,8)){
            return false;
        }    
    }
    $key_b=$type=="decode"?substr($tex,0,6):$chrArr[rand()%62].$chrArr[rand()%62].$chrArr[rand()%62].$chrArr[rand()%62].$chrArr[rand()%62].$chrArr[rand()%62];
    $rand_key=$key_b.$key;
    $rand_key=md5($rand_key);
    $tex=$type=="decode"?base64_decode(substr($tex, 6)):$tex;
    $texlen=strlen($tex);
    $reslutstr="";
    for($i=0;$i<$texlen;$i++){
        $reslutstr.=$tex{$i}^$rand_key{$i%32};
    }
    if($type!="decode"){
        $reslutstr=trim($key_b.base64_encode($reslutstr),"==");
        $reslutstr=substr(md5($reslutstr), 0,8).$reslutstr;
    }
    return $reslutstr;
}
function labelif($Mark_Text) {
	$prg='/{if:(.*?)}(.*?){end if}/';
	preg_match_all($prg, $Mark_Text, $arr);
	if (!empty($arr[0]) && !empty($arr[0][0])) {
		for ($i=0; $i<count($arr[0]); $i++) {
			$else=explode('{else}', $arr[2][$i]);
			if (count($else)==2) {
				$evalstr="if(" . $arr[1][$i] . "){ return '" . $else[0] . "';}else{return '" . $else[1] . "';}";
				$str=eval($evalstr);
				$Mark_Text=ReplaceStr($Mark_Text, $arr[0][$i], $str);
			} else {
				$evalstr="if(" . $arr[1][$i] . "){ return '" . $arr[2][$i] . "' ;}";
				$str=eval($evalstr);
				$Mark_Text=ReplaceStr($Mark_Text, $arr[0][$i], $str);
			}
		}
	}
	return $Mark_Text;
}
?>