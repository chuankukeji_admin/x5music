<?php
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
error_reporting(0);
session_start();
date_default_timezone_set('PRC');
function ss_timing_start($name = 'default') {
	global $ss_timing_start_times;
	$ss_timing_start_times[$name]=explode(' ', microtime());
}
function ss_timing_stop($name = 'default') {
	global $ss_timing_stop_times;
	$ss_timing_stop_times[$name]=explode(' ', microtime());
}
function ss_timing_current($name = 'default') {
	global $ss_timing_start_times, $ss_timing_stop_times;
	if(!isset($ss_timing_start_times[$name])) {
		return 0;
	}
	if(!isset($ss_timing_stop_times[$name])) {
		$stop_time=explode(' ', microtime());
	} else {
		$stop_time=$ss_timing_stop_times[$name];
	}
	$current=$stop_time[1]-$ss_timing_start_times[$name][1];
	$current+=$stop_time[0]-$ss_timing_start_times[$name][0];
	return $current;
}
ss_timing_start();
function getpiclink($filepath) {
	if(empty($filepath)) {
		$url=cd_webpath . 'user/image/nologo.jpg';
	} else {
		$url=$filepath;
		if(file_exists($filepath . ".thumb.jpg")) {
			$url.='.thumb.jpg';
		}
	}
	return $url;
}
function isImage($filename) {
	$types='.gif|.jpeg|.png|.bmp|.jpg';
	if(file_exists($filename)) {
		$info=getimagesize($filename);
		$ext=image_type_to_extension($info['2']);
		return stripos($types, $ext);
	} else {
		return false;
	}
}
function bqwhits($hits) {
	$b=1000;
	$c=10000;
	if($hits>$b) {
		if($hits<$c) {
			//return floor($hits/$b).'千';
			return $hits;
		} else {
			return (floor(($hits/$c)*10)/10) . '万';
		}
	} else {
		return $hits;
	}
}
function formatsize($size) {
	$prec=3;
	$size=round(abs($size));
	$units=array(0=>" B ",1=>" KB",2=>" MB",3=>" GB",4=>" TB");
	if($size==0)
		return str_repeat(" ", $prec) . "0$units[0]";
	$unit=min(4, floor(log($size)/log(2)/10));
	$size=$size*pow(2, -10*$unit);
	$digi=$prec-1-floor(log($size)/log(10));
	$size=round($size*pow(10, $digi))*pow(10, -$digi);
	return $size . $units[$unit];
}
function getlen($para = '', $val = '', $res = '') {
	if(!empty($para)&&!empty($val)) {
		if(is_numeric($val)) {
			$cont=cnsubstr($res, intval($val));
		} else {
			$cont=$res;
		}
	} else {
		$cont=$res;
	}
	return $cont;
}
function fileext($filename) {
	return strtolower(trim(substr(strrchr($filename, '.'), 1)));
}
function tname($name) {
	return cd_tablename . $name;
}
function cookiename($name) {
	return CD_Cookiepre . $name;
}
function IsNum($str) {
	if(is_numeric($str)) {
		return true;
	} else {
		return false;
	}
}
function IsNul($str) {
	if(!is_string($str))
		return false;
	if(empty($str))
		return false;
	if($str=='')
		return false;
	return true;
}
function IsNumID($str) {
	if(preg_match('/^\d*$/', $str)) {
		return true;
	} else {
		return false;
	}
}
function delunlink($type) {
	$types=strtolower(strtolower(trim(substr(strrchr($type, '.'), 1))));
	if(strpos('bmp,jpg,png,jpeg,gif,avi,mov,asf,wmv,3gp,mkv,mp4', $types)!==false) {
		return $types;
	} else {
		return false;
	}
}
//Mobile
function isMobile() {
	$useragent=isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
	$useragent_commentsblock=preg_match('|\(.*?\)|', $useragent, $matches)>0 ? $matches[0] : '';
	function CheckSubstrs($substrs, $text) {
		foreach($substrs as $substr)
			if(false!==strpos($text, $substr)) {
				return true;
			}
		return false;
	}
	$mobile_os_list=array('Google Wireless Transcoder','Windows CE','WindowsCE','Symbian','Android','armv6l','armv5','Mobile','CentOS','mowser','AvantGo','Opera Mobi','J2ME/MIDP','Smartphone','Go.Web','Palm','iPAQ');
	$mobile_token_list=array('Profile/MIDP','Configuration/CLDC-','160×160','176×220','240×240','240×320','320×240','UP.Browser','UP.Link','SymbianOS','PalmOS','PocketPC','SonyEricsson','Nokia','BlackBerry','Vodafone','BenQ','Novarra-Vision','Iris','NetFront','HTC_','Xda_','SAMSUNG-SGH','Wapaka','DoCoMo','iPhone','iPod');
	$found_mobile=CheckSubstrs($mobile_os_list, $useragent_commentsblock) || CheckSubstrs($mobile_token_list, $useragent);
	if($found_mobile) {
		return true;
	} else {
		return false;
	}
}
function RemoveHTML($str) {
	preg_replace("/<.+?>/i", "", $str);
	$str=ReplaceStr($str, "&lt;DIV&gt;", "");
	$str=ReplaceStr($str, "&lt;/DIV&gt;", "");
	$str=ReplaceStr($str, "&gt;", "");
	$str=ReplaceStr($str, "&apos;", "");
	$str=ReplaceStr($str, "&nbsp;", "");
	$str=ReplaceStr($str, " ", "");
	$str=ReplaceStr($str, "&quot;", "");
	$str=ReplaceStr($str, "&amp;", "");
	$str=ReplaceStr($str, chr(13), "");
	$str=ReplaceStr($str, chr(10), "");
	return $str;
}
function str_encode($str) {
	$str=ReplaceStr($str, "<", "&lt;");
	$str=ReplaceStr($str, ">", "&gt;");
	$str=ReplaceStr($str, "'", "&apos;");
	$str=ReplaceStr($str, " ", "&nbsp;");
	$str=ReplaceStr($str, "\r\n", "<br>");
	$str=ReplaceStr($str, "\"", "&quot;");
	$str=ReplaceStr($str, "&", "&amp;");
	return $str;
}
function str_decode($str) {
	$str=ReplaceStr($str, "&lt;", "<");
	$str=ReplaceStr($str, "&gt;", ">");
	$str=ReplaceStr($str, "&apos;", "'");
	$str=ReplaceStr($str, "&nbsp;", " ");
	$str=ReplaceStr($str, "<br>", "\r\n");
	$str=ReplaceStr($str, "&quot;", "\"");
	$str=ReplaceStr($str, "&amp;", "&");
	return $str;
}
function str_sex($str) {
	$str=ReplaceStr($str, "1", "男");
	$str=ReplaceStr($str, "2", "女");
	return $str;
}
function numtoabc($str) {
	$str=ReplaceStr($str, "0", "ling");
	$str=ReplaceStr($str, "1", "yi");
	$str=ReplaceStr($str, "2", "er");
	$str=ReplaceStr($str, "3", "san");
	$str=ReplaceStr($str, "4", "si");
	$str=ReplaceStr($str, "5", "wu");
	$str=ReplaceStr($str, "6", "liu");
	$str=ReplaceStr($str, "7", "qi");
	$str=ReplaceStr($str, "8", "ba");
	$str=ReplaceStr($str, "9", "jiu");
	return $str;
}
function SafeRequest($key, $mode, $isfilter = '') {
	set_magic_quotes_runtime(0);
	$magic=get_magic_quotes_gpc();
	switch($mode) {
	case 'post':
		$value=isset($_POST[$key]) ? $magic ? trim($_POST[$key]) : addslashes(trim($_POST[$key])) : '';
		break;
	case 'posts':
		$value=isset($_POST[$key]) ? $magic ? trim($_POST[$key]) : addslashes(trim($_POST[$key])) : '';
		$value=shtmlspecialchars($value);
		break;
	case 'get':
		$value=isset($_GET[$key]) ? $magic ? trim($_GET[$key]) : addslashes(trim($_GET[$key])) : '';
		break;
	case 'gets':
		$value=isset($_GET[$key]) ? $magic ? trim($_GET[$key]) : addslashes(trim($_GET[$key])) : '';
		$value=shtmlspecialchars($value);
		break;
	default:
		$value=isset($_POST[$key]) ? $magic ? trim($_POST[$key]) : addslashes(trim($_POST[$key])) : '';
		if($value=="") {
			$value=isset($_GET[$key]) ? $magic ? trim($_GET[$key]) : addslashes(trim($_GET[$key])) : '';
		}
		break;
	}
	if($isfilter!='') {
		$value=str_encode($value);
	}
	return $value;
}
function str_checkhtml($document) {
	$search=array('@<script[^>]*?>.*?</script>@si','@<style[^>]*?>.*?</style>@siU','@<[\/\!]*?[^<>]*?>@si','@<![\s\S]*?--[ \t\n\r]*>@');
	$text=preg_replace($search, '', $document);
	return $text;
}
function shtmlspecialchars($string) {
	if(is_array($string)) {
		foreach($string as $key=>$val) {
			$string[$key]=shtmlspecialchars($val);
		}
	} else {
		$string=preg_replace('/&amp;((#(\d{3,5}|x[a-fA-F0-9]{4})|[a-zA-Z][a-z0-9]{2,5});)/', '&\\1', str_replace(array('&','"','<','>'), array('&amp;','&quot;','&lt;','&gt;'), $string));
	}
	return $string;
}
function HotSearch($Search_Key, $type = "0") {
	$Search_List="";
	$Search_Key1="";
	$Search_Key=trim($Search_Key);
	$Str=" @,@|@_";
	$StrArr=explode('@', $Str);
	for($i=0; $i<=3; $i++) {
		if(stristr($Search_Key, $StrArr[$i])) {
			$Search_Key1=explode($StrArr[$i], $Search_Key);
		}
	}
	if(is_array($Search_Key1)) {
		for($j=0; $j<count($Search_Key1); $j++) {
			$Search_Key1[$j]=str_replace(",", '', $Search_Key1[$j]);
			$Search_Key1[$j]=iconv("GB2312", "GB2312", $Search_Key1[$j]);
			if($Search_Key1[$j]) {
				$Search_List=$Search_List . "<a target=\"blank\" href=\"" . cd_webpath . "include/search.php?key=" . urlencode($Search_Key1[$j]) . "\">" . $Search_Key1[$j] . "</a>";
			}
		}
	} else {
		$Search_List=$Search_Key;
	}
	return $Search_List;
}
function RequestBox($key, $classid = '0') {
	if($classid==1) {
		$array=isset($_GET[$key]) ? $_GET[$key] : '';
	} else {
		$array=isset($_POST[$key]) ? $_POST[$key] : '';
	}
	if($array=="") {
		$value="0";
	} else {
		for($i=0; $i<count($array); $i++) {
			$value=implode(',', $array);
		}
	}
	return $value;
}
function ReplaceStr($text, $search, $replace) {
	if(empty($text))
		$text="";
	$result_text=str_replace($search, $replace, $text);
	return $result_text;
}
function spandir($dir) {
	$spandirpos=strrpos($dir, '/');
	$spandir=substr($dir, 0, $spandirpos+1);
	if(!file_exists("../../" . $spandir)) {
		mkdir("../../" . $spandir, 0777, true);
	}
}
function cnsubstr($str, $strlen = 10) {
	if(empty($str)||!is_numeric($strlen)) {
		$strlen=10;
	}
	$strlen=intval($strlen*2);
	if(strlen($str)<=$strlen) {
		return $str;
	}
	$last_word_needed=substr($str, $strlen-1, 1);
	if(!ord($last_word_needed)>128) {
		$needed_sub_sentence=substr($str, 0, $strlen);
		return $needed_sub_sentence;
	} else {
		for($i=0; $i<$strlen; $i++) {
			if(ord($str[$i])>128) {
				$i++;
			}
		}
		$needed_sub_sentence=substr($str, 0, $i);
		return $needed_sub_sentence;
	}
}
function comparetime($start, $end) {
	$timetamp=strtotime(date("Y-m-d H:00:00", strtotime($end)))-strtotime(date("Y-m-d H:00:00", strtotime($start)));
	return $timetamp/3600;
}
function datetime($TimeTime) {
	$limit=time()-strtotime($TimeTime);
	if($limit<5) {$show_t="刚刚";}
	if($limit>=5 and $limit<60) {$show_t=$limit . "秒前";}
	if($limit>=60 and $limit<3600) {$show_t=sprintf("%01.0f", $limit/60) . "分钟前";}
	if($limit>=3600 and $limit<86400) {$show_t=sprintf("%01.0f", $limit/3600) . "小时前";}
	if($limit>=86400 and $limit<2592000) {$show_t=sprintf("%01.0f", $limit/86400) . "天前";}
	if($limit>=2592000 and $limit<31104000) {$show_t=sprintf("%01.0f", $limit/2592000) . "个月前";}
	if($limit>=31104000) {$show_t=$TimeTime;}
	return $show_t;
}
function DateDiff($d1, $d2 = "") {
	if(is_string($d1))
		$d1=strtotime($d1);
	if(is_string($d2))
		$d2=strtotime($d2);
	return ($d2-$d1);
}
//上下页sql
function getpagerow($mysql, $pagesize) {
	global $db;
	$url=$_SERVER["QUERY_STRING"];
	if(stristr($url, '&pages')) {
		$url=preg_replace('/&pages=([\S]+?)$/', '', $url);
	}
	if(stristr($url, 'pages')) {
		$url=preg_replace('/pages=([\S]+?)$/', '', $url);
	}
	if(IsNul($url)) {
		$url.="&";
	}
	$pages=SafeRequest("pages", "get");
	$pagesok=$pagesize;
	if(!isset($pages)||$pages==""||!is_numeric($pages)||$pages<=0) {
		$pages=1;
	}
	$sqlstr=$mysql;
	$res=$db->query($sqlstr);
	$nums=$db->num_rows($res);
	if($nums==0) {
		$nums=1;
	}
	$str="<a>共有&nbsp;<strong>" . $nums . "</strong>&nbsp;条记录</a>";
	$pagejs=ceil($nums/$pagesok);
	if($pages>$pagejs) {
		$pages=$pagejs;
	}
	$sql=$sqlstr . " LIMIT " . $pagesok*($pages-1) . "," . $pagesok;
	$result=$db->query($sql);
	$str.="<a href='?" . $url . "pages=1' class='first' title='第一页'>首页</a>";
	if($pages>1) {
		$str.="<a href='?" . $url . "pages=" . ($pages-1) . "' class='next' title='上一页'>上一页</a>";
	}
	if($pagejs<=10) {
		for($i=1; $i<=$pagejs; $i++) {
			if($i==$pages) {
				$str.="<strong title='第" . $i . "页'>" . $i . "</strong>";
			} else {
				$str.="<a href='?" . $url . "pages=" . $i . "'>" . $i . "</a>";
			}
		}
	} else {
		if($pages>=12) {
			for($i=$pages-5; $i<=$pages+6; $i++) {
				if($i<=$pagejs) {
					if($i==$pages) {
						$str.="<strong title='第" . $i . "页'>" . $i . "</strong>";
					} else {
						$str.="<a href='?" . $url . "pages=" . $i . "'>" . $i . "</a>";
					}
				}
			}
			if($i<=$pagejs) {
				$str.="....";
				$str.="<a href='?" . $url . "pages=" . $pagejs . " '>" . $pagejs . "</a>";
			}
		} else {
			for($i=1; $i<=12; $i++) {
				if($i==$pages) {
					$str.="<strong title='第" . $i . "页'>" . $i . "</strong>";
				} else {
					$str.="<a href='?" . $url . "pages=" . $i . "'>" . $i . "</a>";
				}
			}
			if($i<=$pagejs) {
				$str.="....";
				$str.="<a href='?" . $url . "pages=" . $pagejs . " '>" . $pagejs . "</a>";
			}
		}
	}
	if($pages<$pagejs) {
		$str.="<a href='?" . $url . "pages=" . ($pages+1) . "' class='next' title='下一页'>下一页</a>";
	}
	$str.="<a href='?" . $url . "pages=" . $pagejs . "' class='last' title='最后一页'>末页</a>";
	$str.="<a>当前<font color=red>" . $pages . "</font>/" . $pagejs . "页</a>";
	while($row=$db->fetch_array($result)) {
	}
	$arr=array($str,$result,$sql,$pagejs);
	return $arr;
}
class iFile {
	private $Fp;
	private $Pipe;
	private $File;
	private $OpenMode;
	private $Data;
	function iFile($File, $Mode = 'r', $Data4Write = '', $Pipe = 'f') {
		$this->File=$File;
		$this->Pipe=$Pipe;
		if($Mode=='dr') {
			$this->OpenMode='r';
			$this->OpenFile();
			$this->getFileData();
		} else {
			$this->OpenMode=$Mode;
			$this->OpenFile();
		}
		if($this->OpenMode=='w'&$Data4Write!='') {
			$this->WriteFile($Data4Write, $Mode=3);
		}
	}
	function OpenFile() {
		if($this->OpenMode=='r'||$this->OpenMode=='r+') {
			if($this->CheckFile()) {
				if($this->Pipe=='f') {
					$this->Fp=fopen($this->File, $this->OpenMode);
				} elseif($Pipe=='p') {
					$this->Fp=popen($this->File, $this->OpenMode);
				} else {
					Die("请检查文件打开参数3,f:fopen()");
				}
			} else {
				Die("文件访问错误,请检查文件是否存在!");
			}
		} else {
			if($this->Pipe=='f') {
				$this->Fp=fopen($this->File, $this->OpenMode);
			} elseif($Pipe=='p') {
				$this->Fp=popen($this->File, $this->OpenMode);
			} else {
				Die("请检查文件打开参数3,f:fopen()");
			}
		}
	}
	function CloseFile() {
		if($this->Pipe=='f') {
			@fclose($this->Fp);
		} else {
			@pclose($this->Fp);
		}
	}
	function getFileData() {
		@flock($this->Fp, 1);
		$Content=fread($this->Fp, filesize($this->File));
		$this->Data=$Content;
	}
	function CheckFile() {
		if(file_exists($this->File)) {
			return true;
		} else {
			return false;
		}
	}
	function WriteFile($Data4Write, $Mode = 3) {
		@flock($this->Fp, $Mode);
		fwrite($this->Fp, $Data4Write);
		$this->CloseFile();
		return true;
	}
}
function escape($str) {
	/*        preg_match_all("/[\x80-\xff].|[\x01-\x7f]+/",$str,$r);
	$ar=$r[0];
	foreach($ar as $k=>$v){
	if(ord($v[0]) < 128)
	$ar[$k] = rawurlencode($v);
	else
	$ar[$k] = "%u".bin2hex(iconv("GBK","UCS-2",$v));
	}
	return join("",$ar); */
	$str=iconv("gb2312", "utf-8", $str);
	return $str;
}
function linkweburl($UserID, $UserName) {
	if($UserID==0) {
		$linkweburl="#";
	} elseif(cd_userhtml==0) {
		$linkweburl=cd_webpath . "singer/?id=" . $UserID;
	} elseif(cd_userhtml==1) {
		$linkweburl=cd_webpath . "singer/" . $UserID;
	}
	return $linkweburl;
}
function DownLinkUrl($CD_ID) {
	if(cd_webhtml==3){
		$DownLinkUrl=cd_webpath."play/download_".$CD_ID.".html";
	}else{
		$DownLinkUrl=cd_webpath."play/download.php?id=".$CD_ID."";
	}
	return $DownLinkUrl;
}
function LinkUrl($Table, $ClassID, $SystemID, $ID) {
	$Table=strtolower($Table);
	switch(cd_webhtml) {
	case '1':
		if($Table=='dj') {
			$LinkUrl=cd_webpath . "play/?1," . $ID . ".html";
		} elseif($Table=='special') {
			$LinkUrl=cd_webpath . "play/?2," . $ID . ".html";
		} elseif($Table=='news') {
			$LinkUrl=cd_webpath . "play/?3," . $ID . ".html";
		}
		break;
	case '2':
		if($Table=='dj') {
			$LinkUrl=str_linkurl(cd_caplayfolder, $ID, $ClassID, 0);
		} elseif($Table=='special') {
			$LinkUrl=str_linkurl(cd_caspecialfolder, $ID, $ClassID, 0);
		} elseif($Table=='news') {
			$LinkUrl=str_linkurl(cd_canewsfolder, $ID, $ClassID, 0);
		}
		break;
	case '3':
		if($Table=='dj') {
			$LinkUrl=cd_webpath . "song/" . $ID . "/";
		} elseif($Table=='special') {
			$LinkUrl=cd_webpath . "album/" . $ID . "/";
		} elseif($Table=='news') {
			$LinkUrl=cd_webpath . "article/" . $ID . "/";
		}
		break;
	}
	return $LinkUrl;
}
function LinkClassUrl($Table, $ID, $SystemID, $Page) {
	$Table=strtolower($Table);
	$PageMD=$Page;
	if(!IsNum($Page)) {
		$Page=1;
	}
	switch($Table) {
	case 'dj':
		if($SystemID==0or$SystemID==1) {
			switch(cd_webhtml) {
			case '1':$LinkClassUrl=cd_webpath . "play/?0," . $ID . "," . $Page . ".html";break;
			case '2':$LinkClassUrl=str_linkurl(cd_calistfolder, $ID, $ID, $Page);break;
			case '3':$LinkClassUrl=cd_webpath . "list/" . $ID . "/" . $Page . "/";break;
			}
		} else {
			$LinkClassUrl=GetAlias("x5music_class", "CD_AliasName", "CD_ID", $ID);
		}
		break;
	case 'news':
		switch(cd_webhtml) {
		case '1':$LinkClassUrl=cd_webpath . "play/?4," . $ID . "," . $Page . ".html";break;
		case '2':$LinkClassUrl=str_linkurl(cd_canewscfolder, $ID, $ID, $Page);break;
		case '3':$LinkClassUrl=cd_webpath . "news/" . $ID . "/" . $Page . "/";break;
		}
		break;
	case 'special':
		switch(cd_webhtml) {
		case '1':$LinkClassUrl=cd_webpath . "play/?2," . $ID . "," . $Page . ".html";break;
		case '2':$LinkClassUrl=cd_webpath . "play/?2," . $ID . "," . $Page . ".html";break;
		case '3':$LinkClassUrl=cd_webpath . "album/" . $ID . "/" . $Page . "/";break;
		}
		break;
	}
	return $LinkClassUrl;
}
function LinkPicUrl($Table) {
	if(substr($Table, 0, 7)=="http://" || substr($Table, 0, 1)=="/") {
		$LinkPicUrl=$Table;
	} elseif($Table=="") {
		$LinkPicUrl=cd_webpath . "user/image/nopic.png";
	} else {
		$LinkPicUrl=cd_webpath . $Table;
	}
	return $LinkPicUrl;
}
function linkuserphoto($uid) {
	if(!IsNumID($uid)) {exit();}
	$photo=GetAlias("x5music_user", "cd_photo", "cd_id", $uid);
	$photo=str_replace("null", '', $photo);
	$photo=str_replace("/http://", 'http://', $photo);
	$photo=str_replace("upload/", cd_webpath . 'upload/', $photo);
	if(!IsNul($photo)) {
		$photo=cd_webpath . "user/image/nouserpic.gif";
	}
	return $photo;
}
function GetAlias($TableA, $TableB, $TableC, $ClassID) {
	global $db;
	$TableA=ReplaceStr($TableA, "x5music_", cd_tablename);
	$sql="select " . $TableB . " from " . $TableA . " where " . $TableC . "='" . $ClassID . "'";
	$row=$db->getrow($sql);
	if($row) {
		$aliasname=$row[$TableB];
	} else {
		$aliasname="null";
	}
	return $aliasname;
}
function left($str, $len) {
	$arr=str_split($str);
	$i=0;
	foreach($arr as $chr) {
		if(ord($chr)>128)
			$add=$add ? 0 : 1;
		$i++;
		if($i==$len)
			break;
	}
	return substr($str, 0, $len+$add);
}
function remote_file_exists($url_file) {
	//检测输入
	$url_file=trim($url_file);
	if(empty($url_file)) {
		return false;
	}
	$url_arr=parse_url($url_file);
	if(!is_array($url_arr)||empty($url_arr)) {
		return false;
	}
	//获取请求数据
	$host=$url_arr['host'];
	$path=$url_arr['path'];
	$port=isset($url_arr['port']) ? $url_arr['port'] : "80";
	//连接服务器
	$fp=fsockopen($host, $port, $err_no, $err_str, 30);
	if(!$fp) {
		return false;
	}
	//构造请求协议
	$request_str="GET " . $path . " HTTP/1.1\r\n";
	$request_str.="Host: " . $host . "\r\n";
	$request_str.="Connection: Close\r\n\r\n";
	//发送请求
	fwrite($fp, $request_str);
	$first_header=fgets($fp, 1024);
	fclose($fp);
	//判断文件是否存在
	if(trim($first_header)=="") {
		return false;
	}
	if(!preg_match("/200/", $first_header)) {
		return false;
	}
	return true;
}
function convert_encoding($str, $nfate, $ofate) {
	if(function_exists("mb_convert_encoding")) {
		$str=mb_convert_encoding($str, $nfate, $ofate);
	} else {
		if($nfate=="GBK")
			$nfate="GBK//IGNORE";
		$str=iconv($ofate, $nfate, $str);
	}
	return $str;
}
function getChild($ClassID) {
	if($ClassID!="") {
		global $db;
		$ClassArr=explode(',', $ClassID);
		for($i=0; $i<count($ClassArr); $i++) {
			$sql="select CD_ID from " . tname('class') . " where CD_FatherID=$ClassArr[$i]";
			$result=$db->query($sql);
			if($result) {
				while($row=$db->fetch_array($result)) {
					$ClassArr[]=$row['CD_ID'];
				}
			}
		}
		$ClassID=implode(',', $ClassArr);
	}
	return $ClassID;
}
function Admin_Footer($CD_Type) {
	if($CD_Type==1) {
		echo "<script src=\"http://x5mp3.com/x5music/zan.php\"></script>";
	} else {
		echo "<br><table width='98%' border='0' cellpadding='0' cellspacing='0' align='center' class='tbtitle'><tr><td align='center' valign='top'>Powered by 小5 &nbsp;&nbsp;官方网站：http://x5mp3.com/ &nbsp;&nbsp;官方QQ群：343319601</td></tr></table>";
	}
}
function geturl($url) {
	if(function_exists('curl_init')) {
		$ch=curl_init();
		$timeout=30; // set to zero for no timeout
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		$handles=curl_exec($ch);
		curl_close($ch);
		return $handles;
	}
}
function x5music_Com_Plug($menu) {
	$menuarr=explode(chr(13) . chr(10), $menu);
	for($i=0; $i<count($menuarr); $i++) {
		if($menuarr[$i]<>"") {
			$menuinfo=explode("|", $menuarr[$i]);
			$menustr.="<dt><a href='" . $menuinfo[1] . "' target='main' class='icon-mub'>" . $menuinfo[0] . "</a></dt>\r\n";
		}
	}
	return $menustr;
}
function resizeImage($im, $maxwidth, $maxheight, $name){
	switch(fileext($im)) {
	case 'jpeg':$im=imagecreatefromjpeg($im);break;
	case 'JPEG':$im=imagecreatefromjpeg($im);break;
	case 'jpg':$im=imagecreatefromjpeg($im);break;
	case 'JPG':$im=imagecreatefromjpeg($im);break;
	case 'png':$im=imagecreatefrompng($im);break;
	case 'PNG':$im=imagecreatefrompng($im);break;
	case 'gif':$im=imagecreatefromgif($im);break;
	case 'GIF':$im=imagecreatefromgif($im);break;
	}
	$pic_width=imagesx($im);
	$pic_height=imagesy($im);
	if(($maxwidth&&$pic_width>$maxwidth)||($maxheight&&$pic_height>$maxheight)) {
		if($maxwidth&&$pic_width>$maxwidth) {
			$widthratio=$maxwidth/$pic_width;
			$resizewidth_tag=true;
		}
		if($maxheight&&$pic_height>$maxheight) {
			$heightratio=$maxheight/$pic_height;
			$resizeheight_tag=true;
		}
		if($resizewidth_tag&&$resizeheight_tag) {
			if($widthratio<$heightratio)
				$ratio=$widthratio;
			else
				$ratio=$heightratio;
		}
		if($resizewidth_tag&&!$resizeheight_tag)
			$ratio=$widthratio;
		if($resizeheight_tag&&!$resizewidth_tag)
			$ratio=$heightratio;
		$newwidth=$pic_width*$ratio;
		$newheight=$pic_height*$ratio;
		if(function_exists("imagecopyresampled")) {
			$newim=imagecreatetruecolor($newwidth, $newheight); //PHP系统函数
			imagecopyresampled($newim, $im, 0, 0, 0, 0, $newwidth, $newheight, $pic_width, $pic_height); //PHP系统函数
		} else {
			$newim=imagecreate($newwidth, $newheight);
			imagecopyresized($newim, $im, 0, 0, 0, 0, $newwidth, $newheight, $pic_width, $pic_height);
		}
		imagejpeg($newim, $name);
		imagedestroy($newim);
	} else {
		imagejpeg($im, $name);
	}
}
function unescape($str) {
	$str=rawurldecode($str);
	preg_match_all("/%u.{4}|&#x.{4};|&#d+;|.+/U", $str, $r);
	$ar=$r[0];
	foreach($ar as $k=>$v) {
		if(substr($v, 0, 2)=="%u")
			$ar[$k]=mb_convert_encoding(pack("H4", substr($v, -4)), "gb2312", "UCS-2");
		elseif(substr($v, 0, 3)=="&#x")
			$ar[$k]=mb_convert_encoding(pack("H4", substr($v, 3, -1)), "gb2312", "UCS-2");
		elseif(substr($v, 0, 2)=="&#") {
			$ar[$k]=mb_convert_encoding(pack("H4", substr($v, 2, -1)), "gb2312", "UCS-2");
		}
	}
	return join("", $ar);
}
function spanhtmlpage($mysql, $pagesize, $pagenum = 10) {
	global $db;
	$pages=SafeRequest("pages", "get");
	$pagesok=$pagesize; //每页显示记录数
	if(!isset($pages) || $pages=="" || !is_numeric($pages) || $pages<=0) {
		$pages=1;
	}
	$sqlstr=$mysql;
	$res=$db->query($sqlstr);
	$nums=$db->num_rows($res);
	if($nums==0) {
		$nums=1;
	}
	$pagejs=ceil($nums/$pagesok); //总页数
	if($pages>$pagejs) {
		$pages=$pagejs;
	}
	$sql=$sqlstr . " LIMIT " . $pagesok*($pages-1) . "," . $pagesok;
	$result=$db->query($sql);

	$str="";
	while($row=$db->fetch_array($result)) {}
	$arr=array($str,$result,$sql,$nums,$pagelist,$pages,$pagejs,$first,$pageup,$pagenext,$last,$pagesok);
	return $arr;
}
//添加数据
function inserttable($tablename, $insertsqlarr, $returnid = 0, $replace = false, $silent = 0) {
	global $db;
	$insertkeysql=$insertvaluesql=$comma='';
	foreach($insertsqlarr as $insert_key=>$insert_value) {
		$insertkeysql.=$comma . '`' . $insert_key . '`';
		$insertvaluesql.=$comma . '\'' . $insert_value . '\'';
		$comma=', ';
	}
	$method=$replace ? 'REPLACE' : 'INSERT';
	$db->query($method . ' INTO ' . tname($tablename) . ' (' . $insertkeysql . ') VALUES (' . $insertvaluesql . ')', $silent ? 'SILENT' : '');
	if($returnid&&!$replace) {
		return $db->insert_id();
	}
}
//更新数据
function updatetable($tablename, $setsqlarr, $wheresqlarr, $silent = 0) {
	global $db;
	$setsql=$comma='';
	foreach($setsqlarr as $set_key=>$set_value) {
		$setsql.=$comma . '`' . $set_key . '`' . '=\'' . $set_value . '\'';
		$comma=', ';
	}
	$where=$comma='';
	if(empty($wheresqlarr)) {
		$where='1';
	} elseif(is_array($wheresqlarr)) {
		foreach($wheresqlarr as $key=>$value) {
			$where.=$comma . '`' . $key . '`' . '=\'' . $value . '\'';
			$comma=' AND ';
		}
	} else {
		$where=$wheresqlarr;
	}
	$db->query('UPDATE ' . tname($tablename) . ' SET ' . $setsql . ' WHERE ' . $where, $silent ? 'SILENT' : '');
}
function fetch_match_contents($dykgdlsrrr, $dykgddelrrr, $dykgdlshigdb) {
	$dykgdlsrrr=change_match_string($dykgdlsrrr);
	$dykgddelrrr=change_match_string($dykgddelrrr);
	if(@preg_match("/" . $dykgdlsrrr . "(.*?){$dykgddelrrr}/i", $dykgdlshigdb, $dyqjvgdqss)) {
		return $dyqjvgdqss[1];
	}
	return "";
}
function change_match_string($dykgdkggg) {
	$dykggdkggw=array("/","\$");
	$dykggdkxxw=array("\\/","\$");
	$dykgdkggg=str_replace($dykggdkggw, $dykggdkxxw, $dykgdkggg);
	return $dykgdkggg;
}
?>