<?php
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
error_reporting(0);
include "../../include/x5music.conn.php";
include "../function_common.php";
admincheck(8);

$action=SafeRequest("action", "get");
?>
<?php
$action=SafeRequest("action", "get");
if($action=="hf") {
	//横幅广告代码
	$gg960_90=SafeRequest("gg960_90", "post");
	$gg960_60=SafeRequest("gg960_60", "post");
	$gg760_90=SafeRequest("gg760_90", "post");
	$gg728_90=SafeRequest("gg728_90", "post");
	$gg640_60=SafeRequest("gg640_60", "post");
	$gg580_90=SafeRequest("gg580_90", "post");
	$gg500_200=SafeRequest("gg500_200", "post");
	$gg468_60=SafeRequest("gg468_60", "post");
	$gg468_15=SafeRequest("gg468_15", "post");
	global $db;
	$db->query('update ' . tname('system') . " set gg960_90='" . $gg960_90 . "',gg960_60='" . $gg960_60 . "',gg760_90='" . $gg760_90 . "',gg728_90='" . $gg728_90 . "',gg640_60='" . $gg640_60 . "',gg580_90='" . $gg580_90 . "',gg500_200='" . $gg500_200 . "',gg468_60='" . $gg468_60 . "',gg468_15='" . $gg468_15 . "'");
	showmessage("恭喜您，广告代码已经更新！", $_SERVER['HTTP_REFERER'], 0);
} elseif($action=="sf") {
	//竖幅广告代码
	$gg160_600=SafeRequest("gg160_600", "post");
	$gg120_600=SafeRequest("gg120_600", "post");
	$gg120_240=SafeRequest("gg120_240", "post");
	global $db;
	$db->query('update ' . tname('system') . " set gg160_600='" . $gg160_600 . "',gg120_600='" . $gg120_600 . "',gg120_240='" . $gg120_240 . "'");
	showmessage("恭喜您，广告代码已经更新！", $_SERVER['HTTP_REFERER'], 0);
} elseif($action=="jx") {
	//矩形广告代码
	$gg300_250=SafeRequest("gg300_250", "post");
	$gg250_250=SafeRequest("gg250_250", "post");
	$gg200_200=SafeRequest("gg200_200", "post");
	$gg336_280=SafeRequest("gg336_280", "post");
	$gg360_300=SafeRequest("gg360_300", "post");
	$gg125_125=SafeRequest("gg125_125", "post");
	$gg180_150=SafeRequest("gg180_150", "post");
	global $db;
	$db->query('update ' . tname('system') . " set gg300_250='" . $gg300_250 . "',gg250_250='" . $gg250_250 . "',gg200_200='" . $gg200_200 . "',gg336_280='" . $gg336_280 . "',gg360_300='" . $gg360_300 . "',gg125_125='" . $gg125_125 . "',gg180_150='" . $gg180_150 . "'");
	showmessage("恭喜您，广告代码已经更新！", $_SERVER['HTTP_REFERER'], 0);
} elseif($action=="qt") {
	//其他广告代码
	$gg728_15=SafeRequest("gg728_15", "post");
	$gg234_60=SafeRequest("gg234_60", "post");
	$gg480_160=SafeRequest("gg480_160", "post");
	$gg460_60=SafeRequest("gg460_60", "post");
	$gg120_90=SafeRequest("gg120_90", "post");
	$gg200_90=SafeRequest("gg200_90", "post");
	$gg160_90=SafeRequest("gg160_90", "post");
	$gg180_90=SafeRequest("gg180_90", "post");
	global $db;
	$db->query('update ' . tname('system') . " set gg728_15='" . $gg728_15 . "',gg234_60='" . $gg234_60 . "',gg480_160='" . $gg480_160 . "',gg460_60='" . $gg460_60 . "',gg120_90='" . $gg120_90 . "',gg200_90='" . $gg200_90 . "',gg160_90='" . $gg160_90 . "',gg180_90='" . $gg180_90 . "'");
	showmessage("恭喜您，广告代码已经更新！", $_SERVER['HTTP_REFERER'], 0);
} elseif($action=="zdy") {
	//自定义广告代码
	$ggself_defined=SafeRequest("ggself_defined", "post");
	global $db;
	$db->query('update ' . tname('system') . " set ggself_defined='" . $ggself_defined . "'");
	showmessage("恭喜您，广告代码已经更新！", $_SERVER['HTTP_REFERER'], 0);
}
?>
<!doctype html>
<html>
<head>
<meta charset="gbk">
<meta name="renderer" content="webkit" /> 
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>x5Music 后台管理中心 -x5mp3.com</title> 
<link href="../css/add.css" rel="stylesheet" />
<style type="text/css">
.table2{width: 100%;max-width: 100%;border-collapse: collapse;border-spacing: 0;}
.table2 td{font-size: 12px;border-top: 1px solid #ddd;padding: 8px;vertical-align: top;}
.table2 tr:hover{text-decoration: none;background-color: #e6f2fb;}
.table2 th{border-bottom: 2px solid #ddd;vertical-align: bottom;padding: 2px;text-align: left;}
.table3{width: 100%;max-width: 100%;border-collapse: collapse;border-spacing: 0;}
.table3 tr:hover{text-decoration: none;background-color: #e6f2fb;}
.table3 td{font-size: 12px;line-height:25px;border-top: 1px solid #ddd;padding: 5px;vertical-align: top;border-right: solid 1px #ddd;}
.table3 th{border-bottom: 2px solid #ddd;vertical-align: bottom;padding: 2px;text-align: left;}
</style>
 </head> 
 <body> 
<?php
global $db;
$sql="Select * from ".tname('system')."";
$row=$db->getrow($sql);
?>

<?php
$ac=SafeRequest("ac","get");
if($ac==""){
?>
<form name="myform" method="post" action="?action=hf">
  <div class="contents"> 
   <div class="panel"> 
    <div class="panel-head">
     <strong>横幅广告代码</strong>
    </div>
    <table class="table2"> 
     <tbody>
<tr>
<td width="365"><textarea class="length_6" name="gg960_90"><?php echo $row['gg960_90'];?></textarea></td>
<td>960*90广告代码<br />高：90&nbsp;&nbsp;&nbsp;宽：960<br /> <br />调用标签：[x5music:gg960_90]</td>
</tr>
<tr>
<td><textarea class="length_6" name="gg960_60"><?php echo $row['gg960_60'];?></textarea></td>
<td>960*60广告代码<br />高：60&nbsp;&nbsp;&nbsp;宽：960<br /> <br />调用标签：[x5music:gg960_60]</td>
</tr>
<tr>
<td><textarea class="length_6" name="gg760_90"><?php echo $row['gg760_90'];?></textarea></td>
<td>760*90广告代码<br />高：90&nbsp;&nbsp;&nbsp;宽：760<br /> <br />调用标签：[x5music:gg760_90]</td>
</tr>
<tr>
<td><textarea class="length_6" name="gg728_90"><?php echo $row['gg728_90'];?></textarea></td>
<td>728*90广告代码<br />高：90&nbsp;&nbsp;&nbsp;宽：728<br /> <br />调用标签：[x5music:gg728_90]</td>
</tr>
<tr>
<td><textarea class="length_6" name="gg640_60"><?php echo $row['gg640_60'];?></textarea></td>
<td>640*60广告代码<br />高：60&nbsp;&nbsp;&nbsp;宽：640<br /> <br />调用标签：[x5music:gg640_60]</td>
</tr>
<tr>
<td><textarea class="length_6" name="gg580_90"><?php echo $row['gg580_90'];?></textarea></td>
<td>580*90广告代码<br />高：90&nbsp;&nbsp;&nbsp;宽：580<br /> <br />调用标签：[x5music:gg580_90]</td>
</tr>
<tr>
<td><textarea class="length_6" name="gg500_200"><?php echo $row['gg500_200'];?></textarea></td>
<td>500*200广告代码<br />高：200&nbsp;&nbsp;&nbsp;宽：500<br /> <br />调用标签：[x5music:gg500_200]</td>
</tr>
<tr>
<td><textarea class="length_6" name="gg468_60"><?php echo $row['gg468_60'];?></textarea></td>
<td>468*60广告代码<br />高：60&nbsp;&nbsp;&nbsp;宽：468<br /> <br />调用标签：[x5music:gg468_60]</td>
</tr>
<tr>
<td><textarea class="length_6" name="gg468_15"><?php echo $row['gg468_15'];?></textarea></td>
<td>468*15广告代码<br />高：15&nbsp;&nbsp;&nbsp;宽：468<br /> <br />调用标签：[x5music:gg468_15]</td>
</tr>
<tr><td height="35" colspan="2" align="left" bgcolor="#FAFBF7" class="td_border"><button type="submit" id="adddance" class="btn btn_submit J_ajax_submit_btn" style="margin-left: 80px;">确定提交</button></td></tr>

     </tbody>
    </table> 
</form>
</div>
</div>
<?php }elseif($ac=="sf"){ ?>
<form name="myform" method="post" action="?action=sf">
  <div class="contents"> 
   <div class="panel"> 
    <div class="panel-head">
     <strong>竖幅广告代码</strong>
    </div>
    <table class="table2"> 
     <tbody>
<tr>
<td width="365"><textarea class="length_6" name="gg160_600"><?php echo $row['gg160_600'];?></textarea></td>
<td>160*600广告代码<br />高：600&nbsp;&nbsp;&nbsp;宽：160<br /> <br />调用标签：[x5music:gg160_600]</td>
</tr>
<tr>
<td><textarea class="length_6" name="gg120_600"><?php echo $row['gg120_600'];?></textarea></td>
<td>120*600广告代码<br />高：600&nbsp;&nbsp;&nbsp;宽：120<br /> <br />调用标签：[x5music:gg120_600]</td>
</tr>
<tr>
<td><textarea class="length_6" name="gg120_240"><?php echo $row['gg120_240'];?></textarea></td>
<td>120*240广告代码<br />高：240&nbsp;&nbsp;&nbsp;宽：120<br /> <br />调用标签：[x5music:gg120_240]</td>
</tr>
<tr><td height="35" colspan="2" align="left" bgcolor="#FAFBF7" class="td_border"><button type="submit" id="adddance" class="btn btn_submit J_ajax_submit_btn" style="margin-left: 80px;">确定提交</button></td></tr>
     </tbody>
    </table> 
</form>
</div>
</div>
<?php }elseif($ac=="jx"){ ?>
<form name="myform" method="post" action="?action=jx">
  <div class="contents"> 
   <div class="panel"> 
    <div class="panel-head">
     <strong>矩形广告代码</strong>
    </div>
    <table class="table2"> 
     <tbody>
<tr>
<td width="365"><textarea class="length_6" name="gg300_250"><?php echo $row['gg300_250'];?></textarea></td>
<td>300*250广告代码<br />高：250&nbsp;&nbsp;&nbsp;宽：300<br /> <br />调用标签：[x5music:gg300_250]</td>
</tr>
<tr>
<td><textarea class="length_6" name="gg250_250"><?php echo $row['gg250_250'];?></textarea></td>
<td>250*250广告代码<br />高：250&nbsp;&nbsp;&nbsp;宽：250<br /> <br />调用标签：[x5music:gg250_250]</td>
</tr>
<tr>
<td><textarea class="length_6" name="gg200_200"><?php echo $row['gg200_200'];?></textarea></td>
<td>200*200广告代码<br />高：200&nbsp;&nbsp;&nbsp;宽：200<br /> <br />调用标签：[x5music:gg200_200]</td>
</tr>
<tr>
<td><textarea class="length_6" name="gg336_280"><?php echo $row['gg336_280'];?></textarea></td>
<td>336*280广告代码<br />高：280&nbsp;&nbsp;&nbsp;宽：336<br /> <br />调用标签：[x5music:gg336_280]</td>
</tr>
<tr>
<td><textarea class="length_6" name="gg360_300"><?php echo $row['gg360_300'];?></textarea></td>
<td>360*300广告代码<br />高：280&nbsp;&nbsp;&nbsp;宽：336<br /> <br />调用标签：[x5music:gg360_300]</td>
</tr>
<tr>
<td><textarea class="length_6" name="gg125_125"><?php echo $row['gg125_125'];?></textarea></td>
<td>125*125广告代码<br />高：125&nbsp;&nbsp;&nbsp;宽：125<br /> <br />调用标签：[x5music:gg125_125]</td>
</tr>
<tr>
<td><textarea class="length_6" name="gg180_150"><?php echo $row['gg180_150'];?></textarea></td>
<td>180*150广告代码<br />高：150&nbsp;&nbsp;&nbsp;宽：150<br /> <br />调用标签：[x5music:gg180_150]</td>
</tr>
<tr><td height="35" colspan="2" align="left" bgcolor="#FAFBF7" class="td_border"><button type="submit" id="adddance" class="btn btn_submit J_ajax_submit_btn" style="margin-left: 80px;">确定提交</button></td></tr>
     </tbody>
    </table> 
</form>
</div>
</div>
<?php }elseif($ac=="qt"){ ?>
<form name="myform" method="post" action="?action=qt">
  <div class="contents"> 
   <div class="panel"> 
    <div class="panel-head">
     <strong>其他广告代码</strong>
    </div>
    <table class="table2"> 
     <tbody>
<tr>
<td width="365"><textarea class="length_6" name="gg728_15"><?php echo $row['gg728_15'];?></textarea></td>
<td>728*15广告代码<br />高：15&nbsp;&nbsp;&nbsp;宽：728<br /> <br />调用标签：[x5music:gg728_15]</td>
</tr>
<tr>
<td><textarea class="length_6" name="gg234_60"><?php echo $row['gg234_60'];?></textarea></td>
<td>234*60广告代码<br />高：60&nbsp;&nbsp;&nbsp;宽：234<br /> <br />调用标签：[x5music:gg234_60]</td>
</tr>
<tr>
<td><textarea class="length_6" name="gg480_160"><?php echo $row['gg480_160'];?></textarea></td>
<td>480*160广告代码<br />高：200&nbsp;&nbsp;&nbsp;宽：200<br /> <br />调用标签：[x5music:gg480_160]</td>
</tr>
<tr>
<td><textarea class="length_6" name="gg460_60"><?php echo $row['gg460_60'];?></textarea></td>
<td>460*60广告代码<br />高：60&nbsp;&nbsp;&nbsp;宽：460<br /> <br />调用标签：[x5music:gg460_60]</td>
</tr>
<tr>
<td><textarea class="length_6" name="gg120_90"><?php echo $row['gg120_90'];?></textarea></td>
<td>120*90广告代码<br />高：120&nbsp;&nbsp;&nbsp;宽：90<br /> <br />调用标签：[x5music:gg120_90]</td>
</tr>
<tr>
<td><textarea class="length_6" name="gg200_90"><?php echo $row['gg200_90'];?></textarea></td>
<td>200*90广告代码<br />高：125&nbsp;&nbsp;&nbsp;宽：125<br /> <br />调用标签：[x5music:gg200_90]</td>
</tr>
<tr>
<td><textarea class="length_6" name="gg160_90"><?php echo $row['gg160_90'];?></textarea></td>
<td>160*90广告代码<br />高：150&nbsp;&nbsp;&nbsp;宽：150<br /> <br />调用标签：[x5music:gg160_90]</td>
</tr>
<tr>
<td><textarea class="length_6" name="gg180_90"><?php echo $row['gg180_90'];?></textarea></td>
<td>180*90广告代码<br />高：90&nbsp;&nbsp;&nbsp;宽：180<br /> <br />调用标签：[x5music:gg180_90]</td>
</tr>
<tr><td height="35" colspan="2" align="left" bgcolor="#FAFBF7" class="td_border"><button type="submit" id="adddance" class="btn btn_submit J_ajax_submit_btn" style="margin-left: 80px;">确定提交</button></td></tr>
     </tbody>
    </table> 
</form>
</div>
</div>
<?php }elseif($ac=="zdy"){ ?>
<form name="myform" method="post" action="?action=zdy">
  <div class="contents"> 
   <div class="panel"> 
    <div class="panel-head">
     <strong>自定义广告代码</strong>
    </div>
    <table class="table2"> 
     <tbody>
<tr>
<td width="365"><textarea class="length_6" name="ggself_defined"><?php echo $row['ggself_defined'];?></textarea></td>
<td>自定义广告代码<br /><br /> <br />调用标签：[x5music:ggself_defined]</td>
</tr>
<tr><td height="35" colspan="2" align="left" bgcolor="#FAFBF7" class="td_border"><button type="submit" id="adddance" class="btn btn_submit J_ajax_submit_btn" style="margin-left: 80px;">确定提交</button></td></tr>
     </tbody>
    </table> 
</form>
</div>
</div>
<?php }?>
</body>
</html>