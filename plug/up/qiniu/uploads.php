<?php
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
error_reporting(0);
include "../../../include/x5music.conn.php";
$ac=$_GET["ac"];
if($ac=="") {
	exit('?error=undefined');
}
if(cd_uploadentrance=="no") {
	exit('?error=1' . cd_uploadentrance);
} //没有权限
if($ac=="video") {
	$cd_upmusictype=cd_upvideotype;
} else {
	$cd_upmusictype=cd_upmusictype;
}
if(strpos($cd_upmusictype, fileext($_FILES['Filedata']['name']))!==false) {
} else {
	exit('?error=4');
} //文件类型错误

if(!empty($_FILES)) {
	require_once("qiniu/io.php");
	require_once("qiniu/rs.php");
	include "conf.inc.php";
	$bucket=qiniu_bucket;
	$accessKey=qiniu_accesskey;
	$secretKey=qiniu_secretkey;
	$tempFile=$_FILES['Filedata']['tmp_name'];
	$file=$_FILES['Filedata']['name'];
	$size=$_FILES['Filedata']['size'];
	$zmd5=$size . $file;
	$timemd5='' . substr(md5($zmd5), 0, 32) . '';
	$md5=strtoupper($timemd5);
	$form=strtolower(trim(substr(strrchr($file, "."), 1)));
	$targetFile="" . $md5 . "." . $form . "";
	Qiniu_SetKeys($accessKey, $secretKey);
	$putPolicy=new Qiniu_RS_PutPolicy($bucket);
	$upToken=$putPolicy->Token(null);
	$putExtra=new Qiniu_PutExtra();
	$putExtra->Crc32=1;
	list($ret, $err) = Qiniu_PutFile($upToken, $targetFile, $tempFile, $putExtra);
	@unlink($_FILES['Filedata']['tmp_name']);
	echo "?CD_Name=$CD_Name&CD_Url=$targetFile&CD_siz=" . formatsize($size) . "&CD_md5=$md5&CD_Server=" . qiniu_Server . "";
}
?>