<?php
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
error_reporting(0);
include "../../include/x5music.conn.php";
include "../function_common.php";
admincheck(4);
$action=SafeRequest("action", "get");
$CD_ClassID=SafeRequest("CD_ClassID", "get");
?>
<!DOCTYPE html>
<html>
 <head> 
  <meta charset="gbk" /> 
  <meta name="renderer" content="webkit" /> 
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" /> 
  <title>x5Music 后台管理中心 -x5mp3.com</title> 
  <link href="../css/add.css" rel="stylesheet" /> 
  <script type="text/javascript" src="../js/ajax.js"></script> 
  <script type="text/javascript" src="<?php echo cd_webpath?>user/static/space/layer/jquery.js"></script> 
  <script type="text/javascript" src="<?php echo cd_webpath?>user/static/space/layer/lib.js"></script> 
  <script type="text/javascript">
var pop = {
	up : function (text, url, width, height, top) {
		$.layer({
			type : 2,
			maxmin : true,
			title : text,
			iframe : {
				src : url
			},
			area : [width, height],
			offset : [top, '50%'],
			shade : [0]
		});
	}
}
function MM_jumpMenu(targ, selObj, restore) { //v3.0
	eval(targ + ".location='" + selObj.options[selObj.selectedIndex].value + "'");
	if (restore)
		selObj.selectedIndex = 0;
}
</script> 
  <style type="text/css">
.table2{width: 100%;max-width: 100%;border-collapse: collapse;border-spacing: 0;}
.table2 td{font-size: 12px;border-top: 1px solid #ddd;padding: 8px;vertical-align: top;}
.table2 tr:hover{text-decoration: none;background-color: #e6f2fb;}
.table2 th{border-bottom: 2px solid #ddd;vertical-align: bottom;padding: 2px;text-align: left;}
.table3{width: 100%;max-width: 100%;border-collapse: collapse;border-spacing: 0;}
.table3 tr:hover{text-decoration: none;background-color: #e6f2fb;}
.table3 td{font-size: 12px;line-height:25px;border-top: 1px solid #ddd;padding: 5px;vertical-align: top;border-right: solid 1px #ddd;}
.table3 th{border-bottom: 2px solid #ddd;vertical-align: bottom;padding: 2px;text-align: left;}
</style> 
  <script language="javascript">
function CheckAll(form) {
	for (var i = 0; i < form.elements.length; i++) {
		var e = form.elements[i];
		if (e.name != 'chkall')
			e.checked = form.chkall.checked;
	}
}
</script> 
 </head> 
 <body> 
<?php
if($action=="") {
	$x5='btn_success';
} elseif($action=="add") {
	$x51='btn_success';
} elseif($action=="users") {
	$x52='btn_success';
} elseif($action=="vips") {
	$x53='btn_success';
} elseif($action=="userqq") {
	$x54='btn_success';
} elseif($action=="isbest") {
	$x55='btn_success';
} elseif($action=="checkmusic") {
	$x56='btn_success';
} elseif($action=="checkmm") {
	$x57='btn_success';
} elseif($action=="vlock") {
	$x58='btn_success';
} else {
	$x5='btn_success';
}
?>
  <div class="contents"> 
   <div class="panel"> 
    <div style="padding: 8px;"> 
     <a href="user.php" class="btn <?php echo $x5;?>">所有会员</a> 
     <a href="?action=add" class="btn <?php echo $x51;?>">添加会员</a> 
     <a href="?action=users" class="btn <?php echo $x52;?>">普通会员</a> 
     <a href="?action=vips" class="btn <?php echo $x53;?>">VIP会员</a> 
     <a href="?action=userqq" class="btn <?php echo $x54;?>">QQ登录会员</a> 
     <a href="?action=isbest" class="btn <?php echo $x55;?>">推荐会员</a> 
	 <a href="?action=vlock" class="btn <?php echo $x58;?>">锁定会员</a> 
    </div> 
   </div> 
  </div> 
<?php
switch($action) {
	case 'mmedit':
		MMEdit();
		break;
	case 'savemmedit':
		SaveMMEdit();
		break;
	case 'add':
		Add();
		break;
	case 'saveadd':
		SaveAdd();
		break;
	case 'edit':
		Edit();
		break;
	case 'saveedit':
		SaveEdit();
		break;
	case 'editisbest':
		EditIsBest();
		break;
	case 'del':
		del();
		break;
	case 'save':
		save();
		break;
	case 'keyword':
		$key=SafeRequest("key", "get");
		main("select * from " . tname('user') . " where cd_name like '%" . $key . "%' or cd_sex like '%" . $key . "%' or cd_nicheng like '%" . $key . "%' order by cd_id desc", 20);
		break;
	case 'users':
		main("select * from " . tname('user') . " where cd_grade=0 order by cd_id desc", 20);
		break;
	case 'vips':
		main("select * from " . tname('user') . " where cd_grade=1 order by cd_id desc", 20);
		break;
	case 'userqq':
		main("select * from " . tname('user') . " where cd_qqopenid<>'' order by cd_id desc", 20);
		break;
	case 'ulock':
		main("select * from " . tname('user') . " where cd_lock=0 order by cd_id desc", 20);
		break;
	case 'vlock':
		main("select * from " . tname('user') . " where cd_lock=1 order by cd_id desc", 20);
		break;
	case 'isbest':
		main("select * from " . tname('user') . " where cd_isbest=1 order by cd_id desc", 20);
		break;
	case 'checkmusic':
		main("select * from ".tname('user')." where cd_checkmusic=1 order by cd_id desc",20);
		break;
	case 'checkmm':
		main("select * from ".tname('user')." where cd_checkmm=1 order by cd_id desc",20);
		break;
	case 'verifiedmm':
		main("select * from ".tname('user')." where cd_checkmm=2 order by cd_id desc",20);
		break;
	default:
		main("select * from " . tname('user') . " order by cd_id desc", 20);
		break;
}
?>
 </body>
</html>
<?php
Function EditBoard($Arr, $url) {
	//var_dump ($Arr); 
	$cd_name=$Arr[0];
	$cd_nicheng=$Arr[1];
	$cd_email=$Arr[2];
	$cd_qq=$Arr[3];
	$cd_qqopenid=$Arr[4];
	$cd_photo=$Arr[5];
	$cd_vipindate=$Arr[6];
	$cd_vipenddate=$Arr[7];
	$cd_sex=$Arr[8];
	$cd_birthprovince=$Arr[9];
	$cd_birthcity=$Arr[10];
	$cd_resideprovince=$Arr[11];
	$cd_residecity=$Arr[12];
	$cd_hits=$Arr[13];
	$cd_points=$Arr[14];
	$cd_djmub=$Arr[15];
	$cd_ufans=$Arr[16];
	$cd_ufav=$Arr[17];
	$cd_password=$Arr[18];
	$cd_sign=$Arr[19];
	$cd_birthday=$Arr[20];
	$cd_lock=$Arr[21];
	$cd_isbest=$Arr[22];
	$cd_grade=$Arr[23];
	$cd_regdate=$Arr[24];
	$cd_logintime=$Arr[25];
	if(!IsNul($cd_points)) {
		$cd_points="0";
	}
	if(!IsNul($cd_hits)) {
		$cd_hits="0";
	}
	if(!IsNul($cd_ufav)) {
		$cd_ufav="0";
	}
	if(!IsNul($cd_djmub)) {
		$cd_djmub="0";
	}
	if(!IsNul($cd_ufans)) {
		$cd_ufans="0";
	}
	if(!IsNul($cd_vipindate)) {
		$cd_vipindate="0000-00-00 00:00:00";
	}
	if(!IsNul($cd_vipenddate)) {
		$cd_vipenddate="0000-00-00 00:00:00";
	}
?>
  <div class="contents"> 
   <div class="panel"> 
    <div class="panel-head">
     <strong>
	 <?php 
	 $action=SafeRequest("action","get");
	 if($action==""){echo "会员管理";
	 }elseif($action=="edit"){echo "编辑【".$cd_name."】会员信息";
	 }elseif($action=="add"){echo "添加会员";
	 }else{
	 echo "会员管理";
	 }
	?>
	 </strong>
    </div> 
<form action="<?php echo $url?>" method="post" name="form2" >
    <table class="table3" id="dellist"> 
     <tbody>
      <tr>
       <td width="100" align="left">邮箱帐号</td>
       <td align="left"><input id="cd_name" name="cd_name" value="<?php echo $cd_name?>" type="text" class="input input_hd length_3">　<?php if(!IsNul($cd_id)) {echo'请添加唯一帐号，不能重复。';}else{echo'唯一帐号,建议不要修改它。';}?></td>
      </tr>
      <tr>
       <td width="100" align="left">昵称</td>
       <td align="left"><input id="cd_nicheng" name="cd_nicheng" value="<?php echo $cd_nicheng?>" type="text" class="input input_hd length_3"></td>
      </tr>
      <tr>
       <td align="left">邮箱</td>
       <td align="left"><input id="cd_email" name="cd_email" value="<?php echo $cd_email?>" type="text" class="input input_hd length_3"></td>
      </tr>
      <tr>
       <td align="left">性别</td>
       <td align="left">
       <select name="cd_sex" id="cd_sex"> 
       <option value="男" <?php if ($cd_sex == "男") {echo "selected";} ?>>男</option> 
       <option value="女" <?php if ($cd_sex == "女") {echo "selected";} ?>>女</option> 
       </select>
       </td>
      </tr>
      <tr>
       <td align="left">新密码</td>
       <td align="left"><input id="cd_password" name="cd_password" value="" type="text" class="input input_hd length_3">　*不修改请留空</td>
      </tr>	
      <tr>
       <td align="left">Q Q号码</td>
       <td align="left"><input id="cd_qq" name="cd_qq" value="<?php echo $cd_qq?>" type="text" class="input input_hd length_3">
       </td>
      </tr>
      <tr>
       <td align="left">Q Q登录ID</td>
       <td align="left"><input id="cd_qqopenid" name="cd_qqopenid" value="<?php echo $cd_qqopenid?>" type="text" class="input input_hd length_5">　QQ互联唯一登录标识，编辑请慎重；</td>
      </tr>
      <tr>
       <td align="left">个人头像</td>
       <td align="left"><input id="cd_photo" name="cd_photo" value="<?php echo $cd_photo?>" type="text" class="input input_hd length_5">　<label class="mr20"><input type="checkbox" name="delphoto" class="checkbox" id="delphoto" value="1" />删除头像</label></td>
      </tr>
      <tr>
       <td align="left">家乡</td>
       <td align="left"><script type="text/javascript" src="../../user/js/script_city.js"></script>
<script type="text/javascript">
<!--
showprovince('birthprovince', 'birthcity', '<?php echo $cd_birthprovince;?>', 'birthcitybox');
showcity('birthcity', '<?php echo $cd_birthcity;?>', '', 'birthcitybox');
//-->
</script>
       </td>
      </tr>
      <tr>
       <td align="left">居住地</td>
       <td align="left"><script type="text/javascript">
<!--
showprovince('resideprovince', 'residecity', '<?php echo $cd_resideprovince;?>', 'residecitybox');
showcity('residecity', '<?php echo $cd_residecity;?>', '', 'residecitybox');
//-->
</script>
       </td>
      </tr>
      <tr>
       <td align="left">生日</td>
       <td align="left"><input id="cd_birthday" name="cd_birthday" value="<?php echo $cd_birthday?>" type="text" class="input input_hd length_3">
       </td>
      </tr>
      <tr>
       <td align="left">注册时间</td>
       <td align="left"><input id="cd_regdate" name="cd_regdate" value="<?php echo $cd_regdate?>" type="text" class="input input_hd length_3"></td>
      </tr>
      <tr>
       <td align="left">最后登录时间</td>
       <td align="left"><input id="cd_logintime" name="cd_logintime" value="<?php echo $cd_logintime?>" type="text" class="input input_hd length_3"></td>
      </tr>
      <tr>
       <td align="left">会员状态</td>
       <td align="left">
       <select name="cd_lock" id="cd_lock"> 
       <option value="0" <?php if ($cd_lock == "0") {echo "selected";} ?>>正常会员</option> 
       <option value="1" <?php if ($cd_lock == "1") {echo "selected";} ?>>锁定会员</option> 
       </select>　*锁定后将无法登录站点</td>
      </tr>
      <tr>
       <td align="left">推荐</td>
       <td align="left">
       <select name="cd_isbest" id="cd_isbest"> 
       <option value="0" <?php if ($cd_isbest == "0") {echo "selected";} ?>>否</option> 
       <option value="1" <?php if ($cd_isbest == "1") {echo "selected";} ?>>是</option> 
       </select></td>
      </tr>
      <tr>
       <td align="left">用户组</td>
       <td align="left">
       <select name="cd_grade" id="cd_grade"> 
       <option value="0" <?php if ($cd_grade == "0") {echo "selected";} ?>>普通会员</option> 
       <option value="1" <?php if ($cd_grade == "1") {echo "selected";} ?>>VIP会员</option> 
       </select>
       </td>
      </tr>
      <tr>
       <td align="left">VIP开通日期</td>
       <td align="left"><input id="cd_vipindate" name="cd_vipindate" value="<?php echo $cd_vipindate?>" type="text" class="input input_hd length_3"></td>
      </tr>
      <tr>
       <td align="left">VIP结束日期</td>
       <td align="left"><input id="cd_vipenddate" name="cd_vipenddate" value="<?php echo $cd_vipenddate?>" type="text" class="input input_hd length_3"></td>
      </tr>
      <tr>
       <td align="left">数据</td>
       <td align="left">
	   金币<input id="cd_points" name="cd_points" value="<?php echo $cd_points?>" type="text" class="input input_hd length_1">&nbsp;&nbsp;
       &nbsp;&nbsp;分享数：<input id="cd_djmub" name="cd_djmub" value="<?php echo $cd_djmub?>" type="text" class="input input_hd length_1">
       &nbsp;&nbsp;人气：<input id="cd_hits" name="cd_hits" value="<?php echo $cd_hits?>" type="text" class="input input_hd length_1">&nbsp;&nbsp;
       &nbsp;&nbsp;粉丝：<input id="cd_ufans" name="cd_ufans" value="<?php echo $cd_ufans?>" type="text" class="input input_hd length_1">&nbsp;&nbsp;
       &nbsp;&nbsp;关注：<input id="cd_ufav" name="cd_ufav" value="<?php echo $cd_ufav?>" type="text" class="input input_hd length_1">&nbsp;&nbsp;
       </td>
      </tr>
      <tr>
       <td align="left">个性签名</td>
       <td align="left"><textarea name="cd_sign" cols="50" rows="6" id="cd_sign"><?php echo $cd_sign?></textarea></td>
      </tr>	
      <tr>
       <td height="35" colspan="2" align="left" bgcolor="#FAFBF7" class="td_border"><button type="submit" id="adddance" class="btn btn_submit J_ajax_submit_btn" style="margin-left: 80px;">确定提交</button></td>
      </tr>
     </tbody>
    </table> 
</form>
   </div> 
   </div> 
<?php
}

Function main($sql,$size){
global $db;
$Arr=getpagerow($sql,$size);//sql,每页显示条数
$result=$db->query($Arr[2]);
$videonum=$db->num_rows($result);
?>
  <div class="contents"> 
   <div class="panel"> 
    <div class="panel-head">
     <strong>
	 <?php 
	 $action=SafeRequest("action","get");
	 if($action==""){echo "所有会员列表";
	 }elseif($action=="users"){echo "普通会员";
	 }elseif($action=="vips"){echo "Vip会员";
	 }elseif($action=="userqq"){echo "使用QQ互联登录的会员";
	 }elseif($action=="isbest"){echo "已推荐的会员";
	 }elseif($action=="checkmusic"){echo "已通过音乐认证的会员";
	 }elseif($action=="checkmm"){echo "已通过美女认证的会员";
	 }elseif($action=="vlock"){echo "已被锁定的会员";
	 }elseif($action=="verifiedmm"){echo "等待审核的美女";
	 }else{
	 echo "会员管理";
	 }
	?>
	 </strong>
    </div> 
  <form method="get" action="user.php"> 
   <div class="search_type cc mb10"> 
    <div class="ul_wrap"> 
     <ul class="cc"> 
      <li> <label>关键字：</label> <input type="hidden" name="action" value="keyword" /> <input name="key" id="key" value="" type="text" class="input length_3" placeholder="支持帐号昵称性别" /> &nbsp;&nbsp;<button class="btn mr20" type="submit">搜索</button> </li> 
     </ul> 
    </div> 
   </div> 
  </form>
<form name="form" method="post" action="user.php?action=save">
    <table class="table2" id="dellist"> 
     <tbody>
      <tr>
       <td width="100" align="left"> 序号</td>
       <td align="left">登录帐号</td>
	   <td align="left">用户昵称</td>
	   <td align="left">推荐</td>
	   <td align="left">用户组</td>
	   <td align="left">状态</td>
	   <td align="left">性别</td>
	   <td align="left">分享</td>
	   <td align="left">粉丝</td>
	   <td width="120" align="left">操作</td>
      </tr>
<?php
if($videonum==0) echo "<tr><td height='30' colspan='12' align='center' bgcolor='#FFFFFF' class='td_border'><br><br>没有数据<br><br><br></td></tr>";
if($result){
while ($row = $db ->fetch_array($result)){
?>
      <tr>
	  <?php if($row['cd_id']==1){?>
       <td align="left"><?php echo $row['cd_id']?></td>
	  <?php }elseif($row['cd_id']==2){?>
       <td align="left"><?php echo $row['cd_id']?></td>
	  <?php }else{?>
	  <td align="left"><input type="checkbox" name="CD_ID[]" id="cd_id" value="<?php echo $row['cd_id']?>" class="checkbox"><?php echo $row['cd_id']?></td>
	   <?php }?>
       <td align="left"><a href="?action=edit&cd_id=<?php echo $row['cd_id']?>"><?php echo $row['cd_name']?></td>
	   <td align="left"><?php echo $row['cd_nicheng'];?></td>
	   <td align="left"><?php if($row['cd_isbest']==0){?><a title="未推荐" href="?action=editisbest&cd_id=<?php echo $row['cd_id']?>&cd_isbest=1"><img src='../images/no.gif' border='0'></a><?php }else{?><a title="已推荐" href="?action=editisbest&cd_id=<?php echo $row['cd_id']?>&cd_isbest=0"><img src='../images/yes.gif' border='0'></a><?php }?></td>
	   <td align="left"><?php if($row['cd_grade']==1){?><font color='#FF0000'><?php if ($row['cd_vipgrade'] == "1") {echo "月付";}elseif ($row['cd_vipgrade'] == "2"){echo '年付';}?>VIP会员</font><?php }else{ echo "普通会员"; }?></td>
	   <td align="left"><?php if($row['cd_lock']==1){?><font color='#FF0000'>锁定</font><?php }else{ echo "正常"; }?></td>
	   <td align="left"><?php echo $row['cd_sex']; ?></td>
	   <td align="left"><?php echo $row['cd_djmub']; ?></td>
	   <td align="left"><?php echo $row['cd_ufans']; ?></td>
	   <?php if($action=="verifiedmm" || $action=="checkmm"){ ?> <td align="left"><a href="?action=mmedit&cd_id=<?php echo $row['cd_id']; ?>" class="btn">审核</a></td><?php } else{?>
	   <td align="left"><a href="?action=edit&cd_id=<?php echo $row['cd_id']?>" class="btn">编辑</a> <a href="?action=del&cd_id=<?php echo $row['cd_id']?>" onClick="return confirm('确定删除吗？不可恢复！');" class="btn">删除</a><?php }?></td>
      </tr>
<?php
}
}
?> 
       <tr> 
        <td height="35" colspan="12" align="left" bgcolor="#FAFBF7" class="td_border"><label class="mr20"><input type="checkbox" class="J_check_all" id="chkall" onclick="return CheckAll(this.form)" value="on" />全选</label>
		<input name="button2" type="button" class="btn btn_submit J_ajax_submit_btn" value="批量设置" onclick="EditInfo(this.form,'user_pl.php')" /> 
		</td> 
       </tr> 
     </tbody>
    </table> 
</form>
   </div> 
   </div> 
<div class="p10" style="margin-bottom: 0px;">
<div class="pages"><?php echo $Arr[0];?></div>
</div>
 </body>
</html>
<?php			
}
//推荐
function EditIsBest() {
	global $db;
	$cd_id=SafeRequest("cd_id", "get");
	$cd_isbest=SafeRequest("cd_isbest", "get");
	$sql="update " . tname('user') . " set cd_isbest=" . $cd_isbest . " where CD_ID=" . $cd_id;
	if($db->query($sql)) {
		echo "<script>window.location='" . $_SERVER['HTTP_REFERER'] . "'</script>";
	}
}


//删除
function del() {
	global $db;
	$cd_id=SafeRequest("cd_id", "get");
	$array=array('1','2');
	if(in_array($cd_id, $array)) {
		showmessage("删除失败,系统默认帐号您只可以编辑不可删除。", "user.php", 0);
	}
	$sql="delete from " . tname('user') . " where cd_id=$cd_id";
	if($db->query($sql)) {
		$db->query("delete from " . tname('pic') . " where cd_uid=$cd_id");
		$db->query("delete from " . tname('friend') . " where cd_uid=$cd_id or cd_uids=$cd_id");
		$db->query("delete from " . tname('web') . " where cd_uid=$cd_id");
		$db->query("delete from " . tname('feed') . " where cd_uid=$cd_id");
		@unlink(_x5music_root_ . "upload/headpic/" . $cd_id . ".jpg");
		$db->query('update ' . tname('system') . " set cd_usermub=cd_usermub-1"); //全局数据统计
		showmessage("恭喜您，删除成功！", "user.php", 0);
	}
}

//编辑
function Edit() {
	global $db;
	$cd_id=SafeRequest("cd_id", "get");
	$sql="Select * from " . tname('user') . " where cd_id=$cd_id";
	if($row=$db->Getrow($sql)) {
		$Arr=array(
			$row['cd_name'],
			$row['cd_nicheng'],
			$row['cd_email'],
			$row['cd_qq'],
			$row['cd_qqopenid'],
			$row['cd_photo'],
			$row['cd_vipindate'],
			$row['cd_vipenddate'],
			$row['cd_sex'],
			$row['cd_birthprovince'],
			$row['cd_birthcity'],
			$row['cd_resideprovince'],
			$row['cd_residecity'],
			$row['cd_hits'],
			$row['cd_points'],
			$row['cd_djmub'],
			$row['cd_ufans'],
			$row['cd_ufav'],
			$row['cd_password'],
			$row['cd_sign'],
			$row['cd_birthday'],
			$row['cd_lock'],
			$row['cd_isbest'],
			$row['cd_grade'],
			$row['cd_regdate'],
			$row['cd_logintime']
		);
	}
	EditBoard($Arr, "?action=saveedit&cd_id=$cd_id");
}

//添加数据
function Add() {
	$Arr=array("","","","","","","","","","","","","","","","","","","","","");
	EditBoard($Arr, "?action=saveadd");
}
//执行保存
function SaveAdd() {
	global $db;
	$cd_name=SafeRequest("cd_name", "post");
	$cd_password=SafeRequest("cd_password", "post");
	$cd_nicheng=SafeRequest("cd_nicheng", "post");
	$cd_email=SafeRequest("cd_email", "post");
	$cd_sex=SafeRequest("cd_sex", "post");
	$cd_qq=SafeRequest("cd_qq", "post");
	$cd_photo=SafeRequest("cd_photo", "post");
	$cd_points=SafeRequest("cd_points", "post");
	$cd_vipindate=SafeRequest("cd_vipindate", "post");
	$cd_vipenddate=SafeRequest("cd_vipenddate", "post");
	$cd_rank=SafeRequest("cd_rank", "post");
	$cd_sign=SafeRequest("cd_sign", "post");
	$cd_birthprovince=SafeRequest("birthprovince", "post");
	$cd_birthcity=SafeRequest("birthcity", "post");
	$cd_resideprovince=SafeRequest("resideprovince", "post");
	$cd_residecity=SafeRequest("residecity", "post");
	$cd_hits=SafeRequest("cd_hits", "post");
	$cd_qqopenid=SafeRequest("cd_qqopenid", "post");
	$cd_djmub=SafeRequest("cd_djmub", "post");
	$cd_ufans=SafeRequest("cd_ufans", "post");
	$cd_ufav=SafeRequest("cd_ufav", "post");
	$cd_birthday=SafeRequest("cd_birthday", "post");
	$cd_lock=SafeRequest("cd_lock", "post");
	$cd_isbest=SafeRequest("cd_isbest", "post");
	$cd_grade=SafeRequest("cd_grade", "post");
	$cd_regdate=SafeRequest("cd_regdate", "post");
	$cd_logintime=SafeRequest("cd_logintime", "post");
	$result=$db->getrow("select * from " . tname('user') . " where cd_name='" . $cd_name . "'");
	if($result) {
		showmessage("出错了，添加失败，登录邮箱帐号已存在！", $_SERVER['HTTP_REFERER'], 3);
		exit();
	}
	$setarr=array(
		'cd_name'=>$cd_name,
		'cd_password'=>$cd_password,
		'cd_nicheng'=>$cd_nicheng,
		'cd_email'=>$cd_email,
		'cd_sex'=>$cd_sex,
		'cd_qq'=>$cd_qq,
		'cd_photo'=>$cd_photo,
		'cd_points'=>$cd_points,
		'cd_vipindate'=>$cd_vipindate,
		'cd_vipenddate'=>$cd_vipenddate,
		'cd_rank'=>$cd_rank,
		'cd_sign'=>$cd_sign,
		'cd_birthprovince'=>$birthprovince,
		'cd_birthcity'=>$birthcity,
		'cd_resideprovince'=>$resideprovince,
		'cd_residecity'=>$residecity,
		'cd_hits'=>$cd_hits,
		'cd_qqopenid'=>$cd_qqopenid,
		'cd_djmub'=>$cd_djmub,
		'cd_ufans'=>$cd_ufans,
		'cd_ufav'=>$cd_ufav,
		'cd_birthday'=>$cd_birthday,
		'cd_lock'=>$cd_lock,
		'cd_isbest'=>$cd_isbest,
		'cd_grade'=>$cd_grade,
		'cd_regdate'=>$cd_regdate,
		'cd_logintime'=>$cd_logintime
	);
	inserttable('user', $setarr, 1);
	$db->query('update ' . tname('system') . " set cd_usermub=cd_usermub+1"); //全局数据统计
	showmessage("恭喜您，添加成功！", "user.php", 0);
}
//保存编辑
function SaveEdit() {
	global $db;
	$cd_id=SafeRequest("cd_id", "get");
	$cd_password=SafeRequest("cd_password", "post");
	$cd_nicheng=SafeRequest("cd_nicheng", "post");
	$cd_email=SafeRequest("cd_email", "post");
	$cd_sex=SafeRequest("cd_sex", "post");
	$cd_qq=SafeRequest("cd_qq", "post");
	$cd_photo=SafeRequest("cd_photo", "post");
	$cd_points=SafeRequest("cd_points", "post");
	$cd_vipindate=SafeRequest("cd_vipindate", "post");
	$cd_vipenddate=SafeRequest("cd_vipenddate", "post");
	$cd_rank=SafeRequest("cd_rank", "post");
	$cd_sign=SafeRequest("cd_sign", "post");
	$cd_birthprovince=SafeRequest("birthprovince", "post");
	$cd_birthcity=SafeRequest("birthcity", "post");
	$cd_resideprovince=SafeRequest("resideprovince", "post");
	$cd_residecity=SafeRequest("residecity", "post");
	$cd_hits=SafeRequest("cd_hits", "post");
	$cd_httpurl=SafeRequest("cd_httpurl", "post");
	$cd_qqopenid=SafeRequest("cd_qqopenid", "post");
	$cd_djmub=SafeRequest("cd_djmub", "post");
	$cd_ufans=SafeRequest("cd_ufans", "post");
	$cd_ufav=SafeRequest("cd_ufav", "post");
	$cd_birthday=SafeRequest("cd_birthday", "post");
	$cd_lock=SafeRequest("cd_lock", "post");
	$cd_isbest=SafeRequest("cd_isbest", "post");
	$cd_grade=SafeRequest("cd_grade", "post");
	$cd_regdate=SafeRequest("cd_regdate", "post");
	$cd_logintime=SafeRequest("cd_logintime", "post");
	$delphoto=SafeRequest("delphoto", "post");
	if($delphoto==1) {
		@unlink(_x5music_root_ . "upload/headpic/" . $cd_id . ".jpg");
		$cd_photo="";
	}
	if(!IsNul($cd_password)) {
		$sql="update " . tname('user') . " set cd_nicheng='" . $cd_nicheng . "',cd_hits='" . $cd_hits . "',cd_email='" . $cd_email . "',cd_sex='" . $cd_sex . "',cd_qq='" . $cd_qq . "',cd_photo='" . $cd_photo . "',cd_points='" . $cd_points . "',cd_vipindate='" . $cd_vipindate . "',cd_vipenddate='" . $cd_vipenddate . "',cd_sign='" . $cd_sign . "',cd_birthprovince='" . $cd_birthprovince . "',cd_birthcity='" . $cd_birthcity . "',cd_resideprovince='" . $cd_resideprovince . "',cd_residecity='" . $cd_residecity . "',cd_qqopenid='" . $cd_qqopenid . "',cd_djmub='" . $cd_djmub . "',cd_ufans='" . $cd_ufans . "',cd_ufav='" . $cd_ufav . "',cd_birthday='" . $cd_birthday . "',cd_lock='" . $cd_lock . "',cd_isbest='" . $cd_isbest . "',cd_grade='" . $cd_grade . "',cd_regdate='" . $cd_regdate . "',cd_logintime='" . $cd_logintime . "' where cd_id=" . $cd_id . "";
	} else {
		$sql="update " . tname('user') . " set cd_nicheng='" . $cd_nicheng . "',cd_password='" . substr(md5($cd_password), 8, 16) . "',cd_hits='" . $cd_hits . "',cd_email='" . $cd_email . "',cd_sex='" . $cd_sex . "',cd_qq='" . $cd_qq . "',cd_photo='" . $cd_photo . "',cd_points='" . $cd_points . "',cd_vipindate='" . $cd_vipindate . "',cd_vipenddate='" . $cd_vipenddate . "',cd_sign='" . $cd_sign . "',cd_birthprovince='" . $cd_birthprovince . "',cd_birthcity='" . $cd_birthcity . "',cd_resideprovince='" . $cd_resideprovince . "',cd_residecity='" . $cd_residecity . "',cd_qqopenid='" . $cd_qqopenid . "',cd_djmub='" . $cd_djmub . "',cd_ufans='" . $cd_ufans . "',cd_ufav='" . $cd_ufav . "',cd_birthday='" . $cd_birthday . "',cd_lock='" . $cd_lock . "',cd_isbest='" . $cd_isbest . "',cd_grade='" . $cd_grade . "',cd_regdate='" . $cd_regdate . "',cd_logintime='" . $cd_logintime . "' where cd_id=" . $cd_id . "";
	}
	if($db->query($sql)) {
		showmessage("恭喜您，编辑会员信息成功！", $_SERVER['HTTP_REFERER'], 0);
	} else {
		showmessage("出错了，编辑会员信息失败！" . $cd_password . "", $_SERVER['HTTP_REFERER'], 0);
	}
}
?>