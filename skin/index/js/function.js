window.onerror=function(){ return true; };
$A = function(em) {
	if (document.getElementById){ return document.getElementById(em); }
	else if (document.all){ return document.all[em]; }
	else if (document.layers){ return document.layers[em]; }
	else{ return null; }
};

$F = function(em){	return document.getElementById(em).value;	};

function get_tags(parentobj, tag){
	if (typeof parentobj.getElementsByTagName != 'undefined'){ return parentobj.getElementsByTagName(tag);
	}else if (parentobj.all && parentobj.all.tags){ return parentobj.all.tags(tag);
	}else{ return new Array(); }
};

function clk(_act, _id) {
	var v = [];
	var a = get_tags($A(_id), 'input');

	for (var i = 0; i < a.length; i++) {
		if (_act == 'all' ) {a[i].checked = true;}
		else if (_act == 'un' ) { a[i].checked = (a[i].checked) ? false : true; }
		else { if (0 < a[i].value && a[i].checked){ v.push(a[i].value); } }
	}
	if ( 'play' == _act ) {
		if (3 > v.length){ asyncbox.tips("必须选择3首以上才能播放！", "alert", 2500); return; }
		window.open(web_url+'include/lplayer.php?id=' + v.join(','), 'lplay');
	}
};

function strLen(str) {
	var charset = document.charset; 
	var len = 0;
	for(var i = 0; i < str.length; i++) {
		len += str.charCodeAt(i) < 0 || str.charCodeAt(i) > 255 ? (charset == "utf-8" ? 3 : 2) : 1;
	}
	return len;
}

//控制在同一窗口中打开
function Listen(ListenURL) {
	var Listen = window.open(ListenURL,"Listens","resizable=yes,scrollbars=yes,status=yes,toolbar=yes,menubar=yes,location=yes,top=0,left=0");
	return false;
}

/*选项卡切换*/
function setTab(name,cursel,n){
	for(i=1;i<=n;i++){
		var menu=document.getElementById(name+i);
		var con=document.getElementById("con_"+name+"_"+i);
		menu.className=i==cursel?"hover":"";
		con.style.display=i==cursel?"block":"none";
	}
}

function openWin(tag) {
	window.open(tag,"ylmv","resizable=yes,scrollbars=yes,status=yes,toolbar=yes,menubar=yes,location=yes,top=0,left=0");
};