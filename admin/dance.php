<?php
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
error_reporting(0);
include "../include/x5music.conn.php";
include "function_common.php";
admincheck(3);
$action=SafeRequest("action", "get");
$CD_ClassID=SafeRequest("CD_ClassID", "get");
?>
<!DOCTYPE html>
<html>
 <head> 
  <meta charset="gbk" /> 
  <meta name="renderer" content="webkit" /> 
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" /> 
  <title>x5Music 后台管理中心 -x5mp3.com</title> 
  <link href="css/add.css" rel="stylesheet" /> 
  <script type="text/javascript" src="js/ajax.js"></script> 
  <script type="text/javascript" src="<?php echo cd_webpath?>user/static/space/layer/jquery.js"></script> 
  <script type="text/javascript" src="<?php echo cd_webpath?>user/static/space/layer/lib.js"></script> 
  <script type="text/javascript">
var pop = {
	up : function (text, url, width, height, top) {
		$.layer({
			type : 2,
			maxmin : true,
			title : text,
			iframe : {
				src : url
			},
			area : [width, height],
			offset : [top, '50%'],
			shade : [0]
		});
	}
}
function MM_jumpMenu(targ, selObj, restore) { //v3.0
	eval(targ + ".location='" + selObj.options[selObj.selectedIndex].value + "'");
	if (restore)
		selObj.selectedIndex = 0;
}
</script> 
  <style type="text/css">
.table2{width: 100%;max-width: 100%;border-collapse: collapse;border-spacing: 0;}
.table2 td{font-size: 12px;border-top: 1px solid #ddd;padding: 8px;vertical-align: top;}
.table2 tr:hover{text-decoration: none;background-color: #e6f2fb;}
.table2 th{border-bottom: 2px solid #ddd;vertical-align: bottom;padding: 2px;text-align: left;}
.table3{width: 100%;max-width: 100%;border-collapse: collapse;border-spacing: 0;}
.table3 tr:hover{text-decoration: none;background-color: #e6f2fb;}
.table3 td{font-size: 12px;line-height:25px;border-top: 1px solid #ddd;padding: 5px;vertical-align: top;border-right: solid 1px #ddd;}
.table3 th{border-bottom: 2px solid #ddd;vertical-align: bottom;padding: 2px;text-align: left;}
</style>  
 </head> 
 <body> 
  <div class="contents"> 
   <div class="panel"> 
    <div style="padding: 8px;"> 
<?php
if($action=="") {
	$x5='btn_success';
} elseif($action=="add") {
	$x51='btn_success';
} elseif($action=="pass") {
	$x52='btn_success';
} elseif($action=="isbest") {
	$x53='btn_success';
} elseif($action=="error") {
	$x54='btn_success';
} elseif($action=="deleted") {
	$x55='btn_success';
} else {
	$x5='btn_success';
}
?>
     <a href="dance.php" class="btn <?php echo $x5;?>">所有音乐</a> 
     <a href="dance.php?action=add" class="btn <?php echo $x51;?>">添加音乐</a> 
     <a href="dance.php?action=pass" class="btn <?php echo $x52;?>">审核音乐</a> 
     <a href="dance.php?action=isbest" class="btn <?php echo $x53;?>">星级音乐</a> 
     <a href="dance.php?action=error" class="btn <?php echo $x54;?>">报错的音乐</a> 
     <a href="dance.php?action=deleted" class="btn <?php echo $x55;?>">音乐回收站</a> 
    </div> 
   </div> 
  </div>
<?php
switch($action) {
	case 'add':
		Add();
		break;
	case 'saveadd':
		SaveAdd();
		break;
	case 'edit':
		Edit();
		break;
	case 'saveedit':
		SaveEdit();
		break;
	case 'editpassed':
		EditPassed();
		break;
	case 'keyword':
		$key=SafeRequest("key", "get");
		main("select * from " . tname('dj') . " where CD_Name like '%" . $key . "%' or CD_Singer like '%" . $key . "%' or CD_User like '%" . $key . "%' and CD_Deleted=0 and CD_ClassID<>7 order by CD_ID desc", 20);
		break;
	case 'class':
		$CD_ClassID=SafeRequest("CD_ClassID", "get");
		main("select * from " . tname('dj') . " where CD_ClassID=" . $CD_ClassID . " and CD_Deleted=0 and CD_ClassID<>7 order by CD_ID desc", 20);
		break;
	case 'server':
		$CD_Server=SafeRequest("CD_Server", "get");
		main("select * from " . tname('dj') . " where CD_Server=" . $CD_Server . " and CD_Deleted=0 and CD_ClassID<>7 order by CD_ID desc", 20);
		break;
	case 'pass':
		main("select * from " . tname('dj') . " where CD_Passed=1 and CD_Deleted=0 and CD_ClassID<>7 order by CD_ID desc", 20);
		break;
	case 'error':
		main("select * from " . tname('dj') . " where CD_Error<>0 and CD_Deleted=0 and CD_ClassID<>7 order by CD_ID desc", 20);
		break;
	case 'isbest':
		main("select * from " . tname('dj') . " where CD_IsBest<>0 and CD_Deleted=0 and CD_ClassID<>7 order by CD_ID desc", 20);
		break;
	case 'deleted':
		main("select * from " . tname('dj') . " where CD_Deleted=1 and CD_ClassID<>7 order by CD_ID desc", 20);
		break;
	case 'check':
		main("select * from " . tname('dj') . " where CD_Deleted=0 and CD_ClassID<>7 order by CD_ID desc", 20);
		//main("select * from ".tname('dj')." where CD_Deleted=0 and CD_Name in (select CD_Name from ".tname('dj')." group by CD_Name having count(*)>1) order by CD_Name asc",20);
		break;
	case 'special':
		$CD_SpecialID=SafeRequest("CD_SpecialID", "get");
		main("select * from " . tname('dj') . " where CD_SpecialID=" . $CD_SpecialID . " and CD_Deleted=0 and CD_ClassID<>7 order by CD_ID desc", 20);
		break;
	case 'singer':
		$CD_SingerID=SafeRequest("CD_SingerID", "get");
		main("select * from " . tname('dj') . " where CD_Singer=" . $CD_SingerID . " and CD_Deleted=0 and CD_ClassID<>7 order by CD_ID desc", 20);
	case 'dela':
		DelA();
		break;
	case 'delb':
		DelB();
		break;
	case 'delc':
		DelC();
		break;
	case 'video':
		Add();
		break;
	case 'addlistsave':
		AddListSave();
		break;
	case 'allhtml':
		AllHtml();
		break;
	default:
		main("select * from " . tname('dj') . " where CD_Deleted=0 and CD_ClassID<>7 order by CD_ID desc", 20);
		break;
}
?>
</body>
</html>
<?php
Function EditBoard($Arr, $url) {
	global $db;
	$CD_ID=SafeRequest("CD_ID", "get");
	//var_dump ($Arr); 
	$CD_Name=$Arr[0];
	$CD_ClassID=$Arr[1];
	$CD_SpecialID=$Arr[2];
	$CD_Singer=$Arr[3];
	$CD_User=$Arr[4];
	$CD_Tag=$Arr[5];
	$CD_Pic=$Arr[6];
	$CD_Url=$Arr[7];
	$CD_DownUrl=$Arr[8];
	$CD_Word=$Arr[9];
	$CD_Lrc=$Arr[10];
	$CD_From=$Arr[11];
	$CD_Siz=$Arr[12];
	$CD_Uid=$Arr[13];
	$CD_Md5=$Arr[14];
	$CD_Hits=$Arr[15];
	$CD_DownHits=$Arr[16];
	$CD_FavHits=$Arr[17];
	$CD_uHits=$Arr[18];
	$CD_dHits=$Arr[19];
	$CD_DayHits=$Arr[20];
	$CD_WeekHits=$Arr[21];
	$CD_MonthHits=$Arr[22];
	$CD_LastHitTime=$Arr[23];
	$CD_AddTime=$Arr[24];
	$CD_Time=$Arr[24];
	$CD_Server=$Arr[25];
	$CD_Deleted=$Arr[26];
	$CD_IsBest=$Arr[27];
	$CD_Error=$Arr[28];
	$CD_Passed=$Arr[29];
	$CD_Points=$Arr[30];
	$CD_Grade=$Arr[31];
	$CD_Color=$Arr[32];
	$CD_Skin=$Arr[33];
	$CD_Playtime=$Arr[34];
	$CD_Bitkbps=$Arr[35];
	if(!IsNum($CD_Hits)) {
		$CD_Hits=0;
	}
	if(!IsNum($CD_DayHits)) {
		$CD_DayHits=0;
	}
	if(!IsNum($CD_WeekHits)) {
		$CD_WeekHits=0;
	}
	if(!IsNum($CD_MonthHits)) {
		$CD_MonthHits=0;
	}
	if(!IsNum($CD_uHits)) {
		$CD_uHits=0;
	}
	if(!IsNum($CD_dHits)) {
		$CD_dHits=0;
	}
	if(!IsNum($CD_DownHits)) {
		$CD_DownHits=0;
	}
	if(!IsNum($CD_FavHits)) {
		$CD_FavHits=0;
	}
	if(!IsNum($CD_Points)) {
		$CD_Points=0;
	}
	if(!IsNum($CD_Grade)) {
		$CD_Grade=1;
	}
	if(!IsNul($CD_Skin)) {
		$CD_Skin="play.html";
	}
	if(SafeRequest("ClassID", "get")<>"") {
		$x5music_CD_ClassID=SafeRequest("ClassID", "get");
	} else {
		$x5music_CD_ClassID=$CD_ClassID;
	}
?>
  <div class="contents"> 
   <div class="panel"> 
    <div class="panel-head"> 
     <strong><?php $action=SafeRequest("action","get"); if($action=="add"){echo "添加音乐";}elseif($action=="edit"){echo "编辑音乐";}?></strong> 
    </div> 
    <form action="<?php echo $url?>" method="post" name="form2"> 
     <input name="id" type="hidden" value="<?php echo $CD_ID?>" /> 
     <table class="table3"> 
      <tbody> 
       <tr> 
        <td width="70" align="left" class="p10">标题名称</td> 
        <td align="left"> <input id="CD_Name" name="CD_Name" value="<?php echo $CD_Name?>" type="text" class="input input_hd length_5" /> <label class="mr20"><input type="checkbox" name="edittime" class="checkbox" id="edittime" value="1" checked />更新时间</label>&nbsp;&nbsp; 
        标题颜色： 
        <select name="CD_Color"> 
        <option value=""></option> 
        <option style="background-color:#00FF00;color: #00FF00" value="#00FF00"<?php if($CD_Color=="#00FF00"){echo " selected";} ?>>绿色</option>
        <option style="background-color:#0000CC;color: #0000CC" value="#0000CC"<?php if($CD_Color=="#0000CC"){echo " selected";} ?>>深蓝</option>
        <option style="background-color:#FFFF00;color: #FFFF00" value="#FFFF00"<?php if($CD_Color=="#FFFF00"){echo " selected";} ?>>黄色</option>
        <option style="background-color:#FF33CC;color: #FF33CC" value="#FF33CC"<?php if($CD_Color=="#FF33CC"){echo " selected";} ?>>粉红</option>
        <option style="background-color:#FF0000;color: #FF0000" value="#FF0000"<?php if($CD_Color=="#FF0000"){echo " selected";} ?>>红色</option>
        <option style="background-color:#660099;color: #660099" value="#660099"<?php if($CD_Color=="#660099"){echo " selected";} ?>>紫色</option>
        <option style="background-color:#FFFFFF;color: #FFFFFF" value="">无色</option> 
        </select> &nbsp;&nbsp;
        分类栏目： 
        <select name="CD_ClassID" id="CD_ClassID">
        <option value="0">选择栏目</option>
<?php
$sqlclass="select * from " . tname('class') . " where CD_FatherID=0 and CD_ID<>7";
$results=$db->query($sqlclass);
if($results) {
	while($row3=$db->fetch_array($results)) {
		if($x5music_CD_ClassID==$row3['CD_ID']) {
			echo "        <option value='" . $row3['CD_ID'] . "' selected='selected'>" . $row3['CD_Name'] . "</option>
";
		} else {
			echo "        <option value='" . $row3['CD_ID'] . "' >" . $row3['CD_Name'] . "</option>
";
		}
	}
}
?>
        </select>
        </td> 
       </tr> 
       <tr> 
        <td align="left">歌手名称</td> 
        <td align="left"><input name="CD_Singer" value="<?php echo $CD_Singer?>" type="text" class="input input_hd length_3" /></td> 
       </tr> 
       <tr> 
        <td align="left">Tag标签</td> 
        <td align="left"><input name="CD_Tag" value="<?php echo $CD_Tag?>" type="text" class="input input_hd length_3" />　*多个标签请用小数点逗号(,)隔开</td> 
       </tr> 
       <tr> 
        <td align="left">所属会员</td> 
        <td align="left"><input name="CD_User" value="<?php echo $CD_User?>" type="text" class="input input_hd length_3" /><font color="#d01f3c">&nbsp;&nbsp;必须是站内已存在的用户帐号！不能乱写!</font></td> 
       </tr> 
       <tr> 
        <td align="left">播放模板</td> 
        <td align="left"><input name="CD_Skin" id="CD_Skin" value="<?php echo $CD_Skin?>" type="text" class="input input_hd length_2" /> <button type="button" onclick="pop.up('浏览模板目录', 'inc/select_templets.php?f=form2.CD_Skin', '550px', '400px', '140px');" class="btn">浏览...</button> </td>
       </tr> 
       <tr> 
        <td align="left">数据统计</td> 
        <td align="left"> 日 <input type="text" class="input input_hd length_1" name="CD_DayHits" id="CD_DayHits" value="<?php echo $CD_DayHits?>" size="5" onfocus="this.style.borderColor='#F93'" onblur="this.style.borderColor='#CCC'" onkeyup="this.value=this.value.replace(/[^\d]/g,'')" onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/[^\d]/g,''))" />&nbsp; 周 <input type="text" class="input input_hd length_1" name="CD_WeekHits" id="CD_WeekHits" value="<?php echo $CD_WeekHits?>" size="5" onfocus="this.style.borderColor='#F93'" onblur="this.style.borderColor='#CCC'" onkeyup="this.value=this.value.replace(/[^\d]/g,'')" onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/[^\d]/g,''))" />&nbsp; 月 <input type="text" class="input input_hd length_1" name="CD_MonthHits" id="CD_MonthHits" value="<?php echo $CD_MonthHits?>" size="5" onfocus="this.style.borderColor='#F93'" onblur="this.style.borderColor='#CCC'" onkeyup="this.value=this.value.replace(/[^\d]/g,'')" onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/[^\d]/g,''))" />&nbsp; 总 <input type="text" class="input input_hd length_1" name="CD_Hits" id="CD_Hits" value="<?php echo $CD_Hits?>" size="5" onfocus="this.style.borderColor='#F93'" onblur="this.style.borderColor='#CCC'" onkeyup="this.value=this.value.replace(/[^\d]/g,'')" onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/[^\d]/g,''))" />&nbsp; 顶 <input type="text" class="input input_hd length_1" name="CD_uHits" id="CD_uHits" value="<?php echo $CD_uHits?>" size="5" onfocus="this.style.borderColor='#F93'" onblur="this.style.borderColor='#CCC'" onkeyup="this.value=this.value.replace(/[^\d]/g,'')" onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/[^\d]/g,''))" />&nbsp; 踩 <input type="text" class="input input_hd length_1" name="CD_dHits" id="CD_dHits" value="<?php echo $CD_dHits?>" size="5" onfocus="this.style.borderColor='#F93'" onblur="this.style.borderColor='#CCC'" onkeyup="this.value=this.value.replace(/[^\d]/g,'')" onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/[^\d]/g,''))" />&nbsp; 下载 <input type="text" class="input input_hd length_1" name="CD_DownHits" id="CD_DownHits" value="<?php echo $CD_DownHits?>" size="5" onfocus="this.style.borderColor='#F93'" onblur="this.style.borderColor='#CCC'" onkeyup="this.value=this.value.replace(/[^\d]/g,'')" onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/[^\d]/g,''))" />&nbsp; 收藏 <input type="text" class="input input_hd length_1" name="CD_FavHits" id="CD_FavHits" value="<?php echo $CD_FavHits?>" size="5" onfocus="this.style.borderColor='#F93'" onblur="this.style.borderColor='#CCC'" onkeyup="this.value=this.value.replace(/[^\d]/g,'')" onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/[^\d]/g,''))" />&nbsp; </td> 
       </tr> 
       <tr> 
        <td align="left">数据归属</td> 
        <td align="left">服 务 器：&nbsp;
        <select name="CD_Server" id="CD_Server">
        <option value="0" style="color:#FF0000;">不设置</option>
<?php
$sqlclass="select * from " . tname('server') . "";
$results=$db->query($sqlclass);
if($results) {
	while($row3=$db->fetch_array($results)) {
		if($CD_Server==$row3['CD_ID']) {
			echo "        <option value='" . $row3['CD_ID'] . "' selected='selected'>" . $row3['CD_Name'] . "</option>
";
		} else {
			echo "        <option value='" . $row3['CD_ID'] . "' >" . $row3['CD_Name'] . "</option>
";
		}
	}
}
?>
        </select>
        &nbsp;&nbsp;&nbsp;&nbsp;
        推荐星级：
        <select name="CD_IsBest" id="CD_IsBest">
        <option value="0"<?php if($CD_IsBest==0){echo " selected";} ?> style="color:#FF0000;">不推荐</option>
        <option value="1"<?php if($CD_IsBest==1){echo " selected";} ?>>一星</option>
        <option value="2"<?php if($CD_IsBest==2){echo " selected";} ?>>二星</option>
        <option value="3"<?php if($CD_IsBest==3){echo " selected";} ?>>三星</option>
        <option value="4"<?php if($CD_IsBest==4){echo " selected";} ?>>四星</option>
        <option value="5"<?php if($CD_IsBest==5){echo " selected";} ?>>五星</option>
        </select>
        &nbsp;&nbsp;&nbsp;&nbsp;
        所属专辑：&nbsp;
        <select name="CD_SpecialID" id="CD_SpecialID">
        <option value="0" style="color:#FF0000;">不属任何专辑</option>
<?php
$sqlclass="select CD_ID,CD_Name from " . tname('malbum') . "";
$results=$db->query($sqlclass);
if($results) {
	while($row3=$db->fetch_array($results)) {
		if($CD_SpecialID==$row3['CD_ID']) {
			echo "        <option value='" . $row3['CD_ID'] . "' selected='selected'>" . $row3['CD_Name'] . "</option>
";
		} else {
			echo "        <option value='" . $row3['CD_ID'] . "' >" . $row3['CD_Name'] . "</option>
";
		}
	}
}
?>
        </select> </td> 
       </tr> 
       <tr> 
        <td align="left">文件特性</td> 
        <td align="left"> UID：<input name="CD_Uid" value="<?php echo $CD_Uid?>" type="text" class="input input_hd length_1" />&nbsp;&nbsp;&nbsp;&nbsp; 数据来源：<input name="CD_From" value="<?php echo $CD_From?>" type="text" class="input input_hd length_1" />&nbsp;&nbsp;&nbsp;&nbsp; MD5效验码：<input name="CD_Md5" id="CD_Md5" value="<?php echo $CD_Md5?>" type="text" class="input input_hd length_4" />&nbsp;&nbsp;&nbsp;&nbsp; 大小：<input name="CD_Siz" value="<?php echo $CD_Siz?>" type="text" class="input input_hd length_2" /></td> 
       </tr> 
       <tr> 
        <td align="left">权限/金币</td> 
        <td align="left"> 下载权限： 
        <select name="CD_Grade" id="CD_Grade"> 
        <option value="0"<?php if($CD_Grade==0){echo " selected";} ?>>游客下载</option>
        <option value="1"<?php if($CD_Grade==1){echo " selected";} ?>>普通用户</option>
        <option value="2"<?php if($CD_Grade==2){echo " selected";} ?>>VIP 用户</option>
        </select>&nbsp;&nbsp; 
        下载扣除金币：<input type="text" id="CD_Points" class="input input_hd length_1" name="CD_Points" size="5" value="<?php echo $CD_Points?>" onfocus="this.style.borderColor='#F93'" onblur="this.style.borderColor='#CCC'" onkeyup="this.value=this.value.replace(/[^\d]/g,'')" onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/[^\d]/g,''))" /> </td> 
       </tr> 
       <tr> 
        <td align="left">播放地址</td> 
        <td align="left"><input name="CD_Url" id="CD_Url" value="<?php echo $CD_Url?>" type="text" class="input input_hd length_6" /> <button type="button" onclick="pop.up('上传你电脑上的音乐文件', 'upload/?ac=dance&f=form2.CD_Url', '406px', '180px', '140px');" class="btn">上传文件</button>  <label class="mr20"><input type="checkbox" name="delurl" class="checkbox" id="delurl" value="1">删除文件</label></td> 
       </tr> 
       <tr> 
        <td align="left">下载地址</td> 
        <td align="left"><input name="CD_DownUrl" id="CD_DownUrl" value="<?php echo $CD_DownUrl?>" type="text" class="input input_hd length_6" /></td> 
       </tr> 
       <tr> 
        <td align="left">封面图片</td> 
        <td align="left"><input name="CD_Pic" value="<?php echo $CD_Pic?>" type="text" class="input input_hd length_6" /> <button type="button" onclick="pop.up('上传封面', 'upload/?ac=dancepic&f=form2.CD_Pic', '406px', '180px', '140px');" class="btn">上传封面</button> <label class="mr20"><input type="checkbox" name="delpic" class="checkbox" id="delpic" value="1">删除封面图片</label></td> 
       </tr> 
       <tr> 
        <td align="left">简介内容</td> 
        <td align="left"><textarea class="length_6" name="CD_Word"><?php echo $CD_Word?></textarea></td> 
       </tr> 
       <tr> 
        <td align="left">歌词内容</td> 
        <td align="left"><textarea class="length_6" name="CD_Lrc"><?php echo $CD_Lrc?></textarea></td> 
       </tr> 
       <input type="hidden" name="CD_Time" value="<?php echo $CD_Time?>"> 
       <tr> 
        <td height="35" colspan="2" align="left" bgcolor="#FAFBF7" class="td_border"><button type="submit" id="adddance" class="btn btn_submit J_ajax_submit_btn" style="margin-left: 80px;">确定提交</button></td> 
       </tr> 
      </tbody> 
     </table> 
    </form>
   </div> 
  </div> 
<?php
}
Function main($sql, $size) {
	global $db;
	$Arr=getpagerow($sql, $size); //sql,每页显示条数
	$result=$db->query($Arr[2]);
	$mnum=$db->num_rows($result);
?>
  <div class="contents"> 
   <div class="panel"> 
    <div class="panel-head"> 
     <strong><?php
$action=SafeRequest("action", "get");
$key=SafeRequest("key", "get");
$CD_SingerID=SafeRequest("CD_SingerID", "get");
$CD_SpecialID=SafeRequest("CD_SpecialID", "get");
if($action=="") {
	echo "所有音乐列表";
} elseif($action=="keyword") {
	echo "搜索关键词　$key";
} elseif($action=="class") {
	echo "按音乐分类查看";
} elseif($action=="server") {
	echo "按所属服务器查看";
} elseif($action=="pass") {
	echo "等待审核的音乐";
} elseif($action=="isbest") {
	echo "已被设置星级的音乐";
} elseif($action=="deleted") {
	echo "音乐回收站";
} elseif($action=="special") {
	echo "专辑所属的音乐";
} elseif($action=="error") {
	echo "被报错的音乐 　编辑歌曲可消除错误状态";
} elseif($action=="singer") {
	echo "《" . GetSingerAlias("x5music_singer", "CD_Name", "CD_ID", $CD_SingerID) . "》的相关音乐";
} elseif($action=="special") {
	echo "专辑：《" . GetAlias("x5music_malbum", "CD_Name", "CD_ID", $CD_SpecialID) . "》的相关音乐";
} else {
	echo "所有音乐列表";
}
?></strong> 
    </div> 
    <form method="get" action="dance.php"> 
     <div class="search_type cc mb10"> 
      <div class="ul_wrap"> 
       <ul class="cc"> 
        <li> <label>关键字：</label> <input type="hidden" name="action" value="keyword" /> <input name="key" id="key" value="" type="text" class="input length_3" /> &nbsp;&nbsp;<button class="btn mr20" type="submit">搜索</button> </li> 
        <li> <label>按分类：</label>
        <select class="select_3" onchange="MM_jumpMenu('self',this,0)"> 
        <option value="dance.php">选择分类</option> 
<?php
$sqlclass="select * from " . tname('class') . " where CD_FatherID=0 and CD_ID<>7";
$results=$db->query($sqlclass);
if($results) {
	while($row3=$db->fetch_array($results)) {
		if(SafeRequest("CD_ClassID", "get")==$row3['CD_ID']) {
			echo "        <option value='?action=class&CD_ClassID=" . $row3['CD_ID'] . "' selected='selected'>" . $row3['CD_Name'] . "</option>
";
		} else {
			echo "        <option value='?action=class&CD_ClassID=" . $row3['CD_ID'] . "' >" . $row3['CD_Name'] . "</option>
";
		}
	}
}
?>
        </select> 
        </li> 
        <li> <label>按服务器：</label>
        <select onchange="window.location.href=''+this.options[this.selectedIndex].value+'';"> 
		<option value="dance.php">选择服务器</option> 
<?php
$sqlclass="select * from " . tname('server') . "";
$results=$db->query($sqlclass);
if($results) {
	while($row3=$db->fetch_array($results)) {
		if(SafeRequest("CD_Server", "get")==$row3['CD_ID']) {
			echo "        <option value='?action=server&CD_Server=" . $row3['CD_ID'] . "' selected='selected'>" . $row3['CD_Name'] . "</option>
";
		} else {
			echo "        <option value='?action=server&CD_Server=" . $row3['CD_ID'] . "' >" . $row3['CD_Name'] . "</option>
";
		}
	}
}
?>
        </select> 
        </li> 
       </ul> 
      </div> 
     </div> 
    </form> 
    <form class="J_ajaxForm" method="get" name="formb"> 
     <table class="table2" id="dellist"> 
      <tbody> 
       <tr> 
        <td width="100" align="left"> 序号</td> 
        <td align="left">名称</td> 
        <td align="left">上传用户</td> 
        <td align="left">分类栏目</td> 
        <td align="left">数据来源</td> 
        <td align="left">推荐星级</td> 
        <td align="left">审核</td> 
        <td align="left">更新时间</td> 
        <td width="160" align="left">操作</td> 
       </tr> 
<?php
if($mnum==0)
	echo "<tr><td height='30' colspan='10' align='center' bgcolor='#FFFFFF' class='td_border'><br><br>没有数据<br><br><br></td></tr>";
if($result) {
	while($row=$db->fetch_array($result)) {
?>
       <tr> 
        <td><input type="checkbox" class="J_check" name="CD_ID[]" value="<?php echo $row['CD_ID']?>" id="CD_ID"><?php echo $row['CD_ID']?></td>
        <td><a href="../play/?1,<?php echo $row['CD_ID']?>.html" target="_blank" style="color: <?php echo $row['CD_Color']?>;" title="<?php echo $row['CD_Name']?>"><?php echo getlen("len","25",$row['CD_Name'])?></a></td>
        <td><a href="?action=keyword&key=<?php echo $row['CD_User']?>"><?php echo $row['CD_User']?></a></td>
        <td><?php
$res=$db->getrow("select CD_ID,CD_Name from " . tname('class') . " where CD_ID=" . $row['CD_ClassID'] . "");
if($res) {
	echo "<a href='?action=class&CD_ClassID=" . $res['CD_ID'] . "'>" . $res['CD_Name'] . "</a>";
} else {
	echo "暂无栏目";
}
?></td> 
        <td><?php echo $row['CD_From']?></td>
        <td id="CD_IsBest<?php echo $row['CD_ID']?>"><script language="javascript">ShowStar(<?php echo $row['CD_IsBest']?>,<?php echo $row['CD_ID']?>)</script></td>
        <td><?php if($row['CD_Passed']==1){?><a title="未审核" href="?action=editpassed&CD_ID=<?php echo $row['CD_ID']?>&CD_Passed=0"><img src='images/no.gif' border='0'></a><?php }else{?><a title="已审核" href="?action=editpassed&CD_ID=<?php echo $row['CD_ID']?>&CD_Passed=1"><img src='images/yes.gif' border='0'></a><?php }?></td>
        <td><?php if(date("Y-m-d",strtotime($row['CD_AddTime']))==date('Y-m-d')){ echo "<font color=red>".date("Y-m-d",strtotime($row['CD_AddTime']))."</font>"; }else{ echo date("Y-m-d",strtotime($row['CD_AddTime'])); } ?></td>
        <td>
		<a href="?action=edit&CD_ID=<?php echo $row['CD_ID']?>" class="btn">编辑</a> 
		<?php if($_GET['action']=="deleted"){ ?>
		<a href="?action=delc&CD_ID=<?php echo $row['CD_ID']?>" class="btn">恢复</a> 
		<a href="?action=delb&CD_ID=<?php echo $row['CD_ID']?>" onClick="return confirm('确定要删除吗？不可恢复！');" class="btn">删除</a>
		<?php }else{ ?>
		<a href="?action=dela&CD_ID=<?php echo $row['CD_ID']?>" onClick="return confirm('确定要移进回收站吗?');" class="btn">回收</a> 
		<a href="?action=delb&CD_ID=<?php echo $row['CD_ID']?>" onClick="return confirm('确定要删除吗？不可恢复！');" class="btn">删除</a>
		<?php } ?>
		</td>
       </tr> 
<?php
}
}
?>
       <tr> 
        <td height="35" colspan="10" align="left" bgcolor="#FAFBF7" class="td_border"><label class="mr20"><input type="checkbox" class="J_check_all" id="chkall" onclick="return CheckAll(this.form)" value="on" />全选</label>
		<input name="button2" type="button" class="btn btn_submit J_ajax_submit_btn" value="批量设置" onclick="EditInfo(this.form,'dance_pl.php')" />
		</td> 
       </tr> 
      </tbody> 
     </table> 
    </form> 
   </div> 
  </div> 
  <div class="p10" style="margin-bottom: 0px;"> 
   <div class="pages"><?php echo $Arr[0];?></div>
  </div> 
<?php
}
//移进回收站
function DelA() {
	global $db;
	$CD_ID=SafeRequest("CD_ID", "get");
	$sql="update " . tname('dj') . " set CD_Deleted=1 where CD_ID=" . $CD_ID . "";
	if($db->query($sql)) {
		showmessage("恭喜您，已将音乐移到回收站！", $_SERVER['HTTP_REFERER'], 0);
	}
}
//删除
function DelB(){
    global $db;
    $CD_ID=SafeRequest("CD_ID", "get");
    $sqls="select CD_Url,CD_Pic,CD_DownUrl,CD_User from " . tname('dj') . " where CD_ID=" . $CD_ID;
    if($row=$db->getrow($sqls)) {
        if(delunlink($row['CD_Pic']) == true) {
            @unlink(_x5music_root_ . $row['CD_Pic']);
        }
        if(delunlink($row['CD_Url']) == true) {
            @unlink(_x5music_root_ . $row['CD_Url']);
        }
        if(delunlink($row['CD_DownUrl']) == true) {
            @unlink(_x5music_root_ . $row['CD_DownUrl']);
        }
    }
	$db->query("delete from " . tname('dj') . " where CD_ID='" . $CD_ID . "'");
	$db->query('update ' . tname('system') . " set cd_djmub=cd_djmub-1"); //全局数据统计
	$db->query("update " . tname('user') . " set cd_points=cd_points-" . cd_userdeletemusicpoints . ",cd_rank=cd_rank-" . cd_userdeletemusicrank . ",cd_djmub=cd_djmub-1 where cd_id='" . $row3['cd_id'] . "'");
	showmessage("恭喜您，音乐删除成功！", $_SERVER['HTTP_REFERER'], 0);
}

//从回收站恢复
function DelC() {
	global $db;
	$CD_ID=SafeRequest("CD_ID", "get");
	$sql="update " . tname('dj') . " set CD_Deleted=0 where CD_ID=" . $CD_ID . "";
	if($db->query($sql)) {
		showmessage("恭喜您，恢复成功！", $_SERVER['HTTP_REFERER'], 0);
	}
}
//审核
function EditPassed()
{
    global $db;
    $CD_ID=SafeRequest("CD_ID", "get");
    $CD_Passed=SafeRequest("CD_Passed", "get");
    $sql="update " . tname('dj') . " set CD_Passed=" . $CD_Passed . " where CD_ID=" . $CD_ID . "";
    if($db->query($sql)) {echo "<script>window.location='" . $_SERVER['HTTP_REFERER'] . "'</script>";}
}
//编辑
function Edit() {
	global $db;
	$CD_ID=SafeRequest("CD_ID", "get");
	$sql="Select * from " . tname('dj') . " where CD_ID=" . $CD_ID . "";
	if($row=$db->Getrow($sql)) {
		$Arr=array(
			$row['CD_Name'],
			$row['CD_ClassID'],
			$row['CD_SpecialID'],
			$row['CD_Singer'],
			$row['CD_User'],
			$row['CD_Tag'],
			$row['CD_Pic'],
			$row['CD_Url'],
			$row['CD_DownUrl'],
			$row['CD_Word'],
			$row['CD_Lrc'],
			$row['CD_From'],
			$row['CD_Siz'],
			$row['CD_Uid'],
			$row['CD_Md5'],
			$row['CD_Hits'],
			$row['CD_DownHits'],
			$row['CD_FavHits'],
			$row['CD_uHits'],
			$row['CD_dHits'],
			$row['CD_DayHits'],
			$row['CD_WeekHits'],
			$row['CD_MonthHits'],
			$row['CD_LastHitTime'],
			$row['CD_AddTime'],
			$row['CD_Server'],
			$row['CD_Deleted'],
			$row['CD_IsBest'],
			$row['CD_Error'],
			$row['CD_Passed'],
			$row['CD_Points'],
			$row['CD_Grade'],
			$row['CD_Color'],
			$row['CD_Skin']
		);
	}
	EditBoard($Arr, "?action=saveedit&CD_ID=" . $CD_ID . "");
}
//添加数据
function Add() {
	$Arr=array("","","","","","","","","","","","","","","","","","","","","");
	EditBoard($Arr, "?action=saveadd");
}
//执行保存
function SaveAdd() {
	global $db;
	$CD_Name=SafeRequest("CD_Name", "post");
	$CD_Color=SafeRequest("CD_Color", "post");
	$CD_ClassID=SafeRequest("CD_ClassID", "post");
	$CD_Singer=SafeRequest("CD_Singer", "post");
	$CD_Pic=SafeRequest("CD_Pic", "post");
	$CD_Hits=SafeRequest("CD_Hits", "post");
	$CD_DayHits=SafeRequest("CD_DayHits", "post");
	$CD_WeekHits=SafeRequest("CD_WeekHits", "post");
	$CD_MonthHits=SafeRequest("CD_MonthHits", "post");
	$CD_uHits=SafeRequest("CD_uHits", "post");
	$CD_dHits=SafeRequest("CD_dHits", "post");
	$CD_DownHits=SafeRequest("CD_DownHits", "post");
	$CD_FavHits=SafeRequest("CD_FavHits", "post");
	$CD_Server=SafeRequest("CD_Server", "post");
	$CD_SpecialID=SafeRequest("CD_SpecialID", "post");
	$CD_IsBest=SafeRequest("CD_IsBest", "post");
	$CD_Url=SafeRequest("CD_Url", "post");
	$CD_DownUrl=SafeRequest("CD_DownUrl", "post");
	$CD_Grade=SafeRequest("CD_Grade", "post");
	$CD_Points=SafeRequest("CD_Points", "post");
	$CD_User=SafeRequest("CD_User", "post");
	$CD_Word=SafeRequest("CD_Word", "post");
	$CD_Lrc=SafeRequest("CD_Lrc", "post");
	$CD_Skin=SafeRequest("CD_Skin", "post");
	$CD_AddTime=date('Y-m-d H:i:s');
	$CD_From=SafeRequest("CD_From", "post");
	$CD_Siz=SafeRequest("CD_Siz", "post");
	$CD_Uid=SafeRequest("CD_Uid", "post");
	$CD_Md5=SafeRequest("CD_Md5", "post");
	$CD_Tag=SafeRequest("CD_Tag", "post");
	//判断 
	//if(!$CD_Hits){$CD_Hits=0;}
	if(!IsNum($CD_Hits)) {
		$CD_Hits=0;
	}
	if(!IsNum($CD_DayHits)) {
		$CD_DayHits=0;
	}
	if(!IsNum($CD_WeekHits)) {
		$CD_WeekHits=0;
	}
	if(!IsNum($CD_MonthHits)) {
		$CD_MonthHits=0;
	}
	if(!IsNum($CD_uHits)) {
		$CD_uHits=0;
	}
	if(!IsNum($CD_dHits)) {
		$CD_dHits=0;
	}
	if(!IsNum($CD_DownHits)) {
		$CD_DownHits=0;
	}
	if(!IsNum($CD_FavHits)) {
		$CD_FavHits=0;
	}
	$sql="Insert " . tname('dj') . " (CD_ClassID,CD_SpecialID,CD_Name,CD_Singer,CD_User,CD_Pic,CD_From,CD_Siz,CD_Uid,CD_Md5,CD_Tag,CD_Url,CD_DownUrl,CD_Word,CD_Lrc,CD_Hits,CD_DownHits,CD_FavHits,CD_uHits,CD_dHits,CD_DayHits,CD_WeekHits,CD_MonthHits,CD_AddTime,CD_Server,CD_Deleted,CD_IsBest,CD_Error,CD_Passed,CD_Points,CD_Grade,CD_Color,CD_Skin) values (" . $CD_ClassID . "," . $CD_SpecialID . ",'" . $CD_Name . "','" . $CD_Singer . "','" . $CD_User . "','" . $CD_Pic . "','" . $CD_From . "','" . $CD_Siz . "','" . $CD_Uid . "','" . $CD_Md5 . "','" . $CD_Tag . "','" . $CD_Url . "','" . $CD_DownUrl . "','" . $CD_Word . "','" . $CD_Lrc . "'," . $CD_Hits . "," . $CD_DownHits . "," . $CD_FavHits . "," . $CD_uHits . "," . $CD_dHits . "," . $CD_DayHits . "," . $CD_WeekHits . "," . $CD_MonthHits . ",'" . $CD_AddTime . "'," . $CD_Server . ",0," . $CD_IsBest . ",0,0," . $CD_Points . "," . $CD_Grade . ",'" . $CD_Color . "','" . $CD_Skin . "')";
	if($db->query($sql)) {
		$db->query('update ' . tname('system') . " set cd_djmub=cd_djmub+1"); //全局数据统计
		showmessage("恭喜您，音乐添加成功！", "dance.php", 0);
	} else {
		showmessage("出错了，音乐添加失败！", "dance.php", 0);
	}
}
//保存编辑
function SaveEdit() {
	global $db;
	$CD_ID=SafeRequest("CD_ID", "get");
	$CD_Name=SafeRequest("CD_Name", "post");
	$CD_Color=SafeRequest("CD_Color", "post");
	$CD_ClassID=SafeRequest("CD_ClassID", "post");
	$CD_Singer=SafeRequest("CD_Singer", "post");
	$CD_Pic=SafeRequest("CD_Pic", "post");
	$CD_Hits=SafeRequest("CD_Hits", "post");
	$CD_DayHits=SafeRequest("CD_DayHits", "post");
	$CD_WeekHits=SafeRequest("CD_WeekHits", "post");
	$CD_MonthHits=SafeRequest("CD_MonthHits", "post");
	$CD_uHits=SafeRequest("CD_uHits", "post");
	$CD_dHits=SafeRequest("CD_dHits", "post");
	$CD_DownHits=SafeRequest("CD_DownHits", "post");
	$CD_FavHits=SafeRequest("CD_FavHits", "post");
	$CD_Server=SafeRequest("CD_Server", "post");
	$CD_SpecialID=SafeRequest("CD_SpecialID", "post");
	$CD_IsBest=SafeRequest("CD_IsBest", "post");
	$CD_Url=SafeRequest("CD_Url", "post");
	$CD_DownUrl=SafeRequest("CD_DownUrl", "post");
	$CD_Grade=SafeRequest("CD_Grade", "post");
	$CD_Points=SafeRequest("CD_Points", "post");
	$CD_User=SafeRequest("CD_User", "post");
	$CD_Word=SafeRequest("CD_Word", "post");
	$CD_Lrc=SafeRequest("CD_Lrc", "post");
	$CD_Skin=SafeRequest("CD_Skin", "post");
	$edittime=SafeRequest("edittime", "post");
	$CD_Time=SafeRequest("CD_Time", "post");
	$CD_HttpUrl=SafeRequest("CD_HttpUrl", "post");
	$CD_From=SafeRequest("CD_From", "post");
	$CD_Siz=SafeRequest("CD_Siz", "post");
	$CD_Uid=SafeRequest("CD_Uid", "post");
	$CD_Md5=SafeRequest("CD_Md5", "post");
	$CD_Tag=SafeRequest("CD_Tag", "post");
	$delurl=SafeRequest("delurl", "post");
	$delpic=SafeRequest("delpic", "post");
	if($edittime==1) {
		$CD_AddTime=date('Y-m-d H:i:s');
	} else {
		$CD_AddTime=$CD_Time;
	}
	//判断 
	//if(!$CD_Hits){$CD_Hits=0;}
	if(!IsNum($CD_Hits)) {
		$CD_Hits=0;
	}
	if(!IsNum($CD_DayHits)) {
		$CD_DayHits=0;
	}
	if(!IsNum($CD_WeekHits)) {
		$CD_WeekHits=0;
	}
	if(!IsNum($CD_MonthHits)) {
		$CD_MonthHits=0;
	}
	if(!IsNum($CD_uHits)) {
		$CD_uHits=0;
	}
	if(!IsNum($CD_dHits)) {
		$CD_dHits=0;
	}
	if(!IsNum($CD_DownHits)) {
		$CD_DownHits=0;
	}
	if(!IsNum($CD_FavHits)) {
		$CD_FavHits=0;
	}
	if($delpic==1) {
		if(delunlink($CD_Pic)==true) {@unlink(_x5music_root_ . $CD_Pic);}
		$CD_Pic="";
	}
	if($delurl==1) {
		if(delunlink($CD_Url)==true) {@unlink(_x5music_root_ . $CD_Url);}
		$CD_Url="";
		$CD_DownUrl="";
	}
	$sql="update " . tname('dj') . " set CD_Name='" . $CD_Name . "',CD_Color='" . $CD_Color . "',CD_ClassID=" . $CD_ClassID . ",CD_Singer='" . $CD_Singer . "',CD_Tag='" . $CD_Tag . "',CD_Pic='" . $CD_Pic . "',CD_From='" . $CD_From . "',CD_Siz='" . $CD_Siz . "',CD_Uid='" . $CD_Uid . "',CD_Md5='" . $CD_Md5 . "',CD_Hits=" . $CD_Hits . ",CD_DayHits=" . $CD_DayHits . ",CD_WeekHits=" . $CD_WeekHits . ",CD_MonthHits=" . $CD_MonthHits . ",CD_uHits=" . $CD_uHits . ",CD_dHits=" . $CD_dHits . ",CD_DownHits=" . $CD_DownHits . ",CD_FavHits=" . $CD_FavHits . ",CD_Server=" . $CD_Server . ",CD_SpecialID=" . $CD_SpecialID . ",CD_IsBest=" . $CD_IsBest . ",CD_Url='" . $CD_Url . "',CD_DownUrl='" . $CD_DownUrl . "',CD_Grade=" . $CD_Grade . ",CD_Points=" . $CD_Points . ",CD_User='" . $CD_User . "',CD_Word='" . $CD_Word . "',CD_Lrc='" . $CD_Lrc . "',CD_AddTime='" . $CD_AddTime . "',CD_Skin='" . $CD_Skin . "',CD_Error=0 where CD_ID=" . $CD_ID . "";
	if($db->query($sql)) {
		$db->query("update ".tname('fav')." set cd_musicname='$CD_Name' where cd_musicid=$CD_ID");
		showmessage("恭喜您，音乐编辑成功！", $_SERVER['HTTP_REFERER'], 0);
	} else {
		showmessage("出错了，音乐编辑失败！", $_SERVER['HTTP_REFERER'], 0);
	}
}
?>