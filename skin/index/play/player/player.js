function getplayer(_id) {
	var theHttpRequest = getHttpObject();
	theHttpRequest.onreadystatechange = function () {
		processAJAX();
	};
	theHttpRequest.open("GET", web_skin + "play/player/ajax.php?id=" + _id, true);
	theHttpRequest.send(null);
	function processAJAX() {
		if (theHttpRequest.readyState == 4) {
			if (theHttpRequest.status == 200) {
				document.getElementById("jp-playlist-box").innerHTML = unescape(theHttpRequest.responseText);
			} else {
				document.getElementById("jp-playlist-box").innerHTML = "您请求的页面出现异常错误";
			}
		}
	}
}
function GetCookie(name) {
	var arr = document.cookie.match(new RegExp("(^| )" + name + "=([^;]*)(;|$)"));
	if (arr != null) {
		return unescape(arr[2]);
	}
	return null;
}
function clk(type, id) {
	var newx = "",
	firstid = "";
	var ok = false;
	var lens = eval("document.form.Url").length;
	if (lens == undefined) {
		lens = 1
	}
	if (lens > 1) {
		for (var i = 0; i < lens; i++) {
			
			var temp = eval("document.form.Url[" + i + "]");
			if (temp.checked) {
				if (temp.value.replace("@", "") != "" && temp.value.replace("@", "") != "0") {
					if (firstid == "")
						firstid = temp.value.replace("@", "");
					newx = newx + temp.value.replace("@", "") + ",";
					ok = true;
				}
			}
		}
	} else {
		ok = true;
		if (document.form.Url.value.replace("@", "") != "" && document.form.Url.value.replace("@", "") != "0") {
			newx = document.form.Url.value.replace("@", "") + ",";
		}
	}
	if (ok) {
		Addplay(newx);
	} else {
		asyncbox.tips("请先选择要加入列表的歌曲！", "wait", 1000);
	}
}
function Addplay(url) {
		if (url.split(",").length - 1 > 1) {
			sendplayer(url);
		} else {
			sendplayer(url);
		}
}
var old = "";
function quanxuan(obj) {
	if (old != obj.toString()) {
		document.form.reset();
		old = obj.toString();
	}
	with (document.getElementById(obj)) {
		var ins = getElementsByTagName("input");
		for (var i = 0; i < ins.length; i++) {
			ins[i].checked = !ins[i].checked;
		}
	}
}
function playallquanxuan(obj) {
	document.form.reset();
	with (document.getElementById(obj)) {
		var ins = getElementsByTagName("input");
		for (var i = 0; i < ins.length; i++) {
			ins[i].checked = true;
		}
	}
}
function lbplay() {
	var newx = "",
	firstid = "";
	var ok = false;
	var lens = eval("document.form.Url").length;
	if (lens == undefined) {
		lens = 1
	}
	if (lens > 1) {
		for (var i = 0; i < lens; i++) {
			
			var temp = eval("document.form.Url[" + i + "]");
			if (temp.checked) {
				if (temp.value.replace("@", "") != "" && temp.value.replace("@", "") != "0") {
					if (firstid == "")
						firstid = temp.value.replace("@", "");
					newx = newx + temp.value.replace("@", "") + ",";
					ok = true;
				}
			}
		}
	} else {
		ok = true;
		if (document.form.Url.value.replace("@", "") != "" && document.form.Url.value.replace("@", "") != "0") {
			newx = document.form.Url.value.replace("@", "") + ",";
		}
	}
	if (ok) {
		if (newx.length > 2009) {
			asyncbox.tips("您选择的太多，请保持在60首以内，以便达到最佳效果！", "wait", 1000);
		} else {
			var newx = newx.replace(/{song}/g, "");
			if (newx.split(",").length - 1 > 1)
				window.open(web_url + "include/lplayer.php?id=" + newx + "");
			else
				window.open(web_url + "include/lplayer.php?id=" + newx + "");
		}
	} else {
		asyncbox.tips("请选择后再播放！", "wait", 1000);
	}
}
//cmp.js
(function(a){typeof a.CMP=="undefined"&&(a.CMP=function(){var b=/msie/.test(navigator.userAgent.toLowerCase()),c=function(a,b){if(b&&typeof b=="object")for(var c in b)a[c]=b[c];return a},d=function(a,d,e,f,g,h,i){i=c({width:d,height:e,id:a},i),h=c({allowfullscreen:"true",allowscriptaccess:"always"},h);var j,k,l=[];if(g){j=g;if(typeof g=="object"){for(var m in g)l.push(m+"="+encodeURIComponent(g[m]));j=l.join("&")}h.flashvars=j}k="<object ",k+=b?'classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" ':'type="application/x-shockwave-flash" data="'+f+'" ';for(var m in i)k+=m+'="'+i[m]+'" ';k+=b?'><param name="movie" value="'+f+'" />':">";for(m in h)k+='<param name="'+m+'" value="'+h[m]+'" />';return k+="</object>",k},e=function(c){var d=document.getElementById(c);if(!d||d.nodeName.toLowerCase()!="object")d=b?a[c]:document[c];return d},f=function(a){if(a){for(var b in a)typeof a[b]=="function"&&(a[b]=null);a.parentNode.removeChild(a)}},g=function(a){if(a){var c=typeof a=="string"?e(a):a;if(c&&c.nodeName.toLowerCase()=="object")return b?(c.style.display="none",function(){c.readyState==4?f(c):setTimeout(arguments.callee,15)}()):c.parentNode.removeChild(c),!0}return!1};return{create:function(){return d.apply(this,arguments)},write:function(){var a=d.apply(this,arguments);return document.write(a),a},get:function(a){return e(a)},remove:function(a){return g(a)}}}());var b=function(b){b=b||a.event;var c=b.target||b.srcElement;if(c&&typeof c.cmp_version=="function"){var d=c.skin("list.tree","maxVerticalScrollPosition");if(d>0)return c.focus(),b.preventDefault&&b.preventDefault(),!1}};a.addEventListener&&a.addEventListener("DOMMouseScroll",b,!1),a.onmousewheel=document.onmousewheel=b})(window);
//播放器
var cmpo;
function cmp_loaded(key) {
	cmpo = CMP.get("cmp");
	if (cmpo) {
		cmpo.addEventListener("model_load", "cmp_model_load");
	}
}
function sendplayer(_id) {//列表
	var htm = CMP.create("cmp", "500", "100%", web_url + "plug/cmp/cmp.swf?id=play&play_mode=3&skin=" + web_skin + "play/player.swf", {
			api : "cmp_loaded",
			lists : _id
		}, {
			wmode : "transparent"
		});
	document.getElementById("x5musicplayer").innerHTML = htm;
	var playerid = _id.replace(/{song}/g, "");
	if (playerid.match(/\,/g)) {
		playerid = playerid.substr(0, playerid.length - 1);
	}
	asyncbox.tips("恭喜，已成功添加" + playerid.split(',').length + "首到播放列表！", "success", 1500);
	var title = $("#btnfold").attr("title");
	if (title == "点击展开") {
		$("#divplayer").addClass("m_player_playing");
	}
	document.getElementById("spanaddtips").style.display = "";
	for (i = 1; i <= 30; i++) {
		setTimeout("document.getElementById('spanaddtips').style.top = '-" + i + "px';", i * 30);
		if (i > 29) {
			setTimeout("document.getElementById('spanaddtips').style.display = 'none';", 1500);
		}
	}
	document.getElementById("divnulllist").style.display = "none";
	document.getElementById("jp-playlist-box").innerHTML = getplayer(playerid);
	document.getElementById("spansongnums").innerHTML = playerid.split(",").length;
}
function songplayer(_id) {//单曲
	var htm = CMP.create("cmp", "500", "100%", web_url + "plug/cmp/cmp.swf?id=play&play_mode=3&skin=" + web_skin + "play/player.swf", {
			api : "cmp_loaded",
			lists : _id
		}, {
			wmode : "transparent"
		});
	document.getElementById("x5musicplayer").innerHTML = htm;
}


//伸缩事件
$(function () {
	$("#btnfold").click(function () {
		var tit = $(this).attr('title');
		if (tit == '点击收起') {
			$("#divplayer").animate({
				left : -540
			}, {
				duration : 500
			});
			$(this).attr('title', '点击展开');
			if ($('#spansongnum1 span').html() > 0) {
				$('#divplayer').addClass("mini_version");
				$('#divplayer').addClass("m_player_folded");
				$('#divplayer').addClass("m_player_playing")
			} else {
				$('#divplayer').addClass("m_player_folded")
			}
			$('#divplayframe').hide();
			$('#divplayer').removeClass("mini_version")
		} else {
			$("#divplayer").animate({
				left : 0
			}, {
				duration : 500
			});
			$('#divplayer').removeClass("m_player_folded");
			$('#divplayer').removeClass("m_player_playing");
			$(this).attr('title', '点击收起')
		}
	});
	$("#spansongnum1").click(function () {
		var shows = $('#divplayframe').css('display');
		if (shows == 'none') {
			$('#divplayframe').show();
			$('#divplayer').addClass("mini_version")
		} else {
			$('#divplayframe').hide();
			$('#divplayer').removeClass("mini_version")
		}
	});
	$("#btnclose").click(function () {
		$('#divplayframe').hide();
		$('#divplayer').removeClass("mini_version")
	});
	$("#btnlrc").click(function () {
		var shows = $('#player_lyrics_pannel').css('display');
		if (shows == 'none') {
			$('#player_lyrics_pannel').show()
		} else {
			$('#player_lyrics_pannel').hide()
		}
	});
	$("#closelrcpannel").click(function () {
		$('#player_lyrics_pannel').hide()
	});
	$("#jp-playlist-box li").live({
		"mouseover" : function () {
			$(this).addClass("play_hover")
		},
		"mouseout" : function () {
			$(this).removeClass("play_hover")
		}
	})
});
