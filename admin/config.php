<?php
error_reporting(0);
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
include "../include/x5music.conn.php";
include "function_common.php";
$action=SafeRequest("action","get");
$ac=SafeRequest("ac","get");
admincheck(2);
?>
<?php
if($action=="config") { //站点设置
	$cd_webname=SafeRequest("cd_webname", "post");
	$cd_weburl=SafeRequest("cd_weburl", "post");
	$cd_keywords=SafeRequest("cd_keywords", "post");
	$cd_description=SafeRequest("cd_description", "post");
	$cd_webicp=SafeRequest("cd_webicp", "post");
	$cd_webip=SafeRequest("cd_webip", "post");
	$webscan_white_directory=SafeRequest("webscan_white_directory", "post");
	$cd_webcodea=SafeRequest("cd_webcodea", "post");
	$cd_webcodeb=SafeRequest("cd_webcodeb", "post");
	$cd_weboffa=SafeRequest("cd_weboffa", "post");
	$cd_weboffb=SafeRequest("cd_weboffb", "post");
	$cd_cnzz=SafeRequest("cd_cnzz", "post");
	//保存数据
	$str=file_get_contents("../include/x5music.config.php");
	$str=preg_replace('/"cd_webname","(.*?)"/', '"cd_webname","' . $cd_webname . '"', $str);
	$str=preg_replace('/"cd_weburl","(.*?)"/', '"cd_weburl","' . $cd_weburl . '"', $str);
	$str=preg_replace('/"cd_keywords","(.*?)"/', '"cd_keywords","' . $cd_keywords . '"', $str);
	$str=preg_replace('/"cd_description","(.*?)"/', '"cd_description","' . $cd_description . '"', $str);
	$str=preg_replace('/"cd_webicp","(.*?)"/', '"cd_webicp","' . $cd_webicp . '"', $str);
	$str=preg_replace('/"cd_webip","(.*?)"/', '"cd_webip","' . $cd_webip . '"', $str);
	$str=preg_replace('/"webscan_white_directory","(.*?)"/', '"webscan_white_directory","' . $webscan_white_directory . '"', $str);
	$str=preg_replace('/"cd_webcodea","(.*?)"/', '"cd_webcodea","' . $cd_webcodea . '"', $str);
	$str=preg_replace('/"cd_webcodeb","(.*?)"/', '"cd_webcodeb","' . $cd_webcodeb . '"', $str);
	$str=preg_replace('/"cd_weboffa","(.*?)"/', '"cd_weboffa","' . $cd_weboffa . '"', $str);
	$str=preg_replace('/"cd_weboffb","(.*?)"/', '"cd_weboffb","' . $cd_weboffb . '"', $str);
	if(!$fp=fopen('../include/x5music.config.php', 'w')) {
		showmessage("出错了，文件 ../include/x5music.config.php 没有写入权限！", $_SERVER['HTTP_REFERER'], 0);
	}
	$ifile=new iFile('../include/x5music.config.php', 'w');
	$ifile->WriteFile($str, 3);
	global $db;
	$sql="update " . tname('system') . " set cd_cnzz='" . $cd_cnzz . "'";
	if($db->query($sql)) {
		showmessage("恭喜您，保存设置成功！", $_SERVER['HTTP_REFERER'], 0);
	} else {
		showmessage("恭喜您，保存设置成功！但统计代码保存失败。", $_SERVER['HTTP_REFERER'], 0);
	}
} elseif($action=="admin") { //站长信息
	$cd_webqq=SafeRequest("cd_webqq", "post");
	$cd_webtel=SafeRequest("cd_webtel", "post");
	$cd_webemail=SafeRequest("cd_webemail", "post");
	//保存数据
	$str=file_get_contents("../include/x5music.config.php");
	$str=preg_replace('/"cd_webqq","(.*?)"/', '"cd_webqq","' . $cd_webqq . '"', $str);
	$str=preg_replace('/"cd_webtel","(.*?)"/', '"cd_webtel","' . $cd_webtel . '"', $str);
	$str=preg_replace('/"cd_webemail","(.*?)"/', '"cd_webemail","' . $cd_webemail . '"', $str);
	if(!$fp=fopen('../include/x5music.config.php', 'w')) {
		showmessage("出错了，文件 ../include/x5music.config.php 没有写入权限！", $_SERVER['HTTP_REFERER'], 0);
	}
	$ifile=new iFile('../include/x5music.config.php', 'w');
	$ifile->WriteFile($str, 3);
	showmessage("恭喜您，保存设置成功！", $_SERVER['HTTP_REFERER'], 0);
} elseif($action=="seo") { //SEO
	$cd_webhtml=SafeRequest("cd_webhtml", "post");
	$cd_webdancehtml=SafeRequest("cd_webdancehtml", "post");
	$cd_webvideohtml=SafeRequest("cd_webvideohtml", "post");
	$cd_webnewshtml=SafeRequest("cd_webnewshtml", "post");
	$cd_webmalbumhtml=SafeRequest("cd_webmalbumhtml", "post");
	$cd_webvalbumhtml=SafeRequest("cd_webvalbumhtml", "post");
	$cd_websingerhtml=SafeRequest("cd_websingerhtml", "post");
	$cd_userhtml=SafeRequest("cd_userhtml", "post");
	$cd_stoptime=SafeRequest("cd_stoptime", "post");
	$cd_createpre=SafeRequest("cd_createpre", "post");
	$cd_schtml=SafeRequest("cd_schtml", "post");
	$cd_webmlplay=SafeRequest("cd_webmlplay", "post");
	$cd_webmlmalbum=SafeRequest("cd_webmlmalbum", "post");
	$cd_webmlvideo=SafeRequest("cd_webmlvideo", "post");
	$cd_webmlvalbum=SafeRequest("cd_webmlvalbum", "post");
	$cd_webmlartist=SafeRequest("cd_webmlartist", "post");
	$cd_webmlnews=SafeRequest("cd_webmlnews", "post");
	$cd_webmlsinger=SafeRequest("cd_webmlsinger", "post");
	$cd_caplayfolder=SafeRequest("cd_caplayfolder", "post");
	$cd_caplaylistfolder=SafeRequest("cd_caplaylistfolder", "post");
	$cd_caplaytopfolder=SafeRequest("cd_caplaytopfolder", "post");
	$cd_cavideofolder=SafeRequest("cd_cavideofolder", "post");
	$cd_cavideolistfolder=SafeRequest("cd_cavideolistfolder", "post");
	$cd_cavideotopfolder=SafeRequest("cd_cavideotopfolder", "post");
	$cd_camalbumfolder=SafeRequest("cd_camalbumfolder", "post");
	$cd_camalbumlistfolder=SafeRequest("cd_camalbumlistfolder", "post");
	$cd_cavalbumfolder=SafeRequest("cd_cavalbumfolder", "post");
	$cd_cavalbumlistfolder=SafeRequest("cd_cavalbumlistfolder", "post");
	$cd_canewsfolder=SafeRequest("cd_canewsfolder", "post");
	$cd_canewslistfolder=SafeRequest("cd_canewslistfolder", "post");
	$cd_caartistfolder=SafeRequest("cd_caartistfolder", "post");
	$cd_caartistlistfolder=SafeRequest("cd_caartistlistfolder", "post");
	$cd_caartistabcfolder=SafeRequest("cd_caartistabcfolder", "post");
	//保存数据
	$str=file_get_contents("../include/x5music.config.php");
	$str=preg_replace('/"cd_webhtml","(.*?)"/', '"cd_webhtml","' . $cd_webhtml . '"', $str);
	$str=preg_replace('/"cd_webdancehtml","(.*?)"/', '"cd_webdancehtml","' . $cd_webdancehtml . '"', $str);
	$str=preg_replace('/"cd_webvideohtml","(.*?)"/', '"cd_webvideohtml","' . $cd_webvideohtml . '"', $str);
	$str=preg_replace('/"cd_webnewshtml","(.*?)"/', '"cd_webnewshtml","' . $cd_webnewshtml . '"', $str);
	$str=preg_replace('/"cd_webmalbumhtml","(.*?)"/', '"cd_webmalbumhtml","' . $cd_webmalbumhtml . '"', $str);
	$str=preg_replace('/"cd_webvalbumhtml","(.*?)"/', '"cd_webvalbumhtml","' . $cd_webvalbumhtml . '"', $str);
	$str=preg_replace('/"cd_websingerhtml","(.*?)"/', '"cd_websingerhtml","' . $cd_websingerhtml . '"', $str);
	$str=preg_replace('/"cd_userhtml","(.*?)"/', '"cd_userhtml","' . $cd_userhtml . '"', $str);
	$str=preg_replace('/"cd_stoptime","(.*?)"/', '"cd_stoptime","' . $cd_stoptime . '"', $str);
	$str=preg_replace('/"cd_createpre","(.*?)"/', '"cd_createpre","' . $cd_createpre . '"', $str);
	$str=preg_replace('/"cd_schtml","(.*?)"/', '"cd_schtml","' . $cd_schtml . '"', $str);
	$str=preg_replace('/"cd_webmlplay","(.*?)"/', '"cd_webmlplay","' . $cd_webmlplay . '"', $str);
	$str=preg_replace('/"cd_webmlmalbum","(.*?)"/', '"cd_webmlmalbum","' . $cd_webmlmalbum . '"', $str);
	$str=preg_replace('/"cd_webmlvideo","(.*?)"/', '"cd_webmlvideo","' . $cd_webmlvideo . '"', $str);
	$str=preg_replace('/"cd_webmlvalbum","(.*?)"/', '"cd_webmlvalbum","' . $cd_webmlvalbum . '"', $str);
	$str=preg_replace('/"cd_webmlartist","(.*?)"/', '"cd_webmlartist","' . $cd_webmlartist . '"', $str);
	$str=preg_replace('/"cd_webmlnews","(.*?)"/', '"cd_webmlnews","' . $cd_webmlnews . '"', $str);
	$str=preg_replace('/"cd_webmlsinger","(.*?)"/', '"cd_webmlsinger","' . $cd_webmlsinger . '"', $str);
	$str=preg_replace('/"cd_caplayfolder","(.*?)"/', '"cd_caplayfolder","' . $cd_caplayfolder . '"', $str);
	$str=preg_replace('/"cd_caplaylistfolder","(.*?)"/', '"cd_caplaylistfolder","' . $cd_caplaylistfolder . '"', $str);
	$str=preg_replace('/"cd_caplaytopfolder","(.*?)"/', '"cd_caplaytopfolder","' . $cd_caplaytopfolder . '"', $str);
	$str=preg_replace('/"cd_cavideofolder","(.*?)"/', '"cd_cavideofolder","' . $cd_cavideofolder . '"', $str);
	$str=preg_replace('/"cd_cavideolistfolder","(.*?)"/', '"cd_cavideolistfolder","' . $cd_cavideolistfolder . '"', $str);
	$str=preg_replace('/"cd_cavideotopfolder","(.*?)"/', '"cd_cavideotopfolder","' . $cd_cavideotopfolder . '"', $str);
	$str=preg_replace('/"cd_camalbumfolder","(.*?)"/', '"cd_camalbumfolder","' . $cd_camalbumfolder . '"', $str);
	$str=preg_replace('/"cd_camalbumlistfolder","(.*?)"/', '"cd_camalbumlistfolder","' . $cd_camalbumlistfolder . '"', $str);
	$str=preg_replace('/"cd_cavalbumfolder","(.*?)"/', '"cd_cavalbumfolder","' . $cd_cavalbumfolder . '"', $str);
	$str=preg_replace('/"cd_cavalbumlistfolder","(.*?)"/', '"cd_cavalbumlistfolder","' . $cd_cavalbumlistfolder . '"', $str);
	$str=preg_replace('/"cd_canewsfolder","(.*?)"/', '"cd_canewsfolder","' . $cd_canewsfolder . '"', $str);
	$str=preg_replace('/"cd_canewslistfolder","(.*?)"/', '"cd_canewslistfolder","' . $cd_canewslistfolder . '"', $str);
	$str=preg_replace('/"cd_caartistfolder","(.*?)"/', '"cd_caartistfolder","' . $cd_caartistfolder . '"', $str);
	$str=preg_replace('/"cd_caartistlistfolder","(.*?)"/', '"cd_caartistlistfolder","' . $cd_caartistlistfolder . '"', $str);
	$str=preg_replace('/"cd_caartistabcfolder","(.*?)"/', '"cd_caartistabcfolder","' . $cd_caartistabcfolder . '"', $str);
	if(!$fp=fopen('../include/x5music.config.php', 'w')) {
		showmessage("出错了，文件 ../include/x5music.config.php 没有写入权限！", $_SERVER['HTTP_REFERER'], 0);
	}
	$ifile=new iFile('../include/x5music.config.php', 'w');
	$ifile->WriteFile($str, 3);
	showmessage("恭喜您，保存设置成功！", $_SERVER['HTTP_REFERER'], 0);
} elseif($action=="cachethread") { //性能优化
	$cd_iscache=SafeRequest("cd_iscache", "post");
	$cd_cachetime=SafeRequest("cd_cachetime", "post");
	$cd_cachemark=SafeRequest("cd_cachemark", "post");
	$cd_djdayhitstime=SafeRequest("cd_djdayhitstime", "post");
	$cd_djweekhitstime=SafeRequest("cd_djweekhitstime", "post");
	$cd_djmonthhitstime=SafeRequest("cd_djmonthhitstime", "post");
	//保存数据
	$str=file_get_contents("../include/x5music.config.php");
	$str=preg_replace('/"cd_iscache","(.*?)"/', '"cd_iscache","' . $cd_iscache . '"', $str);
	$str=preg_replace('/"cd_cachetime","(.*?)"/', '"cd_cachetime","' . $cd_cachetime . '"', $str);
	$str=preg_replace('/"cd_cachemark","(.*?)"/', '"cd_cachemark","' . $cd_cachemark . '"', $str);
	$str=preg_replace('/"cd_djdayhitstime","(.*?)"/', '"cd_djdayhitstime","' . $cd_djdayhitstime . '"', $str);
	$str=preg_replace('/"cd_djweekhitstime","(.*?)"/', '"cd_djweekhitstime","' . $cd_djweekhitstime . '"', $str);
	$str=preg_replace('/"cd_djmonthhitstime","(.*?)"/', '"cd_djmonthhitstime","' . $cd_djmonthhitstime . '"', $str);
	if(!$fp=fopen('../include/x5music.config.php', 'w')) {
		showmessage("出错了，文件 ../include/x5music.config.php 没有写入权限！", $_SERVER['HTTP_REFERER'], 0);
	}
	$ifile=new iFile('../include/x5music.config.php', 'w');
	$ifile->WriteFile($str, 3);
	showmessage("恭喜您，保存设置成功！", $_SERVER['HTTP_REFERER'], 0);
} elseif($action=="user") { //会员设置
	$cd_userreg=SafeRequest("cd_userreg", "post");
	$cd_userlogin=SafeRequest("cd_userlogin", "post");
	$cd_userlocalpm=SafeRequest("cd_userlocalpm", "post");
	$cd_userlocalpmtxt=SafeRequest("cd_userlocalpmtxt", "post");
	$cd_userpoints=SafeRequest("cd_userpoints", "post");
	$cd_onlinehold=SafeRequest("cd_onlinehold", "post");
	$cd_userfeedday=SafeRequest("cd_userfeedday", "post");
	$cd_userlogintime=SafeRequest("cd_userlogintime", "post");
	$cd_userloginpoints=SafeRequest("cd_userloginpoints", "post");
	$cd_userupmusicpoints=SafeRequest("cd_userupmusicpoints", "post");
	$cd_useruppicpoints=SafeRequest("cd_useruppicpoints", "post");
	$cd_useruppicceiling=SafeRequest("cd_useruppicceiling", "post");
	$cd_userwebpoints=SafeRequest("cd_userwebpoints", "post");
	$cd_userwebceiling=SafeRequest("cd_userwebceiling", "post");
	$cd_userdeletemusicpoints=SafeRequest("cd_userdeletemusicpoints", "post");
	$cd_userdeletepicpoints=SafeRequest("cd_userdeletepicpoints", "post");
	$cd_vipoff=SafeRequest("cd_vipoff", "post");
	$cd_viptime=SafeRequest("cd_viptime", "post");
	$cd_vippoints=SafeRequest("cd_vippoints", "post");
	$cd_vipktxt=SafeRequest("cd_vipktxt", "post");
	$cd_viptxt=SafeRequest("cd_viptxt", "post");
	$cd_message=SafeRequest("cd_message", "post");
	//保存数据
	$str=file_get_contents("../include/x5music.config.php");
	$str=preg_replace('/"cd_userreg","(.*?)"/', '"cd_userreg","' . $cd_userreg . '"', $str);
	$str=preg_replace('/"cd_userlogin","(.*?)"/', '"cd_userlogin","' . $cd_userlogin . '"', $str);
	$str=preg_replace('/"cd_userlocalpm","(.*?)"/', '"cd_userlocalpm","' . $cd_userlocalpm . '"', $str);
	$str=preg_replace('/"cd_userlocalpmtxt","(.*?)"/', '"cd_userlocalpmtxt","' . $cd_userlocalpmtxt . '"', $str);
	$str=preg_replace('/"cd_userpoints","(.*?)"/', '"cd_userpoints","' . $cd_userpoints . '"', $str);
	$str=preg_replace('/"cd_onlinehold","(.*?)"/', '"cd_onlinehold","' . $cd_onlinehold . '"', $str);
	$str=preg_replace('/"cd_userfeedday","(.*?)"/', '"cd_userfeedday","' . $cd_userfeedday . '"', $str);
	$str=preg_replace('/"cd_userlogintime","(.*?)"/', '"cd_userlogintime","' . $cd_userlogintime . '"', $str);
	$str=preg_replace('/"cd_userloginpoints","(.*?)"/', '"cd_userloginpoints","' . $cd_userloginpoints . '"', $str);
	$str=preg_replace('/"cd_userupmusicpoints","(.*?)"/', '"cd_userupmusicpoints","' . $cd_userupmusicpoints . '"', $str);
	$str=preg_replace('/"cd_useruppicpoints","(.*?)"/', '"cd_useruppicpoints","' . $cd_useruppicpoints . '"', $str);
	$str=preg_replace('/"cd_useruppicceiling","(.*?)"/', '"cd_useruppicceiling","' . $cd_useruppicceiling . '"', $str);
	$str=preg_replace('/"cd_userwebpoints","(.*?)"/', '"cd_userwebpoints","' . $cd_userwebpoints . '"', $str);
	$str=preg_replace('/"cd_userwebceiling","(.*?)"/', '"cd_userwebceiling","' . $cd_userwebceiling . '"', $str);
	$str=preg_replace('/"cd_userdeletemusicpoints","(.*?)"/', '"cd_userdeletemusicpoints","' . $cd_userdeletemusicpoints . '"', $str);
	$str=preg_replace('/"cd_userdeletepicpoints","(.*?)"/', '"cd_userdeletepicpoints","' . $cd_userdeletepicpoints . '"', $str);
	$str=preg_replace('/"cd_vipoff","(.*?)"/', '"cd_vipoff","' . $cd_vipoff . '"', $str);
	$str=preg_replace('/"cd_viptime","(.*?)"/', '"cd_viptime","' . $cd_viptime . '"', $str);
	$str=preg_replace('/"cd_vippoints","(.*?)"/', '"cd_vippoints","' . $cd_vippoints . '"', $str);
	$str=preg_replace('/"cd_vipktxt","(.*?)"/', '"cd_vipktxt","' . $cd_vipktxt . '"', $str);
	$str=preg_replace('/"cd_viptxt","(.*?)"/', '"cd_viptxt","' . $cd_viptxt . '"', $str);
	if(!$fp=fopen('../include/x5music.config.php', 'w')) {
		showmessage("出错了，文件 ../include/x5music.config.php 没有写入权限！", $_SERVER['HTTP_REFERER'], 0);
	}
	$ifile=new iFile('../include/x5music.config.php', 'w');
	$ifile->WriteFile($str, 3);
	global $db;
	$sql="update " . tname('system') . " set cd_message='" . $cd_message . "'";
	if($db->query($sql)) {
		showmessage("恭喜您，保存设置成功！", $_SERVER['HTTP_REFERER'], 0);
	} else {
		showmessage("恭喜您，保存设置成功！但第三方评论代码保存失败。", $_SERVER['HTTP_REFERER'], 0);
	}
} elseif($action=="upload") { //上传设置
	$cd_uploadentrance=SafeRequest("cd_uploadentrance", "post");
	$cd_ykuploadmusic=SafeRequest("cd_ykuploadmusic", "post");
	$cd_upmusicpassed=SafeRequest("cd_upmusicpassed", "post");
	$cd_attachmentpoints=SafeRequest("cd_attachmentpoints", "post");
	$cd_userupload=SafeRequest("cd_userupload", "post");
	//保存数据
	$str=file_get_contents("../include/x5music.config.php");
	$str=preg_replace('/"cd_uploadentrance","(.*?)"/', '"cd_uploadentrance","' . $cd_uploadentrance . '"', $str);
	$str=preg_replace('/"cd_ykuploadmusic","(.*?)"/', '"cd_ykuploadmusic","' . $cd_ykuploadmusic . '"', $str);
	$str=preg_replace('/"cd_upmusicpassed","(.*?)"/', '"cd_upmusicpassed","' . $cd_upmusicpassed . '"', $str);
	$str=preg_replace('/"cd_attachmentpoints","(.*?)"/', '"cd_attachmentpoints","' . $cd_attachmentpoints . '"', $str);
	$str=preg_replace('/"cd_userupload","(.*?)"/', '"cd_userupload","' . $cd_userupload . '"', $str);
	if(!$fp=fopen('../include/x5music.config.php', 'w')) {
		showmessage("出错了，文件 ../include/x5music.config.php 没有写入权限！", $_SERVER['HTTP_REFERER'], 0);
	}
	$ifile=new iFile('../include/x5music.config.php', 'w');
	$ifile->WriteFile($str, 3);
	showmessage("恭喜您，保存设置成功！", $_SERVER['HTTP_REFERER'], 0);
} elseif($action=="dow") { //下载设置
	$cd_dowattachment=SafeRequest("cd_dowattachment", "post");
	$cd_dowattachmentsiz=SafeRequest("cd_dowattachmentsiz", "post");
	$cd_dowattachmentname=SafeRequest("cd_dowattachmentname", "post");
	//保存数据
	$str=file_get_contents("../include/x5music.config.php");
	$str=preg_replace('/"cd_dowattachment","(.*?)"/', '"cd_dowattachment","' . $cd_dowattachment . '"', $str);
	$str=preg_replace('/"cd_dowattachmentsiz","(.*?)"/', '"cd_dowattachmentsiz","' . $cd_dowattachmentsiz . '"', $str);
	$str=preg_replace('/"cd_dowattachmentname","(.*?)"/', '"cd_dowattachmentname","' . $cd_dowattachmentname . '"', $str);
	if(!$fp=fopen('../include/x5music.config.php', 'w')) {
		showmessage("出错了，文件 ../include/x5music.config.php 没有写入权限！", $_SERVER['HTTP_REFERER'], 0);
	}
	$ifile=new iFile('../include/x5music.config.php', 'w');
	$ifile->WriteFile($str, 3);
	showmessage("恭喜您，保存设置成功！", $_SERVER['HTTP_REFERER'], 0);
} elseif($action=="attachtype") { //附件类型尺寸
	$cd_upmusictype=SafeRequest("cd_upmusictype", "post");
	$cd_upmusicsize=SafeRequest("cd_upmusicsize", "post");
	$cd_upvideotype=SafeRequest("cd_upvideotype", "post");
	$cd_upvideosize=SafeRequest("cd_upvideosize", "post");
	$cd_uppictype=SafeRequest("cd_uppictype", "post");
	$cd_uppicsize=SafeRequest("cd_uppicsize", "post");
	//保存数据
	$str=file_get_contents("../include/x5music.config.php");
	$str=preg_replace('/"cd_upmusictype","(.*?)"/', '"cd_upmusictype","' . $cd_upmusictype . '"', $str);
	$str=preg_replace('/"cd_upmusicsize","(.*?)"/', '"cd_upmusicsize","' . $cd_upmusicsize . '"', $str);
	$str=preg_replace('/"cd_upvideotype","(.*?)"/', '"cd_upvideotype","' . $cd_upvideotype . '"', $str);
	$str=preg_replace('/"cd_upvideosize","(.*?)"/', '"cd_upvideosize","' . $cd_upvideosize . '"', $str);
	$str=preg_replace('/"cd_uppictype","(.*?)"/', '"cd_uppictype","' . $cd_uppictype . '"', $str);
	$str=preg_replace('/"cd_uppicsize","(.*?)"/', '"cd_uppicsize","' . $cd_uppicsize . '"', $str);
	if(!$fp=fopen('../include/x5music.config.php', 'w')) {
		showmessage("出错了，文件 ../include/x5music.config.php 没有写入权限！", $_SERVER['HTTP_REFERER'], 0);
	}
	$ifile=new iFile('../include/x5music.config.php', 'w');
	$ifile->WriteFile($str, 3);
	showmessage("恭喜您，保存设置成功！", $_SERVER['HTTP_REFERER'], 0);
} elseif($action=="mobile") { //手机设置
	$cd_mobile=SafeRequest("cd_mobile", "post");
	$cd_mobilepc=SafeRequest("cd_mobilepc", "post");
	$cd_mobileurl=SafeRequest("cd_mobileurl", "post");
	//保存数据
	$str=file_get_contents("../include/x5music.config.php");
	$str=preg_replace('/"cd_mobile","(.*?)"/', '"cd_mobile","' . $cd_mobile . '"', $str);
	$str=preg_replace('/"cd_mobileurl","(.*?)"/', '"cd_mobileurl","' . $cd_mobileurl . '"', $str);
	$str=preg_replace('/"cd_mobilepc","(.*?)"/', '"cd_mobilepc","' . $cd_mobilepc . '"', $str);
	if(!$fp=fopen('../include/x5music.config.php', 'w')) {
		showmessage("出错了，文件 ../include/x5music.config.php 没有写入权限！", $_SERVER['HTTP_REFERER'], 0);
	}
	$ifile=new iFile('../include/x5music.config.php', 'w');
	$ifile->WriteFile($str, 3);
	showmessage("恭喜您，保存设置成功！", $_SERVER['HTTP_REFERER'], 0);
}
?>
<!DOCTYPE html>
<html>
 <head> 
  <meta charset="gbk" /> 
  <meta name="renderer" content="webkit" /> 
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" /> 
  <title>x5Music 后台管理中心 -x5mp3.com</title> 
  <link href="css/add.css" rel="stylesheet" /> 
  <style type="text/css">
.table2{width: 100%;max-width: 100%;border-collapse: collapse;border-spacing: 0;}
.table2 td{font-size: 12px;border-top: 1px solid #ddd;padding: 8px;vertical-align: top;}
.table2 tr:hover{text-decoration: none;background-color: #e6f2fb;}
.table2 th{border-bottom: 2px solid #ddd;vertical-align: bottom;padding: 2px;text-align: left;}
.table3{width: 100%;max-width: 100%;border-collapse: collapse;border-spacing: 0;}
.table3 tr:hover{text-decoration: none;background-color: #e6f2fb;}
.table3 td{font-size: 12px;line-height:25px;border-top: 1px solid #ddd;padding: 5px;vertical-align: top;border-right: solid 1px #ddd;}
.table3 th{border-bottom: 2px solid #ddd;vertical-align: bottom;padding: 2px;text-align: left;}
</style> 
 </head> 
 <body> 
<?php if($ac==""){ ?>
<!--站点信息设置-->
  <div class="contents"> 
   <div class="panel"> 
    <div class="panel-head"> 
     <strong>站点信息设置</strong> 
    </div> 
    <form action="config.php?action=config" method="post" name="form2"> 
     <table class="table3"> 
      <tbody> 
       <tr> 
        <td width="70" align="left">站点名称</td> 
        <td align="left"><input id="cd_webname" name="cd_webname" value="<?php echo cd_webname ?>" type="text" class="input input_hd length_3" />　站点名称，将显示在浏览器窗口标题等位置；</td> 
       </tr> 
       <tr> 
        <td align="left">站点域名</td> 
        <td align="left"><input id="cd_weburl" name="cd_weburl" value="<?php echo cd_weburl ?>" type="text" class="input input_hd length_3" />　例如: x5mp3.com，不要加 http://</td> 
       </tr> 
       <tr> 
        <td align="left">关键词</td> 
        <td align="left"><input id="cd_keywords" name="cd_keywords" value="<?php echo cd_keywords ?>" type="text" class="input input_hd length_6" />　KeyWords，一般不超过100个字符；请用(,)逗号隔开 </td> 
       </tr> 
       <tr> 
        <td align="left">描述</td> 
        <td align="left"><input id="cd_description" name="cd_description" value="<?php echo cd_description ?>" type="text" class="input input_hd length_6" />　Description，一般不超过200个字符；</td> 
       </tr> 
       <tr> 
        <td align="left">ICP备案号</td> 
        <td align="left"><input id="cd_webicp" name="cd_webicp" value="<?php echo cd_webicp ?>" type="text" class="input input_hd length_3" /></td> 
       </tr> 
       <tr> 
        <td align="left">禁止访问IP</td> 
        <td align="left"><input id="cd_webip" name="cd_webip" value="<?php echo cd_webip ?>" type="text" class="input input_hd length_3" />　支持通配符*，用 | 隔开</td> 
       </tr> 
       <tr> 
        <td align="left">防注入设置</td> 
        <td align="left"><input name="webscan_white_directory" id="webscan_white_directory" value="<?php echo webscan_white_directory ?>" type="text" class="input input_hd length_3" />　<font color="#d01f3c">这里添加的目录将不进行拦截,&quot;|&quot;隔开</font></td> 
       </tr> 
       <tr> 
        <td align="left">后台认证码</td> 
        <td align="left">
        <select name="cd_webcodea" id="cd_webcodea"> 
        <option value="yes" <?php if (cd_webcodea == "yes") {echo "selected";} ?>>开启</option>
        <option value="no" <?php if (cd_webcodea == "no") {echo "selected";} ?>>关闭</option> 
        </select>认证码：<input value="<?php echo cd_webcodeb ?>" name="cd_webcodeb" class="input input_hd length_3">　为了程序安全，建议您启用认证码；</td> 
       </tr> 
       <tr> 
        <td align="left">站点开关</td> 
        <td align="left">
        <select name="cd_weboffa" id="cd_weboffa"> 
        <option value="0" <?php if (cd_weboffa == "0") {echo "selected";} ?>>正常开放</option> 
        <option value="1" <?php if (cd_weboffa == "1") {echo "selected";} ?>>关闭维护</option> 
        </select>　用于维护时关闭所有页面访问</td> 
       </tr> 
       <tr> 
        <td align="left">维护说明</td> 
        <td align="left"><textarea class="length_6" name="cd_weboffb"><?php echo cd_weboffb ?></textarea>　支持简单的HTML标签</td> 
       </tr> 
<?php
global $db;
$sql="Select cd_cnzz,cd_message from " . tname('system') . "";
$row=$db->getrow($sql);
?>
       <tr> 
        <td align="left">统计代码</td> 
        <td align="left"><textarea class="length_6" name="cd_cnzz"><?php echo $row['cd_cnzz']; ?></textarea>　建议使用CNZZ网站统计</td> 
       </tr> 
       <tr>
        <td height="35" colspan="2" align="left" bgcolor="#FAFBF7" class="td_border"><button type="submit" id="adddance" class="btn btn_submit J_ajax_submit_btn" style="margin-left: 80px;">确定提交</button></td>
       </tr> 
      </tbody> 
     </table> 
    </form> 
   </div> 
  </div> 
<?php
} elseif ($ac == "admin") { ?>
<!--站长信息-->
  <div class="contents"> 
   <div class="panel"> 
    <div class="panel-head"> 
     <strong>站长信息设置</strong> 
    </div> 
    <form action="config.php?action=admin" method="post" name="form2"> 
     <table class="table3"> 
      <tbody> 
       <tr> 
        <td width="80" align="left">站长ＱＱ</td> 
        <td align="left"><input id="cd_webqq" name="cd_webqq" value="<?php echo cd_webqq ?>" type="text" class="input input_hd length_3" /></td> 
       </tr> 
       <tr> 
        <td align="left">站长电话</td> 
        <td align="left"><input id="cd_webtel" name="cd_webtel" value="<?php echo cd_webtel ?>" type="text" class="input input_hd length_3" /></td> 
       </tr> 
       <tr> 
        <td align="left">联系邮箱</td> 
        <td align="left"><input id="cd_webemail" name="cd_webemail" value="<?php echo cd_webemail ?>" type="text" class="input input_hd length_3" /></td> 
       </tr> 
       <tr>
        <td height="35" colspan="2" align="left" bgcolor="#FAFBF7" class="td_border"><button type="submit" id="adddance" class="btn btn_submit J_ajax_submit_btn" style="margin-left: 90px;">确定提交</button></td>
       </tr> 
      </tbody> 
     </table> 
    </form> 
   </div> 
  </div> 
<?php
} elseif ($ac == "seo") { ?>
<!--SEO优化-->
  <div class="contents"> 
   <div class="panel"> 
    <div class="panel-head"> 
     <strong>SEO优化</strong> 
    </div> 
    <form action="config.php?action=seo" method="post" name="form2"> 
     <table class="table3"> 
      <tbody> 
       <tr> 
        <td width="120" align="left">前台页面</td> 
        <td align="left"> 
        <label class="mr20"><input type="radio" name="cd_webhtml" value="1" <?php if (cd_webhtml == 1) {echo " checked";} ?>>&nbsp;动态&nbsp;&nbsp;</label> 
        <label class="mr20"><input type="radio" name="cd_webhtml" value="3" <?php if (cd_webhtml == 3) {echo " checked";} ?>>&nbsp;伪静态&nbsp;&nbsp;</label> 
        </td> 
       </tr> 
       <tr> 
        <td align="left">会员中心</td> 
        <td align="left"> 
        <label class="mr20"><input type="radio" name="cd_userhtml" value="0"<?php if (cd_userhtml == 0) {echo " checked";} ?>>&nbsp;动态&nbsp;&nbsp;</label>
        <label class="mr20"><input type="radio" name="cd_userhtml" value="1"<?php if (cd_userhtml == 1) {echo " checked";} ?>>&nbsp;伪静态</label> 
        </td> 
       </tr>
       <tr> 
        <td align="left"><a href="Rewrite.html" target="_blank">点我查看伪静态规则</a></td> 
        <td align="left">开启伪静态请在主机或控制面板中添加对应的Rewrite规则</td> 
       </tr> 
       <tr><td height="35" colspan="2" align="left" bgcolor="#FAFBF7" class="td_border"><button type="submit" id="adddance" class="btn btn_submit J_ajax_submit_btn" style="margin-left: 130px;">确定提交</button></td></tr>
      </tbody> 
     </table> 
    </form> 
   </div> 
  </div> 
<?php
} elseif ($ac == "cachethread") { ?>
<!--性能优化-->
  <div class="contents"> 
   <div class="panel"> 
    <div class="panel-head"> 
     <strong>性能优化</strong> 
    </div> 
    <form action="config.php?action=cachethread" method="post" name="form2"> 
     <table class="table3"> 
      <tbody> 
       <tr> 
        <td width="120" align="left">缓存开关</td> 
        <td align="left"> <select name="cd_iscache" id="cd_iscache"> 
        <option value="1" <?php if (cd_iscache == "1") {echo "selected";} ?>>= 开启 =</option>
        <option value="0" <?php if (cd_iscache == "0") {echo "selected";} ?>>= 关闭 =</option>
        </select>&nbsp;&nbsp;缓存时间：<input value="<?php echo cd_cachetime ?>" name="cd_cachetime" class="input input_hd length_1" onkeyup="this.value=this.value.replace(/[^\d]/g,'')" onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/[^\d]/g,''))" />&nbsp;秒&nbsp;&nbsp;缓存标识：<input value="<?php echo cd_cachemark ?>" name="cd_cachemark" class="input input_hd length_1" />　当网站访问量很大时，可开启网站缓存来提高网站性能； </td> 
       </tr> 
       <tr> 
        <td align="left">今日影音人气更新时间</td> 
        <td align="left"> 
        <select name="cd_djdayhitstime" id="cd_djdayhitstime"> 
        <option value="yes" <?php if (cd_djdayhitstime == "yes") {echo "selected";} ?>>自动更新</option>
        <option value="no" <?php if (cd_djdayhitstime == 'no') {echo "selected";} ?>>永不更新</option>
        </select>　*每天更新影音的今日人气数据； </td> 
       </tr> 
       <tr> 
        <td align="left">本周影音人气更新时间</td> 
        <td align="left"> 
        <select name="cd_djweekhitstime" id="cd_djweekhitstime"> 
        <option value="yes" <?php if (cd_djweekhitstime == "yes") {echo "selected";} ?>>自动更新</option>
        <option value="no" <?php if (cd_djweekhitstime == 'no') {echo "selected";} ?>>永不更新</option>
        </select>　*每周一更新数据； 
        </td> 
       </tr> 
       <tr> 
        <td align="left">本月影音人气更新时间</td> 
        <td align="left"> 
        <select name="cd_djmonthhitstime" id="cd_djmonthhitstime"> 
        <option value="yes" <?php if (cd_djmonthhitstime == "yes") {echo "selected";} ?>>自动更新</option>
        <option value="no" <?php if (cd_djmonthhitstime == 'no') {echo "selected";} ?>>永不更新</option>
        </select>　*每月一号更新数据； 
        </td> 
       </tr> 
       <tr>
        <td height="35" colspan="2" align="left" bgcolor="#FAFBF7" class="td_border"><button type="submit" id="adddance" class="btn btn_submit J_ajax_submit_btn" style="margin-left: 80px;">确定提交</button></td>
       </tr> 
      </tbody> 
     </table> 
    </form> 
   </div> 
  </div> 
<?php
} elseif ($ac == "user") { ?>
<!--会员相关设置-->
<script type="text/javascript">
function changetype(itype) {
	if (itype == 1) {
		userlocalpm.style.display = '';
	}
	if (itype == 2) {
		userlocalpm.style.display = 'none';
		
	}
	if (itype == 3) {
		usersmtppm.style.display = '';
	}
	if (itype == 4) {
		usersmtppm.style.display = 'none';
	}
	if (itype == 5) {
		vipoff.style.display = '';
	}
	if (itype == 6) {
		vipoff.style.display = 'none';
	}
	if (itype == 7) {
		messageoff.style.display = 'none';
		messagelocaloff.style.display = '';
	}
	if (itype == 8) {
		messagelocaloff.style.display = 'none';
		messageoff.style.display = '';
	}
	
}
</script>
  <div class="contents"> 
   <div class="panel"> 
    <div class="panel-head"> 
     <strong>会员相关初始化设置</strong> 
    </div> 
    <form action="config.php?action=user" method="post" name="form2"> 
     <table class="table3"> 
      <tbody> 
       <tr> 
        <td width="160" align="left">是否开放会员注册</td> 
        <td align="left"> <label class="mr20"><input name="cd_userreg" type="radio" value="yes" <?php if (cd_userreg == "yes") {echo "checked";} ?>>&nbsp;开放&nbsp;</label> <label class="mr20"><input type="radio" name="cd_userreg" value="no" <?php if (cd_userreg == "no") {echo "checked";} ?>>&nbsp;关闭&nbsp;</label> </td> 
       </tr> 
       <tr> 
        <td align="left">是否开放会员登录</td> 
        <td align="left"> <label class="mr20"><input name="cd_userlogin" type="radio" value="yes" <?php if (cd_userlogin == "yes") {echo "checked";} ?>>&nbsp;开放&nbsp;</label> <label class="mr20"><input type="radio" name="cd_userlogin" value="no" <?php if (cd_userlogin == "no") {echo "checked";} ?>>&nbsp;关闭&nbsp;</label> </td> 
       </tr> 
       <tr> 
        <td align="left">是否向新用户发送站内短消息</td> 
        <td align="left"> <label class="mr20"><input name="cd_userlocalpm" type="radio" value="yes" onClick="changetype(1);" <?php if (cd_userlocalpm == "yes") {echo "checked";} ?>>&nbsp;是&nbsp;</label> <label class="mr20"><input type="radio" name="cd_userlocalpm" value="no" onClick="changetype(2);" <?php if (cd_userlocalpm == "no") {echo "checked";} ?>>&nbsp;否&nbsp;</label> </td> 
       </tr> 
      </tbody>
      <tbody id='userlocalpm'<?php if (cd_userlocalpm == "no") {echo " style='display:none'";} ?>>
       <tr> 
        <td align="left">发送站内短消息内容</td> 
        <td align="left"><textarea class="length_6" name="cd_userlocalpmtxt"><?php echo cd_userlocalpmtxt ?></textarea> </td> 
       </tr> 
      </tbody> 
      <tbody> 
       <tr> 
        <td align="left">新注册会员初始金币</td> 
        <td align="left"><input id="cd_userpoints" name="cd_userpoints" value="<?php echo cd_userpoints ?>" type="text" class="input input_hd length_0" /> </td> 
       </tr> 
       <tr> 
        <td align="left">记录登陆在线时间</td> 
        <td align="left"><input id="cd_onlinehold" name="cd_onlinehold" value="<?php echo cd_onlinehold ?>" type="text" class="input input_hd length_1" />　* (单位 秒) </td> 
       </tr> 
       <tr> 
        <td align="left">动态保留天数</td> 
        <td align="left"><input id="cd_userfeedday" name="cd_userfeedday" value="<?php echo cd_userfeedday ?>" type="text" class="input input_hd length_1" />　* (单位 天) 超过该天数的动态会被清理，从而保证MySQL的效率。建议无需设置太长。 </td> 
       </tr> 
       <tr> 
        <td align="left">登录cookie时间</td> 
        <td align="left"><input id="cd_userlogintime" name="cd_userlogintime" value="<?php echo cd_userlogintime ?>" type="text" class="input input_hd length_2" />　* (单位 秒) 用户登录状态cookie时长，建议加大cookie时间。 </td> 
       </tr> 
       <tr>
        <td height="35" colspan="2" align="left" bgcolor="#FAFBF7" class="td_border"><strong style="font-size: 14px;">　奖励规则设置 (加)</strong></td>
       </tr> 
       <tr> 
        <td align="left">每天登陆</td> 
        <td align="left"> 金币+<input id="cd_userloginpoints" name="cd_userloginpoints" value="<?php echo cd_userloginpoints ?>" type="text" class="input input_hd length_1"></td> 
       </tr> 
       <tr> 
        <td align="left">上传音乐/视频</td> 
        <td align="left"> 金币+<input id="cd_userupmusicpoints" name="cd_userupmusicpoints" value="<?php echo cd_userupmusicpoints ?>" type="text" class="input input_hd length_1"></td> 
       </tr> 
       <tr> 
        <td align="left">上传照片</td> 
        <td align="left"> 金币+<input id="cd_useruppicpoints" name="cd_useruppicpoints" value="<?php echo cd_useruppicpoints ?>" type="text" class="input input_hd length_1">&nbsp;&nbsp;每天上限<input id="cd_useruppicceiling" name="cd_useruppicceiling" value="<?php echo cd_useruppicceiling ?>" type="text" class="input input_hd length_1"></td> 
       </tr> 
       <tr> 
        <td align="left">访问空间</td> 
        <td align="left"> 金币+<input id="cd_userwebpoints" name="cd_userwebpoints" value="<?php echo cd_userwebpoints ?>" type="text" class="input input_hd length_1">&nbsp;&nbsp;每天上限<input id="cd_userwebceiling" name="cd_userwebceiling" value="<?php echo cd_userwebceiling ?>" type="text" class="input input_hd length_1"></td> 
       </tr> 
       <tr>
        <td height="35" colspan="2" align="left" bgcolor="#FAFBF7" class="td_border"><strong style="font-size: 14px;">　惩罚规则设置 (减)</strong></td>
       </tr> 
       <tr> 
        <td align="left">音乐/视频被删除</td> 
        <td align="left"> 金币-<input id="cd_userdeletemusicpoints" name="cd_userdeletemusicpoints" value="<?php echo cd_userdeletemusicpoints ?>" type="text" class="input input_hd length_1"></td> 
       </tr> 
       <tr> 
        <td align="left">照片被删除</td> 
        <td align="left"> 金币-<input id="cd_userdeletepicpoints" name="cd_userdeletepicpoints" value="<?php echo cd_userdeletepicpoints ?>" type="text" class="input input_hd length_1"></td> 
       </tr> 
       <tr>
        <td height="35" colspan="2" align="left" bgcolor="#FAFBF7" class="td_border"><strong style="font-size: 14px;">　VIP用户相关设置</strong></td>
       </tr> 
       <tr> 
        <td align="left">是否开放会员金币转VIP功能</td> 
        <td align="left"> <label class="mr20"><input name="cd_vipoff" type="radio" value="0" <?php if (cd_vipoff == "0") {echo "checked";} ?>>&nbsp;开放&nbsp;</label> <label class="mr20"><input type="radio" name="cd_vipoff" value="1" <?php if (cd_vipoff == "1") {echo "checked";} ?>>&nbsp;关闭&nbsp;</label> </td> 
       </tr> 
       <tr> 
        <td align="left">会员转月VIP会员所需金币</td> 
        <td align="left"><input id="cd_vippoints" name="cd_vippoints" value="<?php echo cd_vippoints ?>" type="text" class="input input_hd length_1" />　(单位 个) </td> 
       </tr> 
       <tr> 
        <td align="left">VIP会员的月天数</td> 
        <td align="left"><input id="cd_viptime" name="cd_viptime" value="<?php echo cd_viptime ?>" type="text" class="input input_hd length_1" />　(单位 天) </td> 
       </tr>  
       <tr> 
        <td align="left">VIP会员开通提醒内容</td> 
        <td align="left"><textarea class="length_6" name="cd_vipktxt"><?php echo cd_vipktxt ?></textarea> </td> 
       </tr> 
       <tr> 
        <td align="left">VIP会员到期提醒内容</td> 
        <td align="left"><textarea class="length_6" name="cd_viptxt"><?php echo cd_viptxt ?></textarea> </td> 
       </tr> 
       <tr>
        <td height="35" colspan="2" align="left" bgcolor="#FAFBF7" class="td_border"><strong style="font-size: 14px;">　评论系统</strong></td>
       </tr> 
<?php
global $db;
$sql = "Select cd_message from " . tname('system') . "";
$row = $db->getrow($sql);
?>
       <tr> 
        <td align="left">第三方评论代码</td> 
        <td align="left"><textarea class="length_6" name="cd_message"><?php echo $row['cd_message']; ?></textarea> </td> 
       </tr> 
      </tbody> 
      <tbody>
       <tr>
        <td height="35" colspan="2" align="left" bgcolor="#FAFBF7" class="td_border"><button type="submit" id="adddance" class="btn btn_submit J_ajax_submit_btn" style="margin-left: 80px;">确定提交</button></td>
       </tr> 
      </tbody> 
     </table> 
    </form> 
   </div> 
  </div> 
<?php
} elseif ($ac == "upload") { ?>
<!--上传设置-->
  <div class="contents"> 
   <div class="panel"> 
    <div class="panel-head"> 
     <strong>站内上传设置</strong> 
    </div> 
    <form action="config.php?action=upload" method="post" name="form2"> 
     <table class="table3"> 
      <tbody> 
       <tr> 
        <td width="120" align="left">上传页面入口</td> 
        <td align="left"> <label class="mr20"><input name="cd_uploadentrance" type="radio" value="yes" <?php if (cd_uploadentrance == "yes") {echo "checked";} ?>>&nbsp;开放&nbsp;</label> <label class="mr20"><input type="radio" name="cd_uploadentrance" value="no" <?php if (cd_uploadentrance == "no") {echo "checked";} ?>>&nbsp;关闭&nbsp;</label>　关闭后，所有人都无法进入上传页面。 </td> 
       </tr> 
       <tr> 
        <td align="left">游客上传音乐/视频</td> 
        <td align="left"> <label class="mr20"><input name="cd_ykuploadmusic" type="radio" value="yes" <?php if (cd_ykuploadmusic == "yes") {echo "checked";} ?>>&nbsp;开放&nbsp;</label> <label class="mr20"><input type="radio" name="cd_ykuploadmusic" value="no" <?php if (cd_ykuploadmusic == "no") {echo "checked";} ?>>&nbsp;关闭&nbsp;</label> </td> 
       </tr> 
       <tr> 
        <td align="left">上传音乐/视频审核</td> 
        <td align="left"> <label class="mr20"><input name="cd_upmusicpassed" type="radio" value="1" <?php if (cd_upmusicpassed == "1") {echo "checked";} ?>>&nbsp;审核&nbsp;</label> <label class="mr20"><input type="radio" name="cd_upmusicpassed" value="0" <?php if (cd_upmusicpassed == "0") {echo "checked";} ?>>&nbsp;关闭&nbsp;</label> </td> 
       </tr> 
       <tr> 
        <td align="left">附件最高售价</td> 
        <td align="left"><input id="cd_attachmentpoints" name="cd_attachmentpoints" value="<?php echo cd_attachmentpoints ?>" type="text" class="input input_hd length_3" />　*允许会员最高售价多少金币 </td> 
       </tr> 
       <tr> 
        <td align="left">前台本地影音上传开关</td> 
        <td align="left"><label class="mr20"><input name="cd_userupload" type="radio" value="1" <?php if (cd_userupload == "1") {echo "checked";} ?>>&nbsp;开放&nbsp;</label> <label class="mr20"><input type="radio" name="cd_userupload" value="0" <?php if (cd_userupload == "0") {echo "checked";} ?>>&nbsp;关闭&nbsp;</label>　如果您不使用本地附件储存，请关闭，避免安全隐患! </td> 
       </tr> 
       <tr>
        <td height="35" colspan="2" align="left" bgcolor="#FAFBF7" class="td_border"><button type="submit" id="adddance" class="btn btn_submit J_ajax_submit_btn" style="margin-left: 80px;">确定提交</button></td>
       </tr> 
      </tbody> 
     </table> 
    </form> 
   </div> 
  </div> 
<?php
} elseif ($ac == "dow") { ?>
<!--下载设置-->
  <div class="contents"> 
   <div class="panel"> 
    <div class="panel-head"> 
     <strong>本地影音文件名下载设置</strong> 
    </div> 
    <form action="config.php?action=dow" method="post" name="form2"> 
     <table class="table3"> 
      <tbody> 
       <tr> 
        <td width="90" align="left">是否启用文件名</td> 
        <td align="left"> <label class="mr20"><input name="cd_dowattachment" type="radio" value="yes" <?php if (cd_dowattachment == "yes") {echo "checked";} ?>>&nbsp;是&nbsp;</label> <label class="mr20"><input type="radio" name="cd_dowattachment" value="no" <?php if (cd_dowattachment == "no") {echo "checked";} ?>>&nbsp;否&nbsp;</label>　启用后，影音文件下载将显示标题名； </td> 
       </tr> 
       <tr> 
        <td align="left">每秒传输字节</td> 
        <td align="left"><input id="cd_dowattachmentsiz" name="cd_dowattachmentsiz" value="<?php echo cd_dowattachmentsiz ?>" type="text" class="input input_hd length_1" />　*单位：(字节) </td> 
       </tr> 
       <tr> 
        <td align="left">文件名附加内容</td> 
        <td align="left"><input id="cd_dowattachmentname" name="cd_dowattachmentname" value="<?php echo cd_dowattachmentname ?>" type="text" class="input input_hd length_4" />　*[dj:name]=&gt;歌名 　[dj:singer]=&gt;歌手 +附加内容 </td> 
       </tr> 
       <tr>
        <td height="25" colspan="2" align="left" bgcolor="#FFF" class="td_border">注：大附件下载会消耗网站的内存，网速！</td>
       </tr> 
       <tr>
        <td height="35" colspan="2" align="left" bgcolor="#FAFBF7" class="td_border"><button type="submit" id="adddance" class="btn btn_submit J_ajax_submit_btn" style="margin-left: 100px;">确定提交</button></td>
       </tr> 
      </tbody> 
     </table> 
    </form> 
   </div> 
  </div>
<?php
} elseif ($ac == "attachtype") { ?>
<!--附件类型-->
  <div class="contents"> 
   <div class="panel"> 
    <div class="panel-head"> 
     <strong>站内附件类型尺寸</strong> 
    </div> 
    <form action="config.php?action=attachtype" method="post" name="form2"> 
     <table class="table3"> 
      <tbody> 
       <tr> 
        <td width="130" align="left">音乐文件上传格式</td> 
        <td align="left"> <input id="cd_upmusictype" name="cd_upmusictype" value="<?php echo cd_upmusictype ?>" type="text" class="input input_hd length_3" />　 文件大小：<input id="cd_upmusicsize" name="cd_upmusicsize" value="<?php echo cd_upmusicsize ?>" type="text" class="input input_hd length_1" /> MB　* 单位 MB </td> 
       </tr> 
       <tr> 
        <td width="130" align="left">视频文件上传格式</td> 
        <td align="left"><input id="cd_upvideotype" name="cd_upvideotype" value="<?php echo cd_upvideotype ?>" type="text" class="input input_hd length_3" />　 文件大小：<input id="cd_upvideosize" name="cd_upvideosize" value="<?php echo cd_upvideosize ?>" type="text" class="input input_hd length_1" /> MB　* 单位 MB </td> 
       </tr> 
       <tr> 
        <td width="130" align="left">图片文件上传格式</td> 
        <td align="left"> <input id="cd_uppictype" name="cd_uppictype" value="<?php echo cd_uppictype ?>" type="text" class="input input_hd length_3" />　 文件大小：<input id="cd_uppicsize" name="cd_uppicsize" value="<?php echo cd_uppicsize ?>" type="text" class="input input_hd length_1" /> MB　* 单位 MB </td> 
       </tr> 
       <tr>
        <td height="35" colspan="2" align="left" bgcolor="#FAFBF7" class="td_border"><button type="submit" id="adddance" class="btn btn_submit J_ajax_submit_btn" style="margin-left: 80px;">确定提交</button></td>
       </tr> 
      </tbody> 
     </table> 
    </form> 
   </div> 
  </div> 
<?php
} elseif ($ac == "mobile") { ?>
<!--手机访问设置-->
  <div class="contents"> 
   <div class="panel"> 
    <div class="panel-head"> 
     <strong>手机访问设置</strong> 
    </div> 
    <form action="config.php?action=mobile" method="post" name="form2"> 
     <table class="table3"> 
      <tbody> 
       <tr> 
        <td width="130" align="left">开启手机版</td> 
        <td align="left"> <label class="mr20"><input name="cd_mobile" type="radio" value="yes" onClick="changetype(2);" <?php if (cd_mobile == "yes") {echo "checked";} ?>>&nbsp;是&nbsp;</label> <label class="mr20"><input type="radio" name="cd_mobile" value="no" onClick="changetype(1);" <?php if (cd_mobile == "no") {echo "checked";} ?>>&nbsp;否&nbsp;</label>开启后手机访问PC将自动转跳</td> 
       </tr> 
       <tr> 
        <td width="130" align="left">是否允许PC端访问</td> 
        <td align="left"> <label class="mr20"><input name="cd_mobilepc" type="radio" value="yes" <?php if (cd_mobilepc == "yes") {echo "checked";} ?>>&nbsp;是&nbsp;</label> <label class="mr20"><input type="radio" name="cd_mobilepc" value="no" <?php if (cd_mobilepc == "no") {echo "checked";} ?>>&nbsp;否&nbsp;</label></td> 
       </tr> 
      </tbody>
      <tbody>
       <tr>
        <td height="35" colspan="2" align="left" bgcolor="#FAFBF7" class="td_border"><button type="submit" id="adddance" class="btn btn_submit J_ajax_submit_btn" style="margin-left: 80px;">确定提交</button></td>
       </tr> 
      </tbody> 
     </table> 
    </form> 
   </div> 
  </div> 
<?php } ?>
</body>
</html>