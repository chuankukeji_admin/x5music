<?php
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
error_reporting(0);
include 'x5music.conn.php';
include 'x5music.inc.php';
@header('Cache-control: private');
$key=SafeRequest('key', 'get');
$key=str_checkhtml($key);
//if(is_utf8($key)) $key=convert_encoding($key,'GBK','UTF-8');
if($key=='') {
	echo '<meta charset="gbk" />';
	die("<script>alert('请输入查询的关键字');history.back();</script>");
}
$Mark_Text=@file_get_contents(_x5music_root_ . cd_templateurl . 'search.html');
$Mark_Text=topandbottom($Mark_Text);
$Mark_Text=ReplaceStr($Mark_Text, '[x5music:search]', $key);
$data_content='';
$pagenum=getpagenum($Mark_Text);
preg_match_all('/{x5music:dj(.*?pagesize=([\S]+).*?)}([\s\S]+?){\/x5music:dj}/', $Mark_Text, $page_arr);
if(!empty($page_arr) && !empty($page_arr[2])) {
	$sqlstr='select * from ' . tname('dj') . " where CD_Deleted=0 and CD_ClassID<>0 and CD_Name like '%" . $key . "%' or CD_Singer like '%" . $key . "%' or CD_Tag like '%" . $key . "%' or CD_User like '%" . $key . "%' order by CD_AddTime desc";
	$Arr=searchpage($sqlstr, $page_arr[2][0], $key);
	$result=$db->query($Arr[2]);
	$recount=$db->num_rows($result);
	if($recount==0) {
		$data_content="<font style='font-size:13px'><div align='center'>对不起，没有找到《" . $key . '》相关内容！</div></font>';
	} else {
		if($result) {
			$sorti=1;
			while($row2=$db->fetch_array($result)) {
				$datatmp=datadj($page_arr[0][0], $page_arr[3][0], $row2, $sorti);
				$sorti=$sorti + 1;
				$data_content.=$datatmp;
			}
		}
	}
	$Mark_Text=Page_Mark($Mark_Text, $Arr);
	$Mark_Text=ReplaceStr($Mark_Text, $page_arr[0][0], $data_content);
	unset($Arr);
}
unset($page_arr);
$Mark_Text=Common_Mark($Mark_Text, 0);
$p=SafeRequest('pages', 'get');
$cache_id='so_' . $key . $p;
if(!($cache_opt->start($cache_id))) {
	echo ($Mark_Text);
	$cache_opt->end();
}
function searchpage($mysql, $pagesize, $key) {
	global $db;
	$url='key=' . $key . '&';
	$pages=SafeRequest('pages', 'get');
	$pagesok=$pagesize;
	if(!isset($pages) || $pages=='' || !is_numeric($pages) || $pages<=0) {
		$pages=1;
	}
	$sqlstr=$mysql;
	$res=$db->query($sqlstr);
	$nums=$db->num_rows($res);
	if($nums==0) {
		$nums=1;
	}
	$pagejs=ceil($nums / $pagesok);
	if($pages>$pagejs) {
		$pages=$pagejs;
	}
	$sql=$sqlstr . ' LIMIT ' . $pagesok * ($pages - 1) . ',' . $pagesok;
	$result=$db->query($sql);
	$str='';
	$first='?' . $url . 'pages=1';
	$pageup='?' . $url . 'pages=' . ($pages - 1) . '';
	$pagenext='?' . $url . 'pages=' . ($pages + 1) . '';
	$last='?' . $url . 'pages=' . $pagejs . '';
	$pagelist='<script>function gotopage(url,page){window.location=url+page;}</script>';
	$pagelist.="<select onchange=\"gotopage('?" . $url . "pages=',this.value)\">\r\n<option value='0'>跳转</option>\r\n";
	for($k=1; $k<=$pagejs; $k++) {
		if($k==$pages) {
			$pagelist.="<option value='" . $k . "' selected>第" . $k . "页</option>\r\n";
		} else {
			$pagelist.="<option value='" . $k . "'>第" . $k . "页</option>\r\n";
		}
	}
	$pagelist.='</select>';
	if($pagejs<=10) {
		for($i=1; $i<=$pagejs; $i++) {
			if($i==$pages) {
				$str.="<a href='?" . $url . 'pages=' . $i . "' class='current'>" . $i . '</a>';
			} else {
				$str.="<a href='?" . $url . 'pages=' . $i . "'>" . $i . '</a>';
			}
		}
	} else {
		if($pages>=12) {
			for($i=$pages - 5; $i<=$pages + 6; $i++) {
				if($i<=$pagejs) {
					if($i==$pages) {
						$str.='<a href=?' . $url . 'pages=' . $i . "' class='current'>" . $i . '</a>';
					} else {
						$str.='<a href=?' . $url . 'pages=' . $i . "'>" . $i . '</a>';
					}
				}
			}
		} else {
			for($i=1; $i<=12; $i++) {
				if($i==$pages) {
					$str.="<a href='?" . $url . 'pages=' . $i . "' class='current'>" . $i . '</a>';
				} else {
					$str.="<a href='?" . $url . 'pages=' . $i . "'>" . $i . '</a>';
				}
			}
		}
	}
	while($row=$db->fetch_array($result)) {
	}
	$arr=array(
		$str,
		$result,
		$sql,
		$nums,
		$pagelist,
		$pages,
		$pagejs,
		$first,
		$pageup,
		$pagenext,
		$last,
		$pagesok
	);
	return $arr;
}
function is_utf8($string) {
	return preg_match('%^(?:
	[\x09\x0A\x0D\x20-\x7E] # ASCII
	| [\xC2-\xDF][\x80-\xBF] # non-overlong 2-byte
	| \xE0[\xA0-\xBF][\x80-\xBF] # excluding overlongs
	| [\xE1-\xEC\xEE\xEF][\x80-\xBF]{2} # straight 3-byte
	| \xED[\x80-\x9F][\x80-\xBF] # excluding surrogates
	| \xF0[\x90-\xBF][\x80-\xBF]{2} # planes 1-3
	| [\xF1-\xF3][\x80-\xBF]{3} # planes 4-15
	| \xF4[\x80-\x8F][\x80-\xBF]{2} # plane 16
	)*$%xs', $string);
}
?>