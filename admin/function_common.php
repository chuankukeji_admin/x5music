<?php
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
error_reporting(0);
$CD_Version="V1.9";
$CD_Build="2016-01-18";
$CD_Vurl="x5mp3.com";
$CD_Vtime="2016-01-18";
function showmessage($CD_Msg, $CD_Url, $CD_Type) {
	if($CD_Type==1) {
		echo "<script language=javascript>alert('" . $CD_Msg . "');history.back(1);</script>";
	} elseif($CD_Type==2) {
		echo "<meta charset=\"gbk\" /> <center><script>";
		echo "document.write('<br><div style=\"width:500px;padding-top:6px;padding-bottom:3px;height:24;font-size:10pt;text-align:left;border-left:1px solid #5CB0DF;border-top:1px solid #5CB0DF;border-right:1px solid #5CB0DF;background-color:#5CB0DF;\">&nbsp;&nbsp;操作提示信息：</div>');";
		echo "document.write('<div style=\"width:500px;height:150;font-size:10pt;border:1px solid #5CB0DF;background-color:#F6F6F6;\"><br><br>');";
		echo "document.write('" . $CD_Msg . "');";
		echo "document.write('</div>');";
		echo "</script>";
		echo "</center>";
	} else {
		echo "<meta charset=\"gbk\" /> <center><script>var pgo=0;function CD_Url(){if(pgo==0){ location='" . $CD_Url . "'; pgo=1; }}";
		echo "document.write('<br><div style=\"width:500px;padding-top:6px;padding-bottom:3px;height:24;font-size:10pt;text-align:left;border-left:1px solid #5CB0DF;border-top:1px solid #5CB0DF;border-right:1px solid #5CB0DF;background-color:#5CB0DF;\">&nbsp;&nbsp;操作提示信息：</div>');";
		echo "document.write('<div style=\"width:500px;height:150;font-size:10pt;border:1px solid #5CB0DF;background-color:#F6F6F6;\"><br><br>');";
		echo "document.write('" . $CD_Msg . "');";
		echo "document.write('<br><br><a href=" . $CD_Url . ">如果你的浏览器没反应，请点击这里...</a><br><br><br></div>');";
		echo "setTimeout('CD_Url()',3000);</script>";
		echo "</center>";
	}
	exit();
}
function admincheck($value) {
	if(empty($_COOKIE['CD_AdminID']) || empty($_COOKIE['CD_Login']) || $_COOKIE['CD_Login']!==md5($_COOKIE['CD_AdminID'] . $_COOKIE['CD_AdminUserName'] . $_COOKIE['CD_AdminPassWord'] . $_COOKIE['CD_Permission'])) {
		showmessage("出错了，登录已过期，请重新登录！", "admin_login.php", 0);
	}
	if(!empty($_COOKIE['CD_Permission'])) {
		$array=explode(",", $_COOKIE['CD_Permission']);
		$adminlogined=false;
		for($i=0; $i<count($array); $i++) {
			if($array[$i]==$value) {
				$adminlogined=true;
			}
		}
		if(!$adminlogined) {
			showmessage("出错了，您没有进入本页面的权限！", "", 2);
		}
	} else {
		showmessage("出错了，您没有进入本页面的权限！", "", 2);
	}
}
?>