<?php
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
error_reporting(0);
include "../../include/x5music.conn.php";
include "../function_common.php";
admincheck(7);
$action=SafeRequest("action", "get");
?>
<!DOCTYPE html>
<html>
 <head> 
  <meta charset="gbk" /> 
  <meta name="renderer" content="webkit" /> 
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" /> 
  <title>x5Music 后台管理中心 -x5mp3.com</title> 
  <link href="../css/add.css" rel="stylesheet" /> 
  <style type="text/css">
.table2{width: 100%;max-width: 100%;border-collapse: collapse;border-spacing: 0;}
.table2 td{font-size: 12px;border-top: 1px solid #ddd;padding: 8px;vertical-align: top;}
.table2 tr:hover{text-decoration: none;background-color: #e6f2fb;}
.table2 th{border-bottom: 2px solid #ddd;vertical-align: bottom;padding: 2px;text-align: left;}
.table3{width: 100%;max-width: 100%;border-collapse: collapse;border-spacing: 0;}
.table3 tr:hover{text-decoration: none;background-color: #e6f2fb;}
.table3 td{font-size: 12px;line-height:25px;border-top: 1px solid #ddd;padding: 5px;vertical-align: top;border-right: solid 1px #ddd;}
.table3 th{border-bottom: 2px solid #ddd;vertical-align: bottom;padding: 2px;text-align: left;}
</style> 
 </head> 
 <body> 
<?php
switch($action) {
	case 'add':
		Add();
		break;
	case 'temp':
		Temp();
		break;
	case 'edit':
		Edit();
		break;
	case 'del':
		Del();
		break;
	case 'templist':
		session_destroy();
		TempList();
		break;
	case 'save':
		Save();
		break;
	case 'delfile':
		DelFile();
		break;
	case 'copyfile':
		CopyFile();
		break;
	default:
		main();
		break;
}
?>
 </body>
</html>
<?php
Function TempList() {
	if(trim(SafeRequest("type", "get"))<>"") {
		if(file_exists("../user/" . trim(SafeRequest("type", "get")))) {
			include("../user/" . trim(SafeRequest("type", "get")));
		}
	}

	if(trim(SafeRequest("file", "get"))<>"") {
		$path=SafeRequest("path", "get");
		if(file_exists($path . trim(SafeRequest("file", "get"))) && is_file($path . trim(SafeRequest("file", "get")))) {
?>
<?php
$str=$_GET['path'];
$in='skin';
if(strpos($str, $in)!==false) {
} else {
	showmessage("出错了，越权操作！", "skin.php", 1);
}
?>
  <div class="contents"> 
   <div class="panel"> 
    <div class="panel-head"> 
     <strong>编辑模板文件：<?php echo SafeRequest("file","get");?></strong> 
    </div> 
    <form action="?action=save" method="post"> 
     <table width="100%" class="table2"> 
      <tbody>
       <tr>
        <td>模版文件名：<input type="text" name="FileName" class="input length_3" value="<?php echo SafeRequest("file","get");?>" />&nbsp;&nbsp;只支持 .htm .html .shtml .js .txt 格式编辑</td>
       </tr> 
       <tr> 
        <td> <textarea name="content" style="width:99%;height:350px;font-family: Arial, Helvetica, sans-serif;font-size: 14px;"><?php echo file_get_contents($path.trim(SafeRequest("file","get")))?></textarea> </td> 
       </tr> 
       <input type="hidden" name="folder" value="<?php echo $path?>" /> 
       <input name="tempname" type="hidden" value="<?php echo SafeRequest("tempname","get")?>"/>
       <tr> 
        <td height="35" colspan="2" align="left" bgcolor="#FAFBF7" class="td_border"> <button type="submit" id="adddance" class="btn btn_submit J_ajax_submit_btn">确定提交</button> <button class="btn J_ajax_submit_btn" type="button" onclick="window.location='javascript:history.go(-1);'">返回上一页</button> </td> 
       </tr> 
      </tbody>
     </table> 
    </form> 
   </div> 
  </div>
<?php			
}
}
else
{
if(trim($_GET['dir'])<>"")
{
$path = trim($_GET['dir']);
}
else
{
$path = getcwd();
}
//$_SESSION['dir'] = $path;
?>
<?php
$str=$_GET['dir'];
$in='skin';
if(strpos($str, $in) !== false){}else{
showmessage("出错了，越权操作！","skin.php",1);	
}
?>
  <div class="contents"> 
   <div class="panel"> 
    <div class="panel-head">
     <strong>模板详情：<?php echo $_GET["dir"]; ?></strong>
    </div>
    <table class="table2" id="dellist"> 
     <tbody>
      <tr>
       <td align="left">模版文件名</td>
       <td align="left">模板类型</td>
	   <td align="left">文件大小</td>
	   <td align="left">修改时间</td>
	   <td width="100" align="left">操作</td>
      </tr>
<?php		
if(file_exists($path) && is_dir($path)) {
	$d=dir($path);
	while(false!==($v=$d->read())) {
		if($v=="." || $v=="..") {
			continue;
		}
		$file=$d->path . "\\" . $v;
		if(is_dir($file)) {
			//echo "<a href='?action=templist&path=".$path."&dir=$v'>[$v]</a><br />";
		}
	}
	$d->rewind();
?>

<?php		
while(false!==($v=$d->read())) {
	if($v=="." || $v=="..") {
		continue;
	}
	$file=$d->path . $v;
	if(is_dir($file)) {
		continue;
	}
	if(is_file($file)) {
?>
      <tr>
       <td><a href="?action=templist&path=<?php echo $path?>&tempname=<?php echo SafeRequest("tempname","get");?>&file=<?php echo $v?>"><?php echo $v?></a></td>
       <td><?php echo getTemplateType($v);?></td>
       <td><?php echo round(filesize($file)/1204,2)."Kb"?></td>
       <td><?php echo date('Y-m-d H:i:s',filemtime($file))?></td>
       <td><a href="?action=templist&path=<?php echo $path?>&tempname=<?php echo SafeRequest("tempname","get");?>&file=<?php echo $v?>" class="btn">编辑</a></td>
      </tr>
<?php					
}
}
?>
     </tbody>
    </table> 
   </div> 
   </div> 
<?php	
$d->close();
}
}
}
?>
<?php
function main(){
global $db;
$sql="select * from ".tname('mold')."";
$result=$db->query($sql);
$tempcount=$db->num_rows($result);
?>
  <div class="contents"> 
   <div class="panel"> 
    <div class="panel-head">
     <strong>模板管理</strong>
    </div> 
    <table class="table2" id="dellist"> 
     <tbody>
      <tr>
       <td width="100" align="left">模版名称</td>
       <td width="200" align="left">模版路径</td>
	   <td width="100" align="left">设置</td>
	   <td align="left">操作</td>
      </tr>
<?php
if($tempcount=="0"){
?>
<tr><td height='30' colspan='9' align='center' bgcolor='#FFFFFF' class='td_border'><br><br>没有数据<br><br><br></td></tr>
<?php	
}
else{
if($result){
while($row=$db->fetch_array($result)){
?>
<form name="form1" action="?action=edit" method="post">
      <tr>
       <td align="left"><input name="CD_Name" type="text" class="input length_3 mr5" value="<?php echo $row['CD_Name']?>"></td>
       <td align="left"><input type="hidden" name="CD_ID" value="<?php echo $row['CD_ID']?>"><input id="CD_TempPath<?php echo $row['CD_ID']?>" name="CD_TempPath" type="text" class="input length_4" value="<?php echo $row['CD_TempPath']?>"></td>
	   <td align="left"><input type="button"<?php if(cd_templateurl==$row['CD_TempPath']){?> value="默认" disabled <?php }else{ ?> value="设为默认" <?php } ?>onclick="javacript:window.location.href='?action=temp&path=<?php echo $row['CD_TempPath']?>'" class="btn"/></td>
	   <td align="left">
	   <input type="submit" class="btn" value="修改">
	   <input type="button" class="btn" value="管理" onClick="javascript:window.location='?action=templist&tempname=<?php echo $row['CD_Name']?>&dir=../../<?php echo $row['CD_TempPath']?>'">
	   <input type="button"<?php if(cd_templateurl==$row['CD_TempPath']){?> value="删除" disabled <?php }else{ ?> value="删除" <?php } ?>onclick="javacript:window.location.href='?action=del&id=<?php echo $row['CD_ID']?>';" class="btn">
	   </td>
      </tr>	 
</form>	  
<?php
}		
}
}	
?>	  	  
     </tbody>
    </table> 
   </div> 
   </div> 
<!--新增模板-->
  <div class="contents"> 
   <div class="panel"> 
    <div class="panel-head">
     <strong>新建模板</strong>
    </div> 
<form action="?action=add" method="post">
<table width="100%"  border="0" cellpadding="0" cellspacing="0"  class="tb_style">
<tr>
<td height="50">&nbsp;模板名称：&nbsp;<input type="text" class="input length_3 mr5" name="CD_Name" id="CD_Name">
&nbsp;新模板路径：<input type="text" name="CD_TempPath" id="CD_TempPath" class="input length_3 mr5"> 如：skin/index/html/
<input type="submit" value="添加模板方案" class="btn btn_submit J_ajax_submit_btn mr10" id="addtemp">
</td>
</tr>
</table>
</form>
   </div> 
   </div> 	
<?php
}
//编辑模板文件
Function Save() {
	$CD_Name=$_POST["FileName"];
	$CD_Path=$_POST["folder"];
	$CD_TempName=$_POST["tempname"];
	$CD_Content=stripslashes($_POST["content"]);
	//下面开始过滤asp,php等危险格式的文件
	$F_Ext=substr(strrchr($CD_Name, '.'), 1);
	$FileType=strtolower($F_Ext);
	if($FileType=="htm" or $FileType=="html" or $FileType=="shtml" or $FileType=="js" or $FileType=="css" or $FileType=="txt") {
		if(!$fp=fopen($CD_Path . $CD_Name, 'w')) {
			showmessage("出错了，文件 " . $CD_Path . $CD_Name . " 没有写入权限！", "?action=templist&tempname=" . $CD_TempName . "&dir=" . $CD_Path . "", 1);
		}
		$ifile=new iFile($CD_Path . $CD_Name, 'w');
		$ifile->WriteFile($CD_Content, 3);
		showmessage("恭喜您，编辑模板文件成功！", "?action=templist&tempname=" . $CD_TempName . "&dir=" . $CD_Path . "", 0);
	} else {
		showmessage("出错了，操作已被禁止！", "?action=templist&tempname=" . $CD_TempName . "&dir=" . $CD_Path . "", 1);
	}
}

//删除模板方案
Function Del() {
	global $db;
	$CD_ID=SafeRequest("id", "get");
	$sql="delete from " . tname('mold') . " where CD_ID='" . $CD_ID . "'";
	if($db->query($sql)) {
		showmessage("模板已经删除！", "skin.php", 0);
	}
}

//编辑模板方案
Function Edit() {
	global $db;
	$CD_ID=SafeRequest("CD_ID", "post");
	$CD_Name=SafeRequest("CD_Name", "post");
	$CD_TempPath=SafeRequest("CD_TempPath", "post");
	$sql="update " . tname('mold') . " set CD_Name='" . $CD_Name . "',CD_TempPath='" . $CD_TempPath . "' where CD_ID=" . $CD_ID . "";
	//下面开始替换.asp成.php
	if(file_exists("../../" . $CD_TempPath)) {
		$TempArr=array(
			"index.html",
			"head.html",
			"bottom.html",
			"play.html",
			"list.html",
			"search.html"
		);
		for($i=0; $i<count($TempArr); $i++) {
			if(file_exists("../../" . $CD_TempPath . $TempArr[$i])) {
				$Content=file_get_contents("../../" . $CD_TempPath . $TempArr[$i]);
				$Content=ReplaceStr($Content, ".asp", ".php");
				fwrite(fopen("../../" . $CD_TempPath . $TempArr[$i], "wb"), $Content);
			}
		}
		unset($TempArr);
	} else {
		showmessage("出错了，模板文件夹不存在！", "skin.php", 1);
	}

	if($db->query($sql)) {
		showmessage("恭喜您，模板编辑成功！", "skin.php", 0);
	} else {
		showmessage("出错了，模板编辑失败！~~~~~~", "skin.php", 1);
	}
}

//添加模板方案
Function Add() {
	global $db;
	$CD_Name=SafeRequest("CD_Name", "post");
	$CD_TempPath=SafeRequest("CD_TempPath", "post");
	$sql="Insert " . tname('mold') . " (CD_Name,CD_tempPath,CD_TheOrder) values ('" . $CD_Name . "','" . $CD_TempPath . "',0)";
	//die($sql);
	//下面开始替换.asp成.php
	if(file_exists("../../" . $CD_TempPath)) {
		$TempArr=array(
			"index.html",
			"head.html",
			"bottom.html",
			"play.html",
			"list.html",
			"search.html"
		);
		for($i=0; $i<count($TempArr); $i++) {
			if(file_exists("../../" . $CD_TempPath . $TempArr[$i])) {
				$Content=file_get_contents("../../" . $CD_TempPath . $TempArr[$i]);
				$Content=ReplaceStr($Content, ".asp", ".php");
				fwrite(fopen("../../" . $CD_TempPath . $TempArr[$i], "wb"), $Content);
			}
		}
		unset($TempArr);
	} else {
		showmessage("出错了，模板文件夹不存在！", "skin.php", 1);
	}
	if($db->query($sql)) {
		showmessage("恭喜您，模板添加成功！", "skin.php", 0);
	} else {
		showmessage("出错了，模板添加失败！", "skin.php", 1);
	}
}

//切换模板方案
Function Temp() {
	$CD_Path=SafeRequest("path", "get");
	$str=file_get_contents("../../include/x5music.config.php");
	$str=preg_replace('/"cd_templateurl","(.*?)"/', '"cd_templateurl","' . $CD_Path . '"', $str);
	if(!$fp=fopen('../../include/x5music.config.php', 'w')) {
		showmessage("出错了，文件 ../../include/x5music.config.php 没有写入权限！", "skin.php", 1);
	}
	$ifile=new iFile('../../include/x5music.config.php', 'w');
	$ifile->WriteFile($str, 3);
	showmessage("恭喜您，模板切换成功", "skin.php", 0);
}

Function getTemplateType($filename) {
	switch(strtolower($filename)) {
	case 'index.html':
		$getTemplateType="首页";
		break;
	case "head.html":
		$getTemplateType="顶部";
		break;
	case "bottom.html":
		$getTemplateType="底部";
		break;
	case "down.html":
		$getTemplateType="下载页面";
		break;
	case "list.html":
		$getTemplateType="分类列表";
		break;
	case "list_mv.html":
		$getTemplateType="MV分类列表";
		break;
	case "lplayer.html":
		$getTemplateType="连播页面";
		break;
	case "mvplay.html":
		$getTemplateType="视频播放页面";
		break;
	case "news.html":
		$getTemplateType="文章内容页面";
		break;
	case "newslist.html":
		$getTemplateType="文章分类页面";
		break;
	case "play.html":
		$getTemplateType="音乐播放页面";
		break;
	case "album_time.html":
		$getTemplateType="专辑列表页面";
		break;
	case "rank_day.html":
		$getTemplateType="今日最新";
		break;
	case "rank_week.html":
		$getTemplateType="本周排行";
		break;
	case "rank_month.html":
		$getTemplateType="本月排行";
		break;
	case "rank_hits.html":
		$getTemplateType="总人气排行";
		break;
	case "rank_time.html":
		$getTemplateType="最新排行";
		break;
	case "rank_good.html":
		$getTemplateType="顶歌排行";
		break;
	case "rank_bad.html":
		$getTemplateType="踩歌排行";
		break;
	case "rank_stars.html":
		$getTemplateType="推荐榜单";
		break;
	case "search.html":
		$getTemplateType="搜索页面";
		break;
	case "special.html":
		$getTemplateType="专辑播放页面";
		break;
		default:
			if(stristr($filename, '.gif') or stristr($filename, '.jpg') or stristr($filename, '.png')) {
				$getTemplateType="图片文件";
			} elseif(stristr($filename, '.css')) {
				$getTemplateType="样式文件";
			} elseif(stristr($filename, '.js')) {
				$getTemplateType="脚本文件";
			} else {
				$getTemplateType="其它文件";
			}
	}
	return $getTemplateType;
}
?>