﻿//弹出提示
function alertText(pNum){
	$("#notice").remove();
    $("body").append('<div id="notice">'+pNum+'</div>')
    $("#notice").fadeOut(2500,function(){$(this).remove();}); 
}
function playerAlert(pNum){
	$("#playerNotice").remove();
    $("body").append('<div id="playerNotice">'+pNum+'</div>')
	$("#playerNotice").css("bottom",$("#cproIframe1Wrap").height()+55);

}

//搜索
$(function(){
	$(".seacrh .soClose").click(function(){
		$(".soText").val("").focus();
	});
	$(".soBtn").click(function(){
		var txt=$(".soText").val();
		if(txt==""||txt=="请输入关键词"){
			alertText("请输入搜索关键词");
			return false;
		}else{
			$(".seacrhTui").hide();
			$(".seacrhBox").show();
		}
	});
});
//标记默认的标题名字
$(function(){
	$(".topTit").attr("defaulttit",$(".topTit").html());
	$(".topBack").attr("defaultback",$(".topBack").attr("href"));
});
//导航悬浮
$(function(){
	if($(".mainNav").length>0){
		var mainNavTop=$(".mainNav").offset().top
       $(window).scroll(function(){
			if($(window).scrollTop()>=mainNavTop){
				$(".mainNav").addClass("mainNavFix");
			}else{
				$(".mainNav").removeClass("mainNavFix");
			} 
       });
  } 
})

//返回顶部按钮
$(function() {
	$("body").append('<div id="backTop"></div>')
		$(window).scroll(function(){
			if ($(window).scrollTop()>100){
			$("#backTop").fadeIn(600);
			}else{
			$("#backTop").fadeOut(600).hide();
			}
		});
		$("#backTop").click(function(){
			$('body,html').animate({scrollTop:0},300);
			return false;
		});
});
//歌手按字母筛选
$(function() {
	$("#lettersHd").click(function(){
		$("#lettersBd").toggle();
		})
});
//返回上一级
$(function(){
	var ref=document.referrer.toLowerCase();
	if(ref.indexOf("9ku.com")!=-1 || ref.indexOf("jiuku.com")!=-1){
		if(ref.indexOf("fuyin/jiu")==-1 && ref.indexOf("fuyin/xin")==-1){
			var goback="javascript:window.history.go(-1);";
			$(".topBack").attr("href",goback);
		}
	}	
});