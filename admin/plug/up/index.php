<?php
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
error_reporting(0);
include '../../../include/x5music.conn.php';
include "../../function_common.php";
admincheck(6);
include "../../../plug/up/conf.inc.php";
$action=SafeRequest("action", "get");
$ac=SafeRequest("ac", "get");
if($action=="config") {
	$width=SafeRequest("width", "post");
	$height=SafeRequest("height", "post");
	$top=SafeRequest("top", "post");
	$tipsmusic=SafeRequest("tipsmusic", "post");
	$tipsvideo=SafeRequest("tipsvideo", "post");
	$strs="<?php" . "\n";
	$strs=$strs . "define(\"upwidth\",\"" . $width . "\");\n";
	$strs=$strs . "define(\"upheight\",\"" . $height . "\");\n";
	$strs=$strs . "define(\"uptop\",\"" . $top . "\");\n";
	$strs=$strs . "define(\"tipsmusic\",\"" . $tipsmusic . "\");\n";
	$strs=$strs . "define(\"tipsvideo\",\"" . $tipsvideo . "\");\n";
	$strs=$strs . "?>";
	if(!$fp=fopen('../../../plug/up/conf.inc.php', 'w')) {
		showmessage("出错了，文件 ../../../plug/up/conf.inc.php 没有写入权限！", $_SERVER['HTTP_REFERER'], 0);
	}
	$ifile=new iFile('../../../plug/up/conf.inc.php', 'w');
	$ifile->WriteFile($strs, 3);
	showmessage("恭喜您，保存设置成功！", $_SERVER['HTTP_REFERER'], 0);
} elseif($action=="configmusic") {
	if(!$fp=fopen('../../../plug/up/musicmub.txt', 'w')) {
		showmessage('文件 ../../../plug/up/musicmub.txt 读写权限设置错误，请设置为可写后再执行！', $_SERVER['HTTP_REFERER'], 0);
	} else {
		$yijianvideourl=SafeRequest("yijianvideourl", "post");
		$yijianvideourl=str_replace('\\','',$yijianvideourl);
		fwrite(fopen('../../../plug/up/musicmub.txt', 'wb'), $yijianvideourl);
		showmessage("恭喜您，保存设置成功！", $_SERVER['HTTP_REFERER'], 0);
	}
} elseif($action=="configvideo") {
	if(!$fp=fopen('../../../plug/up/videomub.txt', 'w')) {
		showmessage('文件 ../../../plug/up/videomub.txt 读写权限设置错误，请设置为可写后再执行！', $_SERVER['HTTP_REFERER'], 0);
	} else {
		$yijianvideourl=SafeRequest("yijianvideourl", "post");
		$yijianvideourl=str_replace('\\','',$yijianvideourl);
		fwrite(fopen('../../../plug/up/videomub.txt', 'wb'), $yijianvideourl);
		showmessage("恭喜您，保存设置成功！", $_SERVER['HTTP_REFERER'], 0);
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head> 
  <meta http-equiv="Content-Type" content="text/html; charset=gb2312" /> 
  <meta name="renderer" content="webkit" /> 
  <title>x5Music 管理后台</title> 
  <link rel="stylesheet" href="../../css/add.css" type="text/css" media="screen" /> 
<style type="text/css">
.table2{width: 100%;max-width: 100%;border-collapse: collapse;border-spacing: 0;}
.table2 td{font-size: 12px;border-top: 1px solid #ddd;padding: 8px;vertical-align: top;}
.table2 tr:hover{text-decoration: none;background-color: #e6f2fb;}
.table2 th{border-bottom: 2px solid #ddd;vertical-align: bottom;padding: 2px;text-align: left;}
.table3{width: 100%;max-width: 100%;border-collapse: collapse;border-spacing: 0;}
.table3 tr:hover{text-decoration: none;background-color: #e6f2fb;}
.table3 td{font-size: 12px;line-height:25px;border-top: 1px solid #ddd;padding: 5px;vertical-align: top;border-right: solid 1px #ddd;}
.table3 th{border-bottom: 2px solid #ddd;vertical-align: bottom;padding: 2px;text-align: left;}
</style>
 </head> 
 <body> 
  <div class="contents"> 
   <div class="panel"> 
    <div class="panel-head">
     <strong>框架管理的是音乐分享和视频分享页面的上传按钮内容</strong>
    </div> 
    <form action="?action=config" method="post" name="form2" >
    <table class="table3" id="dellist"> 
     <tbody>
	 <tr>
       <td width="90" align="left">框架位置调整</td>
       <td align="left">
	   宽：<input id="width" name="width" value="<?php echo upwidth;?>" type="text" class="input input_hd length_1">
	   高：<input id="height" name="height" value="<?php echo upheight;?>" type="text" class="input input_hd length_1">
	   顶部距离：<input id="top" name="top" value="<?php echo uptop;?>" type="text" class="input input_hd length_1">
	   </td>
      </tr>
	 <tr>
       <td align="left">音乐提示内容</td>
       <td align="left">
	   <input id="tipsmusic" name="tipsmusic" value="<?php echo tipsmusic?>" type="text" class="input input_hd length_6">
	   </td>
      </tr>
	  <tr>
       <td align="left">视频提示内容</td>
       <td align="left">
	   <input id="tipsvideo" name="tipsvideo" value="<?php echo tipsvideo?>" type="text" class="input input_hd length_6">
	   </td>
      </tr>
      <tr>
       <td height="35" colspan="2" align="left" bgcolor="#FAFBF7" class="td_border"><button type="submit" id="adddance" class="btn btn_submit J_ajax_submit_btn" style="margin-left: 80px;">保存设置</button></td>
      </tr>
     </tbody>
    </table> 
	</form>
   </div> 
<br />
   <div class="panel"> 
    <div class="panel-head">
     <strong>分享音乐页面：上传音乐URL按钮地址,一行一个。</strong>
    </div> 
    <form action="?action=configmusic" method="post" name="form2" >
    <table class="table3" id="dellist"> 
     <tbody>
      <tr>
       <td width="120" align="left">按钮颜色：<br>btn btn-warning<br>btn btn-success<br>btn btn-info<br>btn btn-danger<br>btn btn-default</td>
       <td align="left"><textarea class="length_6" name="yijianvideourl" style="height: 200px;width:500px;line-height:25px;font-size:14px;"><?php echo file_get_contents('../../../plug/up/musicmub.txt')?></textarea></td>
      </tr>
      <tr>
       <td align="left">友情提醒</td>
       <td align="left">添加地址请带上服务器序号：?rServer=　数字是服务器的序号</td>
      </tr>
      <tr>
       <td height="35" colspan="2" align="left" bgcolor="#FAFBF7" class="td_border"><button type="submit" id="adddance" class="btn btn_submit J_ajax_submit_btn" style="margin-left: 80px;">保存设置</button></td>
      </tr>
     </tbody>
    </table> 
	</form>
   </div>
<br />
   <div class="panel"> 
    <div class="panel-head">
     <strong>分享视频页面：上传视频URL按钮地址,一行一个。</strong>
    </div> 
    <form action="?action=configvideo" method="post" name="form2" >
    <table class="table3" id="dellist"> 
     <tbody>
      <tr>
       <td width="120" align="left">按钮颜色：<br>btn btn-warning<br>btn btn-success<br>btn btn-info<br>btn btn-danger<br>btn btn-default</td>
       <td align="left"><textarea class="length_6" name="yijianvideourl" style="height: 200px;width:500px;line-height:25px;font-size:14px;"><?php echo file_get_contents('../../../plug/up/videomub.txt')?></textarea></td>
      </tr>
      <tr>
       <td align="left">友情提醒</td>
       <td align="left">添加地址请带上服务器序号：?rServer=　数字是服务器的序号</td>
      </tr>
      <tr>
       <td height="35" colspan="2" align="left" bgcolor="#FAFBF7" class="td_border"><button type="submit" id="adddance" class="btn btn_submit J_ajax_submit_btn" style="margin-left: 80px;">保存设置</button></td>
      </tr>
     </tbody>
    </table> 
	</form>
   </div>
  </div>  
 </body>
</html>