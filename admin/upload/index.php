<?php
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
error_reporting(0);
if (!eregi($_SERVER['SERVER_NAME'], $_SERVER['HTTP_REFERER'])) {
	exit();
}
include "../../include/x5music.conn.php";
$f=SafeRequest("f", "get");
$action=SafeRequest("ac", "get");
$randdir=rand(2, pow(2, 24));
$time=date('YmdHis', time());
$randnum=$time . $randdir;
switch ($action) {
	case 'malbum':
		$targetFiles="upload/special/";
		$m=cd_webpath . "upload/malbum";
		$fileexts=cd_uppictype;
		break;
	case 'dancepic':
		$targetFiles="upload/dancepic/";
		$m=cd_webpath . "upload/dancepic";
		$fileexts=cd_uppictype;
		break;
	case 'videopic':
		$targetFiles="upload/videopic/";
		$m=cd_webpath . "upload/videopic";
		$fileexts=cd_uppictype;
		break;
	case 'dance':
		$targetFiles="upload/url/";
		$m=cd_webpath . "upload/url";
		$fileexts=cd_upmusictype;
		break;
	case 'video':
		$targetFiles="upload/video/";
		$m=cd_webpath . "upload/video";
		$fileexts=cd_upvideotype;
		break;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<title>本地上传</title>
<link href="uploadify.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="swfobject.js"></script>
<script type="text/javascript" src="jquery.uploadify.v2.1.0.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#uploadify").uploadify({
		'uploader' : 'uploadify.swf?<?php echo date('YmdHis');?>',
		'script' : 'uploads.php',
		'cancelImg' : 'cancel.png',
		'folder' : 'UploadFile',
		'method' : 'GET',
		'scriptData' : {'is':'<?php echo $randdir; ?>','id':'<?php echo $randnum; ?>','ac':'<?php echo $action;?>'},
		'buttonText' : 'Upload',
		'buttonImg' : 'up.png',
		'width' : '110',
		'height' : '30',
		'queueID' : 'fileQueue',
		'auto' : true,
		'multi' : false,
		'fileExt' : '<?php echo $fileexts;?>',
		'fileDesc' : '请选择<?php echo $fileexts;?>文件',
		'sizeLimit' : 0,
		'onError' : function (a, b, c, d) {
			if (d.status == 404){
				ReturnError("上传出错，请重试！");
			}else if (d.type === "HTTP"){
				ReturnError("error "+d.type+" : "+d.status);
			}else{
				ReturnError("error "+d.type+" : "+d.text);
			}
		},
		'onComplete' : function (event, queueID, fileObj, response, data) {
			if (response == 1){
				alert("请上传被允许的文件！");
			}else if (response == 2){
				alert("<?php echo $m?>文件夹不存在！");
			}else if (response == 3){
				alert("<?php echo $m?>文件夹没有读写权限！");
			}else if (response == 0){
				alert("恭喜您，文件上传成功。");
				ReturnValue('<?php echo $targetFiles."".$randnum; ?>'+fileObj.type);
			}
		}
	});
});
</script>
<?php if($action=="video"){?>
<script type="text/javascript">
function ReturnValue(reimg){
	this.parent.document.<?php echo $f;?>.value=reimg;
this.parent.document.form2.CD_DownUrl.value=reimg;
this.parent.document.form2.CD_Md5.value="<?php echo $randnum;?>";
this.parent.document.form2.CD_From.value="local";
this.parent.document.form2.CD_Server.value="1";
this.parent.document.form2.CD_Uid.value="<?php echo $randnum;?>";
this.parent.document.form2.CD_Siz.value="<?php echo "".rand(1,9).".".rand(1,9).rand(1,9)." MB"?>";
	this.parent.layer.closeAll();
}
</script>
<?php }elseif($action=="dance"){?>
<script type="text/javascript">
function ReturnValue(reimg){
	this.parent.document.<?php echo $f;?>.value=reimg;
this.parent.document.form2.CD_DownUrl.value=reimg;
this.parent.document.form2.CD_Md5.value="<?php echo $randnum;?>";
this.parent.document.form2.CD_From.value="local";
this.parent.document.form2.CD_Server.value="1";
this.parent.document.form2.CD_Uid.value="<?php echo $randnum;?>";
this.parent.document.form2.CD_Siz.value="<?php echo "".rand(1,9).".".rand(1,9).rand(1,9)." MB"?>";
	this.parent.layer.closeAll();
}
</script>
<?php }else{?>
<script type="text/javascript">
function ReturnValue(reimg){
	this.parent.document.<?php echo $f;?>.value=reimg;
	this.parent.layer.closeAll();
}
</script>
<?php }?>
</head>
<body>
<div id="fileQueue"></div>
<input type="file" name="uploadify" id="uploadify" />
</body>
</html>