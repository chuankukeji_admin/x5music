<?php
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
error_reporting(0);
include '../include/x5music.conn.php';
include "function_common.php";
admincheck(1);
global $db;
$row=$db->getrow("Select * from " . tname('system') . "");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
 <head> 
  <meta charset="gbk" /> 
  <meta name="renderer" content="webkit" /> 
  <title>x5Music 后台管理中心 -x5mp3.com</title> 
  <link href="css/index.css" rel="stylesheet" /> 
  <script language="javascript" src="js/jquery.min.js"></script> 
  <style>
.fullScreen .content th{display:none;width:0;}
.fullScreen .head,.fullScreen .tab{height:0;display:none;}
.fullScreen #default{*left:0;*top:-90px;}
.fullScreen div.options{top:0;}
</style> 
<script type="text/javascript">
function killErrors() {
    return true;
}
window.onerror = killErrors;
//全屏/非全屏
var isiPad = navigator.userAgent.match(/iPad|iPhone|iPod/i); //苹果浏览器
var isAndroid = navigator.userAgent.match(/Android/i); //安卓浏览器
var TheWorld = navigator.userAgent.indexOf("TheWorld") > 0; //世界之窗浏览器
var Firefox = navigator.userAgent.indexOf("Firefox") > 0; //火狐浏览器
var Chrome = navigator.userAgent.indexOf("Chrome") > 0; //谷歌浏览器
function quanpin(_id) {
    if (_id == 1) {
        document.getElementById('B_top').style.display = 'none';
        document.getElementById('B_menunav').style.display = 'none';
        document.getElementById('B_route').style.display = 'none';
        document.getElementById('B_routes').style.display = 'none';
        document.getElementById('B_routea').style.display = 'none';
        if (!isiPad && !isAndroid && !TheWorld && !Firefox && !Chrome) {
            document.getElementById('B_TH').style.display = '';
        }
        document.getElementById("qp").innerHTML = '<a href="javascript:;" onclick="quanpin(2);" class="full_screen" title="退出全屏">退出全屏</a>';
    } else {
        document.getElementById('B_top').style.display = '';
        document.getElementById('B_menunav').style.display = '';
        document.getElementById('B_route').style.display = '';
        document.getElementById('B_routes').style.display = '';
        document.getElementById('B_routea').style.display = '';
        if (!isiPad && !isAndroid && !TheWorld && !Firefox && !Chrome) {
            document.getElementById('B_TH').style.display = 'none';
        }
        document.getElementById("qp").innerHTML = '<a href="javascript:;" onclick="quanpin(1);" class="full_screen" title="全屏">全屏</a>';
    }
}
//导航转换
function menu(n) {
    for (var i = 1; i <= 12; i++) {
        var curC = document.getElementById('left' + i);
        var curB = document.getElementById('menu' + i);
        if (n == i) {
            curC.style.display = "block";
            curB.className = "current";
        } else {
            curC.style.display = "none";
            curB.className = "";
        }
    }
}
$(document).ready(function() {
    $('dt').click(function(e) {
        $('dt').removeClass('current');
        $(this).addClass('current');
    });
});
</script> 
 </head> 
 <body> 
  <div class="wrap"> 
   <table width="100%" height="100%" style="table-layout:fixed;"> 
    <tbody> 
     <tr class="head" id="B_top"> 
      <th></th> 
      <td> 
       <div class="nav"> 
        <ul> 
         <li id="menu1" class="current"><a href="config/index.php" target="main" class="icon-home" onclick="menu(1);"> 开始</a></li> 
         <li id="menu2"><a href="config.php" class="icon-cog" target="main" onclick="menu(2);"> 全局</a></li> 
         <li id="menu3"><a href="dance.php" class="icon-music" target="main" onclick="menu(3);"> 内容</a> </li> 
         <li id="menu4"><a href="user/user.php" class="icon-user" target="main" onclick="menu(4);"> 会员</a></li> 
         <li id="menu5"><a href="news.php" class="icon-new" target="main" onclick="menu(5);"> 文章</a></li> 
         <li id="menu6"><a href="plug/index.php" class="icon-plug" target="main" onclick="menu(6);"> 插件</a></li> 
         <li id="menu7"><a href="skin/skin.php" class="icon-skin" target="main" onclick="menu(7);"> 模板</a></li> 
         <li id="menu8"><a href="gg/index.php" class="icon-adgg" target="main" onclick="menu(8);"> 广告</a></li> 
         <li id="menu9"><a href="tool/index.php" class="icon-tool" target="main" onclick="menu(9);"> 工具</a></li> 
         <li><a href="http://www.x5mp3.com/free/skin/1.html" class="icon-addon" target="main"> 应用</a></li> 
        </ul> 
       </div> 
       <div class="login_info"> 
        <a class="btn btn-primary" href="../" target="_blank">前台首页</a> 
        <a class="btn btn-success" href="admin_cache.php" target="main">更新缓存</a> 
        <a class="btn btn-danger" href="admin_login.php?action=logout">注销登录</a> 
       </div></td> 
     </tr> 
     <tr class="route"> 
      <th id="B_route"><a href="admin_index.php" class="logo">管理中心</a></th> 
      <td> 
       <div class="route_bg"> 
        <span><code id="B_routes">管理员：<?php echo $_COOKIE['CD_AdminUserName']?> </code> <a href="javascript:;" onclick="parent.main.location.reload();" id="J_refresh" title="刷新">刷新</a> <code id="qp"><a href="javascript:;" onclick="quanpin(1);" title="全屏">全屏</a></code> </span> 
        <code id="B_routea"><p><a>数据统计:</a>
		音乐：<?php echo $row['cd_djmub'];?> | 
		会员：<?php echo $row['cd_usermub'];?> | 
		<a href="tool/djuser.php"> 更新数据统计</a>
		</p> </code> 
       </div> </td> 
     </tr> 
     <tr class="content"> 
      <th style="overflow:hidden;" id="B_menunav"> 
       <div class="menubar"> 
        <dl class="menu" id="left1"> 
         <dt>
          <a href="plug.php" target="main"> 添加常用操作</a>
         </dt> 
         <?php echo x5music_Com_Plug(file_get_contents('../data/plug.txt'));?>
        </dl> 
        <dl class="menu" id="left2" style="display:none"> 
         <dt><a href="config.php" target="main"> 站点信息</a></dt> 
         <dt><a href="config.php?ac=admin" target="main"> 站长信息</a></dt> 
         <dt><a href="config.php?ac=seo" target="main"> SEO优化</a></dt> 
         <dt><a href="config.php?ac=cachethread" target="main"> 性能优化</a></dt> 
         <dt><a href="config.php?ac=user" target="main"> 会员设置</a></dt> 
         <dt><a href="config.php?ac=upload" target="main"> 上传设置</a></dt> 
         <dt><a href="config.php?ac=dow" target="main"> 本地下载设置</a></dt> 
         <dt><a href="config.php?ac=attachtype" target="main"> 附件类型尺寸</a></dt> 
         <dt><a href="config.php?ac=mobile" target="main"> 手机访问</a></dt> 
        </dl> 
        <dl class="menu" id="left3" style="display:none"> 
		<dt><a href="class.php" target="main"> 分类管理</a></dt> 
         <dt><a href="dance.php" target="main"> 音乐管理</a></dt> 
         <dt><a href="malbum.php" target="main"> 音乐专辑</a></dt> 
         <dt><a href="video.php" target="main"> 视频管理</a></dt> 
         <dt><a href="tool/localftp.php" target="main"> 影音文件扫描</a></dt> 
         <dt><a href="server.php" target="main"> 服务器设置</a></dt> 
        </dl> 
        <dl class="menu" id="left4" style="display:none"> 
         <dt><a href="user/user.php" target="main"> 会员管理</a></dt> 
         <dt><a href="user/admin.php" target="main"> 后台管理员</a></dt> 
        </dl> 
        <dl class="menu" id="left5" style="display:none"> 
         <dt><a href="newsclass.php" target="main"> 栏目管理</a></dt> 
         <dt><a href="news.php" target="main"> 文章管理</a></dt> 
         <dt><a href="news.php?action=newsadd" target="main"> 添加文章</a></dt> 
         <dt><a href="news.php?action=classc&CD_ClassID=1" target="main"> 公告管理</a></dt> 
         <dt><a href="news.php?action=newsggadd&bid=1" target="main"> 添加公告</a></dt> 
        </dl> 
        <dl class="menu" id="left6" style="display:none"> 
         <dt><a href="plug/qqhl/index.php" target="main"> QQ互联登录</a></dt> 
         <dt><a href="plug/up/index.php" target="main"> 上传框架管理</a></dt> 
         <dt><a href="plug/caiji/index.php" target="main"> x5Music采集器</a></dt> 
		 <dt><a href="plug/up_qiniu/index.php" target="main">七牛云储存</a></dt>
		 <dt><a href="plug/up_alioss/index.php" target="main">阿里云OSS</a></dt>
		 <dt><a href="plug/up_julong/index.php" target="main">聚龙网盘</a></dt>
        </dl> 
        <dl class="menu" id="left7" style="display:none"> 
         <dt><a href="skin/skin.php" target="main"> 模板列表</a></dt> 
		 <dt><a href="http://www.x5mp3.com/develop/" target="main"> 模板标签</a></dt> 
        </dl> 
        <dl class="menu" id="left8" style="display:none"> 
         <dt><a href="gg/index.php" target="main"> 横幅广告</a></dt> 
         <dt><a href="gg/index.php?ac=sf" target="main"> 竖幅广告</a></dt> 
         <dt><a href="gg/index.php?ac=jx" target="main"> 矩形广告</a></dt> 
         <dt><a href="gg/index.php?ac=qt" target="main"> 其他广告</a></dt> 
         <dt><a href="gg/index.php?ac=zdy" target="main"> 自定义广告</a></dt> 
        </dl> 
        <dl class="menu" id="left9" style="display:none"> 
         <dt><a href="tool/index.php" target="main"> 数据库备份</a></dt>
         <dt><a href="tool/sql_replace.php" target="main"> 数据批量替换</a></dt> 
         <dt><a href="tool/sql_random.php" target="main"> 数据批量人气</a></dt> 
		 <dt><a href="tool/upload_list.php" target="main"> 文件上传记录</a></dt> 
		 <!--<dt><a href="tool/attachment.php" target="main"> 清理冗余附件</a></dt> -->
		 <dt><a href="tool/link.php" target="main"> 友情链接管理</a></dt> 
        </dl> 
       </div> </th> 
      <th id="B_TH" style="display:none;"></th> 
      <td id="B_frame"> <iframe name="main" id="main" src="config/index.php" style="height: 100%; width: 100%;" frameborder="0" scrolling="auto"></iframe> </td> 
     </tr> 
    </tbody> 
   </table> 
  </div>  
 </body>
</html>