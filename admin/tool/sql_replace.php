<?php
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
error_reporting(0);
include "../../include/x5music.conn.php";
include "../function_common.php";
admincheck(9);
set_time_limit(0);
@ini_set('memory_limit', '-1');
$action=SafeRequest("action","get");
if($action=="replacement"){
	echo "<style type='text/css'><!--body,td,th {background-color: #FFFFFF;font-size: 9pt;}a {font-size: 9pt;}a:link {text-decoration: none;color:#3F628C;}a:visited {text-decoration: none;color:#3F628C;}a:hover {text-decoration: none;color:#3F628C;}a:active {text-decoration: none;color:#3F628C;}--></style>";
	global $db;
	$CD_Table=SafeRequest("CD_Table","post");
	$CD_Rescord=SafeRequest("CD_Rescord","post");
	$Old_Str=SafeRequest("Old_Str","post");
	$New_Str=SafeRequest("New_Str","post");
	if($CD_Rescord==""){
		exit("<script language='javascript'>alert('出错了，请先选择要替换的字段！');</script>");
	}
	if($CD_Table=="dj"){
		$name="音乐表";
		$table=tname('dj');
		$sql="select ".$CD_Rescord.",CD_ID from ".tname('dj')."  where ".$CD_Rescord." like '%".$Old_Str."%'";
	}elseif($CD_Table=="news"){
		$name="文章";
		$table=tname('news');
		$sql="select ".$CD_Rescord.",CD_ID from ".tname('news')."  where ".$CD_Rescord." like '%".$Old_Str."%'";
	}elseif($CD_Table=="user"){
		$name="会员";
		$table=tname('user');
		$sql="select ".$CD_Rescord.",cd_id from ".tname('user')."  where ".$CD_Rescord." like '%".$Old_Str."%'";
	}else{
		exit("<script language='javascript'>alert('参数错误！');</script>");
	}
	$result=$db->query($sql);
	if($result){
		while($row=$db->fetch_array($result)){
			$row[$CD_Rescord]=ReplaceStr($row[$CD_Rescord],$Old_Str,$New_Str);
			if($CD_Table=="user"){
			$sql3="update ".$table." set ".$CD_Rescord."='".$row[$CD_Rescord]."' where cd_id=".$row['cd_id']."";
			}else{
			$sql3="update ".$table." set ".$CD_Rescord."='".$row[$CD_Rescord]."' where CD_ID=".$row['CD_ID']."";	
			}
			$db->query($sql3);
			echo "&nbsp;<font color=red style=font-size:12px;>".$row[$CD_Rescord]."</font> 替换完毕！<br/>";
		}
	}
	exit("<br/>&nbsp;<font color=red style=font-size:12px;>恭喜您，(".$name.")字符已全部替换完毕！</font><script language='javascript'>alert('恭喜您，(".$name.")字符已全部替换完毕！');</script>");
}else{
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head> 
  <meta http-equiv="Content-Type" content="text/html; charset=gb2312" /> 
  <meta name="renderer" content="webkit" /> 
  <title>x5Music 管理后台</title> 
  <link rel="stylesheet" href="../css/add.css" type="text/css" media="screen" /> 
  <style type="text/css">
.table2{width: 100%;max-width: 100%;border-collapse: collapse;border-spacing: 0;}
.table2 td{font-size: 12px;border-top: 1px solid #ddd;padding: 8px;vertical-align: top;}
.table2 tr:hover{text-decoration: none;background-color: #e6f2fb;}
.table2 th{border-bottom: 2px solid #ddd;vertical-align: bottom;padding: 2px;text-align: left;}
.table3{width: 100%;max-width: 100%;border-collapse: collapse;border-spacing: 0;}
.table3 tr:hover{text-decoration: none;background-color: #e6f2fb;}
.table3 td{font-size: 12px;line-height:25px;border-top: 1px solid #ddd;padding: 5px;vertical-align: top;border-right: solid 1px #ddd;}
.table3 th{border-bottom: 2px solid #ddd;vertical-align: bottom;padding: 2px;text-align: left;}
</style> 
 </head> 
 <body> 
  <div class="contents"> 
   <div class="panel"> 
    <div class="panel-head"> 
     <strong>数据批量替换</strong> 
    </div> 
    <table class="table3"> 
     <tbody> 
     <form method="post" name="SpanSel" action="?action=replacement" target="Html_Bottom"> 
      <tr> 
       <td width="60" align="left" class="p10">音乐</td> 
       <td align="left">
       <input name="CD_Table" type="hidden" value="dj" />
       <span id="select_r">
       <select name="CD_Rescord" id="CD_Record" style="width: 164px;">
	   <option value="">请选择字段</option>
       <option value="CD_Name">标题名称(CD_Name)</option>
       <option value="CD_User">所属会员(CD_User)</option>
       <option value="CD_Pic">链接图片(CD_Pic)</option>
       <option value="CD_Url">试听地址(CD_Url)</option>
       <option value="CD_DownUrl">下载地址(CD_DownUrl)</option>
       <option value="CD_Word">简介(CD_Word)</option>
       <option value="CD_Lrc">动态歌词(CD_Lrc)</option>
       </select>
       </span>
       &nbsp;字段中的字符&nbsp;<input type="text" class="input length_3 mr5" id="Old_Str" name="Old_Str" onfocus="this.style.borderColor='#F93'" onblur="this.style.borderColor='#CCC'" />
       &nbsp;替换成&nbsp;<input class="input length_3 mr5" type="text" name="New_Str" id="New_Str" onfocus="this.style.borderColor='#F93'" onblur="this.style.borderColor='#CCC'" />
       &nbsp;<input type="submit" value="确定替换" class="btn mr20" onclick="return CheckForm();" />
	   </td> 
      </tr> 
	 </form>
     <form method="post" name="SpanSel" action="?action=replacement" target="Html_Bottom"> 
      <tr> 
       <td width="60" align="left" class="p10">文章</td> 
       <td align="left">
       <input name="CD_Table" type="hidden" value="news" />
       <span id="select_r">
       <select name="CD_Rescord" id="CD_Record" style="width: 164px;">
	   <option value="">请选择字段</option>
       <option value="CD_Name">文章标题(CD_Name)</option>
	   <option value="CD_Intro">文章内容(CD_Intro)</option>
       </select>
       </span>
       &nbsp;字段中的字符&nbsp;<input type="text" class="input length_3 mr5" id="Old_Str" name="Old_Str" onfocus="this.style.borderColor='#F93'" onblur="this.style.borderColor='#CCC'" />
       &nbsp;替换成&nbsp;<input class="input length_3 mr5" type="text" name="New_Str" id="New_Str" onfocus="this.style.borderColor='#F93'" onblur="this.style.borderColor='#CCC'" />
       &nbsp;<input type="submit" value="确定替换" class="btn mr20" onclick="return CheckForm();" />
	   </td> 
      </tr> 
	 </form>
     <form method="post" name="SpanSel" action="?action=replacement" target="Html_Bottom"> 
      <tr> 
       <td width="60" align="left" class="p10">会员</td> 
       <td align="left">
       <input name="CD_Table" type="hidden" value="user" />
       <span id="select_r">
       <select name="CD_Rescord" id="CD_Record" style="width: 164px;">
	   <option value="">请选择字段</option>
       <option value="cd_name">登录帐号(cd_name)</option>
	   <option value="cd_nicheng">昵称(cd_nicheng)</option>
	   <option value="cd_sign">签名(cd_sign)</option>
	   <option value="cd_sex">性别(cd_sex)</option>
       </select>
       </span>
       &nbsp;字段中的字符&nbsp;<input type="text" class="input length_3 mr5" id="Old_Str" name="Old_Str" onfocus="this.style.borderColor='#F93'" onblur="this.style.borderColor='#CCC'" />
       &nbsp;替换成&nbsp;<input class="input length_3 mr5" type="text" name="New_Str" id="New_Str" onfocus="this.style.borderColor='#F93'" onblur="this.style.borderColor='#CCC'" />
       &nbsp;<input type="submit" value="确定替换" class="btn mr20" onclick="return CheckForm();" />
	   </td> 
      </tr> 
	 </form>
     </tbody>  
    </table> 
   </div> 
  </div> 
  <div class="contents"> 
   <div class="panel"> 
    <div class="panel-head"> 
     <strong>进行状态：<span style="color:#ff0000;">在没看到提示完成的情况下不要刷新或关闭此页面</span></strong> 
    </div> 
    <div id="mdv" style="width:100%;height:200px;"> 
     <iframe name="Html_Bottom" frameborder="0" id="Html_Bottom" width="100%" height="100%"></iframe> 
    </div> 
   </div> 
  </div>  
 </body>
</html>
<?php
}
?>