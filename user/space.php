<?php
error_reporting(0);
include "../include/x5music.conn.php";
include "../include/x5music.inc.php";
ob_start();
//允许动作
$dos = array('reg', 'login', 'lostpasswd', 'assets', 'profile', 'userapp', 'doing', 'top', 'pm', 'friend', 'pay', 'web', 'pic', 'blog', 'supply', 'music', 'upmusic', 'upvideo', 'special', 'feed', 'error');
//获取变量
$id=SafeRequest("id", "get");
$key=SafeRequest("key", "get");
$view=SafeRequest("view", "get");
$orderby=SafeRequest("orderby", "get");
$day=SafeRequest("day", "get");
$error="您访问的Url页面不存在，请检查Url是否正确或包含非法字符！";
if(!IsNumID($day | $id)) {
    exit(include _x5music_root_ . "/include/error/content_error.php");
}
$ac=(!empty($_GET['do']) && in_array($_GET['do'], $dos)) ? $_GET['do'] : 'home';
include "source/function_common.php";
//判断空间状态
if($userlogined==true) {
	if($x5music_com_weblock==0) {
		//检测空间是否已存在
		$cd_ids=$db->Getone("select cd_id from " . tname('web') . " where cd_uid='$x5music_com_userid'");
		if($cd_ids) {
			//更新
			updatetable('user', array(
				'cd_weblock'=>1
			), array(
				'cd_id'=>$cd_ids
			));
		} else {
			//添加
			$setarr=array(
				'cd_uid'=>$x5music_com_userid,
				'cd_uname'=>$x5music_com_username,
				'cd_title'=>'',
				'cd_skin'=>'skin1',
				'cd_hits'=>0,
				'cd_lock'=>0,
				'cd_banner'=>'upload/web/banner.gif',
				'cd_bg'=>'upload/web/bg.gif',
				'cd_addtime'=>time()
			);
			inserttable('web', $setarr, 1);
			$setarrs=array(
				'cd_uid'=>$x5music_com_userid,
				'cd_username'=>$x5music_com_username,
				'cd_icon'=>'web',
				'cd_title'=>'开通了音乐空间',
				'cd_data'=>'',
				'cd_image'=>'',
				'cd_imagelink'=>'',
				'cd_dataid'=>$x5music_com_userid,
				'cd_addtime'=>date('Y-m-d H:i:s')
			);
			inserttable('feed', $setarrs, 1);
			updatetable('user', array(
				'cd_weblock'=>1
			), array(
				'cd_id'=>$x5music_com_userid
			));
		}
	}
	if(cd_djdayhitstime=="yes") {
		$daytime=date('Ymd');
		$newtime_daynewtime=@file_get_contents(_x5music_root_ . "user/data/daynewtime.txt");
		if($newtime_daynewtime!=$daytime) {
			$db->query('update ' . tname('dj') . " set CD_DayHits=0");
			fwrite(fopen(_x5music_root_ . "user/data/daynewtime.txt", "wb"), $daytime);
		}
	}
	if(cd_djweekhitstime=="yes") {
		$daytime=date('w');
		$daytimes=date('Ymdw');
		if($daytime==1) {
			$newtime_weeknewtime=@file_get_contents(_x5music_root_ . "user/data/weeknewtime.txt");
			if($newtime_weeknewtime!==$daytimes) {
				$db->query('update ' . tname('dj') . " set CD_WeekHits=0");
				fwrite(fopen(_x5music_root_ . "user/data/weeknewtime.txt", "wb"), $daytimes);
			}
		}
	}
	if(cd_djmonthhitstime=="yes") {
		$daytime=date('d');
		$daytimes=date('Ymd');
		if($daytime=='01') {
			$newtime_monthnewtime=@file_get_contents(_x5music_root_ . "user/data/monthnewtime.txt");
			if($newtime_monthnewtime!==$daytimes) {
				$db->query('update ' . tname('dj') . " set CD_MonthHits=0");
				fwrite(fopen(_x5music_root_ . "user/data/monthnewtime.txt", "wb"), $daytimes);
			}
		}
	}
if($x5music_com_grade==1) {
	$vipenddate=strtotime($x5music_com_vipenddate) - time();
	if($vipenddate<=0) {
		$db->query("update " . tname('user') . " set cd_grade=0,cd_vipindate='0000-00-00 00:00:00',cd_vipenddate='0000-00-00 00:00:00' where cd_id='$x5music_com_userid'");
		//发消息通知
		$rowlast=$db->getrow("select cd_id from " . tname('user') . " where cd_name='$x5music_username' order by cd_id desc");
		$cd_lastid=$rowlast['cd_id'];
		$setarrs=array(
			'cd_uid'=>0,
			'cd_uname'=>'系统消息',
			'cd_uids'=>$x5music_com_userid,
			'cd_unames'=>$x5music_com_username,
			'cd_title'=>'VIP资格到期',
			'cd_content'=>''.cd_viptxt.'',
			'cd_class'=>1,
			'cd_dela'=>1,
			'cd_delb'=>0,
			'cd_addtime'=>date('Y-m-d H:i:s')
		);
		inserttable('message', $setarrs, 1);
	}
}
}
//清理时间长的动态
$db->query("delete from " . tname('feed') . " where DATEDIFF(DATE(cd_addtime),'" . date('Y-m-d') . "')<=-" . cd_feedday . "");
?>
<?php include_once('source/space_header.php');?>
<?php include_once('source/space_' . $ac . '.php');?>
<?php include_once('source/space_footer.php');?>