<?php
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
error_reporting(0);
include '../../../include/x5music.conn.php';
include "../../function_common.php";
admincheck(6);
$version="v1.0";
include "../../../plug/up/qiniu/conf.inc.php";
$action=SafeRequest("action", "get");
$ac=SafeRequest("ac", "get");
if($action=="config") {
	$qiniu_off=SafeRequest("qiniu_off", "post");
	$qiniu_txt=SafeRequest("qiniu_txt", "post");
	$qiniu_bucket=SafeRequest("qiniu_bucket", "post");
	$qiniu_accesskey=SafeRequest("qiniu_accesskey", "post");
	$qiniu_secretkey=SafeRequest("qiniu_secretkey", "post");
	$qiniu_Server=SafeRequest("qiniu_Server", "post");
	$strs="<?php" . "\n";
	$strs=$strs . "define(\"qiniu_off\",\"" . $qiniu_off . "\");\n";
	$strs=$strs . "define(\"qiniu_txt\",\"" . $qiniu_txt . "\");\n";
	$strs=$strs . "define(\"qiniu_bucket\",\"" . $qiniu_bucket . "\");\n";
	$strs=$strs . "define(\"qiniu_accesskey\",\"" . $qiniu_accesskey . "\");\n";
	$strs=$strs . "define(\"qiniu_secretkey\",\"" . $qiniu_secretkey . "\");\n";
	$strs=$strs . "define(\"qiniu_Server\",\"" . $qiniu_Server . "\");\n";
	$strs=$strs . "?>";
	if(!$fp=fopen('../../../plug/up/qiniu/conf.inc.php', 'w')) {
		showmessage("出错了，文件 /plug/yijianvideo/conf.inc.php 没有写入权限！", $_SERVER['HTTP_REFERER'], 0);
	}
	$ifile=new iFile('../../../plug/up/qiniu/conf.inc.php', 'w');
	$ifile->WriteFile($strs, 3);
	showmessage("恭喜您，保存设置成功！", $_SERVER['HTTP_REFERER'], 0);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head> 
  <meta http-equiv="Content-Type" content="text/html; charset=gb2312" /> 
  <meta name="renderer" content="webkit" /> 
  <title>x5Music 管理后台</title> 
  <link rel="stylesheet" href="../../css/add.css" type="text/css" media="screen" /> 
<style type="text/css">
.table2{width: 100%;max-width: 100%;border-collapse: collapse;border-spacing: 0;}
.table2 td{font-size: 12px;border-top: 1px solid #ddd;padding: 8px;vertical-align: top;}
.table2 tr:hover{text-decoration: none;background-color: #e6f2fb;}
.table2 th{border-bottom: 2px solid #ddd;vertical-align: bottom;padding: 2px;text-align: left;}
.table3{width: 100%;max-width: 100%;border-collapse: collapse;border-spacing: 0;}
.table3 tr:hover{text-decoration: none;background-color: #e6f2fb;}
.table3 td{font-size: 12px;line-height:25px;border-top: 1px solid #ddd;padding: 5px;vertical-align: top;border-right: solid 1px #ddd;}
.table3 th{border-bottom: 2px solid #ddd;vertical-align: bottom;padding: 2px;text-align: left;}
</style>
 </head> 
 <body> 
  <div class="contents"> 
   <div class="panel"> 
    <div class="panel-head">
     <strong>七牛云储存上传</strong>
    </div> 
    <form action="?action=config" method="post" name="form2" >
    <table class="table3" id="dellist"> 
     <tbody>
	 <tr>
       <td width="70" align="left">版本信息</td>
       <td align="left"><?php echo $version;?>　<a href="http://www.x5mp3.com/free/8.html">查看更新</a></td>
      </tr>
	 <tr>
       <td width="90" align="left">是否启用</td>
       <td align="left">
<label class="mr20"><input name="qiniu_off" type="radio" value="yes" <?php if(qiniu_off=="yes"){echo "checked";} ?>>&nbsp;启用&nbsp;</label>
<label class="mr20"><input type="radio" name="qiniu_off" value="no" <?php if(qiniu_off=="no"){echo "checked";} ?>>&nbsp;关闭&nbsp;</label>
	   </td>
      </tr>
      <tr>
       <td align="left">七牛bucket</td>
       <td align="left"><input id="qiniu_bucket" name="qiniu_bucket" value="<?php echo qiniu_bucket;?>" type="text" class="input input_hd length_2">&nbsp;*七牛的空间名称</td>
      </tr>
      <tr>
       <td align="left">七牛AK</td>
       <td align="left"><input id="qiniu_accesskey" name="qiniu_accesskey" value="<?php echo qiniu_accesskey;?>" type="text" class="input input_hd length_5">&nbsp;*AccessKey</td>
      </tr>
      <tr>
       <td align="left">七牛SK</td>
       <td align="left"><input id="qiniu_secretkey" name="qiniu_secretkey" value="<?php echo qiniu_secretkey;?>" type="text" class="input input_hd length_5">&nbsp;*SecretKey</td>
      </tr>
      <tr>
       <td align="left">提示内容</td>
       <td align="left"><input id="qiniu_txt" name="qiniu_txt" value="<?php echo qiniu_txt;?>" type="text" class="input input_hd length_6"></td>
      </tr>
      <tr>
       <td align="left">关联服务器</td>
       <td align="left">
	   <select name="qiniu_Server" id="qiniu_Server">
        <option value="0" style="color:#FF0000;">不设置</option>
<?php
global $db;
$sqlclass="select * from " . tname('server') . "";
$results=$db->query($sqlclass);
if($results) {
	while($row3=$db->fetch_array($results)) {
		if(qiniu_Server==$row3['CD_ID']) {
			echo "        <option value='" . $row3['CD_ID'] . "' selected='selected'>" . $row3['CD_Name'] . "</option>
";
		} else {
			echo "        <option value='" . $row3['CD_ID'] . "' >" . $row3['CD_Name'] . "</option>
";
		}
	}
}
?>
        </select></td>
      </tr>
      <tr>
       <td align="left">友情提醒</td>
       <td align="left">允许上传的格式与大小，请到全局中设置！&nbsp;&nbsp;<a href="https://portal.qiniu.com/setting/key" target="_blank" title="x5mp3.com">查看七牛ASK</a></td>
      </tr>
      <tr>
       <td height="35" colspan="2" align="left" bgcolor="#FAFBF7" class="td_border"><button type="submit" id="adddance" class="btn btn_submit J_ajax_submit_btn" style="margin-left: 80px;">保存设置</button></td>
      </tr>
     </tbody>
    </table> 
	</form>
   </div> 
  </div>  
 </body>
</html>