<?php
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
error_reporting(0);
include "../include/x5music.conn.php";
include "function_common.php";
admincheck(3);
$action=SafeRequest("action", "get");
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="gbk">
<meta name="renderer" content="webkit" /> 
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>x5Music 后台管理中心 -x5mp3.com</title> 
<link href="css/add.css" rel="stylesheet" />
<style type="text/css">
.table2{width: 100%;max-width: 100%;border-collapse: collapse;border-spacing: 0;}
.table2 td{font-size: 12px;border-top: 1px solid #ddd;padding: 8px;vertical-align: top;}
.table2 tr:hover{text-decoration: none;background-color: #e6f2fb;}
.table2 th{border-bottom: 2px solid #ddd;vertical-align: bottom;padding: 2px;text-align: left;}
.table3{width: 100%;max-width: 100%;border-collapse: collapse;border-spacing: 0;}
.table3 tr:hover{text-decoration: none;background-color: #e6f2fb;}
.table3 td{font-size: 12px;line-height:25px;border-top: 1px solid #ddd;padding: 5px;vertical-align: top;border-right: solid 1px #ddd;}
.table3 th{border-bottom: 2px solid #ddd;vertical-align: bottom;padding: 2px;text-align: left;}
</style>
<script language='javascript'>
function SelectAll() {
	for (var i = 0; i < document.myform.CD_ClassID.length; i++) {
		document.myform.CD_ClassID.options[i].selected = true;
	}
}
function UnSelectAll() {
	for (var i = 0; i < document.myform.CD_ClassID.length; i++) {
		document.myform.CD_ClassID.options[i].selected = false;
	}
}
function $(vId) {
	return document.all ? document.all[vId] : document.getElementById(vId);
}
function switchSupport(vTribe, vFlag) {
	vTribes = {
		'ModifyColor' : '0',
		'ModifyGrade' : '1',
		'ModifyPoints' : '2',
		'ModifyIsbest' : '3',
		'ModifyServer' : '4',
		'ModifySpecial' : '5',
		'ModifyClass' : '6',
		'ModifyUser' : '7',
		'ModifyFrom' : '8',
		'ModifyBitkbps' : '9',
		'ModifySkin' : '10',
		'ModifyDeleted' : '11',
		'ModifyPassed' : '12',
		'ModifySinger' : '13'
	};
	
	for (var i = 0; i < 11; i++) {
		try {
			$('supportTribe' + vTribes[vTribe]).style.color = vFlag ? 'red' : '#000';
			$('supportTitle' + vTribes[vTribe]).style.color = vFlag ? 'black' : '#000';
		} catch (e) {}
		try {
			$('myform').elements['support[' + vTribe + '][soldier][' + i + ']'].disabled = vFlag ? false : true;
			$('myform').elements['support[' + vTribe + '][soldier][' + i + ']'].style.borderColor = vFlag ? '#F93' : '#CCC';
		} catch (e) {}
		try {
			$('myform').elements['support[' + vTribe + '][level][' + i + ']'].disabled = vFlag ? false : true;
		} catch (e) {}
	}
}
</script>
</head> 
<body>
<?php
switch($action) {
	case 'edit':
		Edit();
		break;
	default:
		Main();
		break;
}
?>
</body>
</html>
<?php
Function Main() {
	global $db;
	$id=SafeRequest("id", "get");
?> 
  <div class="contents"> 
   <div class="panel"> 
    <div style="padding: 8px;"> 
     <a href="video.php" class="btn <?php echo $x5;?>">所有视频</a> 
     <a href="video.php?action=add" class="btn <?php echo $x51;?>">添加视频</a> 
     <a href="video.php?action=pass" class="btn <?php echo $x52;?>">审核视频</a> 
     <a href="video.php?action=isbest" class="btn <?php echo $x53;?>">星级视频</a> 
     <a href="video.php?action=error" class="btn <?php echo $x54;?>">报错的视频</a> 
     <a href="video.php?action=deleted" class="btn <?php echo $x55;?>">视频回收站</a> 
    </div> 
   </div> 
  </div> 
  <div class="contents"> 
   <div class="panel"> 
    <div class="panel-head"> 
     <strong>若要批量修改某个属性的值，请先选中其左侧的复选框，然后再设定属性值。 </strong> 
    </div> 
    <form method="post" id="myform" name="myform" action="?action=edit"> 
     <table class="table3"> 
      <tbody> 
       <tr> 
        <td align="left"><input type='radio' id='BatchType1' name='BatchType' value='1' <?php if($id<>''){echo ' checked';}?>></td>
        <td width='120' align="left"><label for="BatchType1">修改指定音乐ID</label></td>
        <td align="left"><input type="text" id="CD_ID" name="CD_ID" class="input input_hd length_6" value="<?php echo $id;?>"></td>
       </tr> 
       <tr> 
        <td align="left"><input type='radio' id='BatchType2' name='BatchType' value='2'></td>
        <td align="left"><label for="BatchType2">修改指定服务器的音乐</label></td>
        <td align="left"><select name="CD_ServerID[]"> <?php
$sqlclass='select * from ' . tname('server') . '';
$results=$db->query($sqlclass);
if($results) {
	while($row3=$db->fetch_array($results)) {
		echo "<option value='" . $row3['CD_ID'] . "' >" . $row3['CD_Name'] . '</option>';
	}
}
?></select></td>  
       </tr> 
       <tr> 
        <td height="35" colspan="3" align="left" bgcolor="#FAFBF7" class="td_border">以上三种指定为修改范围</td> 
       </tr> 
       <tr> 
        <td width="30" align="center"><input type="checkbox" name="ModifyColor" value="Yes" id="support[ModifyColor][is_able]" onclick="switchSupport('ModifyColor', this.checked)" /></td> 
        <td width="100" align=" left" id="supportTribe0"><label for="support[ModifyColor][is_able]">标题颜色</label></td> 
        <td><select name="CD_Color" id="support[ModifyColor][soldier][0]" disabled=""> 
        <option value=""></option> 
        <option style="background-color:#00FF00;color: #00FF00" value="#00FF00">绿色</option>
        <option style="background-color:#0000CC;color: #0000CC" value="#0000CC">深蓝</option>
        <option style="background-color:#FFFF00;color: #FFFF00" value="#FFFF00">黄色</option>
        <option style="background-color:#FF33CC;color: #FF33CC" value="#FF33CC">粉红</option>
        <option style="background-color:#FF0000;color: #FF0000" value="#FF0000">红色</option>
        <option style="background-color:#660099;color: #660099" value="#660099">紫色</option>
        <option style="background-color:#FFFFFF;color: #FFFFFF" value="">无色</option> 
        </select>
        </td> 
       </tr> 
       <tr> 
        <td width="30" align="center"><input type="checkbox" name="ModifyGrade" value="Yes" id="support[ModifyGrade][is_able]" onclick="switchSupport('ModifyGrade', this.checked)" /></td> 
        <td width="100" align=" left" id="supportTribe1"><label for="support[ModifyGrade][is_able]">下载权限</label></td> 
        <td><select name="CD_Grade" id="support[ModifyGrade][soldier][0]" disabled=""> <option value="0">游客下载</option> <option value="1" selected="">普通用户</option> <option value="2">VIP 用户</option> </select></td> 
       </tr> 
       <tr> 
        <td width="30" align="center"><input type="checkbox" name="ModifyPoints" value="Yes" id="support[ModifyPoints][is_able]" onclick="switchSupport('ModifyPoints', this.checked)" /></td> 
        <td width="100" align=" left" id="supportTribe2"><label for="support[ModifyPoints][is_able]">下载扣除金币</label></td> 
        <td><input class="input length_1" name="CD_Points" id="support[ModifyPoints][soldier][0]" type="text" size="5" value="0" onkeyup="this.value=this.value.replace(/[^\d]/g,'')" onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/[^\d]/g,''))" disabled="" /></td> 
       </tr> 
       <tr> 
        <td width="30" align="center"><input type="checkbox" name="ModifyIsbest" value="Yes" id="support[ModifyIsbest][is_able]" onclick="switchSupport('ModifyIsbest', this.checked)" /></td> 
        <td width="100" align=" left" id="supportTribe3"><label for="support[ModifyIsbest][is_able]">星级推荐</label></td> 
        <td><select name="CD_Isbest" id="support[ModifyIsbest][soldier][0]" disabled=""> <option value="0" selected="">不推荐</option> <option value="1">一星级</option> <option value="2">二星级</option> <option value="3">三星级</option> <option value="4">四星级</option> <option value="5">五星级</option> </select></td> 
       </tr> 
       <tr> 
        <td width="30" align="center"><input type="checkbox" name="ModifyServer" value="Yes" id="support[ModifyServer][is_able]" onclick="switchSupport('ModifyServer', this.checked)" /></td> 
        <td width="100" align=" left" id="supportTribe4"><label for="support[ModifyServer][is_able]">服 务 器</label></td>
        <td><select name="CD_Server" id="support[ModifyServer][soldier][0]" disabled=""> <option value="0" selected="">不设置</option> <?php
$sqlclass='select * from ' . tname('server') . '';
$results=$db->query($sqlclass);
if($results) {
	while($row3=$db->fetch_array($results)) {
		echo "<option value='" . $row3['CD_ID'] . "' >" . $row3['CD_Name'] . '</option>';
	}
}
?></select></td> 
       </tr> 
       <tr> 
        <td width="30" align="center"><input type="checkbox" name="ModifyClass" value="Yes" id="support[ModifyClass][is_able]" onclick="switchSupport('ModifyClass', this.checked)" /></td> 
        <td width="100" align=" left" id="supportTribe6"><label for="support[ModifyClass][is_able]">移动到分类</label></td> 
        <td><select name="CD_Class" id="support[ModifyClass][soldier][0]" disabled=""> <option value="0" selected="">选择分类</option> <?php
$sqlclass='select * from ' . tname('class') . ' where CD_FatherID=0 ';
$results=$db->query($sqlclass);
if($results) {
	while($row3=$db->fetch_array($results)) {
		echo "<option value='" . $row3['CD_ID'] . "' >" . $row3['CD_Name'] . '</option>';
	}
}
?></select></td> 
       </tr> 
       <tr> 
        <td width="30" align="center"><input type="checkbox" name="ModifySpecial" value="Yes" id="support[ModifySpecial][is_able]" onclick="switchSupport('ModifySpecial', this.checked)" /></td> 
        <td width="100" align=" left" id="supportTribe5"><label for="support[ModifySpecial][is_able]">移动到专辑</label></td> 
        <td><select name="CD_Special" id="support[ModifySpecial][soldier][0]" disabled=""> <option value="0">不选择</option> <?php
$sqlclass='select CD_ID,CD_Name from ' . tname('malbum') . '';
$results=$db->query($sqlclass);
if($results) {
	while($row3=$db->fetch_array($results)) {
		echo "<option value='" . $row3['CD_ID'] . "' >" . $row3['CD_Name'] . '</option>';
	}
}
?></select></td> 
       </tr> 
       <tr> 
        <td width="30" align="center"><input type="checkbox" name="ModifyUser" value="Yes" id="support[ModifyUser][is_able]" onclick="switchSupport('ModifyUser', this.checked)" /></td> 
        <td width="100" align=" left" id="supportTribe7"><label for="support[ModifyUser][is_able]">所属会员</label></td> 
        <td><input class="input length_3" name="CD_User" id="support[ModifyUser][soldier][0]" type="text" size="5" value="" disabled="" /><font color="#d01f3c">&nbsp;必须是站内注册用户帐号！必须存在！不能乱写！</font></td> 
       </tr> 
       <tr> 
        <td width="30" align="center"><input type="checkbox" name="ModifySinger" value="Yes" id="support[ModifySinger][is_able]" onclick="switchSupport('ModifySinger', this.checked)" /></td> 
        <td width="100" align=" left" id="supportTribe7"><label for="support[ModifySinger][is_able]">所属歌手</label></td> 
        <td><input class="input length_3" name="CD_Singer" id="support[ModifySinger][soldier][0]" type="text" size="5" value="" disabled="" /></td> 
       </tr> 
       <tr> 
        <td width="30" align="center"><input type="checkbox" name="ModifyFrom" value="Yes" id="support[ModifyFrom][is_able]" onclick="switchSupport('ModifyFrom', this.checked)" /></td> 
        <td width="100" align=" left" id="supportTribe8"><label for="support[ModifyFrom][is_able]">所属来源</label></td> 
        <td><input class="input length_1" name="CD_From" id="support[ModifyFrom][soldier][0]" type="text" size="5" value="" disabled="" /></td> 
       </tr> 
       <tr> 
        <td width="30" align="center"><input type="checkbox" name="ModifySkin" value="Yes" id="support[ModifySkin][is_able]" onclick="switchSupport('ModifySkin', this.checked)" /></td> 
        <td width="100" align=" left" id="supportTribe10"><label for="support[ModifySkin][is_able]">播放模板</label></td> 
        <td><input class="input length_2" name="CD_Skin" id="support[ModifySkin][soldier][0]" type="text" size="5" value="" disabled="" /></td> 
       </tr> 
       <tr> 
        <td width="30" align="center"><input type="checkbox" name="ModifyDeleted" value="Yes" id="support[ModifyDeleted][is_able]" onclick="switchSupport('ModifyDeleted', this.checked)" /></td> 
        <td width="100" align=" left" id="supportTribe11"><label for="support[ModifyDeleted][is_able]">删除音乐</label></td> 
        <td><select name="CD_Deleted" id="support[ModifyDeleted][soldier][0]" disabled=""> <option value="0" selected="">移进回收站</option> <option value="1">从回收站恢复</option> <option value="2">直接删除</option> </select></td> 
       </tr> 
       <tr> 
        <td width="30" align="center"><input type="checkbox" name="ModifyPassed" value="Yes" id="support[ModifyPassed][is_able]" onclick="switchSupport('ModifyPassed', this.checked)" /></td> 
        <td width="100" align=" left" id="supportTribe12"><label for="support[ModifyPassed][is_able]">音乐审核</label></td> 
        <td><input name="CD_Passed" type="radio" id="support[ModifyPassed][soldier][0]" value="0" checked="" disabled="" />&nbsp;通过&nbsp;<input name="CD_Passed" type="radio" id="support[ModifyPassed][level][0]" value="1" disabled="" />&nbsp;取消</td> 
       </tr> 
       <tr> 
        <td height="35" colspan="3" align="left" bgcolor="#FAFBF7" class="td_border"> <button type="submit" id="adddance" class="btn btn_submit J_ajax_submit_btn" style="margin-left: 10px;">确定提交</button> &nbsp;&nbsp;<font color="#d01f3c">注意：批量设置只对该表进行设置，不与积分,奖惩,动态挂钩！</font> </td> 
       </tr> 
      </tbody> 
     </table> 
    </form>
   </div> 
  </div> 
<?php
}
Function Edit() {
	global $db;
	$x5music_Com_CD_ID=SafeRequest('CD_ID', 'post');
	$x5music_Com_BatchType=SafeRequest('BatchType', 'post');
	$x5music_Com_ClassID=RequestBox('CD_ClassID');
	$x5music_Com_ServerID=RequestBox('CD_ServerID');
	$x5music_Com_Hits=SafeRequest('CD_Hits', 'post');
	$x5music_Com_User=SafeRequest('CD_User', 'post');
	$x5music_Com_Singer=SafeRequest('CD_Singer', 'post');
	$x5music_Com_Color=SafeRequest('CD_Color', 'post');
	$x5music_Com_Grade=SafeRequest('CD_Grade', 'post');
	$x5music_Com_Points=SafeRequest('CD_Points', 'post');
	$x5music_Com_Server=SafeRequest('CD_Server', 'post');
	$x5music_Com_Isbest=SafeRequest('CD_Isbest', 'post');
	$x5music_Com_Skin=SafeRequest('CD_Skin', 'post');
	$x5music_Com_Passed=SafeRequest('CD_Passed', 'post');
	$x5music_Com_Class=SafeRequest('CD_Class', 'post');
	$x5music_Com_SpecialID=SafeRequest('CD_Special', 'post');
	$x5music_Com_Deleted=SafeRequest('CD_Deleted', 'post');
	$x5music_Com_Bitkbps=SafeRequest('CD_Bitkbps', 'post');
	$x5music_Com_From=SafeRequest('CD_From', 'post');
	$x5music_Com_ModifyHits=SafeRequest('ModifyHits', 'post');
	$x5music_Com_ModifyUser=SafeRequest('ModifyUser', 'post');
	$x5music_Com_ModifySinger=SafeRequest('ModifySinger', 'post');
	$x5music_Com_ModifyColor=SafeRequest('ModifyColor', 'post');
	$x5music_Com_ModifyGrade=SafeRequest('ModifyGrade', 'post');
	$x5music_Com_ModifyPoints=SafeRequest('ModifyPoints', 'post');
	$x5music_Com_ModifyServer=SafeRequest('ModifyServer', 'post');
	$x5music_Com_ModifyIsbest=SafeRequest('ModifyIsbest', 'post');
	$x5music_Com_ModifySkin=SafeRequest('ModifySkin', 'post');
	$x5music_Com_ModifyPassed=SafeRequest('ModifyPassed', 'post');
	$x5music_Com_ModifyClass=SafeRequest('ModifyClass', 'post');
	$x5music_Com_ModifySpecial=SafeRequest('ModifySpecial', 'post');
	$x5music_Com_ModifyDeleted=SafeRequest('ModifyDeleted', 'post');
	$x5music_Com_ModifyBitkbps=SafeRequest('ModifyBitkbps', 'post');
	$x5music_Com_ModifyFrom=SafeRequest('ModifyFrom', 'post');
	If($x5music_Com_BatchType=='1') {
		$sql1="CD_ID in ($x5music_Com_CD_ID)";
	} ElseIf($x5music_Com_BatchType=='2') {
		$sql1="CD_Server in ($x5music_Com_ServerID)";
	} ElseIf($x5music_Com_BatchType=='3') {
		$sql1="CD_ClassID in ($x5music_Com_ClassID)";
	}
	If($x5music_Com_ModifyPoints=='Yes') {
		If($x5music_Com_Points=='') {
			showmessage('出错了，下载扣除点数不能为空！', '', 1);
		}
		if($db->query($sql='update ' . tname('dj') . ' set CD_Points=' . $x5music_Com_Points . ' where ' . $sql1 . '')) {
		}
	}
	If($x5music_Com_ModifyHits=='Yes') {
		If($x5music_Com_Hits=='') {
			showmessage('出错了，试听次数不能为空！', '', 1);
		}
		if($db->query($sql='update ' . tname('dj') . ' set CD_Hits=' . $x5music_Com_Hits . ' where ' . $sql1 . '')) {
		}
	}
	If($x5music_Com_ModifyUser=='Yes') {
		If($x5music_Com_User=='') {
			showmessage('出错了，添加会员不能为空！', '', 1);
		}
		if($db->query($sql='update ' . tname('dj') . " set CD_User='" . $x5music_Com_User . "' where " . $sql1 . '')) {
		}
	}
	If($x5music_Com_ModifyFrom=='Yes') {
		If($x5music_Com_From=='') {
			showmessage('出错了，来源不能为空！', '', 1);
		}
		if($db->query($sql='update ' . tname('dj') . " set CD_From='" . $x5music_Com_From . "' where " . $sql1 . '')) {
		}
	}
	If($x5music_Com_ModifySinger=='Yes') {
		If($x5music_Com_Singer=='') {
			showmessage('出错了，所属歌手不能为空！', '', 1);
		}
		if($db->query($sql='update ' . tname('dj') . " set CD_Singer='" . $x5music_Com_Singer . "' where " . $sql1 . '')) {
		}
	}
	If($x5music_Com_ModifyColor=='Yes') {
		if($db->query($sql='update ' . tname('dj') . " set CD_Color='" . $x5music_Com_Color . "' where " . $sql1 . '')) {
		}
	}
	If($x5music_Com_ModifyGrade=='Yes') {
		if($db->query($sql='update ' . tname('dj') . ' set CD_Grade=' . $x5music_Com_Grade . ' where ' . $sql1 . '')) {
		}
	}
	If($x5music_Com_ModifyServer=='Yes') {
		if($db->query($sql='update ' . tname('dj') . ' set CD_Server=' . $x5music_Com_Server . ' where ' . $sql1 . '')) {
		}
	}
	If($x5music_Com_ModifyIsbest=='Yes') {
		if($db->query($sql='update ' . tname('dj') . ' set CD_Isbest=' . $x5music_Com_Isbest . ' where ' . $sql1 . '')) {
		}
	}
	If($x5music_Com_ModifySkin=='Yes') {
		If($x5music_Com_Skin=='') {
			showmessage('出错了，播放页风格模板不能为空！', '', 1);
		}
		if($db->query($sql='update ' . tname('dj') . " set CD_Skin='" . $x5music_Com_Skin . "' where " . $sql1 . '')) {
		}
	}
	If($x5music_Com_ModifyPassed=='Yes') {
		if($db->query($sql='update ' . tname('dj') . ' set CD_Passed=' . $x5music_Com_Passed . ' where ' . $sql1 . '')) {
		}
	}
	If($x5music_Com_ModifyClass=='Yes') {
		If($x5music_Com_Class=='0') {
			showmessage('出错了，请先选择分类！', '', 1);
		}
		if($db->query($sql='update ' . tname('dj') . ' set CD_ClassID=' . $x5music_Com_Class . ' where ' . $sql1 . '')) {
		}
	}
	If($x5music_Com_ModifySpecial=='Yes') { //$x5music_Com_CD_ID
		if($db->query($sql='update ' . tname('dj') . ' set CD_SpecialID=' . $x5music_Com_SpecialID . ' where ' . $sql1 . '')) {
		}
	}
	If($x5music_Com_ModifyDeleted=='Yes') {
		If($x5music_Com_Deleted=='0') {
			if($db->query($sql='update ' . tname('dj') . ' set CD_Deleted=1 where ' . $sql1 . '')) {
			}
		} ElseIf($x5music_Com_Deleted=='1') {
			if($db->query($sql='update ' . tname('dj') . ' set CD_Deleted=0 where ' . $sql1 . '')) {
			}
		} ElseIf($x5music_Com_Deleted=='2') {
			$query=$db->query("select CD_Url,CD_Pic,CD_DownUrl from " . tname('dj') . " where CD_ID in ($x5music_Com_CD_ID)");
			while($row=$db->fetch_array($query)) {
				if(delunlink($row['CD_Pic'])==true) {
					@unlink(_x5music_root_ . $row['CD_Pic']);
				}
				if(delunlink($row['CD_Url'])==true) {
					@unlink(_x5music_root_ . $row['CD_Url']);
				}
				if(delunlink($row['CD_DownUrl'])==true) {
					@unlink(_x5music_root_ . $row['CD_DownUrl']);
				}
			}
			$arr=explode(",", $x5music_Com_CD_ID);
			$num=count($arr);
			if($db->query($sql='delete from ' . tname('dj') . ' where ' . $sql1 . '')) {
			}
			if($db->query('update ' . tname('system') . " set cd_djmub=cd_djmub-'$num'")) {
			}
		}
	}
	showmessage('恭喜您，批量设置数据成功！', 'video.php', 0);
}
?>