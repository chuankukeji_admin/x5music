<?php
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
error_reporting(0);
include 'include/x5music.conn.php';
header("Content-Type: text/xml;");
function strs($str){
	$str=preg_replace("/[\\x00-\\x08\\x0b-\\x0c\\x0e-\\x1f]/", "", $str);
	$str=strip_tags($str);
	$str=htmlspecialchars_decode($str);
	$str=preg_replace("/<(.*?)>/", "", $str);
	$str=str_replace("&", "", $str);
	return $str;
}
echo '<?xml version="1.0" encoding="gb2312"?>
<rss version="2.0">
<channel>
<title>' . cd_webname . '</title>
<link>http://' . cd_weburl . '/</link>
<description>' . cd_description . '</description>
<language>zh-cn</language>
<lastBuildDate>' . date('Y-m-d H:i:s', time()) . '</lastBuildDate>';
global $db;
$query=$db->query('select * from ' . tname('dj') . " where CD_Deleted=0 and CD_Passed=0 order by CD_AddTime desc LIMIT 0,100");
while($row=$db->fetch_array($query)) {
	$name=strs($row['CD_Name']);
	$Word=strs($row['CD_Word']);
	$user=strs($row['CD_User']);
	$class=GetAlias('x5music_class', 'CD_Name', 'CD_ID', $row['CD_ClassID']);
	if($Word=="") {
		$Word="暂无介绍";
	}
	echo "<item>
<title>" . $name . "</title>
<link>http://" . cd_weburl . "" . LinkUrl("dj", $row['CD_ClassID'], 1, $row['CD_ID']) . "</link>
<description>" . $Word . "</description>
<pubDate>" . $row['CD_AddTime'] . "</pubDate>
<category>" . $class . "</category>
<author>" . $user . "</author>
</item>
";
}
echo '
</channel>
</rss>';
?> 