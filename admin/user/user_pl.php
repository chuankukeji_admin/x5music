<?php
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
error_reporting(0);
include "../../include/x5music.conn.php";
include "../function_common.php";
admincheck(4);
$action=SafeRequest("action", "get");
?>
<!DOCTYPE html>
<html>
 <head> 
  <meta charset="gbk" /> 
  <meta name="renderer" content="webkit" /> 
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" /> 
  <title>x5Music 后台管理中心 -x5mp3.com</title> 
  <link href="../css/add.css" rel="stylesheet" /> 
  <style type="text/css">
.table2{width: 100%;max-width: 100%;border-collapse: collapse;border-spacing: 0;}
.table2 td{font-size: 12px;border-top: 1px solid #ddd;padding: 8px;vertical-align: top;}
.table2 tr:hover{text-decoration: none;background-color: #e6f2fb;}
.table2 th{border-bottom: 2px solid #ddd;vertical-align: bottom;padding: 2px;text-align: left;}
.table3{width: 100%;max-width: 100%;border-collapse: collapse;border-spacing: 0;}
.table3 tr:hover{text-decoration: none;background-color: #e6f2fb;}
.table3 td{font-size: 12px;line-height:25px;border-top: 1px solid #ddd;padding: 5px;vertical-align: top;border-right: solid 1px #ddd;}
.table3 th{border-bottom: 2px solid #ddd;vertical-align: bottom;padding: 2px;text-align: left;}
</style> 
  <script language="javascript">
function SelectAll() {
	for (var i = 0; i < document.myform.CD_ClassID.length; i++) {
		document.myform.CD_ClassID.options[i].selected = true;
	}
}
function UnSelectAll() {
	for (var i = 0; i < document.myform.CD_ClassID.length; i++) {
		document.myform.CD_ClassID.options[i].selected = false;
	}
}
function $(vId) {
	return document.all ? document.all[vId] : document.getElementById(vId);
}
function switchSupport(vTribe, vFlag) {
	vTribes = {
		'Modifylock' : '0',
		'Modifyskin' : '1',
		'ModifySex' : '2',
		'Modifyisbest' : '3',
		'Modifycheckmusic' : '4',
		'Modifygrade' : '5',
		'Modifyvipgrade' : '6',
		'Modifyviprank' : '7',
		'Modifyvipindate' : '8',
		'Modifyvipenddate' : '9',
		'Modifypoints' : '10',
		'Modifyhits' : '11',
		'Modifyrank' : '12',
		'ModifyDeleted' : '13'
		
		
	};
	
	for (var i = 0; i < 11; i++) {
		try {
			$('supportTribe' + vTribes[vTribe]).style.color = vFlag ? 'red' : '#000';
			$('supportTitle' + vTribes[vTribe]).style.color = vFlag ? 'black' : '#000';
		} catch (e) {}
		try {
			$('myform').elements['support[' + vTribe + '][soldier][' + i + ']'].disabled = vFlag ? false : true;
			$('myform').elements['support[' + vTribe + '][soldier][' + i + ']'].style.borderColor = vFlag ? '#F93' : '#CCC';
		} catch (e) {}
		try {
			$('myform').elements['support[' + vTribe + '][level][' + i + ']'].disabled = vFlag ? false : true;
		} catch (e) {}
	}
}

</script> 
 </head> 
 <body>
<?php
switch($action) {
	case 'edit':
		Edit();
		break;
	default:
		Main();
		break;
}
?>
</body>
</html>
<?php
Function Main() {
	global $db;
	$id=SafeRequest("id", "get");
?> 
  <div class="contents"> 
   <div class="panel"> 
    <div style="padding: 8px;"> 
     <a href="user.php" class="btn">所有会员</a> 
     <a href="?action=add" class="btn ">添加会员</a> 
     <a href="?action=users" class="btn ">普通会员</a> 
     <a href="?action=vips" class="btn ">VIP会员</a> 
     <a href="?action=userqq" class="btn ">QQ登录会员</a> 
     <a href="?action=isbest" class="btn ">推荐会员</a> 
     <a href="?action=vlock" class="btn ">锁定会员</a> 
    </div> 
   </div> 
  </div> 
  <div class="contents"> 
   <div class="panel"> 
    <div class="panel-head"> 
     <strong>若要批量修改某个属性的值，请先选中其左侧的复选框，然后再设定属性值。 </strong> 
    </div> 
    <form method="post" id="myform" name="myform" action="?action=edit"> 
     <table class="table3"> 
      <tbody> 
       <tr> 
        <td align="left"><input type='radio' id='BatchType1' name='BatchType' value='1' <?php if($id<>''){echo ' checked';}?>></td> 
        <td width="120" align="left"><label for="BatchType1">修改指定会员ID</label></td> 
        <td align="left"><input type="text" id="CD_ID" name="CD_ID" class="input input_hd length_6" value="<?php echo $id;?>"></td> 
       </tr> 
       <tr> 
        <td width="30" align="center"><input type="checkbox" name="Modifylock" value="Yes" id="support[Modifylock][is_able]" onclick="switchSupport('Modifylock', this.checked)" /></td> 
        <td width="100" align=" left" id="supportTribe0"><label for="support[Modifylock][is_able]">状态</label></td> 
        <td><select name="cd_lock" id="support[Modifylock][soldier][0]" disabled="" > <option value="0">正常会员</option> <option value="1">锁定会员</option> </select></td> 
       </tr> 
       <tr> 
        <td width="30" align="center"><input type="checkbox" name="ModifySex" value="Yes" id="support[ModifySex][is_able]" onclick="switchSupport('ModifySex', this.checked)" /></td> 
        <td width="100" align=" left" id="supportTribe2"><label for="support[ModifySex][is_able]">性别</label></td> 
        <td><select name="cd_sex" id="support[ModifySex][soldier][0]" disabled="" > <option value="男">男</option> <option value="女">女</option> </select></td> 
       </tr> 
       <tr> 
        <td width="30" align="center"><input type="checkbox" name="Modifyisbest" value="Yes" id="support[Modifyisbest][is_able]" onclick="switchSupport('Modifyisbest', this.checked)" /></td> 
        <td width="100" align=" left" id="supportTribe3"><label for="support[Modifyisbest][is_able]">推荐</label></td> 
        <td><select name="cd_isbest" id="support[Modifyisbest][soldier][0]" disabled="" > <option value="1">是</option> <option value="0">否</option> </select></td> 
       </tr> 
       <tr> 
        <td width="30" align="center"><input type="checkbox" name="Modifygrade" value="Yes" id="support[Modifygrade][is_able]" onclick="switchSupport('Modifygrade', this.checked)" /></td> 
        <td width="100" align=" left" id="supportTribe5"><label for="support[Modifygrade][is_able]">用户组</label></td> 
        <td><select name="cd_grade" id="support[Modifygrade][soldier][0]" disabled="" > <option value="0" selected>普通会员</option> <option value="1" >VIP会员</option> </select></td> 
       </tr> 
       <tr> 
        <td width="30" align="center"><input type="checkbox" name="Modifyvipindate" value="Yes" id="support[Modifyvipindate][is_able]" onclick="switchSupport('Modifyvipindate', this.checked)" /></td> 
        <td width="100" align=" left" id="supportTribe8"><label for="support[Modifyvipindate][is_able]">VIP开通日期</label></td> 
        <td><input name="cd_vipindate" value="" id="support[Modifyvipindate][soldier][0]" disabled="" type="text" class="input input_hd length_3"></td> 
       </tr> 
       <tr> 
        <td width="30" align="center"><input type="checkbox" name="Modifyvipenddate" value="Yes" id="support[Modifyvipenddate][is_able]" onclick="switchSupport('Modifyvipenddate', this.checked)" /></td> 
        <td width="100" align=" left" id="supportTribe9"><label for="support[Modifyvipenddate][is_able]">VIP到期日期</label></td> 
        <td><input name="cd_vipenddate" value="" id="support[Modifyvipenddate][soldier][0]" disabled="" type="text" class="input input_hd length_3"></td> 
       </tr>
       <tr> 
        <td width="30" align="center"><input type="checkbox" name="Modifypoints" value="Yes" id="support[Modifypoints][is_able]" onclick="switchSupport('Modifypoints', this.checked)" /></td> 
        <td width="100" align=" left" id="supportTribe10"><label for="support[Modifypoints][is_able]">金币</label></td> 
        <td><input name="cd_points" id="support[Modifypoints][soldier][0]" disabled="" value="" type="text" class="input input_hd length_1"></td> 
       </tr>
       <tr> 
        <td width="30" align="center"><input type="checkbox" name="Modifyhits" value="Yes" id="support[Modifyhits][is_able]" onclick="switchSupport('Modifyhits', this.checked)" /></td> 
        <td width="100" align=" left" id="supportTribe11"><label for="support[Modifyhits][is_able]">人气</label></td> 
        <td><input name="cd_hits" id="support[Modifyhits][soldier][0]" disabled="" value="" type="text" class="input input_hd length_1"></td> 
       </tr>
       <tr> 
        <td width="30" align="center"><input type="checkbox" name="ModifyDeleted" value="Yes" id="support[ModifyDeleted][is_able]" onclick="switchSupport('ModifyDeleted', this.checked)" /></td> 
        <td width="100" align=" left" id="supportTribe13"><label for="support[ModifyDeleted][is_able]">删除帐号</label></td> 
        <td> &nbsp;&nbsp;<font color="#d01f3c">注意：批量删除不可恢复，请慎重！</font> </td> 
       </tr>
       <tr> 
        <td height="35" colspan="3" align="left" bgcolor="#FAFBF7" class="td_border"> <button type="submit" id="adddance" class="btn btn_submit J_ajax_submit_btn" style="margin-left: 10px;">确定提交</button> &nbsp;&nbsp;<font color="#d01f3c">注意：批量设置只对该表进行设置，不与积分,奖惩,动态挂钩！</font> </td> 
       </tr> 
      </tbody> 
     </table> 
    </form>
   </div> 
  </div> 		
<?php
}
Function Edit() {
	global $db;
	$x5music_Com_CD_ID=SafeRequest('CD_ID', 'post');
	$x5music_Com_BatchType=SafeRequest('BatchType', 'post');
	$x5music_Com_lock=SafeRequest('cd_lock', 'post');
	$x5music_Com_skin=SafeRequest('cd_skin', 'post');
	$x5music_Com_sex=SafeRequest('cd_sex', 'post');
	$x5music_Com_isbest=SafeRequest('cd_isbest', 'post');
	$x5music_Com_checkmusic=SafeRequest('cd_checkmusic', 'post');
	$x5music_Com_grade=SafeRequest('cd_grade', 'post');
	$x5music_Com_vipgrade=SafeRequest('cd_vipgrade', 'post');
	$x5music_Com_viprank=SafeRequest('cd_viprank', 'post');
	$x5music_Com_vipindate=SafeRequest('cd_vipindate', 'post');
	$x5music_Com_vipenddate=SafeRequest('cd_vipenddate', 'post');
	$x5music_Com_points=SafeRequest('cd_points', 'post');
	$x5music_Com_hits=SafeRequest('cd_hits', 'post');
	$x5music_Com_rank=SafeRequest('cd_rank', 'post');
	$x5music_Com_ModifyDeleted=SafeRequest('ModifyDeleted', 'post');
	$x5music_Com_Modifylock=SafeRequest('Modifylock', 'post');
	$x5music_Com_Modifyskin=SafeRequest('Modifyskin', 'post');
	$x5music_Com_ModifySex=SafeRequest('ModifySex', 'post');
	$x5music_Com_Modifyisbest=SafeRequest('Modifyisbest', 'post');
	$x5music_Com_Modifycheckmusic=SafeRequest('Modifycheckmusic', 'post');
	$x5music_Com_Modifygrade=SafeRequest('Modifygrade', 'post');
	$x5music_Com_Modifyvipgrade=SafeRequest('Modifyvipgrade', 'post');
	$x5music_Com_Modifyviprank=SafeRequest('Modifyviprank', 'post');
	$x5music_Com_Modifyvipindate=SafeRequest('Modifyvipindate', 'post');
	$x5music_Com_Modifyvipenddate=SafeRequest('Modifyvipenddate', 'post');
	$x5music_Com_Modifypoints=SafeRequest('Modifypoints', 'post');
	$x5music_Com_Modifyhits=SafeRequest('Modifyhits', 'post');
	$x5music_Com_Modifyrank=SafeRequest('Modifyrank', 'post');
	If($x5music_Com_BatchType=='1') {
		$sql1="cd_id in ($x5music_Com_CD_ID)";
	}
	If($x5music_Com_Modifylock=='Yes') {
		if($db->query($sql='update ' . tname('user') . " set cd_lock='" . $x5music_Com_lock . "' where " . $sql1 . '')) {
		}
	}
	If($x5music_Com_ModifySex=='Yes') {
		if($db->query($sql='update ' . tname('user') . " set cd_sex='" . $x5music_Com_sex . "' where " . $sql1 . '')) {
		}
	}
	If($x5music_Com_Modifyisbest=='Yes') {
		if($db->query($sql='update ' . tname('user') . " set cd_isbest='" . $x5music_Com_isbest . "' where " . $sql1 . '')) {
		}
	}
	If($x5music_Com_Modifygrade=='Yes') {
		if($db->query($sql='update ' . tname('user') . " set cd_grade='" . $x5music_Com_grade . "' where " . $sql1 . '')) {
		}
	}

	If($x5music_Com_Modifyvipindate=='Yes') {
		if($db->query($sql='update ' . tname('user') . " set cd_vipindate='" . $x5music_Com_vipindate . "' where " . $sql1 . '')) {
		}
	}
	If($x5music_Com_Modifyvipenddate=='Yes') {
		if($db->query($sql='update ' . tname('user') . " set cd_vipenddate='" . $x5music_Com_vipenddate . "' where " . $sql1 . '')) {
		}
	}
	If($x5music_Com_Modifypoints=='Yes') {
		if($db->query($sql='update ' . tname('user') . " set cd_points='" . $x5music_Com_points . "' where " . $sql1 . '')) {
		}
	}
	If($x5music_Com_Modifyhits=='Yes') {
		if($db->query($sql='update ' . tname('user') . " set cd_hits='" . $x5music_Com_hits . "' where " . $sql1 . '')) {
		}
	}
	If($x5music_Com_ModifyDeleted=='Yes') {
		$arr = explode(",",$x5music_Com_CD_ID);
		$num = count($arr);
		$query=$db->query("select cd_url from " . tname('pic') . " where cd_uid in ($x5music_Com_CD_ID)");
		while($row=$db->fetch_array($query)) {
			@unlink(_x5music_root_ . "upload/pic/" . $row['cd_url']);
		}
		$query=$db->query("select cd_id from " . tname('user') . " where cd_id in ($x5music_Com_CD_ID)");
		while($row=$db->fetch_array($query)) {
			@unlink(_x5music_root_ . "upload/headpic/" . $cd_id . ".jpg");
		}
		$sql="delete from " . tname('user') . " where cd_id in ($x5music_Com_CD_ID)";
		if($db->query($sql)) {
			$db->query("delete from " . tname('pic') . " where cd_uid in ($x5music_Com_CD_ID)");
			$db->query("delete from " . tname('pic') . " where cd_uid in ($x5music_Com_CD_ID)");
			$db->query("delete from " . tname('friend') . " where cd_uid in ($x5music_Com_CD_ID) or cd_uids in ($x5music_Com_CD_ID)");
			$db->query("delete from " . tname('web') . " where cd_uid in ($x5music_Com_CD_ID)");
			$db->query("delete from " . tname('feed') . " where cd_uid in ($x5music_Com_CD_ID)");
		}
		$db->query('update '.tname('system')." set cd_usermub=cd_usermub-'$num'");//全局数据统计
	}
	showmessage('恭喜您，批量设置数据成功！', 'user.php', 0);
}
?>