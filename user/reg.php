<?php
error_reporting(0);
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
include "../include/x5music.conn.php";
include "../include/x5music.inc.php";
include "source/function_common.php";
?>

<?php include_once('source/space_header.php');?>
<?php if($userlogined){showmessage("出错了，您已经登录！",$_SERVER['HTTP_REFERER'],0);}?>
<?php if(cd_userreg=="no"){ showmessage("出错了，本站暂不开放新会员注册。",$_SERVER['HTTP_REFERER'],0); } ?>
  <div class="album_banner">
<h2>请完整填写以下信息进行注册。</h2>
<?php 
include "../plug/qqhl/conf.inc.php";
if(qqapp_open=="yes") {
?>
<div style="color: #808080;margin: 10px 10px; font-size: 18px;">
<div style="float:right">快捷登录↓<br /><br />
<?php echo '<a href="' . cd_webpath . 'plug/qqhl/index.php"><img src="' . cd_webpath . 'plug/qqhl/qq.png"></a>';?>
</div>
</div>
<?php }?>
<script type="text/javascript">
$ = function(em){	return document.getElementById(em);	};
$F = function(em){	return document.getElementById(em).value;	};
function editMe(o){
	o.style.border="1px #febb00 solid"
	o.style.backgroundColor="#fffbe7";
}
function blurMe(o){
	o.style.border="1px solid #cccccc"
	o.style.backgroundColor="transparent";
}
function doLength(intS,intB,oTd,strReg){
	if(oTd.value.length < intS || oTd.value.length > intB){
		$(strReg).innerHTML=" <img src='image/check_error.gif'/>&nbsp;&nbsp;<span class='alert'>长度在"+intS+"-"+intB+"个字符之间</span>";
		editMe(oTd);
	}			
}
function doEmpty(oTd,strReg,msg){
	if(oTd.value==""){
		$(strReg).innerHTML=msg;
		editMe(oTd);
	}
}


doRe=function(strReg,intType,oTd){
	if(intType==1){
			$(strReg).innerHTML=" <img src='image/check_right.gif'/>";
		blurMe(oTd);
			if(!isEmail(oTd.value)){	$(strReg).innerHTML=" <img src='image/check_error.gif'/>&nbsp;&nbsp;<span class='alert'>请输入正确格式的邮箱地址</span>";
		doEmpty(oTd,strReg," <img src='image/check_error.gif'/>&nbsp;&nbsp;<span class='alert'>邮箱地址不允许为空</span>");
		
		}else{
			if(oTd.value.length < 3 || oTd.value.length > 30){
				editMe(oTd);
				$(strReg).innerHTML=" <img src='image/check_error.gif'/>&nbsp;&nbsp;<span class='alert'>长度在3-30个字符之间</span>";
			}else{
				var xmlHttp;
				if(window.ActiveXObject){
					xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
				}else if(window.XMLHttpRequest){
					xmlHttp = new XMLHttpRequest();
				}
				xmlHttp.open("get","source/ajax.php?ac=checkusername&username="+oTd.value);
				xmlHttp.onreadystatechange=function(){
					if(xmlHttp.readyState == 4){
						if(xmlHttp.status == 200){
							$("Re_1").innerHTML = unescape(xmlHttp.responseText);
						}
					}
				}
				xmlHttp.send(null);
				blurMe(oTd);
			}
		}
	}
	if(intType==2){
		$(strReg).innerHTML=" <img src='image/check_right.gif'/>";
		blurMe(oTd);
		doLength(5,16,oTd,strReg);
		doEmpty(oTd,strReg," <img src='image/check_error.gif'/>&nbsp;&nbsp;<span class='alert'>用户密码不允许为空</span>");
	}
	if(intType==3){
		$(strReg).innerHTML=" <img src='image/check_right.gif'/>";
		blurMe(oTd);
		if(oTd.value!=$F("ReI_2")){
			$(strReg).innerHTML=" <img src='image/check_error.gif'/>&nbsp;&nbsp;<span class='alert'>两次密码不同</span>";
			editMe(oTd);
		}
		doEmpty(oTd,strReg," <img src='image/check_error.gif'/>&nbsp;&nbsp;<span class='alert'>确认密码不允许为空</span>");
	}

	if(intType==4){
		if(oTd.value==""){
			editMe(oTd);
			$(strReg).innerHTML=" <img src='image/check_error.gif'/>&nbsp;&nbsp;<span class='alert'>昵称不允许为空</span>";
		}else{
			if(oTd.value.length < 2 || oTd.value.length > 16){
				editMe(oTd);
				$(strReg).innerHTML=" <img src='image/check_error.gif'/>&nbsp;&nbsp;<span class='alert'>长度在3-16个字符之间</span>";
			}else{
				var xmlHttp;
				if(window.ActiveXObject){
					xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
				}else if(window.XMLHttpRequest){
					xmlHttp = new XMLHttpRequest();
				}
				xmlHttp.open("get","source/ajax.php?ac=checknicheng&nicheng="+oTd.value);
				xmlHttp.onreadystatechange=function(){
					if(xmlHttp.readyState == 4){
						if(xmlHttp.status == 200){
							$("Re_4").innerHTML = unescape(xmlHttp.responseText);
						}
					}
				}
				xmlHttp.send(null);
				blurMe(oTd);
			}
		}
	}

	if(intType==5){
		if(oTd.value==""){
			editMe(oTd);
			$(strReg).innerHTML=" <img src='image/check_error.gif'/>&nbsp;&nbsp;<span class='alert'>验证码不允许为空</span>";
		}else{
			if(oTd.value.length < 1 || oTd.value.length > 16){
				editMe(oTd);
				$(strReg).innerHTML=" <img src='image/check_error.gif'/>&nbsp;&nbsp;<span class='alert'>长度在1-16个字符之间</span>";
			}else{
				var xmlHttp;
				if(window.ActiveXObject){
					xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
				}else if(window.XMLHttpRequest){
					xmlHttp = new XMLHttpRequest();
				}
				xmlHttp.open("get","source/ajax.php?ac=checkccode&seccode="+oTd.value);
				xmlHttp.onreadystatechange=function(){
					if(xmlHttp.readyState == 4){
						if(xmlHttp.status == 200){
							$("Re_5").innerHTML = unescape(xmlHttp.responseText);
						}
					}
				}
				xmlHttp.send(null);
				blurMe(oTd);
			}
		}
	}
	if(intType==6){
		$(strReg).innerHTML=" <img src='image/check_right.gif'/>";
		blurMe(oTd);
		doLength(4,16,oTd,strReg);
		doEmpty(oTd,strReg," <img src='image/check_error.gif'/>&nbsp;&nbsp;<span class='alert'>密码问题不允许为空</span>");
	}
	if(intType==7){
		$(strReg).innerHTML=" <img src='image/check_right.gif'/>";
		blurMe(oTd);
		doLength(4,16,oTd,strReg);
		if(oTd.value!=$F("ReI_6")){
		}else{
			$(strReg).innerHTML=" <img src='image/check_error.gif'/>&nbsp;&nbsp;<span class='alert'>问题和答案不能相同</span>";
			editMe(oTd);
		}
		doEmpty(oTd,strReg," <img src='image/check_error.gif'/>&nbsp;&nbsp;<span class='alert'>问题答案不允许为空</span>");
	}
}

function isEmail(a){
	 var i=a.length;
	 var temp = a.indexOf('@');
	 var tempd = a.indexOf('.');
	 if (temp > 1) {
		if ((i-temp) > 3){
			if (tempd!=-1){
				 return 1;
			}
		}
	 }
	 return 0;
}	
doErrTest=function(){
	var t = true;
	for(i=1;i<8;i++){
		try{
			doRe("Re_"+i,i,$("ReI_"+i));
		}catch(e){}
		if((($("Re_"+i).innerHTML).indexOf("right"))<0) t = false;
	}
	return t;
}
</script>


<form name="reg" action="do.php?ac=reg" method="post" onsubmit="return doErrTest()">
<div class="name">
<div class="name">
 <br />
<label> 验证码：</label><input type="text" class="input1" id="ReI_5" name="seccode" style="width: 100px;margin-left: 12px;" onblur="doRe('Re_5',5,this)"/>  <script>seccode();</script><a href="javascript:updateseccode()">更换</a>
<span id="Re_5" style="color: #808080; margin-left: 5px; font-size: 12px;"></span>
  </div>  <br />
<label>邮箱帐号：</label><input type="text" name="username" id="ReI_1" value="" class="input1" style="width: 200px;" onblur="doRe('Re_1',1,this)"/>
<span id="Re_1" style="color: #808080; margin-left: 5px; font-size: 12px;"></span>
  </div><br />
<div class="name">
<label>登录密码：</label><input type="password" class="input1" name="password" style="width: 200px;" id="ReI_2"onblur="doRe('Re_2',2,this)"/>
<span id="Re_2" style="color: #808080; margin-left: 5px; font-size: 12px;"></span>
  </div><br />
<div class="name">
<label>确认密码：</label><input type="password" class="input1" name="password2" style="width: 200px;" id="ReI_3"onblur="doRe('Re_3',3,this)"/>
<span id="Re_3" style="color: #808080; margin-left: 5px; font-size: 12px;"></span>
  </div><br />
<div class="name">
<label>会员昵称：</label><input type="text" class="input1" onblur="doRe('Re_4',4,this)" id="ReI_4" style="width: 200px;" name="nicheng""/>
<span id="Re_4" style="color: #808080; margin-left: 5px; font-size: 12px;"></span>
  </div><br />
<div class="name">
<label>密保问题：</label><input type="text" class="input1" onblur="doRe('Re_6',6,this)" id="ReI_6" style="width: 200px;" name="question""/>
<span id="Re_6" style="color: #808080; margin-left: 5px; font-size: 12px;"></span>
  </div><br />
<div class="name">
<label>密保答案：</label><input type="text" class="input1" onblur="doRe('Re_7',7,this)" id="ReI_7" style="width: 200px;" name="answer""/>
<span id="Re_7" style="color: #808080; margin-left: 5px; font-size: 12px;"></span>
  </div><br />
<div class="name">
<label>您的性别：</label><input type="radio" name="sex" value="男">男&nbsp;&nbsp;<input type="radio" name="sex" value="女" checked="checked">女&nbsp;&nbsp;&nbsp;<font color="#FF6969">注册后不可更改</font>
<span id="Re_7" style="color: #808080; margin-left: 5px; font-size: 12px;"></span>
  </div><br />
<div style="color: #808080;margin: 20px 60px; font-size: 12px;">
<input type="hidden" name="action" value="addMember" />
<input type="submit" name="submitreg" value="注　册" class="button square red"/>
&nbsp;&nbsp;<a href="login.php">已有帐号？->登录</a>
</div>
        </form>




</div>

<?php include_once('source/space_footer.php');?>