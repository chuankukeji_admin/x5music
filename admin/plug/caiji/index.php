<?php
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
error_reporting(0);
include '../../../include/x5music.conn.php';
include "../../function_common.php";
admincheck(6);
$version="v1.0";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head> 
  <meta http-equiv="Content-Type" content="text/html; charset=gb2312" /> 
  <meta name="renderer" content="webkit" /> 
  <title>x5Music 管理后台</title> 
  <link rel="stylesheet" href="../../css/add.css" type="text/css" media="screen" /> 
  <style type="text/css">
.table2{width: 100%;max-width: 100%;border-collapse: collapse;border-spacing: 0;}
.table2 td{font-size: 12px;border-top: 1px solid #ddd;padding: 8px;vertical-align: top;}
.table2 tr:hover{text-decoration: none;background-color: #e6f2fb;}
.table2 th{border-bottom: 2px solid #ddd;vertical-align: bottom;padding: 2px;text-align: left;}
.table3{width: 100%;max-width: 100%;border-collapse: collapse;border-spacing: 0;}
.table3 tr:hover{text-decoration: none;background-color: #e6f2fb;}
.table3 td{font-size: 12px;line-height:25px;border-top: 1px solid #ddd;padding: 5px;vertical-align: top;border-right: solid 1px #ddd;}
.table3 th{border-bottom: 2px solid #ddd;vertical-align: bottom;padding: 2px;text-align: left;}
</style>
 </head> 
 <body>   
<?php include "header.php"; ?>
  <div class="contents"> 
   <div class="panel"> 
    <div class="panel-head">
     <strong>采集相关说明：</strong>
    </div> 
<?php
$lockfile = 'install.lock';
if(file_exists($lockfile)) {
?>
	<table class="table3"> 
      <tbody> 
       <tr> 
        <td width="50" align="left" class="p10">版本</td> 
        <td align="left"><?php echo $version?></td> 
       </tr> 
       <tr> 
        <td align="left">更新</td> 
        <td align="left"><a href="http://www.x5mp3.com/free/11.html" class="btn">更新版本</a></td> 
       </tr>
      </tbody> 
     </table>
<div style="padding: 10px;line-height: 25px;">
<b>采集须知：</b><br />
1、采集请尽量避开流量高峰期，采集信息会消耗内存。<br />
2、采集暂停时间建议在10秒，时间越长对主机消耗越少！<br />
3、如遇到无法采集，可以尝试切换采集模式。如果支持cURL建议用此模式。消耗内存少，响应时间快！<br />
4、采集必须依赖file_get_contents 或 cURL 如果您的主机或服务器不支持将无法使用此插件！<br />
<b>file_get_contents And cURL 检测：<a href="file_get_curl.php" class="btn">开始检测</a></b><br /><br />
<b>采集教程：</b><br />
(*^__^*)...正则表达式采集，自己领悟!<br /><br />
<b>采集定制：</b><br />
1、定制采集站点规则，统一价格：50元一个站点<br />
2、定制站点解析代理播放文件，统一价格：150元一个站点<br />
3、站点采集规则永久免费包更新，代理解析文件包一年更新！<br />
4、联系QQ：196859961 (小5)
<br />
</div> 
<?php }else{?>
	<table class="table3"> 
      <tbody> 
       <tr> 
        <td width="50" align="left" class="p10">提醒</td> 
        <td align="left">请先安装插件！<a href="install.php" class="btn">安装插件！</a></td> 
       </tr> 
      </tbody> 
     </table>
<?php }?>
   </div> 
  </div>
 </body>
</html>