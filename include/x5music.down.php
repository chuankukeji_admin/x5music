<?php
error_reporting(0);
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
include "x5music.conn.php";
include "x5music.inc.php";
$action=SafeRequest("ac", "get");
$cd_id=SafeRequest("id", "get");
if(!eregi($_SERVER['SERVER_NAME'], $_SERVER['HTTP_REFERER'])) {
	$error="来路不正确，请从本站访问！";
	exit(include _x5music_root_ . "/include/error/content_error.php");
}
if(!IsNumID($cd_id)) {
	$error="ID错误，请检查Url地址是否正确！";
	exit(include _x5music_root_ . "/include/error/content_error.php");
}
if(!IsNum($cd_id)) {
	$error="缺少ID，请检查Url地址是否正确！";
	exit(include _x5music_root_ . "/include/error/content_error.php");
}
$sql="select * from " . tname('dj') . " where CD_ID='$cd_id'";
if($row=$db->getrow($sql)) {
	$server=$db->getrow("select * from " . tname('server') . " where CD_ID=" . $row['CD_Server'] . "");
	$dowmurl=$server['CD_Url'] . urlencode(mb_convert_encoding($row['CD_DownUrl'], 'utf-8', 'gb2312'));
	$dowmurl=str_replace('%2F', '/', $dowmurl);
	$dowmurl=str_replace('+', ' ', $dowmurl);
	$dowmurl=str_replace('%3A', ':', $dowmurl);
	$dowmurl=str_replace("upload/url/upload/url/","upload/url/",$dowmurl);//兼容x5musicV1.8本地服务器
	$dowmurl=str_replace("upload/url/upload/video/","upload/video/",$dowmurl);//兼容x5musicV1.8本地服务器
	if($dowmurl=="") {
		$error="暂无下载地址！";
		exit(include _x5music_root_ . "/include/error/content_error.php");
	}
	if($row['CD_Grade']=="0") {
		$row['CD_Points']=0;
		if($x5music_com_userid) {
		$downsql="select cd_id,cd_addtime from " . tname('fav') . " where cd_uid='" . $x5music_com_userid . "' and cd_type='down' and cd_musicid='" . $row['CD_ID'] . "'";
		$down=$db->getrow($downsql);
		if($down) {
			if(comparetime(date('Y-m-d H:i:s', $down['cd_addtime']), date('Y-m-d H:i:s'))>24) {
				$db->query("update " . tname('fav') . " set cd_addtime=" . time() . " where cd_id='" . $down['cd_id'] . "'");
			} else {
				$row['CD_Points']=0;
			}
		} else {
			$setarr=array(
				'cd_type'=>'down',
				'cd_uid'=>$x5music_com_userid,
				'cd_uname'=>''.$x5music_com_username.'',
				'cd_musicid'=>$row['CD_ID'],
				'cd_musicname'=>$row['CD_Name'],
				'cd_addtime'=>time()
			);
			inserttable('fav', $setarr, 1);
		}
		$cd_ids=$db->Getone("select cd_id from " . tname('feed') . " where cd_icon='jifen' and cd_uid=$x5music_com_userid and cd_dataid=$cd_id");
		if($cd_ids) {
			updatetable('feed', array(
				'cd_addtime'=>date('Y-m-d H:i:s')
			), array(
				'cd_icon'=>'jifen',
				'cd_id'=>$cd_ids
			));
		} else {
			$setarrs=array(
				'cd_uid'=>$x5music_com_userid,
				'cd_username'=>''.$x5music_com_username.'',
				'cd_icon'=>'jifen',
				'cd_title'=>'下载了音乐 <a href="' . LinkUrl("dj", $row['CD_ClassID'], 1, $cd_id) . '" target="play">' . $row['CD_Name'] . '</a>',
				'cd_data'=>'-' . $row['CD_Points'] . '',
				'cd_image'=>'',
				'cd_imagelink'=>'',
				'cd_dataid'=>$row['CD_ID'],
				'cd_addtime'=>date('Y-m-d H:i:s')
			);
			inserttable('feed', $setarrs, 1);
		}
		}
		$db->query("update " . tname('dj') . " set CD_DownHits=CD_DownHits+1 where CD_ID='" . $row['CD_ID'] . "'");
		//本地附件下载
		if($row['CD_Server']=="1") {
			if(cd_dowattachment=="yes") {
				$file_name=$row['CD_DownUrl'];
				$form=strtolower(trim(substr(strrchr($file_name, "."), 1)));
				$down_name=str_replace('[dj:name]', $row['CD_Name'], cd_dowattachmentname);
				$down_name=str_replace('[dj:singer]', $row['CD_Singer'], $down_name);
				$down_name=$down_name . '.' . $form;
				header("Content-type:text/html;charset=gb2312");
				$file_path=_x5music_root_ . $file_name;
				if(!file_exists($file_path)) {
					echo '<meta charset="gbk" />';
					exit("<script language=javascript>alert('出错了,下载资源不存在!');JavaScript:window.close();</script>");
				}
				$fp=@fopen($file_path, "r");
				$file_size=filesize($file_path);
				//下载文件需要用到的头 
				header("Content-type: application/octet-stream");
				header("Accept-Ranges: bytes");
				header("Accept-Length:" . $file_size);
				header("Content-Disposition: attachment; filename=" . $down_name);
				//向浏览器返回数据 
				while(!feof($fp)) {
					echo fread($fp, cd_dowattachmentsiz); //每秒传输字节
				}
				fclose($file);
			}
		}
		header("Content-Type: application/force-download");
		header("Location:$dowmurl");
	} elseif($row['CD_Grade']=="1") { //普通会员下载
		if(!$x5music_com_userid) {
			echo '<meta charset="gbk" />';
			exit("<script language=javascript>alert('您需要先登录才能进行此操作!');top.location='" . cd_webpath . "user/login.php';</script>");
		}
		$user=$db->getrow("select cd_name,cd_id from " . tname('user') . " where cd_name='" . $row['CD_User'] . "'");
		if($x5music_com_username==$user['cd_name']) {
			$row['CD_Points']=0;
		}
		if($x5music_com_grade==1) {
			$row['CD_Points']=0;
		}
		$downsql="select cd_id,cd_addtime from " . tname('fav') . " where cd_uid='" . $x5music_com_userid . "' and cd_type='down' and cd_musicid='" . $row['CD_ID'] . "'";
		$down=$db->getrow($downsql);
		if($down) {
			if(comparetime(date('Y-m-d H:i:s', $down['cd_addtime']), date('Y-m-d H:i:s'))>24) {
				$db->query("update " . tname('fav') . " set cd_addtime=" . time() . " where cd_id='" . $down['cd_id'] . "'");
			} else {
				$row['CD_Points']=0;
			}
		} else {
			$setarr=array(
				'cd_type'=>'down',
				'cd_uid'=>$x5music_com_userid,
				'cd_uname'=>''.$x5music_com_username.'',
				'cd_musicid'=>$row['CD_ID'],
				'cd_musicname'=>$row['CD_Name'],
				'cd_addtime'=>time()
			);
			inserttable('fav', $setarr, 1);
		}
		$cd_ids=$db->Getone("select cd_id from " . tname('feed') . " where cd_icon='jifen' and cd_uid=$x5music_com_userid and cd_dataid=$cd_id");
		if($cd_ids) {
			updatetable('feed', array(
				'cd_addtime'=>date('Y-m-d H:i:s')
			), array(
				'cd_icon'=>'jifen',
				'cd_id'=>$cd_ids
			));
		} else {
			$setarrs=array(
				'cd_uid'=>$x5music_com_userid,
				'cd_username'=>''.$x5music_com_username.'',
				'cd_icon'=>'jifen',
				'cd_title'=>'下载了音乐 <a href="' . LinkUrl("dj", $row['CD_ClassID'], 1, $cd_id) . '" target="play">' . $row['CD_Name'] . '</a>',
				'cd_data'=>'-' . $row['CD_Points'] . '',
				'cd_image'=>'',
				'cd_imagelink'=>'',
				'cd_dataid'=>$row['CD_ID'],
				'cd_addtime'=>date('Y-m-d H:i:s')
			);
			inserttable('feed', $setarrs, 1);
		}
		if($row['CD_Points']>=1) {
			if($x5music_com_points<$row['CD_Points']) {
				echo '<meta charset="gbk" />';
				exit("<script language='javascript'>if( confirm('出错了,您的帐号金币不足无法下载!是否前往充值中心？') ){location.href='" . cd_webpath . "user/space.php?do=assets&view=rechargestepone';}else{;JavaScript:window.close();}</script>");
			}
			$db->query("update " . tname('user') . " set cd_points=cd_points-'" . $row['CD_Points'] . "' where cd_id='" . $x5music_com_userid . "'");
			$db->query("update " . tname('user') . " set cd_points=cd_points+'" . $row['CD_Points'] . "' where cd_name='" . $row['CD_User'] . "'");
		}
		$db->query("update " . tname('dj') . " set CD_DownHits=CD_DownHits+1 where CD_ID='" . $row['CD_ID'] . "'");
		//本地附件下载
		if($row['CD_Server']=="1") {
			if(cd_dowattachment=="yes") {
				$file_name=$row['CD_DownUrl'];
				$form=strtolower(trim(substr(strrchr($file_name, "."), 1)));
				$down_name=str_replace('[dj:name]', $row['CD_Name'], cd_dowattachmentname);
				$down_name=str_replace('[dj:singer]', $row['CD_Singer'], $down_name);
				$down_name=$down_name . '.' . $form;
				header("Content-type:text/html;charset=gb2312");
				$file_path=_x5music_root_ . $file_name;
				if(!file_exists($file_path)) {
					echo '<meta charset="gbk" />';
					exit("<script language=javascript>alert('出错了,下载资源不存在!');JavaScript:window.close();</script>");
				}
				$fp=@fopen($file_path, "r");
				$file_size=filesize($file_path);
				//下载文件需要用到的头 
				header("Content-type: application/octet-stream");
				header("Accept-Ranges: bytes");
				header("Accept-Length:" . $file_size);
				header("Content-Disposition: attachment; filename=" . $down_name);
				//向浏览器返回数据 
				while(!feof($fp)) {
					echo fread($fp, cd_dowattachmentsiz); //每秒传输字节
				}
				fclose($file);
			}
		}
		header("Content-Type: application/force-download");
		header("Location:$dowmurl");
	} elseif($row['CD_Grade']=="2") { //VIP会员下载
		if(!$x5music_com_userid) {
			echo '<meta charset="gbk" />';
			exit("<script language=javascript>alert('您需要先登录才能进行此操作!');top.location='" . cd_webpath . "user/login.php';</script>");
		}
		$user=$db->getrow("select cd_name,cd_id from " . tname('user') . " where cd_name='" . $row['CD_User'] . "'");
		if($x5music_com_username==$user['cd_name']) {
			$row['CD_Points']=0;
			$x5music_com_grade=1;
		}
		if($x5music_com_grade==1) {
			$row['CD_Points']=0;
		} else {
			echo '<meta charset="gbk" />';
			exit("<script language='javascript'>if( confirm('抱歉，您还不是VIP会员，无法下载，是否升级VIP会员?') ){location.href='" . cd_webpath . "user/space.php?do=assets&view=vipbill';}else{;JavaScript:window.close();}</script>");
		}
		$cd_ids=$db->Getone("select cd_id from " . tname('feed') . " where cd_icon='jifen' and cd_uid=$x5music_com_userid and cd_dataid=$cd_id");
		if($cd_ids) {
			updatetable('feed', array(
				'cd_addtime'=>date('Y-m-d H:i:s')
			), array(
				'cd_icon'=>'jifen',
				'cd_id'=>$cd_ids
			));
		} else {
			$setarrs=array(
				'cd_uid'=>$x5music_com_userid,
				'cd_username'=>''.$x5music_com_username.'',
				'cd_icon'=>'jifen',
				'cd_title'=>'下载了音乐 <a href="' . LinkUrl("dj", $row['CD_ClassID'], 1, $cd_id) . '" target="play">' . $row['CD_Name'] . '</a>',
				'cd_data'=>'-' . $row['CD_Points'] . '',
				'cd_image'=>'',
				'cd_imagelink'=>'',
				'cd_dataid'=>$row['CD_ID'],
				'cd_addtime'=>date('Y-m-d H:i:s')
			);
			inserttable('feed', $setarrs, 1);
		}
		$db->query("update " . tname('dj') . " set CD_DownHits=CD_DownHits+1 where CD_ID='" . $row['CD_ID'] . "'");
		//本地附件下载
		if($row['CD_Server']=="1") {
			if(cd_dowattachment=="yes") {
				$file_name=$row['CD_DownUrl'];
				$form=strtolower(trim(substr(strrchr($file_name, "."), 1)));
				$down_name=str_replace('[dj:name]', $row['CD_Name'], cd_dowattachmentname);
				$down_name=str_replace('[dj:singer]', $row['CD_Singer'], $down_name);
				$down_name=$down_name . '.' . $form;
				header("Content-type:text/html;charset=gb2312");
				$file_path=_x5music_root_ . $file_name;
				if(!file_exists($file_path)) {
					echo '<meta charset="gbk" />';
					exit("<script language=javascript>alert('出错了,下载资源不存在!');JavaScript:window.close();</script>");
				}
				$fp=@fopen($file_path, "r");
				$file_size=filesize($file_path);
				//下载文件需要用到的头 
				header("Content-type: application/octet-stream");
				header("Accept-Ranges: bytes");
				header("Accept-Length:" . $file_size);
				header("Content-Disposition: attachment; filename=" . $down_name);
				//向浏览器返回数据 
				while(!feof($fp)) {
					echo fread($fp, cd_dowattachmentsiz); //每秒传输字节
				}
				fclose($file);
			}
		}
		header("Content-Type: application/force-download");
		header("Location:$dowmurl");
	}
} else {
	echo '<meta charset="gbk" />';
	exit("<script language=javascript>alert('出错了,下载资源不存在!');JavaScript:window.close();</script>");
}
?>