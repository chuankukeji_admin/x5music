<?php
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
error_reporting(0);
include "../../../include/x5music.conn.php";
$x5music_com_userid=SafeRequest("uid", "get");
$x5music_com_username=base64_decode(SafeRequest("uname", "get"));
if(cd_userupload=="0") {
	exit('?error=1');
}
if(cd_uploadentrance=="no") {
	exit('?error=1');
} //没有权限
$m="upload/url";
if(!file_exists(_x5music_root_ . $m)) {
	exit('?error=3');
} //文件夹不存在
if(strpos(cd_upmusictype, fileext($_FILES['Filedata']['name']))!==false) {
} else {
	exit('?error=4');
} //文件类型错误
if(!empty($_FILES)) {
	$tempFile=$_FILES['Filedata']['tmp_name'];
	$file=$_FILES['Filedata']['name'];
	$size=$_FILES['Filedata']['size'];
	$zmd5=$size . $file;
	$timemd5='' . substr(md5($zmd5), 0, 32) . '';
	$md5=strtoupper($timemd5);
	$form=strtolower(trim(substr(strrchr($file, "."), 1)));
	$CD_Name=str_replace('.' . $form . '', '', $file);
	$targetFile="" . $md5 . "." . $form . "";
	$targetFiles="../../../upload/url/" . $targetFile . "";
	move_uploaded_file($tempFile, $targetFiles);
	@unlink($_FILES['Filedata']['tmp_name']);
	//记录上传信息
	$setarrss=array(
		'cd_userid'=>$x5music_com_userid,
		'cd_username'=>$x5music_com_username,
		'cd_userip'=>$_SERVER["REMOTE_ADDR"],
		'cd_filetype'=>'音频文件',
		'cd_filename'=>$targetFile,
		'cd_filesize'=>$_FILES["Filedata"]["size"],
		'cd_fileurl'=>ReplaceStr($targetFiles, "../../../", ""),
		'cd_filetime'=>time()
	);
	inserttable('upload', $setarrss, 1);
	echo "?CD_Name=$CD_Name&CD_Url=upload/url/$targetFile&CD_siz=" . formatsize($size) . "&CD_md5=$md5";
}
?>