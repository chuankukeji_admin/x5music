<?php
if($ac=="profile" and $view=="") {
	$titles="我的头像";
} elseif($ac=="profile" and $view=="base") {
	$titles="个人资料";
} elseif($ac=="profile" and $view=="password") {
	$titles="修改密码 & 密码保护";
} elseif($ac=="pm") {
	$titles="短消息";
} elseif($ac=="friend" and $view=="") {
	$titles="好友列表";
} elseif($ac=="friend" and $view=="request") {
	$titles="好友请求";
} elseif($ac=="pic" and $id=="") {
	$titles="相册";
} elseif($ac=="pic" and $id<>"") {
	$title_pic=GetAlias("x5music_pic", "cd_title", "cd_id", $id);
	$titles="$title_pic - 相册";
} elseif($ac=="music" and $view=="") {
	$titles="我的影音";
} elseif($ac=="music" and $view=="fav") {
	$titles="我收藏的影音";
} elseif($ac=="music" and $view=="down") {
	$titles="我下载的影音";
} elseif($ac=="assets" and $view=="") {
	$titles="积分";
} elseif($ac=="assets" and $view=="goldbill") {
	$titles="金币账单";
} elseif($ac=="assets" and $view=="rechargestepone") {
	$titles="金币充值";
} elseif($ac=="assets" and $view=="vipbill") {
	$titles="升级VIP";
} elseif($ac=="upmusic") {
	$titles="上传音乐";
} elseif($ac=="upvideo") {
	$titles="上传视频";
} elseif($ac=="special" and $view=="") {
	$titles="我的专辑";
} elseif($ac=="special" and $view=="add") {
	$titles="新建专辑";
} elseif($ac=="feed") {
	$titles="动态";
} elseif($ac=="web") {
	$titles="空间";
} else {
	$titles="会员中心";
}
if(!$userlogined==true) {
	$x5music_com_userid=2;
	$x5music_com_username="游客";
	$x5music_com_nicheng="未登陆";
	$x5music_com_photo="image/noavatar_small.gif";
	$x5music_com_hits=0;
	$x5music_com_points=0;
	$x5music_com_rank=0;
	$x5music_com_credit=0;
	$x5music_com_loginnum=0;
}
function random($length) {
	$hash='';
	$chars='1234'; // 指定要返回的字符串
	$max=strlen($chars) - 1;
	mt_srand((double) microtime() * 1000000);
	for($i=0; $i<$length; $i++) {
		$hash.=$chars[mt_rand(0, $max)];
	}
	return $hash;
}
//经验转换
function getrank($rank, $l) {
	if($rank>=0 and $rank<500) {
		$userranka="Lv1";
		$userrankb=sprintf("%01.2f", $rank / 500 * 100);
		$userrankc=500 - $rank;
	} elseif($rank>=500 and $rank<1000) {
		$userranka="Lv2";
		$ranka=$rank - 500;
		$userrankb=sprintf("%01.2f", $ranka / 500 * 100);
		$userrankc=1000 - $rank;
	}
	if($l==2) {
		$x5music_rank=$userrankc;
	} elseif($l==1) {
		$x5music_rank=$userrankb . "%";
	} else {
		$x5music_rank=$userranka;
	}
	return $x5music_rank;
}
//编码转换
function siconv($str, $out_charset, $in_charset = '') {
	$in_charset=empty($in_charset) ? strtoupper(cd_charset) : strtoupper($in_charset);
	$out_charset=strtoupper($out_charset);
	if($in_charset!=$out_charset) {
		if(function_exists('iconv') && (@$outstr=iconv("$in_charset//IGNORE", "$out_charset//IGNORE", $str))) {
			return $outstr;
		} elseif(function_exists('mb_convert_encoding') && (@$outstr=mb_convert_encoding($str, $out_charset, $in_charset))) {
			return $outstr;
		}
	}
	return $str; //转换失败
}
function pic_get($filepath) {
	if(empty($filepath)) {
		$url='image/nopic.png';
	} else {
		$url=$filepath;
	}
	return $url;
}
//判断提交是否正确
function submitcheck($var) {
	if(!empty($_POST[$var]) && $_SERVER['REQUEST_METHOD']=='POST') {
		if((empty($_SERVER['HTTP_REFERER']) || preg_replace("/https?:\/\/([^\:\/]+).*/i", "\\1", $_SERVER['HTTP_REFERER'])==preg_replace("/([^\:]+).*/", "\\1", $_SERVER['HTTP_HOST']))) {
			return true;
		} else {
			showmessage("您的请求来路不正确或表单验证串不符，无法提交。请尝试使用标准的web浏览器进行操作。", $_SERVER['HTTP_REFERER'], 2);
		}
	} else {
		return false;
	}
}
//rewrite链接
function rewrite_url($para) {
	if(cd_userhtml==1) {
		$para=str_replace(array(
			'&',
			'=',
			'.php?'
		), array(
			'-',
			'-',
			'-'
		), $para);
		return $para . '.html';
	} else {
		return $para;
	}
}
//检查登陆状态
function checklogin() {
	if(!$userlogined==true) {
		showmessage("对不起，请登陆后再操作！", "space.php?do=login", 0);
	}
}
//对话框
function showmessage($ErrMsg, $ErrUrl, $ErrType) {
	if($ErrType==3) {
		echo "<script language=javascript>alert('" . $ErrMsg . "');history.back(1);</script>";
	} else {
		echo "<div class=\"album_banner\">";
		echo "<h2>信息提示：";
		echo "<font color=\"red\">" . $ErrMsg . "</font></h2><br /><br /><h2>您可以选择：";
		if($ErrType==1) {
			echo "<script>setTimeout(\"window.location.href ='" . $ErrUrl . "';\", 3000);</script><a href='" . $ErrUrl . "'>页面跳转中...</a>";
		} elseif($ErrType==2) {
			echo "<script>setTimeout(\"history.back(1);\", 3000);</script><a href='javascript:history.go(-1);'>返回上一页</a> |";
			echo "<a href='space.php?do=home'>返回首页</a>";
		} else {
			echo "<script>setTimeout(\"window.location.href ='" . $ErrUrl . "';\", 3000);</script><a href='" . $ErrUrl . "'>返回上一页</a> |";
			echo "<a href='space.php?do=home'>返回首页</a>";
		}
		echo "</h2></div>";
	}
	exit();
}
//获取在线IP
function getonlineip($format = 0) {
	global $_SGLOBAL;
	if(empty($_SGLOBAL['onlineip'])) {
		if(getenv('HTTP_CLIENT_IP') && strcasecmp(getenv('HTTP_CLIENT_IP'), 'unknown')) {
			$onlineip=getenv('HTTP_CLIENT_IP');
		} elseif(getenv('HTTP_X_FORWARDED_FOR') && strcasecmp(getenv('HTTP_X_FORWARDED_FOR'), 'unknown')) {
			$onlineip=getenv('HTTP_X_FORWARDED_FOR');
		} elseif(getenv('REMOTE_ADDR') && strcasecmp(getenv('REMOTE_ADDR'), 'unknown')) {
			$onlineip=getenv('REMOTE_ADDR');
		} elseif(isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'], 'unknown')) {
			$onlineip=$_SERVER['REMOTE_ADDR'];
		}
		preg_match("/[\d\.]{7,15}/", $onlineip, $onlineipmatches);
		$_SGLOBAL['onlineip']=$onlineipmatches[0] ? $onlineipmatches[0] : 'unknown';
	}
	if($format) {
		$ips=explode('.', $_SGLOBAL['onlineip']);
		for($i=0; $i<3; $i++) {
			$ips[$i]=intval($ips[$i]);
		}
		return sprintf('%03d%03d%03d', $ips[0], $ips[1], $ips[2]);
	} else {
		return $_SGLOBAL['onlineip'];
	}
}
//站点链接
function getsiteurl() {
	$_SCONFIG['siteallurl']=cd_weburl;
	if(empty($_SCONFIG['siteallurl'])) {
		$uri=$_SERVER['REQUEST_URI'] ? $_SERVER['REQUEST_URI'] : ($_SERVER['PHP_SELF'] ? $_SERVER['PHP_SELF'] : $_SERVER['SCRIPT_NAME']);
		return shtmlspecialchars('http://' . $_SERVER['HTTP_HOST'] . substr($uri, 0, strrpos($uri, '/') + 1));
	} else {
		return "http://" . $_SCONFIG['siteallurl'] . "/user/";
	}
}
function getuserpage($mysql, $pagesize) {
	global $db;
	$url=$_SERVER["QUERY_STRING"];
	if(stristr($url, '&pages')) {
		$url=preg_replace('/&pages=([\S]+?)$/', '', $url);
	}
	if(stristr($url, 'pages')) {
		$url=preg_replace('/pages=([\S]+?)$/', '', $url);
	}
	if(IsNul($url)) {
		$url.="&";
	}
	//$url="do=".$_GET['do']."&view=".$_GET['view']."&orderby=".$_GET['orderby']."&day=".$_GET['day']."&";
	$pages=SafeRequest("pages", "get");
	$pagesok=$pagesize; //每页显示记录数
	if(!isset($pages) || $pages=="" || !is_numeric($pages) || $pages<=0) {
		$pages=1;
	}
	$sqlstr=$mysql;
	$res=$db->query($sqlstr);
	$nums=$db->num_rows($res);
	if($nums==0) {
		$nums=1;
	}
	$pagejs=ceil($nums / $pagesok); //总页数
	if($pages>$pagejs) {
		$pages=$pagejs;
	}
	$sql=$sqlstr . " LIMIT " . $pagesok * ($pages - 1) . "," . $pagesok;
	$result=$db->query($sql);
	if($pagejs>1) {
		$str="<em>&nbsp;" . $nums . "&nbsp;</em>";
		if($pages>3) {
			$str.="<a href='" . rewrite_url("space.php?" . $url . "pages=1") . "' class='last' title='首页'>1&nbsp;...</a>";
		}
		if($pages>1) {
			$str.="<a href='" . rewrite_url("space.php?" . $url . "pages=" . ($pages - 1)) . "' class='prev' title='上一页'>&lsaquo;&lsaquo;</a>";
		}
		if($pagejs<=10) {
			for($i=1; $i<=$pagejs; $i++) {
				if($i==$pages) {
					$str.="<strong title='第" . $i . "页'>" . $i . "</strong>";
				} else {
					$str.="<a href='" . rewrite_url("space.php?" . $url . "pages=" . $i) . "'>" . $i . "</a>";
				}
			}
		} else {
			if($pages>=12) {
				for($i=$pages - 5; $i<=$pages + 6; $i++) {
					if($i<=$pagejs) {
						if($i==$pages) {
							$str.="<strong title='第" . $i . "页'>" . $i . "</strong>";
						} else {
							$str.="<a href='" . rewrite_url("space.php?" . $url . "pages=" . $i) . "'>" . $i . "</a>";
						}
					}
				}
				//if($i<=$pagejs){ 
				//	$str.="....";
				//	$str.="<a href='".rewrite_url("space.php?".$url."pages=".$pagejs)."'>".$pagejs."</a>";
				//}
			} else {
				for($i=1; $i<=12; $i++) {
					if($i==$pages) {
						$str.="<strong title='第" . $i . "页'>" . $i . "</strong>";
					} else {
						$str.="<a href='" . rewrite_url("space.php?" . $url . "pages=" . $i) . "'>" . $i . "</a>";
					}
				}
				//if($i<=$pagejs){ 
				//	$str.="....";
				//	$str.="<a href='".rewrite_url("space.php?".$url."pages=".$pagejs)."'>".$pagejs."</a>";
				//}
			}
		}
		if($pages<$pagejs) {
			$str.="<a href='" . rewrite_url("space.php?" . $url . "pages=" . ($pages + 1)) . "' class='next' title='下一页'>&rsaquo;&rsaquo;</a>";
		}
		if($pages<$pagejs - 2) {
			$str.="<a href='" . rewrite_url("space.php?" . $url . "pages=" . $pagejs . "") . "' class='last' title='最后一页'>...&nbsp;" . $pagejs . "</a>";
		}
	}
	while($row=$db->fetch_array($result)) {
	}
	$arr=array(
		$str,
		$result,
		$sql,
		$pagejs
	);
	return $arr;
}
?>