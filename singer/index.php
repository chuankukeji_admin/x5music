<?php
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
error_reporting(0);
include '../include/x5music.conn.php';
include '../include/x5music.inc.php';
include 'source/common.php';
$dos = array('music','fav','album','blog','pic','friend','star','fans','book');
$ac = (!empty($_GET['ac']) &&in_array($_GET['ac'],$dos))?$_GET['ac']:'index';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<title><?php echo $x5music_web_nicheng?>的音乐空间 -<?php echo $x5music_web_nicheng?>- <?php echo cd_webname;?></title>
<meta name="Keywords" content="<?php echo $x5music_web_nicheng?>-<?php echo $x5music_web_title?>的音乐空间 - <?php echo cd_webname;?>" />
<meta name="Description" content="<?php echo $x5music_web_nicheng?>" />
<link href="theme/<?php echo $x5music_web_skin?>/v8.css" rel="stylesheet" type="text/css">
<link href="images/profile.css" rel="stylesheet" type="text/css">
<style id="mainJSBg" type="text/css">
.background-container {
	background-repeat: repeat;
	background-position: center top;
	background-attachment: scroll;
	background-image: url(<?php echo cd_webpath?><?php echo $x5music_web_banner?>);
}

.layout-nav {
	background-image: url(theme/<?php echo $x5music_web_skin?>/menu_bg.png);
	_background-image: url(theme/<?php echo $x5music_web_skin?>/menu_bg_ie6.png);
	border: none;
}

.layout-background {
	background-image: url(<?php echo cd_webpath?><?php echo $x5music_web_bg?>);
	background-repeat: repeat;
}
</style>
<style type="text/css">
<!--
a:link {
 text-decoration: none;
}
a:visited {
 text-decoration: none;
}
a:hover {
 text-decoration: none;
}
a:active {
 text-decoration: none;
}
-->
</style>
<script language="javascript" type="text/javascript" src="source/jquery.js"></script>
<script language="javascript" type="text/javascript" src="source/js.js"></script>
</head>
<body>
<?php include_once('source/x5music_header.php');?>
<?php include_once('source/x5music_'.$ac.'.php');?>
<div style="display:none">
<?php
global $db;
$sql="Select cd_cnzz from ".tname('system')."";
$row=$db->getrow($sql);
echo $row['cd_cnzz'];
?>
</div>
</body>
</html>