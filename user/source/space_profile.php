<?php if(!$userlogined==true){showmessage("出错了，请登陆后再操作。","login.php",0);}?>
<div class="user">
<div class="user_center">
<?php include_once('source/space_menu.php');?>
<div class="user_main">
<div class="uMain_content">
<div class="main_nav">
<ul>
<li class="modify"><a <?php if($view=="" and $orderby==""){ echo ' class="on"'; } ?> href="<?php echo rewrite_url('space.php?do=profile');?>">我的头像</a></li>
<li class="message"><a <?php if($view=="base" and $orderby==""){ echo ' class="on"'; } ?> href="<?php echo rewrite_url('space.php?do=profile&view=base');?>">个人资料</a></li>
<li class="message"><a <?php if($view=="password" and $orderby==""){ echo ' class="on"'; } ?> href="<?php echo rewrite_url('space.php?do=profile&view=password');?>">修改密码</a></li>
</ul>
</div>
<?php if($view=="base"){?>
<script type="text/javascript">
function CheckForm() {
var d_nicheng = document.form1.nicheng.value;
if( 1 > strLen(d_nicheng) || strLen(d_nicheng) > 10){
alert('呢称长度(1~10字符)不符合要求');
document.form1.nicheng.focus();
return false;
}
if (document.form1.b_year.value=="0"){
alert("请选择生日:年");
document.form1.b_year.focus();
return false;
}
if (document.form1.b_month.value=="0"){
alert("请选择生日:月");
document.form1.b_month.focus();
return false;
}
if (document.form1.birthprovince.value==""){
alert("请选择家乡:省份");
document.form1.birthprovince.focus();
return false;
}
if (document.form1.birthcity.value==""){
alert("请选择家乡:城市");
document.form1.birthcity.focus();
return false;
}
if (document.form1.resideprovince.value==""){
alert("请选择居住地:省份");
document.form1.resideprovince.focus();
return false;
}
if (document.form1.residecity.value==""){
alert("请选择居住地:城市");
document.form1.residecity.focus();
return false;
}
var d_qq = document.form1.qq.value;
if( 5 > strLen(d_qq) || strLen(d_qq) > 13){
alert('QQ长度(5~13字符)不符合要求');
document.form1.qq.focus();
return false;
}
var d_email = document.form1.email.value;
if(!isEmail(d_email)){
alert('请输入正确格式的邮箱地址');
document.form1.email.focus();
return false;
}
var d_sign = document.form1.sign.value;
if( 1 > strLen(d_sign) || strLen(d_sign) > 200){
alert('个性宣言长度(1~200字符)不符合要求');
document.form1.sign.focus();
return false;
}
}
function isEmail(a){
var i=a.length;
var temp = a.indexOf('@');
var tempd = a.indexOf('.');
if (temp > 1) {
if ((i-temp) > 3){
if (tempd!=-1){
return 1;
}
}
}
return 0;
}
</script>

<div id="favoritesList" class="minHeight500">

<div id="modifyProfile" class="profile">
<div class="title">
<div class="name">
修改个人资料：填写完整准确的个人资料，可以让更多的朋友找到您。
</div>
</div>
<ul>
<form name="form1" method="post" action="do.php?ac=base" class="c_form">

<div style="padding: 0px 0px 8px 10px;"> 

<div class="name">
登录帐号：<?php echo $x5music_com_username;?>
</div><br />

<div class="name">
您的呢称：<input type="text" name="nicheng" id="nicheng" value="<?php echo $x5music_com_nicheng;?>" maxlength="13" class="input_normal" readonly/>&nbsp;&nbsp;
<input type="button" value="修改昵称" onclick="showiframe('no','请输入您要修改的昵称','<?php echo cd_webpath?>user/nichengx.php',370,75);" class="submit">
</div><br />

<div class="name">性　　别：<?php echo $x5music_com_sex;?>
</div><br />

<div class="name">生　　日：<select name="b_year" id="b_year">
<option value="0">&nbsp;</option>
<?php
//生日:年
for ($i=0; $i<100; $i++) {
$they = date('Y') - $i;
$selectstr = $they == date('Y',strtotime($x5music_com_birthday))?' selected':'';
echo "<option value=\"$they\"$selectstr>$they</option>";
}
?>
</select>&nbsp;年 <select name="b_month" id="b_month">
<option value="0">&nbsp;</option>
<?php
//生日:月
for ($i=1; $i<13; $i++) {
$selectstr = $i == date('m',strtotime($x5music_com_birthday))?' selected':'';
echo "<option value=\"$i\"$selectstr>$i</option>";
}
?>
</select>&nbsp;月 <select name="b_day" id="b_day">
<option value="0">&nbsp;</option>
<?php
//生日:日
for ($i=1; $i<32; $i++) {
$selectstr = $i == date('d',strtotime($x5music_com_birthday))?' selected':'';
echo "<option value=\"$i\"$selectstr>$i</option>";
}
?>
</select>&nbsp;日
</div><br />

<div class="name">家　　乡：<script type="text/javascript" src="js/script_city.js"></script>
<script type="text/javascript">
<!--
showprovince('birthprovince', 'birthcity', '<?php echo $x5music_com_birthprovince;?>', 'birthcitybox');
showcity('birthcity', '<?php echo $x5music_com_birthcity;?>', '', 'birthcitybox');
//-->
</script>
</div><br />

<div class="name">居　　住： <script type="text/javascript">
<!--
showprovince('resideprovince', 'residecity', '<?php echo $x5music_com_resideprovince;?>', 'residecitybox');
showcity('residecity', '<?php echo $x5music_com_residecity;?>', '', 'residecitybox');
//-->
</script>
</div><br />

<div class="name">Q　　 Q：<input type="text" name="qq" id="qq" maxlength="13" onKeyUp="value=value.replace(/[^\d]/g,'')" onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/[^\d]/g,''))" value="<?php echo $x5music_com_qq;?>" class="input_normal" />
</div><br />

<div class="name">邮　　箱：<input type="text" name="email" id="email" maxlength="50" value="<?php echo $x5music_com_email;?>" style="width: 200px;" class="input_normal" />
</div><br />

<div class="name">个性宣言：<input type="text" name="sign" id="sign" maxlength="100" value="<?php echo $x5music_com_sign;?>" style="width: 500px;" class="input_normal" />
</div><br />

<input type="submit" name="submitbase" value="提交" class="button square lv" onClick="return CheckForm();" />
</form>
</div>

</ul>

</div>
</div>

<?php }elseif($view=="password"){?> 

<div id="favoritesList" class="minHeight500">

<div id="modifyProfile" class="profile">
<div class="title">
<div class="name">
请记住修改后的密码，若忘记了请使用找回密码功能。
</div>
</div>
<ul>
<form name="form2" method="post" action="do.php?ac=password">

<div style="padding: 0px 0px 8px 10px;"> 

<div class="name">
登录帐号：<?php echo $x5music_com_username;?>
</div><br />

<div class="name">
旧密码：<input type="password" name="password" value="" size="30" maxlength="16" class="input_normal" />
* 必填
</div><br />
<div class="name">
新密码：<input type="password" name="new_pwd" value="" size="30" maxlength="16" class="input_normal" />
* 必填, 6 - 32 个字符
</div><br />
<div class="name">
确认码：<input type="password" name="affirm_pwd" value="" size="30" maxlength="16" class="input_normal" />
* 必填, 确认上面输入的新密码
</div><br />

<input type="submit" name="submitpassword" value="提交" class="button square lv" onClick="return CheckForm();" />
</form>
<br /><br />
<hr><br />
<h2>忘记密码时通过正确回答以下问题，就可以重新设置密码。</h2>
<br />

<form name="form3" method="post" action="do.php?ac=protect">
<div class="name">
登陆密码：<input type="password" name="password" value="" size="30" maxlength="16" class="input_normal" />
* 必填
</div><br />

<div class="name">
密保问题：<input type="password" name="question" value="" size="30" maxlength="16" class="input_normal" />
* 必填, 3 - 16 个字符之间
</div><br />

<div class="name">
密保答案：<input type="password" name="answer" value="" size="30" maxlength="16" class="input_normal" />
* 答案采用了MD5加密，如要修改可删除后再填写
</div><br />

<input type="submit" name="submitprotect" value="修改密保" class="button square lv" />
</form>
</div>
</ul>
</div>
</div>
<?php }else{?>
<div id="favoritesList" class="minHeight500">

<div id="modifyProfile" class="profile">
<div class="title">
<div class="name">
修改头像：如果您还没有设置自己的头像，系统会显示为默认头像，您需要自己上传一张新照片来作为自己的个人头像。
</div>
</div>



<div class="avatar_box">
<div class="avatarTitle">
当前头像
<span>设置新头像</span>
</div>
<div class="myAvatar">
<img class="avatar-160" id="my-avatar" width="160" height="160" src="<?php echo LinkPicUrl($x5music_com_photo);?>"  onerror="this.onerror=null;this.src='image/noavatar_big.gif'"/>
</div>
<div class="myAvatarUpload">
<form method="post" action="do.php?ac=profileupload" enctype="multipart/form-data">
<input type="file" name="attach" size="25" style="padding:10px;" class="input_normal"/>
<input type="submit" name="uploadsubmit" id="btnupload" value="开始上传" class="button square lv" />
</form>
</div>
</div>
</div>
</div>
<?php } ?>
</div>
</div>
</div>
</div>