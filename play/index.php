<?php
/*
'**************************************************************************************************
' 程序名称: x5Music开源音乐管理系统
' 官方网站: http://x5mp3.com
' 联系 Q Q: 196859961
' QQ交流群：343319601
' 版本：(免费版)
' 备注：未经书面授权，不得向任何第三方提供出售本软件系统！
' 功能，模板，插件，扩展，定制请联系QQ：196859961
'**************************************************************************************************
*/
error_reporting(0);
include '../include/x5music.conn.php';
include '../include/x5music.inc.php';
$x5music_IDA=$_SERVER['QUERY_STRING'];
$x5music_IDA=ReplaceStr(ReplaceStr($x5music_IDA, '.html', ''), 'id=', '');
$x5music_IDB=explode(',', $x5music_IDA);
$x5music_IDC=$x5music_IDB[1];
$error="您访问的Url页面不存在，请检查Url是否正确或包含非法字符！";
if (!IsNumID($x5music_IDB[0])) {exit(include _x5music_root_ . "/include/error/content_error.php");}
if (!IsNumID($x5music_IDB[1])) {exit(include _x5music_root_ . "/include/error/content_error.php");}
if(cd_webhtml==1 or cd_webhtml==2 or cd_webhtml==3) {
	switch($x5music_IDB[0]) {
	case '0':
		if(cd_mobile=="yes"){if (isMobile()){header("Location:".cd_webpath."mobile/list.php?id=".$x5music_IDC."");exit();}}
		$cache_id='list_' . $x5music_IDC . '_' . $x5music_IDB[2];
		if(!($cache_opt->start($cache_id))) {
			echo SpanDjList($x5music_IDC);
			$cache_opt->end();
		}
		break;
	case '1':
		if(cd_mobile=="yes"){if (isMobile()){header("Location:".cd_webpath."mobile/play.php?id=".$x5music_IDC."");exit();}}
		$cache_id='play_' . $x5music_IDC;
		if(!($cache_opt->start($cache_id))) {
			echo SpanDjPlay($x5music_IDC);
			$cache_opt->end();
		}
		break;
	case '2':
		if(cd_mobile=="yes"){if (isMobile()){header("Location:".cd_webpath."mobile/special.php?id=".$x5music_IDC."");exit();}}
		$cache_id='special_' . $x5music_IDC . '_' . $x5music_IDB[2];
		if(!($cache_opt->start($cache_id))) {
			echo SpanDjSpecial($x5music_IDC);
			$cache_opt->end();
		}
		break;
	case '3':
		if(cd_mobile=="yes"){if (isMobile()){header("Location:".cd_webpath."mobile/news.php?id=".$x5music_IDC."");exit();}}
		$cache_id='newsintro_' . $x5music_IDC;
		if(!($cache_opt->start($cache_id))) {
			echo SpanNewsIntro($x5music_IDC);
			$cache_opt->end();
		}
		break;
	case '4':
		if(cd_mobile=="yes"){if (isMobile()){header("Location:".cd_webpath."mobile/newslist.php?id=".$x5music_IDC."");exit();}}
		$cache_id='newslist_' . $x5music_IDC . '_' . $x5music_IDB[2];
		if(!($cache_opt->start($cache_id))) {
			echo SpanNewsList($x5music_IDC);
			$cache_opt->end();
		}
		break;
	}
}
?>